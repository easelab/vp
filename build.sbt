name := "vp"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.5"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"

libraryDependencies += "com.googlecode.kiama" %% "kiama" % "1.8.0"

//libraryDependencies += "org.slf4j" % "slf4j-api" % "1.7.25"

libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.25"

//libraryDependencies += "org.slf4j" % "log4j-over-slf4j " % "1.7.2"


libraryDependencies += "commons-io" % "commons-io" % "2.6"

libraryDependencies += "info.picocli" % "picocli" % "4.6.3"
libraryDependencies += "com.google.code.findbugs" % "jsr305" % "3.0.2"

//libraryDependencies += "io.github.kostaskougios" % "cloning" % "1.10.3"

// https://mvnrepository.com/artifact/net.liftweb/lift-json
libraryDependencies += "net.liftweb" %% "lift-json" % "3.4.3"

//this line prevents the sbt.TrapExitSecurityException from occuring when using the CLI
trapExit := false

// set the main class for 'sbt run'
mainClass in (Compile, run) := Some("se.gu.vp.cli.CliMain")

// set the main class for packaging the main jar
mainClass in (Compile, packageBin) := Some("se.gu.vp.cli.CliMain")

libraryDependencies += "org.rogach" % "scallop_2.12" % "1.0.0"
libraryDependencies += "org.bitbucket.inkytonik.dsprofile" % "dsprofile_2.12" % "0.4.0"
//libraryDependencies += "org.bitbucket.inkytonik.dsinfo" %% "dsinfo" % "0.4.0"

// https://mvnrepository.com/artifact/org.bitbucket.inkytonik.dsinfo/dsinfo
libraryDependencies += "org.bitbucket.inkytonik.dsinfo" %% "dsinfo" % "0.4.0"

// https://mvnrepository.com/artifact/jline/jline
libraryDependencies += "jline" % "jline" % "2.12"

// https://mvnrepository.com/artifact/com.thoughtworks.paranamer/paranamer
//libraryDependencies += "com.thoughtworks.paranamer" % "paranamer" % "2.8"

// https://mvnrepository.com/artifact/com.google.guava/guava
libraryDependencies += "com.google.guava" % "guava" % "17.0"

libraryDependencies += "com.google.code.findbugs" % "jsr305" % "2.0.3"

// https://mvnrepository.com/artifact/org.scala-lang/scalap
libraryDependencies += "org.scala-lang" % "scalap" % "2.12.8"

// https://mvnrepository.com/artifact/org.scala-lang/scala-library
libraryDependencies += "org.scala-lang" % "scala-library" % "2.12.8"

// https://mvnrepository.com/artifact/org.scala-lang/scala-reflect
libraryDependencies += "org.scala-lang" % "scala-reflect" % "2.12.8"

// https://mvnrepository.com/artifact/org.scala-lang/scala-compiler
libraryDependencies += "org.scala-lang" % "scala-compiler" % "2.12.8"

// https://mvnrepository.com/artifact/org.scala-lang.modules/scala-xml
libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "1.3.0"

// https://mvnrepository.com/artifact/com.google.guava/guava
libraryDependencies += "com.google.guava" % "guava" % "17.0"

// https://mvnrepository.com/artifact/com.thoughtworks.paranamer/paranamer
libraryDependencies += "com.thoughtworks.paranamer" % "paranamer" % "2.8"

// https://mvnrepository.com/artifact/org.scala-lang.modules/scala-parser-combinators
libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4"

scalacOptions += "-language:postfixOps"


