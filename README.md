## VP (Virtual Platform)
[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

The Virtual Platform is a novel variation control system and framework. It aims to manage software variants and facilitate their incremental migration from a clone&own approach to a platform architecture.

Please refer to and cite the following paper for more information:

- Wardah Mahmood, Daniel Strueber, Thorsten Berger, Ralf Laemmel, Mukelabai Mukelabai. Seamless Variability Management With the Virtual Platform. 43rd International Conference on Software Engineering (ICSE), 2021. https://www.cse.chalmers.se/~bergert/paper/2021-icse-vp.pdf
 
The features of the Virtual Platform are:

  - Mapping code fragments to features
  - Mapping files or directories to features
  - Reusability of assets and features via automated cloning
  - Automated change propagation between assets and their clones
  - Automated change propagation between assets and their features
  - Convenience functions for querying the asset tree and feature model 
  - Incremental feature model 
  - Configurable project overview

## Installation
Presently the Virtual Platform has a commind-line interface and to be able to use it follow these steps: 

VP Installation Guide

1. Clone VP from easelab: git clone https://bitbucket.org/easelab/vp.git
2. Add path of the cloned vp to your system variables Path e.g., C:\vpexp\vp (search for environment variables, go to system variables, go to path, and add the path to vp there).
3. Change the path in the file located in the project vp/src/main/scala/se/gu/vp/cli/RootPathFile.txt and make it point to a location in your PC for using the operators. 
4. Test that you can run vp. From the command line, type vp help. Ignore the warnings.
5. If you get the message "sbt not recognized", then install sbt (link: https://www.scala-sbt.org/1.x/docs/Installing-sbt-on-Windows.html).
6. After installing sbt, close the previous comand window if still open. Then open a new command window and rerun the command "vp help"
7. If it still does not work, please email me at wardah@chalmers.se

In order to use the operators you can refer to the following PDF: 
[operatorsusage](operatorsusage 2.pdf)

### Contribution guidelines ###
You are free to contribute to this repository.

#### Getting started
 + Fork the repository
 + Start adding your contribution as an annotated feature in the code
 + Build and Test
 + Make a pull request


#### As a reviewer
The repository has policies in place (similar to other Git repositories) for which a team of members is responsible for ensuring the code quality of the features before they can be merged to the main branch. If you have experience with plugin development or want to help out by becoming a member of the review team, please reach out and contact any member in the existing team. 

### Who do I talk to? 
- Wardah Mahmood (wardah@chalmers.se)
- Thorsten Berger (thorsten.berger@rub.de)
- Ramzi Abu Zahra (gusabuzra@student.gu.se)

### Research group

[![Chair of Software Engineering / easelab/isselab research group](https://img.shields.io/website.svg?down_color=red&down_message=down&up_color=green&up_message=se.ruhr-uni-bochum.de&url=http%3A%2F%2Fshields.io)](http://se.rub.de)

<!--
[![jhc github](https://img.shields.io/badge/GitHub-isselab-181717.svg?style=flat&logo=github)](https://www.github.com/isselab)
[![](https://img.shields.io/website.svg?down_color=red&down_message=down&up_color=green&up_message=isselab.org&url=http%3A%2F%2Fshields.io)](https://www.isselab.org)
-->

### Related Projects

#### HAnS -- an IntelliJ IDE plugin
![Build](https://github.com/isselab/HAnS/workflows/Build/badge.svg)
[![Version](https://img.shields.io/jetbrains/plugin/v/22759.svg)](https://plugins.jetbrains.com/plugin/22759)
[![Downloads](https://img.shields.io/jetbrains/plugin/d/22759.svg)](https://plugins.jetbrains.com/plugin/22759)

- HAnS: IDE-based editing support for embedded feature annotations by Johan Martinson, Herman Jansson, Mukelabai Mukelabai, Thorsten Berger, Alexandre Bergel, and Truong Ho-Quang.  https://www.cse.chalmers.se/~bergert/paper/2021-splc-hanstext.pdf
  [![](https://zenodo.org/badge/DOI/10.1145/3461002.3473072.svg)](https://doi.org/10.1145/3461002.3473072)


<!--
#### Papers published:
Wardah Mahmood; Daniel Strueber; Thorsten Berger; Ralf Laemmel; Mukelabai Mukelabai
Seamless Variability Management With the Virtual Platform (Summary) Proceedings Article
In: Software Engineering (SE), 2022, (Extended Abstract of our ICSE’21 (main track) paper, see below.).
BibTeX
<!--
##### 2021
- Master Thesis by Johan Martinson & Herman Jansson  
  [![Johan & Herman](https://zenodo.org/badge/DOI/20.500.12380/302926.svg)](https://doi.org/20.500.12380/302926)

-->
### [Contributors](CONTRIBUTORS.md)
+ Wardah Mahmood
+ Christoph Derks
+ Bob Ruiken
+ Khaled Al-Mustafa
+ Thorsten Berger
+ Daniel Strueber 
+ Ralf Laemmel
+ Ramzi Abu Zahra
+ Mukelabai Mukelabai 
