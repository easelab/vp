package operations

import org.scalatest.FunSuite
import se.gu.vp.model._
import se.gu.vp.operations.Utilities.{computeAssetPath, computeFeaturePath}
import se.gu.vp.operations._
import java.nio.file.Paths

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */
// Failing
class RemoveFeatureTest extends FunSuite {
  test("test1") {
    // Assuming Configuration object's basePath is correctly set to point to the "Dummy" folder
    val basePathToDummy = Paths.get(Configuration.basePath)

    // Paths for each asset based on the basePath
    val rootPath = basePathToDummy.resolve("CalculatorRootJava")
    val repPath = basePathToDummy.resolve("rep")
    val fol1Path = repPath.resolve("Folder 1")
    val fol2Path = repPath.resolve("Folder 2")
    val fil1Path = fol1Path.resolve("File 1") // Assuming File 1 is inside Folder 1
    val fil2Path = fol2Path.resolve("File 3") // Assuming File 3 is inside Folder 2

    // Define features and feature models
    val f1 = new RootFeature("A")
    val f2 = Feature("B")
    val f3 = Feature("C")
    val fm = new FeatureModel(f1)
    AddFeatureToFeatureModel(f2, f1)
    AddFeatureToFeatureModel(f3, f2)

    // Instantiating assets with their respective paths converted to strings
    val root = Asset(rootPath.toString, VPRootType)
    val rep = Asset(repPath.toString, RepositoryType)
    val fol1 = Asset(fol1Path.toString, FolderType)
    val fol2 = Asset(fol2Path.toString, FolderType)
    val fil1 = Asset(fil1Path.toString, FileType)
    val fil2 = Asset(fil2Path.toString, FileType)

    // Proceed with the asset manipulation logic
    AddFeatureModelToAsset(rep, fm)

    AddAsset(rep, root)
    AddAsset(fol1, rep)
    AddAsset(fol2, rep)
    AddAsset(fil1, fol1)
    AddAsset(fil2, fol2)
    MapAssetToFeature(fol1, f1)
    MapAssetToFeature(fol2, f1)
    MapAssetToFeature(fol2, f2)

    // Assuming computeAssetPath and computeFeaturePath are defined and RemoveFeature operation is correctly implemented
    val featurepath = Path(root, computeAssetPath(rep) ++ computeFeaturePath(f2))
    RemoveFeature(featurepath)

    // Assertions
    assert(Utilities.featureModelContainsFeature(f2, fm) == false)
    assert(fol2.isMappedToFeature(f2) == false)
  }

}
