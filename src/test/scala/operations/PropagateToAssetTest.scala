package operations

import org.scalatest.FunSuite
import se.gu.vp.model._
import se.gu.vp.operations._
/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */
/**
  * @author Wardah Mahmood
  */
class PropagateToAssetTest extends FunSuite {

  import org.scalatest.FunSuite
  import se.gu.vp.model._
  import se.gu.vp.operations._

  import java.nio.file.Paths

  class PropagateToAssetTest extends FunSuite {

    /*test("test1") {
      // Assuming Configuration object's basePath is correctly set to point to the "Dummy" folder
      val basePathToDummy = Paths.get(Configuration.basePath)

      // Defining paths for each asset based on the basePath
      val rootPath = basePathToDummy.resolve("CalculatorRootJava")
      val repo1Path = basePathToDummy.resolve("Repo 01")
      val folderPath = basePathToDummy.resolve("Folder 1.01")
      val filePath = folderPath.resolve("File 1.1.1") // Assuming the file is inside Folder 1.01
      val repo2Path = basePathToDummy.resolve("Repo 02")
      val file3Path = folderPath.resolve("File 1.1.2") // Assuming this file is also intended to be inside Folder 1.01
      val file2Path = folderPath.resolve("File 1.1.3") // Assuming this file is also intended to be inside Folder 1.01
      val class2Path = file2Path.resolve("Class 1.1.3.1") // Assuming this class is inside File 1.1.3

      // Define features and feature models
      val feature1 = new RootFeature("f1")
      val feature2 = Feature("f2")
      val feature3 = Feature("f3")
      val feature4 = Feature("f4")
      val feature5 = Feature("f5")
      val fm1 = new FeatureModel(feature1)
      val fm2 = new FeatureModel(new RootFeature("fm2"))
      AddFeatureToFeatureModel(feature2, feature1)
      AddFeatureToFeatureModel(feature4, feature3)
      AddFeatureToFeatureModel(feature3, feature1, true, true, true)

      // Instantiating assets with their respective paths converted to strings
      val root = Asset(rootPath.toString, VPRootType)
      val repo1 = Asset(repo1Path.toString, RepositoryType)
      val folder = Asset(folderPath.toString, FolderType)
      val file = Asset(filePath.toString, FileType)
      val repo2 = Asset(repo2Path.toString, RepositoryType)
      val file3 = Asset(file3Path.toString, FileType)
      val file2 = Asset(file2Path.toString, FileType)
      val class2 = Asset(class2Path.toString, ClassType)

      // Proceed with the asset manipulation logic
      AddAsset(repo1, root)
      AddFeatureModelToAsset(repo1, fm1)
      AddAsset(folder, repo1)
      AddAsset(file, folder)
      AddAsset(repo2, root)
      MapAssetToFeature(folder, feature1)
      AddFeatureModelToAsset(repo2, fm2)
      CloneAsset(folder, repo2)
      val foldertarget = Utilities.findClones(folder)
      if (foldertarget.nonEmpty) {
        AddAsset(file3, foldertarget.head)
        PropagateToAsset(folder, foldertarget.head)
      }
      ChangeAsset(file) // Assuming ChangeAsset is a defined operation you want to perform
      AddAsset(file2, folder)
      AddAsset(class2, file2)
      MapAssetToFeature(folder, feature3)

      // Assertions
      assert(Utilities.assetClonedInAST(folder, repo2))
      val folderclone = Utilities.findClones(folder)
      if (folderclone.nonEmpty) {
        assert(Utilities.assetPropagatedInAST(folder, folderclone.head))
        println(assert(Utilities.assetPropagatedInAST(folder, folderclone.head)))
      }
    }

  }*/

    test("test1") {
      // Assuming Configuration object's basePath is correctly set to point to the "Dummy" folder
      val basePathToDummy = Paths.get(Configuration.basePath)

      // Paths for each asset based on basePath
      val rootPath = basePathToDummy.resolve("CalculatorRootJava")
      val repo1Path = basePathToDummy.resolve("Repo 01")
      val folderPath = basePathToDummy.resolve("Folder 1.01")
      val filePath = folderPath.resolve("File 1.1.1")
      val repo2Path = basePathToDummy.resolve("Repo 02")
      val file3Path = folderPath.resolve("File 1.1.2")
      val file2Path = folderPath.resolve("File 1.1.3")
      val class2Path = file2Path.resolve("Class 1.1.3.1")

      // Feature and Feature Model Setup
      val feature1 = new RootFeature("f1")
      val feature2 = Feature("f2")
      val feature3 = Feature("f3")
      val feature4 = Feature("f4")
      val fm1 = new FeatureModel(feature1)
      val fm2 = new FeatureModel(new RootFeature("fm2"))
      AddFeatureToFeatureModel(feature2, feature1)
      AddFeatureToFeatureModel(feature4, feature3)
      AddFeatureToFeatureModel(feature3, feature1, true, true, true)

      // Asset Instantiation
      val root = Asset(rootPath.toString, VPRootType)
      val repo1 = Asset(repo1Path.toString, RepositoryType)
      val folder = Asset(folderPath.toString, FolderType)
      val file = Asset(filePath.toString, FileType)
      val repo2 = Asset(repo2Path.toString, RepositoryType)
      val file3 = Asset(file3Path.toString, FileType)
      val file2 = Asset(file2Path.toString, FileType)
      val class2 = Asset(class2Path.toString, ClassType)

      // Asset Manipulation and Assertions
      AddAsset(repo1, root)
      AddFeatureModelToAsset(repo1, fm1)
      AddAsset(folder, repo1)
      AddAsset(file, folder)
      AddAsset(repo2, root)
      MapAssetToFeature(folder, feature1)
      AddFeatureModelToAsset(repo2, fm2)
      CloneAsset(folder, repo2)

      val foldertarget = Utilities.findClones(folder)
      if (foldertarget.nonEmpty) {
        AddAsset(file3, foldertarget.head)
        PropagateToAsset(folder, foldertarget.head)
      }

      ChangeAsset(file) // Assuming ChangeAsset is a defined operation
      AddAsset(file2, folder)
      AddAsset(class2, file2)
      MapAssetToFeature(folder, feature3)

      assert(Utilities.assetClonedInAST(folder, repo2))
      val folderclone = Utilities.findClones(folder)
      if (folderclone.nonEmpty) {
        assert(Utilities.assetPropagatedInAST(folder, folderclone.head))
      }
    }

    // Make sure to import or define necessary utilities and classes
  }
}