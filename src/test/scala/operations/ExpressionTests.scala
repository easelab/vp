package operations

import org.kiama.rewriting.Rewriter
import org.scalatest.FunSuite
import se.gu.vp.model._
/**
Copyright [2021] [Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class ExpressionTests extends FunSuite{

  import Rewriter._

  test("traverse expression and collect feature names"){
    val e = ( Feature("a") & Feature("b") ) | !Feature("c")
    val names = collectl{
      case Feature( name ,_, _ ,_) => name
    }(e)
    println( e )
    names foreach println

    val conjunctions = collectl{
      case a@And( _, _ ) => a
    }(e)
    conjunctions foreach println

    val disjunctionsOfConjunctions = collectl{
      case a@Or( And(_,_), _ ) => a
      case a@Or( _, And(_,_)) => a
    }(e)
    disjunctionsOfConjunctions foreach println
  }

  test( "find features in presence conditions" ){
    val folder = Folder( "test folder" )
    folder.presenceCondition = ( Feature("a") & Feature("b") ) | !Feature("c")
    assert( folder isMappedToFeature Feature("a") )

  }
}
