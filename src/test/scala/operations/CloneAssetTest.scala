package operations

import org.scalatest.FunSuite
import se.gu.vp.model._
import se.gu.vp.operations.Utilities._
import se.gu.vp.operations._

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class CloneAssetTest extends FunSuite {

  /*test("Simple Cloning"){

  val root = Asset("CalculatorRootJava", VPRootType)
  val repo1 = Asset("repo 1", RepositoryType)
  val folder = Asset("folder", FolderType)
  val file = Asset("file", FileType)
  val repo2 = Asset("repo 2", RepositoryType)

  AddAsset(repo1, root)
  AddAsset(folder, repo1)
  AddAsset(file, folder)
  AddAsset(repo2,root)
  CloneAsset(folder,repo2)
    assert(assetClonedInAST(folder,repo2)==true)
}*/
  test("Simple Cloning") {
    // Retrieve the base path from the configuration
    val basePath = Configuration.basePath

    // Create assets with basePath to ensure they are stored outside the project directory
    val root = Asset(s"$basePath/CalculatorRootJava", VPRootType)
    val repo1 = Asset(s"$basePath/repo1", RepositoryType)
    val folder = Asset(s"$basePath/folder", FolderType)
    val file = Asset(s"$basePath/file", FileType)
    val repo2 = Asset(s"$basePath/repo2", RepositoryType)

    // Build the asset hierarchy and perform the cloning operation
    AddAsset(repo1, root)
    AddAsset(folder, repo1)
    AddAsset(file, folder)
    AddAsset(repo2, root)
    CloneAsset(folder, repo2)

    // Assertion to verify the cloning was successful
    assert(assetClonedInAST(folder, repo2) == true)
  }


  /*test("Clone repository by Christoph"){
    val f1 = new RootFeature("feature1")
    val fm = new FeatureModel(f1)
    val f2 = new Feature ("feature2")
    val f3 = new Feature ("feature3")
    val f4 = new Feature ("feature4")
    val f5 = new Feature ("feature5")
    val f6 = new Feature ("feature6")
    val f7 = new Feature ("feature7")

    AddFeatureToFeatureModel(f1, fm, true, runImmediately = true, createMetadata = false)
    AddFeatureToFeatureModel(f2,f1, createMetadata = false)
    AddFeatureToFeatureModel(f3,f1, createMetadata = false)
    AddFeatureToFeatureModel(f4,f3, createMetadata = false)
    AddFeatureToFeatureModel(f5,f3, createMetadata = false)
    AddFeatureToFeatureModel(f6,f3, createMetadata = false)
    AddFeatureToFeatureModel(f7,f6, createMetadata = false)

    fm.prettyprint

    val root = Asset("CalculatorRootJava",VPRootType)
    val repository = Asset("repo",RepositoryType)
    val folder1 = Asset("folder1",FolderType)
    val folder2 = Asset("folder2",FolderType)
    val file1 = Asset("file1",FileType)
    val file2 = Asset("file2",FileType)
    val file3 = Asset("file3",FileType)

    AddAsset(repository,root)
    AddAsset(folder1,repository)
    AddAsset(folder2,repository)
    AddAsset(file1,folder1)
    AddAsset(file2,folder2)
    AddAsset(file3,folder2)
    AddFeatureModelToAsset(repository,fm)

    root.prettyprint

    val cloneRepoOp = CloneAsset(repository, root, cloneName = Some("repoClone"))
    //CloneRepository(repository, None)

    val repo2 = Asset("repo2", RepositoryType)
    val file4 = Asset("file4", FileType)
    val method = Asset("method", MethodType)
    val block = Asset("block", BlockType)
    val f8 = new RootFeature("f8")
    val fm2 = new FeatureModel(f8)
    AddFeatureModelToAsset(repo2, fm2)
    AddAsset(repo2, root)
    AddAsset(file4, repo2)
    AddAsset(method, file1)
    AddAsset(block, file2)

    MapAssetToFeature(method, f3)
    MapAssetToFeature(method, f4)
    val cloneMethodOp = CloneAsset(method, file2, Some(block))

    root.prettyprint
    root.children.foreach(a=>a.featureModel.get.prettyprint())
  }*/

  test("Clone repository by Christoph") {
    // Retrieve the base path from the configuration
    val basePath = Configuration.basePath

    // Setup of feature models and features
    val f1 = new RootFeature("feature1")
    val fm = new FeatureModel(f1)
    val f2 = new Feature("feature2")
    val f3 = new Feature("feature3")
    val f4 = new Feature("feature4")
    val f5 = new Feature("feature5")
    val f6 = new Feature("feature6")
    val f7 = new Feature("feature7")

    AddFeatureToFeatureModel(f1, fm, true, runImmediately = true, createMetadata = false)
    AddFeatureToFeatureModel(f2, f1, createMetadata = false)
    AddFeatureToFeatureModel(f3, f1, createMetadata = false)
    AddFeatureToFeatureModel(f4, f3, createMetadata = false)
    AddFeatureToFeatureModel(f5, f3, createMetadata = false)
    AddFeatureToFeatureModel(f6, f3, createMetadata = false)
    AddFeatureToFeatureModel(f7, f6, createMetadata = false)

    fm.prettyprint

    // Create assets with basePath to ensure they are stored outside the project directory
    val root = Asset(s"$basePath/CalculatorRootJava", VPRootType)
    val repository = Asset(s"$basePath/repo", RepositoryType)
    val folder1 = Asset(s"$basePath/folder1", FolderType)
    val folder2 = Asset(s"$basePath/folder2", FolderType)
    val file1 = Asset(s"$basePath/file1", FileType)
    val file2 = Asset(s"$basePath/file2", FileType)
    val file3 = Asset(s"$basePath/file3", FileType)

    // Build asset hierarchy and add feature models to assets
    AddAsset(repository, root)
    AddAsset(folder1, repository)
    AddAsset(folder2, repository)
    AddAsset(file1, folder1)
    AddAsset(file2, folder2)
    AddAsset(file3, folder2)
    AddFeatureModelToAsset(repository, fm)

    root.prettyprint

    // Perform cloning operations
    val cloneRepoOp = CloneAsset(repository, root, cloneName = Some("repoClone"))

    val repo2 = Asset(s"$basePath/repo2", RepositoryType)
    val file4 = Asset(s"$basePath/file4", FileType)
    val method = Asset(s"$basePath/method", MethodType)
    val block = Asset(s"$basePath/block", BlockType)
    val f8 = new RootFeature("f8")
    val fm2 = new FeatureModel(f8)
    AddFeatureModelToAsset(repo2, fm2)
    AddAsset(repo2, root)
    AddAsset(file4, repo2)
    AddAsset(method, file1)
    AddAsset(block, file2)

    // Map assets to features and perform additional cloning
    MapAssetToFeature(method, f3)
    MapAssetToFeature(method, f4)
    val cloneMethodOp = CloneAsset(method, file2, Some(block))

    root.prettyprint
    // Print feature models of all children of the root
    root.children.foreach(a => a.featureModel.get.prettyprint())
  }


  // Failing
  /*test("Cloning with features"){

    val rootfeature1 = new RootFeature("A")
    val fm = new FeatureModel(rootfeature1)

    val rootfeature2 = new Feature("B")
    AddFeatureToFeatureModel(rootfeature1, fm, true, true, true)
    AddFeatureToFeatureModel(rootfeature2, fm, true, true, true)

    val fm2 = new FeatureModel(new RootFeature("fm2"))

    val root = Asset("CalculatorRootJava", VPRootType)
    val repo1 = Asset("repo 1", RepositoryType)
    val folder = Asset("folder", FolderType)
    val file = Asset("file", FileType)
    val repo2 = Asset("repo 2", RepositoryType)

    AddFeatureModelToAsset(repo1, fm)
    AddFeatureModelToAsset(repo2, fm2)

    AddAsset(repo1, root)
    AddAsset(folder, repo1)
    AddAsset(file, folder)
    AddAsset(repo2,root)

    MapAssetToFeature(folder,rootfeature1)
    MapAssetToFeature(file,rootfeature2)

    assert(fm2.rootfeature.UNASSIGNED.subfeatures.length==0)
    CloneAsset(folder,repo2)
    assert(fm2.rootfeature.UNASSIGNED.subfeatures.length==1)

    val clones1 = findFeatureClones(rootfeature1)
    val clones2 = findFeatureClones(rootfeature2)

    assert(featureModelContainsFeature(clones1(0),fm2)===true)
    assert(Utilities.featureClonedInFM(rootfeature2,fm2)===true)

    println(Utilities.getAllFeatures(root))
  }*/

  test("Cloning with features") {
    // Retrieve the base path from the configuration
    val basePath = Configuration.basePath

    // Setup of feature models and features
    val rootfeature1 = new RootFeature("A")
    val fm = new FeatureModel(rootfeature1)

    val featureB = new Feature("B")
    AddFeatureToFeatureModel(featureB, rootfeature1, true, true, true)

    val fm2 = new FeatureModel(new RootFeature("fm2"))

    // Create assets with basePath to ensure they are stored outside the project directory
    val root = Asset(s"$basePath/CalculatorRootJava", VPRootType)
    val repo1 = Asset(s"$basePath/repo1", RepositoryType)
    val folder = Asset(s"$basePath/folder", FolderType)
    val file = Asset(s"$basePath/file", FileType)
    val repo2 = Asset(s"$basePath/repo2", RepositoryType)

    // Add feature models to assets and establish the asset hierarchy
    AddFeatureModelToAsset(repo1, fm)
    AddFeatureModelToAsset(repo2, fm2)

    AddAsset(repo1, root)
    AddAsset(folder, repo1)
    AddAsset(file, folder)
    AddAsset(repo2, root)

    // Map assets to features
    MapAssetToFeature(folder, rootfeature1)
    MapAssetToFeature(file, featureB)

    // Assertions before cloning to validate initial state
    assert(fm2.rootfeature.UNASSIGNED.subfeatures.length == 0)

    // Perform cloning operation
    CloneAsset(folder, repo2)

    // Assertions after cloning to validate state change
    assert(fm2.rootfeature.UNASSIGNED.subfeatures.length == 1)

    // Find clones of features and validate them in the feature model
    val clones1 = findFeatureClones(rootfeature1)
    val clones2 = findFeatureClones(featureB)

    assert(featureModelContainsFeature(clones1(0), fm2) === true)
    assert(Utilities.featureClonedInFM(featureB, fm2) === true)

    // Print all features of the root asset
    println(Utilities.getAllFeatures(root))
  }

}
