package operations

import org.scalatest.FunSuite
import se.gu.vp.model.{Feature, FeatureModel, RootFeature}
import se.gu.vp.operations.{AddFeatureToFeatureModel, MoveFeature, Utilities}
/**
Copyright [2019] [Thorsten Berger, Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */
class MoveFeatureTest extends FunSuite {

  // Failing
  test("Feature Movement") {
    val feature1 = new RootFeature("A")
    val feature2 = Feature("B")
    val fm1 = new FeatureModel(feature1)
    AddFeatureToFeatureModel( feature2 , feature1 , true , true)

    val feature3 = new RootFeature("C")
    val feature4 = new Feature("D")
    val fm2 = new FeatureModel(feature3)
    AddFeatureToFeatureModel(feature4, feature3)

    MoveFeature(feature3,feature2)
    assert(Utilities.featureModelContainsFeature(feature3,fm1))
  }
  test("Feature Movement Success") {
    // Create a root feature and another feature to add to the feature model
    val rootFeature = new RootFeature("RootFeature")
    val featureToAdd = Feature("FeatureToAdd")
    val featureModel = new FeatureModel(rootFeature)
    AddFeatureToFeatureModel(featureToAdd, rootFeature, true, true)

    // Create another root feature to move
    val featureToMove = new RootFeature("FeatureToMove")
    val featureModel2 = new FeatureModel(featureToMove)

    // Perform the move operation
    MoveFeature(featureToMove, featureToAdd)

    // Assert that the feature model of the root feature now contains the moved feature
    assert(Utilities.featureModelContainsFeature(featureToMove, featureModel))
  }
}
