package operations.metadata

import org.scalatest.FunSuite
import se.gu.vp.model._
import se.gu.vp.operations.metadata._
import se.gu.vp.operations.{AddAsset, AddFeatureModelToAsset, AddFeatureToFeatureModel}

/**
Copyright [2022] [Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class DependsOnConversionTest extends FunSuite {
  // Incremental test
  test("dependsOn works between any level and element in main asset tree") {
    // Setup main asset tree
    val root1 = Asset("MyNonExistentDrive\\Root1", VPRootType)
    val repo1 = Asset("MyNonExistentDrive\\Root1\\Repo1", RepositoryType)
    AddAsset(repo1, root1)

    val rf1 = new RootFeature("rf1")
    val fm1 = FeatureModel(rf1)
    AddFeatureModelToAsset(asset = repo1, featuremodel = fm1)

    // Start incremental tests
    val f21 = Feature("f21")
    f21.dependsOn = Some(rf1)

    val converted1 = MetadataConverter.convertDependsOnToMetadata(f21.dependsOn.get, f21)
    assert(converted1.getClass == classOf[FeatureExpressionMetadata])
    assert(converted1.asInstanceOf[FeatureExpressionMetadata].featureMetadata.getClass == classOf[InAssetFeatureMetadata])
    assert(converted1.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(converted1.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path == Some("Root1\\Repo1"))
    assert(converted1.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(converted1.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[InAssetFeatureMetadata].lpq == "rf1")

    val rf2 = new RootFeature("rf2")
    AddFeatureToFeatureModel(f21, rf2)

    val converted2 = MetadataConverter.convertDependsOnToMetadata(f21.dependsOn.get, f21)
    assert(converted2.getClass == classOf[FeatureExpressionMetadata])
    assert(converted2.asInstanceOf[FeatureExpressionMetadata].featureMetadata.getClass == classOf[InAssetFeatureMetadata])
    assert(converted2.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(converted2.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path == Some("Root1\\Repo1"))
    assert(converted2.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(converted2.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[InAssetFeatureMetadata].lpq == "rf1")

    val fm2 = FeatureModel(rf2)

    val converted3 = MetadataConverter.convertDependsOnToMetadata(f21.dependsOn.get, f21)
    assert(converted3.getClass == classOf[FeatureExpressionMetadata])
    assert(converted3.asInstanceOf[FeatureExpressionMetadata].featureMetadata.getClass == classOf[InAssetFeatureMetadata])
    assert(converted3.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(converted3.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path == Some("Root1\\Repo1"))
    assert(converted3.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(converted3.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[InAssetFeatureMetadata].lpq == "rf1")

    val asset = Asset("block", BlockType)
    AddFeatureModelToAsset(asset, fm2)

    val converted4 = MetadataConverter.convertDependsOnToMetadata(f21.dependsOn.get, f21)
    assert(converted4.getClass == classOf[FeatureExpressionMetadata])
    assert(converted4.asInstanceOf[FeatureExpressionMetadata].featureMetadata.getClass == classOf[InAssetFeatureMetadata])
    assert(converted4.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(converted4.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path == Some("Root1\\Repo1"))
    assert(converted4.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(converted4.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[InAssetFeatureMetadata].lpq == "rf1")
  }

  test("dependsOn Conversion works in the same external AT") {
    val repo1 = Asset("MyNonExistentDrive\\Repo1", RepositoryType)
    val fold1 = Asset("MyNonExistentDrive\\Repo1\\Fold1", FolderType)
    val file1 = Asset("MyNonExistentDrive\\Repo1\\Fold1\\File1", FileType)
    val file2 = Asset("MyNonExistentDrive\\Repo1\\Fold1\\File2", FileType)
    AddAsset(fold1, repo1)
    AddAsset(file1, fold1)
    AddAsset(file2, fold1)

    val rf1 = new RootFeature("rf1")
    val f11 = Feature("f11")
    val f12 = Feature("f12")
    val f13 = Feature("f13")
    val f14 = Feature("f12")
    val fm1 = FeatureModel(rf1)
    AddFeatureToFeatureModel(f11, rf1, true)
    AddFeatureToFeatureModel(f12, rf1, true)
    AddFeatureToFeatureModel(f13, f11, true)
    AddFeatureToFeatureModel(f14, f11, true)
    AddFeatureModelToAsset(fold1, fm1)

    val rf2 = new RootFeature("rf2")
    val f21 = Feature("f21")
    val f22 = Feature("f22")
    val f23 = Feature("f23")
    val f24 = Feature("f22")
    val fm2 = FeatureModel(rf2)
    AddFeatureToFeatureModel(f21, rf2, true)
    AddFeatureToFeatureModel(f22, rf2, true)
    AddFeatureToFeatureModel(f23, f21, true)
    AddFeatureToFeatureModel(f24, f21, true)
    AddFeatureModelToAsset(file1, fm2)

    val rf3 = new RootFeature("rf3")
    val f31 = Feature("f31")
    val f32 = Feature("f32")
    val f33 = Feature("f33")
    val f34 = Feature("f32")
    val fm3 = FeatureModel(rf3)
    AddFeatureToFeatureModel(f31, rf3, true)
    AddFeatureToFeatureModel(f32, rf3, true)
    AddFeatureToFeatureModel(f33, f31, true)
    AddFeatureToFeatureModel(f34, f31, true)
    AddFeatureModelToAsset(file2, fm3)

    f11.dependsOn = Some(f12) // dependsOn in same featureModel
    f14.dependsOn = Some(f21) // dependsOn from parent asset fm to child asset fm
    f23.dependsOn = Some(f12) // dependsOn from child asset fm to parent asset fm
    f24.dependsOn = Some(f31) // dependsOn between two sibling asset fms

    val converted1 = MetadataConverter.convertDependsOnToMetadata(f11.dependsOn.get, f11)
    assert(converted1.getClass == classOf[FeatureExpressionMetadata])
    assert(converted1.asInstanceOf[FeatureExpressionMetadata].featureMetadata.getClass == classOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata])
    assert(converted1.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(converted1.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path == Some("Repo1\\Fold1"))
    assert(converted1.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(converted1.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata].lpq == "rf1-f12")

    val converted2 = MetadataConverter.convertDependsOnToMetadata(f14.dependsOn.get, f14)
    assert(converted2.getClass == classOf[FeatureExpressionMetadata])
    assert(converted2.asInstanceOf[FeatureExpressionMetadata].featureMetadata.getClass == classOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata])
    assert(converted2.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(converted2.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path == Some("Repo1\\Fold1\\File1"))
    assert(converted2.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(converted2.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata].lpq == "f21")

    val converted3 = MetadataConverter.convertDependsOnToMetadata(f23.dependsOn.get, f23)
    assert(converted3.getClass == classOf[FeatureExpressionMetadata])
    assert(converted3.asInstanceOf[FeatureExpressionMetadata].featureMetadata.getClass == classOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata])
    assert(converted3.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(converted3.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path == Some("Repo1\\Fold1"))
    assert(converted3.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(converted3.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata].lpq == "rf1-f12")

    val converted4 = MetadataConverter.convertDependsOnToMetadata(f24.dependsOn.get, f24)
    assert(converted4.getClass == classOf[FeatureExpressionMetadata])
    assert(converted4.asInstanceOf[FeatureExpressionMetadata].featureMetadata.getClass == classOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata])
    assert(converted4.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(converted4.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path == Some("Repo1\\Fold1\\File2"))
    assert(converted4.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(converted4.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata].lpq == "f31")
  }

  test("dependsOn Conversion works in the same external FM") {
    val rf1 = new RootFeature("rf1")
    val f11 = Feature("f11")
    val f12 = Feature("f12")
    val f13 = Feature("f13")
    val f14 = Feature("f12")
    val fm1 = FeatureModel(rf1)
    AddFeatureToFeatureModel(f11, rf1, true)
    AddFeatureToFeatureModel(f12, rf1, true)
    AddFeatureToFeatureModel(f13, f11, true)
    AddFeatureToFeatureModel(f14, f11, true)

    f11.dependsOn = Some(f12) // dependsOn between siblings
    f14.dependsOn = Some(f12) // dependsOn between element and siblings child
    rf1.dependsOn = Some(f13) // dependsOn from ancestor to child

    val converted1 = MetadataConverter.convertDependsOnToMetadata(f11.dependsOn.get, f11)
    assert(converted1.getClass == classOf[FeatureExpressionMetadata])
    assert(converted1.asInstanceOf[FeatureExpressionMetadata].featureMetadata.getClass == classOf[SelectedExternalFeatureInCurrentFeatureModelMetadata])
    assert(converted1.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentFeatureModelMetadata].lpq == "rf1-f12")

    val converted2 = MetadataConverter.convertDependsOnToMetadata(f14.dependsOn.get, f14)
    assert(converted2.getClass == classOf[FeatureExpressionMetadata])
    assert(converted2.asInstanceOf[FeatureExpressionMetadata].featureMetadata.getClass == classOf[SelectedExternalFeatureInCurrentFeatureModelMetadata])
    assert(converted2.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentFeatureModelMetadata].lpq == "rf1-f12")

    val converted3 = MetadataConverter.convertDependsOnToMetadata(rf1.dependsOn.get, rf1)
    assert(converted3.getClass == classOf[FeatureExpressionMetadata])
    assert(converted3.asInstanceOf[FeatureExpressionMetadata].featureMetadata.getClass == classOf[SelectedExternalFeatureInCurrentFeatureModelMetadata])
    assert(converted3.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentFeatureModelMetadata].lpq == "f13")
  }

  test("dependsOn Conversion works in the same external FT") {
    val f11 = Feature("f11")
    val f12 = Feature("f12")
    val f13 = Feature("f13")
    val f14 = Feature("f12")
    AddFeatureToFeatureModel(f12, f11, true)
    AddFeatureToFeatureModel(f13, f11, true)
    AddFeatureToFeatureModel(f14, f12, true)

    f12.dependsOn = Some(f13) // dependsOn between siblings
    f11.dependsOn = Some(f14) // dependsOn from ancestor to child

    val converted1 = MetadataConverter.convertDependsOnToMetadata(f12.dependsOn.get, f12)
    assert(converted1.getClass == classOf[FeatureExpressionMetadata])
    assert(converted1.asInstanceOf[FeatureExpressionMetadata].featureMetadata.getClass == classOf[SelectedExternalFeatureInCurrentExternalFeatureTree])
    assert(converted1.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalFeatureTree].lpq == "f13")

    val converted2 = MetadataConverter.convertDependsOnToMetadata(f11.dependsOn.get, f11)
    assert(converted2.getClass == classOf[FeatureExpressionMetadata])
    assert(converted2.asInstanceOf[FeatureExpressionMetadata].featureMetadata.getClass == classOf[SelectedExternalFeatureInCurrentExternalFeatureTree])
    assert(converted2.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalFeatureTree].lpq == "f12-f12")
  }
}
