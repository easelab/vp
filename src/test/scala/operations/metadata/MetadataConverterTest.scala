package operations.metadata

import org.scalatest.FunSuite
import se.gu.vp.model._
import se.gu.vp.operations.metadata.MetadataConverter.convertPresenceConditionToMetadata
import se.gu.vp.operations.metadata._
import se.gu.vp.operations.{AddAsset, AddFeatureModelToAsset, AddFeatureToFeatureModel, MapAssetToFeature}

/**
Copyright [2022] [Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class MetadataConverterTest extends FunSuite {

  def getFeatureExpressionMetadataFromExpressionMetadata(expr: ExpressionMetadata) : List[FeatureExpressionMetadata] = {
    expr match {
      case ex: UnaryExpressionMetadata      => getFeatureExpressionMetadataFromExpressionMetadata(ex.exp)
      case ex: BinaryExpressionMetadata     => getFeatureExpressionMetadataFromExpressionMetadata(ex.left) ++ getFeatureExpressionMetadataFromExpressionMetadata(ex.right)
      case ex: FeatureExpressionMetadata    => ex :: Nil
      case ex: TrueExpressionMetadata       => Nil
      case ex: FalseExpressionMetadata      => Nil
      case ex: IdentifierExpressionMetadata => Nil
    }
  }

  test("Conversion to Metadata works for Assets in Main Asset Tree") {
    val root1 = Asset("MyNonExistentDrive\\Root1", VPRootType)
    val repo1 = Asset("MyNonExistentDrive\\Root1\\Repo1", RepositoryType)
    val fold1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1", FolderType)
    val file1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1\\File1", FileType)
    val meth1 = Asset("foo", MethodType)
    val blck1 = Asset("block", BlockType)
    val blck2 = Asset("block", BlockType)
    val blck3 = Asset("block", BlockType)
    AddAsset(repo1, root1)
    AddAsset(fold1, repo1)
    AddAsset(file1, fold1)
    AddAsset(blck1, file1)
    AddAsset(meth1, file1)
    AddAsset(blck2, meth1)
    AddAsset(blck3, meth1)

    val rootMD = MetadataConverter.convertAssetToMetadata(root1)
    val foldMD = MetadataConverter.convertAssetToMetadata(fold1)
    val blckMD = MetadataConverter.convertAssetToMetadata(blck2)

    assert(rootMD.getClass == classOf[InternalAssetMetadata])
    assert(foldMD.getClass == classOf[InternalAssetMetadata])
    assert(blckMD.getClass == classOf[InternalAssetMetadata])
    assert(rootMD.asInstanceOf[InternalAssetMetadata].path == Some("Root1") && rootMD.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(foldMD.asInstanceOf[InternalAssetMetadata].path == Some("Root1\\Repo1\\Fold1") && foldMD.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(blckMD.asInstanceOf[InternalAssetMetadata].path == Some("Root1\\Repo1\\Fold1\\File1") && blckMD.asInstanceOf[InternalAssetMetadata].indexPath == Some(List(1,0)))
  }

  test("Conversion to Metadata works for Assets in External Asset Tree") {
    val repo1 = Asset("MyNonExistentDrive\\Repo1", RepositoryType)
    val fold1 = Asset("MyNonExistentDrive\\Repo1\\Fold1", FolderType)
    val file1 = Asset("MyNonExistentDrive\\Repo1\\Fold1\\File1", FileType)
    val meth1 = Asset("foo", MethodType)
    val blck1 = Asset("block", BlockType)
    val blck2 = Asset("block", BlockType, content = Some(List("Block2")))
    val blck3 = Asset("block", BlockType, content = Some(List("Block3")))
    AddAsset(fold1, repo1)
    AddAsset(file1, fold1)
    AddAsset(blck1, file1)
    AddAsset(meth1, file1)
    AddAsset(blck2, meth1)
    AddAsset(blck3, meth1)

    val repoMD = MetadataConverter.convertAssetToMetadata(repo1)
    val fileMD = MetadataConverter.convertAssetToMetadata(file1)
    val block1MD = MetadataConverter.convertAssetToMetadata(blck1)
    val block3MD = MetadataConverter.convertAssetToMetadata(blck3)

    assert(repoMD.getClass == classOf[ExternalAssetMetadata])
    assert(fileMD.getClass == classOf[SelectedExternalAssetMetadata])
    assert(block1MD.getClass == classOf[SelectedExternalAssetMetadata])
    assert(block3MD.getClass == classOf[SelectedExternalAssetMetadata])

    val repoMDC = repoMD.asInstanceOf[ExternalAssetMetadata]
    val fileSelMDC = fileMD.asInstanceOf[SelectedExternalAssetMetadata]
    val block1SelMDC = block1MD.asInstanceOf[SelectedExternalAssetMetadata]
    val block3SelMDC = block3MD.asInstanceOf[SelectedExternalAssetMetadata]

    assert(repoMDC.name == "Repo1")
    assert(repoMDC.children.length == 1)
    assert(repoMDC.assetType == "RepositoryType")
    assert(repoMDC.presenceCondition == TrueExpressionMetadata())

    val foldMDC = repoMDC.children(0).asInstanceOf[ExternalAssetMetadata]
    assert(foldMDC.name == "Repo1\\Fold1")
    assert(foldMDC.children.length == 1)
    assert(foldMDC.assetType == "FolderType")
    assert(foldMDC.presenceCondition == TrueExpressionMetadata())

    val fileMDC = foldMDC.children(0).asInstanceOf[ExternalAssetMetadata]
    assert(fileMDC.name == "Repo1\\Fold1\\File1")
    assert(fileMDC.children.length == 2)
    assert(fileMDC.assetType == "FileType")
    assert(fileMDC.presenceCondition == TrueExpressionMetadata())

    val block1MDC = fileMDC.children(0).asInstanceOf[ExternalAssetMetadata]
    assert(block1MDC.name == "block")
    assert(block1MDC.children.length == 0)
    assert(block1MDC.assetType == "BlockType")
    assert(block1MDC.presenceCondition == TrueExpressionMetadata())

    val methMDC = fileMDC.children(1).asInstanceOf[ExternalAssetMetadata]
    assert(methMDC.name == "foo")
    assert(methMDC.children.length == 2)
    assert(methMDC.assetType == "MethodType")
    assert(methMDC.presenceCondition == TrueExpressionMetadata())

    val block2MDC = methMDC.children(0).asInstanceOf[ExternalAssetMetadata]
    assert(block2MDC.name == "block")
    assert(block2MDC.children.length == 0)
    assert(block2MDC.assetType == "BlockType")
    assert(block2MDC.presenceCondition == TrueExpressionMetadata())

    val block3MDC = methMDC.children(1).asInstanceOf[ExternalAssetMetadata]
    assert(block3MDC.name == "block")
    assert(block3MDC.children.length == 0)
    assert(block3MDC.assetType == "BlockType")
    assert(block3MDC.presenceCondition == TrueExpressionMetadata())

    assert(fileSelMDC.rootAsset == repoMDC)
    assert(fileSelMDC.concreteAsset.path == Some("Repo1\\Fold1\\File1"))
    assert(fileSelMDC.concreteAsset.indexPath == None)

    assert(block1SelMDC.rootAsset == repoMDC)
    assert(block1SelMDC.concreteAsset.path == Some("Repo1\\Fold1\\File1"))
    assert(block1SelMDC.concreteAsset.indexPath == Some(List(0)))

    assert(block3SelMDC.rootAsset == repoMDC)
    assert(block3SelMDC.concreteAsset.path == Some("Repo1\\Fold1\\File1"))
    assert(block3SelMDC.concreteAsset.indexPath == Some(List(1,1)))
  }

  test ("Presence Condition Conversion works for features in main asset tree") {
    val root1 = Asset("MyNonExistentDrive\\Root1", VPRootType)
    val repo1 = Asset("MyNonExistentDrive\\Root1\\Repo1", RepositoryType)
    val fold1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1", FolderType)
    val file1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1\\File1", FileType)
    val meth1 = Asset("foo", MethodType)
    val blck1 = Asset("block", BlockType)
    val blck2 = Asset("block", BlockType, content = Some(List("Block2")))
    val blck3 = Asset("block", BlockType, content = Some(List("Block3")))
    AddAsset(repo1, root1)
    AddAsset(fold1, repo1)
    AddAsset(file1, fold1)
    AddAsset(blck1, file1)
    AddAsset(meth1, file1)
    AddAsset(blck2, meth1)
    AddAsset(blck3, meth1)

    val rf1 = new RootFeature("rf1")
    val f11 = Feature("f11")
    val f12 = Feature("f12")
    val f13 = Feature("f13")
    val f14 = Feature("f12")
    val fm1 = FeatureModel(rf1)
    AddFeatureToFeatureModel(f11, rf1, true)
    AddFeatureToFeatureModel(f12, rf1, true)
    AddFeatureToFeatureModel(f13, f11, true)
    AddFeatureToFeatureModel(f14, f11, true)
    AddFeatureModelToAsset(repo1, fm1)

    MapAssetToFeature(fold1, f11)
    MapAssetToFeature(file1, f11)
    MapAssetToFeature(file1, f12)
    MapAssetToFeature(meth1, f12)
    MapAssetToFeature(meth1, f13)
    MapAssetToFeature(blck1, f11)
    MapAssetToFeature(blck1, f12)
    MapAssetToFeature(blck1, f13)
    MapAssetToFeature(blck1, f14)

    val repoMD = MetadataConverter.convertAssetTreeToMetadata(repo1)
    assert(repoMD.getClass == classOf[ExternalAssetMetadata])

    val repoMDC = repoMD.asInstanceOf[ExternalAssetMetadata]
    val foldMDC = repoMDC.children(0).asInstanceOf[ExternalAssetMetadata]
    val fileMDC = foldMDC.children(0).asInstanceOf[ExternalAssetMetadata]
    val methMDC = fileMDC.children(1).asInstanceOf[ExternalAssetMetadata]
    val blckMDC = fileMDC.children(0).asInstanceOf[ExternalAssetMetadata]

    assert(repoMDC.presenceCondition == TrueExpressionMetadata())

    val foldMDCpcfeats = getFeatureExpressionMetadataFromExpressionMetadata(foldMDC.presenceCondition)
    assert(foldMDCpcfeats.length == 1)

    assert(foldMDCpcfeats(0).featureMetadata.getClass == classOf[InAssetFeatureMetadata])
    assert(foldMDCpcfeats(0).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(foldMDCpcfeats(0).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path == Some("Root1\\Repo1"))
    assert(foldMDCpcfeats(0).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(foldMDCpcfeats(0).featureMetadata.asInstanceOf[InAssetFeatureMetadata].lpq == "f11")


    val fileMDCpcfeats = getFeatureExpressionMetadataFromExpressionMetadata(fileMDC.presenceCondition)
    assert(fileMDCpcfeats.length == 2)

    assert(fileMDCpcfeats(0).featureMetadata.getClass == classOf[InAssetFeatureMetadata])
    assert(fileMDCpcfeats(0).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(fileMDCpcfeats(0).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path == Some("Root1\\Repo1"))
    assert(fileMDCpcfeats(0).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(fileMDCpcfeats(0).featureMetadata.asInstanceOf[InAssetFeatureMetadata].lpq == "f11")

    assert(fileMDCpcfeats(1).featureMetadata.getClass == classOf[InAssetFeatureMetadata])
    assert(fileMDCpcfeats(1).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(fileMDCpcfeats(1).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path == Some("Root1\\Repo1"))
    assert(fileMDCpcfeats(1).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(fileMDCpcfeats(1).featureMetadata.asInstanceOf[InAssetFeatureMetadata].lpq == "rf1-f12")


    val blockMDCpcfeats = getFeatureExpressionMetadataFromExpressionMetadata(blckMDC.presenceCondition)
    assert(blockMDCpcfeats.length == 4)

    assert(blockMDCpcfeats(0).featureMetadata.getClass == classOf[InAssetFeatureMetadata])
    assert(blockMDCpcfeats(0).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(blockMDCpcfeats(0).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path == Some("Root1\\Repo1"))
    assert(blockMDCpcfeats(0).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(blockMDCpcfeats(0).featureMetadata.asInstanceOf[InAssetFeatureMetadata].lpq == "f11")

    assert(blockMDCpcfeats(1).featureMetadata.getClass == classOf[InAssetFeatureMetadata])
    assert(blockMDCpcfeats(1).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(blockMDCpcfeats(1).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path == Some("Root1\\Repo1"))
    assert(blockMDCpcfeats(1).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(blockMDCpcfeats(1).featureMetadata.asInstanceOf[InAssetFeatureMetadata].lpq == "rf1-f12")

    assert(blockMDCpcfeats(2).featureMetadata.getClass == classOf[InAssetFeatureMetadata])
    assert(blockMDCpcfeats(2).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(blockMDCpcfeats(2).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path == Some("Root1\\Repo1"))
    assert(blockMDCpcfeats(2).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(blockMDCpcfeats(2).featureMetadata.asInstanceOf[InAssetFeatureMetadata].lpq == "f13")

    assert(blockMDCpcfeats(3).featureMetadata.getClass == classOf[InAssetFeatureMetadata])
    assert(blockMDCpcfeats(3).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(blockMDCpcfeats(3).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path == Some("Root1\\Repo1"))
    assert(blockMDCpcfeats(3).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(blockMDCpcfeats(3).featureMetadata.asInstanceOf[InAssetFeatureMetadata].lpq == "f11-f12")
  }

  test ("Presence Condition conversion works for features from various sources") {
    val root1 = Asset("MyNonExistentDrive\\Root1", VPRootType)
    val repo1 = Asset("MyNonExistentDrive\\Root1\\Repo1", RepositoryType)
    val fold1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1", FolderType)
    val file1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1\\File1", FileType)
    val meth1 = Asset("foo", MethodType)
    val blck1 = Asset("block", BlockType)
    val blck2 = Asset("block", BlockType, content = Some(List("Block2")))
    val blck3 = Asset("block", BlockType, content = Some(List("Block3")))
    AddAsset(repo1, root1)
    AddAsset(fold1, repo1)
    AddAsset(file1, fold1)
    AddAsset(blck1, file1)
    AddAsset(meth1, file1)
    AddAsset(blck2, meth1)
    AddAsset(blck3, meth1)

    val rf1 = new RootFeature("rf1")
    val f11 = Feature("f11")
    val f12 = Feature("f12")
    val f13 = Feature("f13")
    val f14 = Feature("f12")
    val fm1 = FeatureModel(rf1)
    AddFeatureToFeatureModel(f11, rf1, true)
    AddFeatureToFeatureModel(f12, rf1, true)
    AddFeatureToFeatureModel(f13, f11, true)
    AddFeatureToFeatureModel(f14, f11, true)
    AddFeatureModelToAsset(repo1, fm1)

    // Build external Asset Tree
    val repo2 = Asset("SomeOtherDirectory\\SomeAnotherDir\\Repo2", RepositoryType)
    val fold2 = Asset("SomeOtherDirectory\\SomeAnotherDir\\Repo2\\Fold2", FolderType)
    val file2 = Asset("SomeOtherDirectory\\SomeAnotherDir\\Repo2\\Fold2\\File2", FileType)
    AddAsset(fold2, repo2)
    AddAsset(file2, fold2)

    val rf2 = new RootFeature("rf1")
    val f21 = Feature("f11")
    val f22 = Feature("f12")
    val f23 = Feature("f13")
    val f24 = Feature("f12")
    val fm2 = FeatureModel(rf2)

    AddFeatureToFeatureModel(f21, rf2, true)
    AddFeatureToFeatureModel(f22, rf2, true)
    AddFeatureToFeatureModel(f23, f21, true)
    AddFeatureToFeatureModel(f24, f21, true)
    AddFeatureModelToAsset(fold2, fm2)

    // Build structure starting with Feature Model
    val rf4 = new RootFeature("rf4")
    val f41 = Feature("f41")
    val f42 = Feature("f42")
    val f43 = Feature("f43")
    val fm4 = FeatureModel(rf4)
    AddFeatureToFeatureModel(f41, rf4, true)
    AddFeatureToFeatureModel(f42, rf4, true)
    AddFeatureToFeatureModel(f43, f41, true)

    // Build structure starting with RootFeature
    val rf5 = new RootFeature("rf5")
    val f51 = Feature("f51")
    val f52 = Feature("f52")
    val f53 = Feature("f53")
    AddFeatureToFeatureModel(f51, rf5, true)
    AddFeatureToFeatureModel(f52, rf5, true)
    AddFeatureToFeatureModel(f53, f51, true)

    // Build structure starting with feature
    val f61 = Feature("f61")
    val f62 = Feature("f62")
    val f63 = Feature("f63")
    AddFeatureToFeatureModel(f62, f61, true)
    AddFeatureToFeatureModel(f63, f62, true)

    // Single feature
    val f71 = Feature("f71")

    // Build presence condition in external asset tree
    file2.presenceCondition = f71 | f62 | f53 | f42 | f24 | f13
    val metadata = convertPresenceConditionToMetadata(file2.presenceCondition, file2)
    val featureMetadataList = getFeatureExpressionMetadataFromExpressionMetadata(metadata)

    assert(featureMetadataList.length == 6)

    assert(featureMetadataList(0).featureMetadata.getClass == classOf[ExternalFeatureMetadata])
    assert(featureMetadataList(0).featureMetadata.asInstanceOf[ExternalFeatureMetadata].name == "f71")
    assert(featureMetadataList(0).featureMetadata.asInstanceOf[ExternalFeatureMetadata].subfeatures == Nil)
    assert(featureMetadataList(0).featureMetadata.asInstanceOf[ExternalFeatureMetadata].isRootFeature == false)

    assert(featureMetadataList(1).featureMetadata.getClass == classOf[SelectedExternalSubFeatureMetadata])
    assert(featureMetadataList(1).featureMetadata.asInstanceOf[SelectedExternalSubFeatureMetadata].topFeature.name == "f61")
    assert(featureMetadataList(1).featureMetadata.asInstanceOf[SelectedExternalSubFeatureMetadata].topFeature.subfeatures.length == 1)
    assert(featureMetadataList(1).featureMetadata.asInstanceOf[SelectedExternalSubFeatureMetadata].topFeature.subfeatures(0).name == "f61-f62")
    assert(featureMetadataList(1).featureMetadata.asInstanceOf[SelectedExternalSubFeatureMetadata].topFeature.isRootFeature == false)
    assert(featureMetadataList(1).featureMetadata.asInstanceOf[SelectedExternalSubFeatureMetadata].lpq == "f62")

    assert(featureMetadataList(2).featureMetadata.getClass == classOf[SelectedExternalSubFeatureMetadata])
    assert(featureMetadataList(2).featureMetadata.asInstanceOf[SelectedExternalSubFeatureMetadata].topFeature.name == "rf5")
    assert(featureMetadataList(2).featureMetadata.asInstanceOf[SelectedExternalSubFeatureMetadata].topFeature.subfeatures.length == 3) // == 3 due to unassigned
    assert(featureMetadataList(2).featureMetadata.asInstanceOf[SelectedExternalSubFeatureMetadata].topFeature.isRootFeature)
    assert(featureMetadataList(2).featureMetadata.asInstanceOf[SelectedExternalSubFeatureMetadata].lpq == "f53")

    assert(featureMetadataList(3).featureMetadata.getClass == classOf[SelectedExternalFeatureInFMMetadata])
    assert(featureMetadataList(3).featureMetadata.asInstanceOf[SelectedExternalFeatureInFMMetadata].fm.name == "FeatureModel")
    assert(featureMetadataList(3).featureMetadata.asInstanceOf[SelectedExternalFeatureInFMMetadata].fm.rootFeature.name == "rf4")
    assert(featureMetadataList(3).featureMetadata.asInstanceOf[SelectedExternalFeatureInFMMetadata].fm.rootFeature.isRootFeature)
    assert(featureMetadataList(3).featureMetadata.asInstanceOf[SelectedExternalFeatureInFMMetadata].fm.rootFeature.subfeatures.length == 3) // == 3 due to unassigned
    assert(featureMetadataList(3).featureMetadata.asInstanceOf[SelectedExternalFeatureInFMMetadata].lpq == "f42")

    assert(featureMetadataList(4).featureMetadata.getClass == classOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata])
    assert(featureMetadataList(4).featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path == Some("Repo2\\Fold2"))
    assert(featureMetadataList(4).featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(featureMetadataList(4).featureMetadata.asInstanceOf[SelectedExternalFeatureInCurrentExternalAssetTreeMetadata].lpq == "f11-f12")

    assert(featureMetadataList(5).featureMetadata.getClass == classOf[InAssetFeatureMetadata])
    assert(featureMetadataList(5).featureMetadata.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path == Some("Root1\\Repo1"))
    assert(featureMetadataList(5).featureMetadata.asInstanceOf[InAssetFeatureMetadata].lpq == "f13")
  }

  test ("Convert Feature Model To Metadata in Existing Asset works") {
    // Build external Asset Tree
    val repo2 = Asset("SomeOtherDirectory\\SomeAnotherDir\\Repo2", RepositoryType)
    val fold2 = Asset("SomeOtherDirectory\\SomeAnotherDir\\Repo2\\Fold2", FolderType)
    val file2 = Asset("SomeOtherDirectory\\SomeAnotherDir\\Repo2\\Fold2\\File2", FileType)
    AddAsset(fold2, repo2)
    AddAsset(file2, fold2)

    val rf2 = new RootFeature("rf1")
    val f21 = Feature("f11")
    val f22 = Feature("f12")
    val f23 = Feature("f13")
    val f24 = Feature("f12")
    val fm2 = FeatureModel(rf2)

    AddFeatureToFeatureModel(f21, rf2, true)
    AddFeatureToFeatureModel(f22, rf2, true)
    AddFeatureToFeatureModel(f23, f21, true)
    AddFeatureToFeatureModel(f24, f21, true)
    AddFeatureModelToAsset(fold2, fm2)

    val convertedFM = MetadataConverter.convertFeatureModelToMetadata(fm2)
    assert(convertedFM.getClass == classOf[InAssetFeatureModelMetadata])
    val convertedFMC = convertedFM.asInstanceOf[InAssetFeatureModelMetadata]

    assert(convertedFMC.featureModelAsset.asInstanceOf[SelectedExternalAssetMetadata].concreteAsset.path == Some("Repo2\\Fold2"))
    assert(convertedFMC.featureModelAsset.asInstanceOf[SelectedExternalAssetMetadata].rootAsset.name == "Repo2")
  }

  test ("Convert FeatureModel to Metadata without containing asset works") {
    val rf2 = new RootFeature("rf1")
    val f21 = Feature("f11")
    val f22 = Feature("f12")
    val f23 = Feature("f13")
    val f24 = Feature("f12")
    val fm2 = FeatureModel(rf2)

    AddFeatureToFeatureModel(f21, rf2, true)
    AddFeatureToFeatureModel(f22, rf2, true)
    AddFeatureToFeatureModel(f23, f21, true)
    AddFeatureToFeatureModel(f24, f21, true)

    val convertedFM = MetadataConverter.convertFeatureModelToMetadata(fm2)

    assert(convertedFM.getClass == classOf[ExternalFeatureModelMetadata])
    assert(convertedFM.asInstanceOf[ExternalFeatureModelMetadata].rootFeature.name == "rf1")
    assert(convertedFM.asInstanceOf[ExternalFeatureModelMetadata].rootFeature.subfeatures.length == 3)
    assert(convertedFM.asInstanceOf[ExternalFeatureModelMetadata].rootFeature.subfeatures(1).name == "rf1-f11")
    assert(convertedFM.asInstanceOf[ExternalFeatureModelMetadata].rootFeature.subfeatures(1).subfeatures.length == 2)
    assert(convertedFM.asInstanceOf[ExternalFeatureModelMetadata].rootFeature.subfeatures(1).subfeatures(0).name == "rf1-f11-f13")
    assert(convertedFM.asInstanceOf[ExternalFeatureModelMetadata].rootFeature.subfeatures(1).subfeatures(0).subfeatures.length == 0)
    assert(convertedFM.asInstanceOf[ExternalFeatureModelMetadata].rootFeature.subfeatures(1).subfeatures(1).name == "rf1-f11-f12")
    assert(convertedFM.asInstanceOf[ExternalFeatureModelMetadata].rootFeature.subfeatures(1).subfeatures(1).subfeatures.length == 0)
    assert(convertedFM.asInstanceOf[ExternalFeatureModelMetadata].rootFeature.subfeatures(2).name == "rf1-f12")
    assert(convertedFM.asInstanceOf[ExternalFeatureModelMetadata].rootFeature.subfeatures(2).subfeatures.length == 0)
    assert(convertedFM.asInstanceOf[ExternalFeatureModelMetadata].name == "FeatureModel")
  }

  test ("Convert Feature To Metadata works") {
    val f11 = Feature("f11")
    val f12 = Feature("f12")
    val f13 = Feature("f13")
    val f14 = Feature("f12")
    val rf1 = new RootFeature("rf1")

    AddFeatureToFeatureModel(f11, rf1, true)
    AddFeatureToFeatureModel(f12, rf1, true)
    AddFeatureToFeatureModel(f13, f11, true)
    AddFeatureToFeatureModel(f14, f11, true)

    val convertedFeat1 = MetadataConverter.convertFeatureToMetadata(rf1)
    assert(convertedFeat1.getClass == classOf[ExternalFeatureMetadata])
    assert(convertedFeat1.asInstanceOf[ExternalFeatureMetadata].name == "rf1")
    assert(convertedFeat1.asInstanceOf[ExternalFeatureMetadata].subfeatures.length == 3)
    assert(convertedFeat1.asInstanceOf[ExternalFeatureMetadata].isRootFeature)

    val convertedFeat2 = MetadataConverter.convertFeatureToMetadata(f11)
    assert(convertedFeat2.getClass == classOf[SelectedExternalSubFeatureMetadata])
    assert(convertedFeat2.asInstanceOf[SelectedExternalSubFeatureMetadata].topFeature == convertedFeat1)
    assert(convertedFeat2.asInstanceOf[SelectedExternalSubFeatureMetadata].lpq == "f11")

    val fm1 = FeatureModel(rf1)

    val convertedFeat3 = MetadataConverter.convertFeatureToMetadata(f11)
    assert(convertedFeat3.getClass == classOf[SelectedExternalFeatureInFMMetadata])
    assert(convertedFeat3.asInstanceOf[SelectedExternalFeatureInFMMetadata].fm.rootFeature == convertedFeat1)

    val repo1 = Asset("SomeOtherDirectory\\SomeAnotherDir\\Root1\\Repo1", RepositoryType)
    val fold1 = Asset("SomeOtherDirectory\\SomeAnotherDir\\Root1\\Repo1\\Fold1", FolderType)
    val file1 = Asset("SomeOtherDirectory\\SomeAnotherDir\\Root1\\Repo1\\Fold1\\File1", FileType)
    AddAsset(fold1, repo1)
    AddAsset(file1, fold1)
    AddFeatureModelToAsset(repo1, fm1)

    val convertedFeat4 = MetadataConverter.convertFeatureToMetadata(f11)
    assert(convertedFeat4.getClass == classOf[InAssetFeatureMetadata])
    assert(convertedFeat4.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.getClass == classOf[ExternalAssetMetadata])
    assert(convertedFeat4.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[ExternalAssetMetadata].name == "Repo1")
    assert(convertedFeat4.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[ExternalAssetMetadata].children.length == 1)
    assert(convertedFeat4.asInstanceOf[InAssetFeatureMetadata].lpq == "f11")

    val root1 = Asset("SomeOtherDirectory\\SomeAnotherDir\\Root1", VPRootType)
    AddAsset(repo1, root1)

    val convertedFeat5 = MetadataConverter.convertFeatureToMetadata(f11)
    assert(convertedFeat5.getClass == classOf[InAssetFeatureMetadata])
    assert(convertedFeat5.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(convertedFeat5.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path == Some("Root1\\Repo1"))
    assert(convertedFeat5.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(convertedFeat5.asInstanceOf[InAssetFeatureMetadata].lpq == "f11")
  }

  // TODO: Uniqueness? And exception in convertFInPC

  test ("Converts Expression correctly") {
    val f1 = Feature("f1")
    val f2 = Feature("f2")
    val f3 = Feature("f3")

    val asset = Asset("SuperAsset", RepositoryType)
    val complexExpression = Negation(And(Implies(f1, Or(f2, False())), Or(True(), And(f3, Identifier("TestId")))))
    asset.presenceCondition = complexExpression

    val convertedExpression = MetadataConverter.convertPresenceConditionToMetadata(complexExpression, asset)

    assert(convertedExpression.getClass == classOf[UnaryExpressionMetadata])
    assert(convertedExpression.asInstanceOf[UnaryExpressionMetadata].op == "!")
    assert(convertedExpression.asInstanceOf[UnaryExpressionMetadata].exp.getClass == classOf[BinaryExpressionMetadata])
    assert(convertedExpression.asInstanceOf[UnaryExpressionMetadata].exp.asInstanceOf[BinaryExpressionMetadata].op == "&")
    assert(convertedExpression.asInstanceOf[UnaryExpressionMetadata].exp.asInstanceOf[BinaryExpressionMetadata].left.getClass == classOf[BinaryExpressionMetadata])
    assert(convertedExpression.asInstanceOf[UnaryExpressionMetadata].exp.asInstanceOf[BinaryExpressionMetadata].left.asInstanceOf[BinaryExpressionMetadata].op == "=>")
    assert(convertedExpression.asInstanceOf[UnaryExpressionMetadata].exp.asInstanceOf[BinaryExpressionMetadata].left.asInstanceOf[BinaryExpressionMetadata].left.getClass == classOf[FeatureExpressionMetadata])
    assert(convertedExpression.asInstanceOf[UnaryExpressionMetadata].exp.asInstanceOf[BinaryExpressionMetadata].left.asInstanceOf[BinaryExpressionMetadata].left.asInstanceOf[FeatureExpressionMetadata].featureMetadata.asInstanceOf[ExternalFeatureMetadata].name == "f1")
    assert(convertedExpression.asInstanceOf[UnaryExpressionMetadata].exp.asInstanceOf[BinaryExpressionMetadata].left.asInstanceOf[BinaryExpressionMetadata].right.getClass == classOf[BinaryExpressionMetadata])
    assert(convertedExpression.asInstanceOf[UnaryExpressionMetadata].exp.asInstanceOf[BinaryExpressionMetadata].left.asInstanceOf[BinaryExpressionMetadata].right.asInstanceOf[BinaryExpressionMetadata].op == "|")
    assert(convertedExpression.asInstanceOf[UnaryExpressionMetadata].exp.asInstanceOf[BinaryExpressionMetadata].left.asInstanceOf[BinaryExpressionMetadata].right.asInstanceOf[BinaryExpressionMetadata].left.getClass == classOf[FeatureExpressionMetadata])
    assert(convertedExpression.asInstanceOf[UnaryExpressionMetadata].exp.asInstanceOf[BinaryExpressionMetadata].left.asInstanceOf[BinaryExpressionMetadata].right.asInstanceOf[BinaryExpressionMetadata].right.getClass == classOf[FalseExpressionMetadata])
    assert(convertedExpression.asInstanceOf[UnaryExpressionMetadata].exp.asInstanceOf[BinaryExpressionMetadata].right.getClass == classOf[BinaryExpressionMetadata])
    assert(convertedExpression.asInstanceOf[UnaryExpressionMetadata].exp.asInstanceOf[BinaryExpressionMetadata].right.asInstanceOf[BinaryExpressionMetadata].op == "|")
    assert(convertedExpression.asInstanceOf[UnaryExpressionMetadata].exp.asInstanceOf[BinaryExpressionMetadata].right.asInstanceOf[BinaryExpressionMetadata].left.getClass == classOf[TrueExpressionMetadata])
    assert(convertedExpression.asInstanceOf[UnaryExpressionMetadata].exp.asInstanceOf[BinaryExpressionMetadata].right.asInstanceOf[BinaryExpressionMetadata].right.getClass == classOf[BinaryExpressionMetadata])
    assert(convertedExpression.asInstanceOf[UnaryExpressionMetadata].exp.asInstanceOf[BinaryExpressionMetadata].right.asInstanceOf[BinaryExpressionMetadata].right.asInstanceOf[BinaryExpressionMetadata].op == "&")
    assert(convertedExpression.asInstanceOf[UnaryExpressionMetadata].exp.asInstanceOf[BinaryExpressionMetadata].right.asInstanceOf[BinaryExpressionMetadata].right.asInstanceOf[BinaryExpressionMetadata].left.getClass == classOf[FeatureExpressionMetadata])
    assert(convertedExpression.asInstanceOf[UnaryExpressionMetadata].exp.asInstanceOf[BinaryExpressionMetadata].right.asInstanceOf[BinaryExpressionMetadata].right.asInstanceOf[BinaryExpressionMetadata].right.getClass == classOf[IdentifierExpressionMetadata])
    assert(convertedExpression.asInstanceOf[UnaryExpressionMetadata].exp.asInstanceOf[BinaryExpressionMetadata].right.asInstanceOf[BinaryExpressionMetadata].right.asInstanceOf[BinaryExpressionMetadata].right.asInstanceOf[IdentifierExpressionMetadata].name == "TestId")
  }

  test("Throws Exception, when referencing feature from another external asset tree") {
    val repo1 = Asset("Repo1", RepositoryType)
    val fold1 = Asset("Fold1", FolderType)
    val file1 = Asset("File1", FileType)
    AddAsset(fold1, repo1)
    AddAsset(file1, fold1)

    val fold2 = Asset("Fold2", FolderType)
    val file2 = Asset("File2", FileType)
    AddAsset(file2, fold2)

    val rf1 = new RootFeature("rf1")
    val f11 = Feature("f11")
    val f12 = Feature("f12")
    val f13 = Feature("f13")
    val fm1 = FeatureModel(rf1)
    AddFeatureToFeatureModel(f11, rf1, true)
    AddFeatureToFeatureModel(f12, rf1, true)
    AddFeatureToFeatureModel(f13, f11, true)
    AddFeatureModelToAsset(repo1, fm1)

    val rf2 = new RootFeature("rf2")
    val f21 = Feature("f21")
    val f22 = Feature("f22")
    val f23 = Feature("f23")
    val fm2 = FeatureModel(rf2)
    AddFeatureToFeatureModel(f21, rf2, true)
    AddFeatureToFeatureModel(f22, rf2, true)
    AddFeatureToFeatureModel(f23, f21, true)
    AddFeatureModelToAsset(fold2, fm2)

    try {
      MetadataConverter.convertFeatureInPresenceCondition(f22, fold1)
      assert(false)
    } catch {
      case _ => assert(true)
    }
  }

  test("Version numbers are successfully converted") {
    val asset = Asset("testAsset", RepositoryType)
    asset.versionNumber = 10

    val convertedAsset = MetadataConverter.convertAssetToMetadata(asset)
    assert(convertedAsset.asInstanceOf[ExternalAssetMetadata].versionNumber == 10)

    val feature = Feature("testFeature")
    feature.versionNumber = 15

    val convertedFeature = MetadataConverter.convertFeatureToMetadata(feature)
    assert(convertedFeature.asInstanceOf[ExternalFeatureMetadata].versionNumber == 15)
  }
}
