package operations.metadata

import org.scalatest.FunSuite
import se.gu.vp.model._
import se.gu.vp.operations.Utilities.{computeAssetPath, computeFeaturePath, getassetbyname}
import se.gu.vp.operations.metadata._
import se.gu.vp.operations._
import vp.operations.AddExternalAsset

import java.nio.file.Paths

/**
Copyright [2022] [Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */
class OperationMetadataConversionTest extends FunSuite {
  test("AddAsset Metadata Creation works (w/o features)") {
    val root1 = Asset("MyNonExistentDrive\\Root1", VPRootType)
    val repo1 = Asset("MyNonExistentDrive\\Root1\\Repo1", RepositoryType)
    val fold1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1", FolderType)
    val file1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1\\File1", FileType)
    val meth1 = Asset("foo", MethodType)
    val blck1 = Asset("block", BlockType)
    val blck2 = Asset("block", BlockType)
    val blck3 = Asset("block", BlockType)
    AddAsset(repo1, root1)
    AddAsset(fold1, repo1)
    AddAsset(file1, fold1)
    AddAsset(blck1, file1)
    AddAsset(meth1, file1)
    AddAsset(blck2, meth1)
    AddAsset(blck3, meth1)

    val repo2 = Asset("MyNonExistentDrive\\Root1\\Repo2", RepositoryType)
    val fold2 = Asset("MyNonExistentDrive\\Root1\\Repo2\\Fold2", FolderType)
    val file2 = Asset("MyNonExistentDrive\\Root1\\Repo2\\Fold2\\File2", FileType)
    AddAsset(fold2, repo2)
    AddAsset(file2, fold2)

    val op = AddAsset(repo2, root1)
    val sut = op.metadata.get

    assert(sut.getClass == classOf[AddAssetMetadata])
    assert(sut.asInstanceOf[AddAssetMetadata].asset.getClass == classOf[ExternalAssetMetadata])
    assert(sut.asInstanceOf[AddAssetMetadata].asset.asInstanceOf[ExternalAssetMetadata].name == "Repo2")
    assert(sut.asInstanceOf[AddAssetMetadata].parent.getClass == classOf[InternalAssetMetadata])
    assert(sut.asInstanceOf[AddAssetMetadata].parent.asInstanceOf[InternalAssetMetadata].path.get == "Root1")
    assert(sut.asInstanceOf[AddAssetMetadata].subOperations == Some(Nil))
  }

  test("AddAsset Metadata Creation works (with features)") {
    val root1 = Asset("MyNonExistentDrive\\Root1", VPRootType)
    val repo1 = Asset("MyNonExistentDrive\\Root1\\Repo1", RepositoryType)
    val fold1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1", FolderType)
    val file1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1\\File1", FileType)
    val meth1 = Asset("foo", MethodType)
    val blck1 = Asset("block", BlockType)
    val blck2 = Asset("block", BlockType)
    val blck3 = Asset("block", BlockType)
    AddAsset(repo1, root1)
    AddAsset(fold1, repo1)
    AddAsset(file1, fold1)
    AddAsset(blck1, file1)
    AddAsset(meth1, file1)
    AddAsset(blck2, meth1)
    AddAsset(blck3, meth1)
    val fm = FeatureModel(new RootFeature("rf1"))
    AddFeatureModelToAsset(asset = repo1, fm)

    val fold2 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold2", FolderType)
    val file2 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold2\\File2", FileType)
    val newFeat = Feature("NewFeature")
    AddAsset(file2, fold2)
    fold2.presenceCondition = newFeat

    val op = AddAsset(fold2, repo1)
    val sut = op.metadata.get

    assert(sut.getClass == classOf[AddAssetMetadata])
    assert(sut.asInstanceOf[AddAssetMetadata].asset.getClass == classOf[ExternalAssetMetadata])
    assert(sut.asInstanceOf[AddAssetMetadata].asset.asInstanceOf[ExternalAssetMetadata].name == "Fold2")
    assert(sut.asInstanceOf[AddAssetMetadata].parent.getClass == classOf[InternalAssetMetadata])
    assert(sut.asInstanceOf[AddAssetMetadata].parent.asInstanceOf[InternalAssetMetadata].path.get == "Root1\\Repo1")
    assert(sut.asInstanceOf[AddAssetMetadata].subOperations.get.length == 1)
    assert(sut.asInstanceOf[AddAssetMetadata].subOperations.get(0).getClass == classOf[AddFeatureToFeatureModelMetadata])
    assert(sut.asInstanceOf[AddAssetMetadata].subOperations.get(0).asInstanceOf[AddFeatureToFeatureModelMetadata].feature.getClass == classOf[ExternalFeatureMetadata])
    assert(sut.asInstanceOf[AddAssetMetadata].subOperations.get(0).asInstanceOf[AddFeatureToFeatureModelMetadata].feature.asInstanceOf[ExternalFeatureMetadata].name == "NewFeature")
    assert(sut.asInstanceOf[AddAssetMetadata].subOperations.get(0).asInstanceOf[AddFeatureToFeatureModelMetadata].featuremodel == None)
    assert(sut.asInstanceOf[AddAssetMetadata].subOperations.get(0).asInstanceOf[AddFeatureToFeatureModelMetadata].parent.get.getClass == classOf[InAssetFeatureMetadata])
    assert(sut.asInstanceOf[AddAssetMetadata].subOperations.get(0).asInstanceOf[AddFeatureToFeatureModelMetadata].parent.get.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(sut.asInstanceOf[AddAssetMetadata].subOperations.get(0).asInstanceOf[AddFeatureToFeatureModelMetadata].parent.get.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path.get == "Root1\\Repo1")
    assert(sut.asInstanceOf[AddAssetMetadata].subOperations.get(0).asInstanceOf[AddFeatureToFeatureModelMetadata].parent.get.asInstanceOf[InAssetFeatureMetadata].lpq == "UnAssigned")
    assert(sut.asInstanceOf[AddAssetMetadata].subOperations.get(0).asInstanceOf[AddFeatureToFeatureModelMetadata].subOperations == None)
  }

  // Only tests eager evaluation
  test("AddExternalAsset Metadata Creation works") {
    val root1 = Asset("MyNonExistentDrive\\Root1", VPRootType)
    val repo1 = Asset("MyNonExistentDrive\\Root1\\Repo1", RepositoryType)
    val fold1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1", FolderType)
    val file1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1\\File1", FileType)
    val meth1 = Asset("foo", MethodType)
    val blck1 = Asset("block", BlockType)
    val blck2 = Asset("block", BlockType)
    val blck3 = Asset("block", BlockType)
    AddAsset(repo1, root1)
    AddAsset(fold1, repo1)
    AddAsset(file1, fold1)
    AddAsset(blck1, file1)
    AddAsset(meth1, file1)
    AddAsset(blck2, meth1)
    AddAsset(blck3, meth1)

    val assetPathFolder = Paths.get("CalculatorRoot\\AdvancedCalculator")

    val opFolder = AddExternalAsset(pathToAsset = assetPathFolder, parentAsset = repo1, assetName = None)
    val sutFolder = opFolder.metadata.get

    assert(sutFolder.getClass == classOf[AddExternalAssetMetadata])
    assert(sutFolder.asInstanceOf[AddExternalAssetMetadata].pathToAsset == "CalculatorRoot\\AdvancedCalculator")
    assert(sutFolder.asInstanceOf[AddExternalAssetMetadata].parent.getClass == classOf[InternalAssetMetadata])
    assert(sutFolder.asInstanceOf[AddExternalAssetMetadata].parent.asInstanceOf[InternalAssetMetadata].path.get == "Root1\\Repo1")
    assert(sutFolder.asInstanceOf[AddExternalAssetMetadata].assetName == None)
    assert(sutFolder.asInstanceOf[AddExternalAssetMetadata].subOperations.get.length == 0)

    val assetPathFile = Paths.get("CalculatorRoot\\AdvancedCalculator\\AdvancedCalculator.js")

    val fileParent = getassetbyname("MyNonExistentDrive\\Root1\\Repo1\\AdvancedCalculator", root1).get
    val opFile = AddExternalAsset(pathToAsset = assetPathFile, parentAsset = fileParent, assetName = None)
    val sutFile = opFile.metadata.get

    assert(sutFile.getClass == classOf[AddExternalAssetMetadata])
    assert(sutFile.asInstanceOf[AddExternalAssetMetadata].pathToAsset == "CalculatorRoot\\AdvancedCalculator\\AdvancedCalculator.js")
    assert(sutFile.asInstanceOf[AddExternalAssetMetadata].parent.getClass == classOf[InternalAssetMetadata])
    assert(sutFile.asInstanceOf[AddExternalAssetMetadata].parent.asInstanceOf[InternalAssetMetadata].path.get == "Root1\\Repo1\\AdvancedCalculator")
    assert(sutFile.asInstanceOf[AddExternalAssetMetadata].assetName == None)
    assert(sutFile.asInstanceOf[AddExternalAssetMetadata].subOperations.get.length == 0)
  }

  test("AddFeatureToFeatureModel Metadata Creation works (for parametrization 1") {
    val rf1 = new RootFeature("rf1")
    val fm1 = FeatureModel(new RootFeature("oldrf"), name = "MyTestingFM")

    val op = AddFeatureToFeatureModel(rf1, fm1,true,  true, true)
    val sut = op.metadata.get

    assert(sut.getClass == classOf[AddFeatureToFeatureModelMetadata])
    assert(sut.asInstanceOf[AddFeatureToFeatureModelMetadata].feature.getClass == classOf[ExternalFeatureMetadata])
    assert(sut.asInstanceOf[AddFeatureToFeatureModelMetadata].feature.asInstanceOf[ExternalFeatureMetadata].name == "rf1")
    assert(sut.asInstanceOf[AddFeatureToFeatureModelMetadata].feature.asInstanceOf[ExternalFeatureMetadata].isRootFeature)
    assert(sut.asInstanceOf[AddFeatureToFeatureModelMetadata].parent == None)
    assert(sut.asInstanceOf[AddFeatureToFeatureModelMetadata].featuremodel.get.getClass == classOf[ExternalFeatureModelMetadata])
    assert(sut.asInstanceOf[AddFeatureToFeatureModelMetadata].featuremodel.get.asInstanceOf[ExternalFeatureModelMetadata].rootFeature.name == "oldrf")
    assert(sut.asInstanceOf[AddFeatureToFeatureModelMetadata].featuremodel.get.asInstanceOf[ExternalFeatureModelMetadata].name == "MyTestingFM")
    assert(sut.subOperations == None)
  }

  test("AddFeatureToFeatureModel Metadata Creation works (for parametrization 2") {
    val f11 = Feature("f11")
    val f12 = Feature("f12")

    val op = AddFeatureToFeatureModel(f11, f12)
    val sut = op.metadata.get

    assert(sut.getClass == classOf[AddFeatureToFeatureModelMetadata])
    assert(sut.asInstanceOf[AddFeatureToFeatureModelMetadata].feature.getClass == classOf[ExternalFeatureMetadata])
    assert(sut.asInstanceOf[AddFeatureToFeatureModelMetadata].feature.asInstanceOf[ExternalFeatureMetadata].name == "f11")
    assert(sut.asInstanceOf[AddFeatureToFeatureModelMetadata].feature.asInstanceOf[ExternalFeatureMetadata].isRootFeature == false)
    assert(sut.asInstanceOf[AddFeatureToFeatureModelMetadata].parent.get.getClass == classOf[ExternalFeatureMetadata])
    assert(sut.asInstanceOf[AddFeatureToFeatureModelMetadata].parent.get.asInstanceOf[ExternalFeatureMetadata].name == "f12")
    assert(sut.asInstanceOf[AddFeatureToFeatureModelMetadata].parent.get.asInstanceOf[ExternalFeatureMetadata].isRootFeature == false)
    assert(sut.asInstanceOf[AddFeatureToFeatureModelMetadata].featuremodel == None)
    assert(sut.subOperations == None)
  }

  test("ChangeAsset Metadata Creation works") {
    val root1 = Asset("MyNonExistentDrive\\Root1", VPRootType)
    val repo1 = Asset("MyNonExistentDrive\\Root1\\Repo1", RepositoryType)
    val fold1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1", FolderType)
    val file1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1\\File1", FileType)
    val meth1 = Asset("foo", MethodType)
    val blck1 = Asset("block", BlockType)
    val blck2 = Asset("block", BlockType)
    val blck3 = Asset("block", BlockType)
    AddAsset(repo1, root1)
    AddAsset(fold1, repo1)
    AddAsset(file1, fold1)
    AddAsset(blck1, file1)
    AddAsset(meth1, file1)
    AddAsset(blck2, meth1)
    AddAsset(blck3, meth1)

    val op = ChangeAsset(fold1)
    val sut = op.metadata.get

    assert(sut.getClass == classOf[ChangeAssetMetadata])
    assert(sut.asInstanceOf[ChangeAssetMetadata].assetToChange.get.getClass == classOf[InternalAssetMetadata])
    assert(sut.asInstanceOf[ChangeAssetMetadata].assetToChange.get.asInstanceOf[InternalAssetMetadata].path.get == "Root1\\Repo1\\Fold1")
    assert(sut.asInstanceOf[ChangeAssetMetadata].assetToChange.get.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(sut.asInstanceOf[ChangeAssetMetadata].assetsToChange == None)
    assert(sut.subOperations.isEmpty)
  }

  test("ChangeAsset Metadata Creation works (in plural case)") {
    val root1 = Asset("MyNonExistentDrive\\Root1", VPRootType)
    val repo1 = Asset("MyNonExistentDrive\\Root1\\Repo1", RepositoryType)
    val fold1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1", FolderType)
    val file1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1\\File1", FileType)
    val meth1 = Asset("foo", MethodType)
    val blck1 = Asset("block", BlockType)
    val blck2 = Asset("block", BlockType, content = Some(List("block2")))
    val blck3 = Asset("block", BlockType, content = Some(List("block3")))
    AddAsset(repo1, root1)
    AddAsset(fold1, repo1)
    AddAsset(file1, fold1)
    AddAsset(blck1, file1)
    AddAsset(meth1, file1)
    AddAsset(blck2, meth1)
    AddAsset(blck3, meth1)

    val op = ChangeAsset(List(meth1, blck3),true, true)
    val sut = op.metadata.get

    assert(sut.getClass == classOf[ChangeAssetMetadata])
    assert(sut.asInstanceOf[ChangeAssetMetadata].assetToChange == None)
    assert(sut.asInstanceOf[ChangeAssetMetadata].assetsToChange.get.length == 2)
    assert(sut.asInstanceOf[ChangeAssetMetadata].assetsToChange.get(0).getClass == classOf[InternalAssetMetadata])
    assert(sut.asInstanceOf[ChangeAssetMetadata].assetsToChange.get(0).asInstanceOf[InternalAssetMetadata].path.get == "Root1\\Repo1\\Fold1\\File1")
    assert(sut.asInstanceOf[ChangeAssetMetadata].assetsToChange.get(0).asInstanceOf[InternalAssetMetadata].indexPath.get == List(1))
    assert(sut.asInstanceOf[ChangeAssetMetadata].assetsToChange.get(1).getClass == classOf[InternalAssetMetadata])
    assert(sut.asInstanceOf[ChangeAssetMetadata].assetsToChange.get(1).asInstanceOf[InternalAssetMetadata].path.get == "Root1\\Repo1\\Fold1\\File1")
    assert(sut.asInstanceOf[ChangeAssetMetadata].assetsToChange.get(1).asInstanceOf[InternalAssetMetadata].indexPath.get == List(1,1))
    assert(sut.subOperations.isEmpty)
  }

  test("MapAssetToFeature Metadata Creation works") {
    val root1 = Asset("MyNonExistentDrive\\Root1", VPRootType)
    val repo1 = Asset("MyNonExistentDrive\\Root1\\Repo1", RepositoryType)
    val fold1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1", FolderType)
    val file1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1\\File1", FileType)
    val meth1 = Asset("foo", MethodType)
    val blck1 = Asset("block", BlockType)
    val blck2 = Asset("block", BlockType)
    val blck3 = Asset("block", BlockType)
    AddAsset(repo1, root1)
    AddAsset(fold1, repo1)
    AddAsset(file1, fold1)
    AddAsset(blck1, file1)
    AddAsset(meth1, file1)
    AddAsset(blck2, meth1)
    AddAsset(blck3, meth1)

    val fm = FeatureModel(new RootFeature("rf1"))
    AddFeatureModelToAsset(asset = repo1, fm)
    val newFeat = Feature("newFeat")

    val op = MapAssetToFeature(file1, newFeat)
    val sut = op.metadata.get

    assert(sut.getClass == classOf[MapAssetToFeatureMetadata])
    assert(sut.asInstanceOf[MapAssetToFeatureMetadata].asset.getClass == classOf[InternalAssetMetadata])
    assert(sut.asInstanceOf[MapAssetToFeatureMetadata].asset.asInstanceOf[InternalAssetMetadata].path.get == "Root1\\Repo1\\Fold1\\File1")
    assert(sut.asInstanceOf[MapAssetToFeatureMetadata].asset.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(sut.asInstanceOf[MapAssetToFeatureMetadata].feat.getClass == classOf[ExternalFeatureMetadata])
    assert(sut.asInstanceOf[MapAssetToFeatureMetadata].feat.asInstanceOf[ExternalFeatureMetadata].name == "newFeat")
    assert(sut.subOperations.get.length == 1)
    assert(sut.subOperations.get(0).getClass == classOf[AddFeatureToFeatureModelMetadata])
  }

  test("MapAssetToFeature Metadata Creation works with pre-existing feature") {
    val root1 = Asset("MyNonExistentDrive\\Root1", VPRootType)
    val repo1 = Asset("MyNonExistentDrive\\Root1\\Repo1", RepositoryType)
    val fold1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1", FolderType)
    val file1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1\\File1", FileType)
    val meth1 = Asset("foo", MethodType)
    val blck1 = Asset("block", BlockType)
    val blck2 = Asset("block", BlockType)
    val blck3 = Asset("block", BlockType)
    AddAsset(repo1, root1)
    AddAsset(fold1, repo1)
    AddAsset(file1, fold1)
    AddAsset(blck1, file1)
    AddAsset(meth1, file1)
    AddAsset(blck2, meth1)
    AddAsset(blck3, meth1)

    val fm = FeatureModel(new RootFeature("rf1"))
    val feat = Feature("f11")
    AddFeatureModelToAsset(asset = repo1, fm)
    AddFeatureToFeatureModel(feat, fm.rootfeature)

    val op = MapAssetToFeature(file1, feat)
    val sut = op.metadata.get

    assert(sut.getClass == classOf[MapAssetToFeatureMetadata])
    assert(sut.asInstanceOf[MapAssetToFeatureMetadata].asset.getClass == classOf[InternalAssetMetadata])
    assert(sut.asInstanceOf[MapAssetToFeatureMetadata].asset.asInstanceOf[InternalAssetMetadata].path.get == "Root1\\Repo1\\Fold1\\File1")
    assert(sut.asInstanceOf[MapAssetToFeatureMetadata].asset.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(sut.asInstanceOf[MapAssetToFeatureMetadata].feat.getClass == classOf[InAssetFeatureMetadata])
    assert(sut.asInstanceOf[MapAssetToFeatureMetadata].feat.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(sut.asInstanceOf[MapAssetToFeatureMetadata].feat.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path.get == "Root1\\Repo1")
    assert(sut.asInstanceOf[MapAssetToFeatureMetadata].feat.asInstanceOf[InAssetFeatureMetadata].lpq == "f11")
    assert(sut.subOperations.get.length == 0)
  }

  test("RemoveAsset Metadata Creation works (singular case") {
    val root1 = Asset("MyNonExistentDrive\\Root1", VPRootType)
    val repo1 = Asset("MyNonExistentDrive\\Root1\\Repo1", RepositoryType)
    val fold1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1", FolderType)
    val file1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1\\File1", FileType)
    val meth1 = Asset("foo", MethodType)
    val blck1 = Asset("block", BlockType)
    val blck2 = Asset("block", BlockType)
    val blck3 = Asset("block", BlockType)
    AddAsset(repo1, root1)
    AddAsset(fold1, repo1)
    AddAsset(file1, fold1)
    AddAsset(blck1, file1)
    AddAsset(meth1, file1)
    AddAsset(blck2, meth1)
    AddAsset(blck3, meth1)

    val op = RemoveAsset(root1, file1)
    val sut = op.metadata.get

    assert(sut.getClass == classOf[RemoveAssetMetadata])
    assert(sut.asInstanceOf[RemoveAssetMetadata].ast.getClass == classOf[InternalAssetMetadata])
    assert(sut.asInstanceOf[RemoveAssetMetadata].ast.asInstanceOf[InternalAssetMetadata].path.get == "Root1")
    assert(sut.asInstanceOf[RemoveAssetMetadata].assetToDelete.get.getClass == classOf[InternalAssetMetadata])
    assert(sut.asInstanceOf[RemoveAssetMetadata].assetToDelete.get.asInstanceOf[InternalAssetMetadata].path.get == "Root1\\Repo1\\Fold1\\File1")
    assert(sut.asInstanceOf[RemoveAssetMetadata].assetToDelete.get.asInstanceOf[InternalAssetMetadata].indexPath == None)
    assert(sut.asInstanceOf[RemoveAssetMetadata].assetsToDelete == None)
    assert(sut.subOperations == None)
  }

  test("RemoveAsset Metadata Creation works (plural case") {
    val root1 = Asset("MyNonExistentDrive\\Root1", VPRootType)
    val repo1 = Asset("MyNonExistentDrive\\Root1\\Repo1", RepositoryType)
    val fold1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1", FolderType)
    val file1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1\\File1", FileType)
    val meth1 = Asset("foo", MethodType)
    val blck1 = Asset("block", BlockType)
    val blck2 = Asset("block", BlockType)
    val blck3 = Asset("block", BlockType)
    AddAsset(repo1, root1)
    AddAsset(fold1, repo1)
    AddAsset(file1, fold1)
    AddAsset(blck1, file1)
    AddAsset(meth1, file1)
    AddAsset(blck2, meth1)
    AddAsset(blck3, meth1)

    val op = RemoveAsset(root1, List(blck1, blck2), true, true)
    val sut = op.metadata.get

    assert(sut.getClass == classOf[RemoveAssetMetadata])
    assert(sut.asInstanceOf[RemoveAssetMetadata].ast.getClass == classOf[InternalAssetMetadata])
    assert(sut.asInstanceOf[RemoveAssetMetadata].ast.asInstanceOf[InternalAssetMetadata].path.get == "Root1")
    assert(sut.asInstanceOf[RemoveAssetMetadata].assetToDelete == None)
    assert(sut.asInstanceOf[RemoveAssetMetadata].assetsToDelete.get.length == 2)
    assert(sut.asInstanceOf[RemoveAssetMetadata].assetsToDelete.get(0).getClass == classOf[InternalAssetMetadata])
    assert(sut.asInstanceOf[RemoveAssetMetadata].assetsToDelete.get(0).asInstanceOf[InternalAssetMetadata].path.get == "Root1\\Repo1\\Fold1\\File1")
    assert(sut.asInstanceOf[RemoveAssetMetadata].assetsToDelete.get(0).asInstanceOf[InternalAssetMetadata].indexPath.get == List(0))
    assert(sut.asInstanceOf[RemoveAssetMetadata].assetsToDelete.get(1).getClass == classOf[InternalAssetMetadata])
    assert(sut.asInstanceOf[RemoveAssetMetadata].assetsToDelete.get(1).asInstanceOf[InternalAssetMetadata].path.get == "Root1\\Repo1\\Fold1\\File1")
    assert(sut.asInstanceOf[RemoveAssetMetadata].assetsToDelete.get(1).asInstanceOf[InternalAssetMetadata].indexPath.get == List(1,0))
    assert(sut.subOperations == None)
  }

  test("RemoveFeature Metadata Creation works (w/o asset mappings)") {
    val root1 = Asset("MyNonExistentDrive\\Root1", VPRootType)
    val repo1 = Asset("MyNonExistentDrive\\Root1\\Repo1", RepositoryType)
    val fold1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1", FolderType)
    val file1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1\\File1", FileType)
    val meth1 = Asset("foo", MethodType)
    val blck1 = Asset("block", BlockType)
    val blck2 = Asset("block", BlockType)
    val blck3 = Asset("block", BlockType)
    AddAsset(repo1, root1)
    AddAsset(fold1, repo1)
    AddAsset(file1, fold1)
    AddAsset(blck1, file1)
    AddAsset(meth1, file1)
    AddAsset(blck2, meth1)
    AddAsset(blck3, meth1)

    val fm = FeatureModel(new RootFeature("rf1"))
    val f11 = Feature("f11")
    val f12 = Feature("f12")
    val f13 = Feature("f13")
    AddFeatureModelToAsset(asset = repo1, fm)
    AddFeatureToFeatureModel(f11, fm.rootfeature)
    AddFeatureToFeatureModel(f12, fm.rootfeature)
    AddFeatureToFeatureModel(f13, f11)

    val originalAssetPath = computeAssetPath(repo1)
    val originalFeaturePath = computeFeaturePath(f11)
    val featurePath = se.gu.vp.model.Path(root1, originalAssetPath ++ originalFeaturePath)

    val op = RemoveFeature(featurePath)
    val sut = op.metadata.get

    assert(sut.getClass == classOf[RemoveFeatureMetadata])
    assert(sut.asInstanceOf[RemoveFeatureMetadata].feature.getClass == classOf[InAssetFeatureMetadata])
    assert(sut.asInstanceOf[RemoveFeatureMetadata].feature.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(sut.asInstanceOf[RemoveFeatureMetadata].feature.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path.get == "Root1\\Repo1")
    assert(sut.asInstanceOf[RemoveFeatureMetadata].feature.asInstanceOf[InAssetFeatureMetadata].lpq == "f11")
    assert(sut.subOperations.get.length == 0)
  }

  test("RemoveFeature Metadata Creation works (with asset mappings)") {
    val root1 = Asset("MyNonExistentDrive\\Root1", VPRootType)
    val repo1 = Asset("MyNonExistentDrive\\Root1\\Repo1", RepositoryType)
    val fold1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1", FolderType)
    val file1 = Asset("MyNonExistentDrive\\Root1\\Repo1\\Fold1\\File1", FileType)
    val meth1 = Asset("foo", MethodType)
    val blck1 = Asset("block", BlockType)
    val blck2 = Asset("block", BlockType)
    val blck3 = Asset("block", BlockType)
    AddAsset(repo1, root1)
    AddAsset(fold1, repo1)
    AddAsset(file1, fold1)
    AddAsset(blck1, file1)
    AddAsset(meth1, file1)
    AddAsset(blck2, meth1)
    AddAsset(blck3, meth1)

    val fm = FeatureModel(new RootFeature("rf1"))
    val f11 = Feature("f11")
    val f12 = Feature("f12")
    val f13 = Feature("f13")
    AddFeatureModelToAsset(asset = repo1, fm)
    AddFeatureToFeatureModel(f11, fm.rootfeature)
    AddFeatureToFeatureModel(f12, fm.rootfeature)
    AddFeatureToFeatureModel(f13, f11)

    MapAssetToFeature(meth1, f12)

    val originalAssetPath = computeAssetPath(repo1)
    val originalFeaturePath = computeFeaturePath(f12)
    val featurePath = se.gu.vp.model.Path(root1, originalAssetPath ++ originalFeaturePath)

    val op = RemoveFeature(featurePath)
    val sut = op.metadata.get

    assert(sut.getClass == classOf[RemoveFeatureMetadata])
    assert(sut.asInstanceOf[RemoveFeatureMetadata].feature.getClass == classOf[InAssetFeatureMetadata])
    assert(sut.asInstanceOf[RemoveFeatureMetadata].feature.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.getClass == classOf[InternalAssetMetadata])
    assert(sut.asInstanceOf[RemoveFeatureMetadata].feature.asInstanceOf[InAssetFeatureMetadata].featureModelAsset.asInstanceOf[InternalAssetMetadata].path.get == "Root1\\Repo1")
    assert(sut.asInstanceOf[RemoveFeatureMetadata].feature.asInstanceOf[InAssetFeatureMetadata].lpq == "f12")
    assert(sut.subOperations.get.length == 1)
    assert(sut.subOperations.get(0).getClass == classOf[RemoveAssetMetadata])
    assert(sut.subOperations.get(0).asInstanceOf[RemoveAssetMetadata].assetToDelete.get.getClass == classOf[InternalAssetMetadata])
    assert(sut.subOperations.get(0).asInstanceOf[RemoveAssetMetadata].assetToDelete.get.asInstanceOf[InternalAssetMetadata].path.get == "Root1\\Repo1\\Fold1\\File1")
    assert(sut.subOperations.get(0).asInstanceOf[RemoveAssetMetadata].assetToDelete.get.asInstanceOf[InternalAssetMetadata].indexPath.get == List(1))
  }
}
