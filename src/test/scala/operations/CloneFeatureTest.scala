package operations

import org.scalatest.FunSuite
import se.gu.vp.model._
import se.gu.vp.operations.FeatureCloneHelpers.deepCloneFeature
import se.gu.vp.operations.Utilities._
import se.gu.vp.operations._

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */
class CloneFeatureTest extends FunSuite {

  /*test("Regular test") {
    val feature1 = new RootFeature("feature1")
    val fmleft = new FeatureModel(feature1)
    val feature2 = Feature("feature2")
    AddFeatureToFeatureModel(feature1, fmleft, true, true, true)
    AddFeatureToFeatureModel(feature2, feature1)

    val fmright = new FeatureModel(new RootFeature("fmright"))

    val root = Asset("CalculatorRootJava", VPRootType)
    var repo1 = Asset("Repository 1", RepositoryType)
    val repo2 = Asset("Repository 2", RepositoryType)
    val folder1 = Asset("Folder 1", FolderType)
    val file1 = Asset("File 1", FileType)
    val file2 = Asset("File2", FileType)
    val file3 = Asset("File3", FileType)
    val class1 = Asset("Class1", ClassType)
    val class2 = Asset("Class2", ClassType)

    AddFeatureModelToAsset(repo1, fmleft)
    AddFeatureModelToAsset(repo2, fmright)

    AddAsset(repo1, root)
    AddAsset(repo2, root)
    AddAsset(folder1, repo1)
    AddAsset(file1, folder1)
    AddAsset(file2, folder1)
    AddAsset(file3, folder1)
    AddAsset(class1, file1)
    AddAsset(class2, file2)

    MapAssetToFeature(class1, feature1)
    MapAssetToFeature(file3, feature2)

    val sourcepath = Path(root, computeAssetPath(repo1) ++ computeFeaturePath(feature1))
    val targetpath = Path(root, computeAssetPath(repo2))

    CloneFeature(sourcepath, targetpath)

    assert(featureClonedInFM(feature1, fmright) == true)
    assert(featureClonedInFM(feature2, fmright) == true)
  }*/
  test("Regular test") {
    // Retrieve the base path from the configuration
    val basePath = Configuration.basePath

    // Setup of feature models and features
    val feature1 = new RootFeature("feature1")
    val fmleft = new FeatureModel(feature1)
    val feature2 = Feature("feature2")
    AddFeatureToFeatureModel(feature1, fmleft, true, true, true)
    AddFeatureToFeatureModel(feature2, feature1)

    val fmright = new FeatureModel(new RootFeature("fmright"))

    // Create assets with basePath to ensure they are stored outside the project directory
    val root = Asset(s"$basePath/CalculatorRootJava", VPRootType)
    var repo1 = Asset(s"$basePath/Repository1", RepositoryType)
    val repo2 = Asset(s"$basePath/Repository2", RepositoryType)
    val folder1 = Asset(s"$basePath/Folder1", FolderType)
    val file1 = Asset(s"$basePath/File1", FileType)
    val file2 = Asset(s"$basePath/File2", FileType)
    val file3 = Asset(s"$basePath/File3", FileType)
    val class1 = Asset(s"$basePath/Class1", ClassType)
    val class2 = Asset(s"$basePath/Class2", ClassType)

    // Add feature models to assets and establish the asset hierarchy
    AddFeatureModelToAsset(repo1, fmleft)
    AddFeatureModelToAsset(repo2, fmright)

    AddAsset(repo1, root)
    AddAsset(repo2, root)
    AddAsset(folder1, repo1)
    AddAsset(file1, folder1)
    AddAsset(file2, folder1)
    AddAsset(file3, folder1)
    AddAsset(class1, file1)
    AddAsset(class2, file2)

    // Map assets to features
    MapAssetToFeature(class1, feature1)
    MapAssetToFeature(file3, feature2)

    // Define paths for cloning operation
    val sourcepath = Path(root, computeAssetPath(repo1) ++ computeFeaturePath(feature1))
    val targetpath = Path(root, computeAssetPath(repo2))

    // Perform the cloning operation
    CloneFeature(sourcepath, targetpath)

    // Assertions to verify the cloning was successful
    assert(featureClonedInFM(feature1, fmright) == true)
    assert(featureClonedInFM(feature2, fmright) == true)
  }

  test("Deep clone feature"){
    val feature1 = new RootFeature("feature1")
    val fmleft = new FeatureModel(feature1)
    val feature2 = Feature("feature2")
    val feature3 = Feature("Feature3")
    val feature4 = Feature("Feature4")
    val feature5 = Feature("Feature5")
    val feature6 = Feature("Feature6")
    val feature7 = Feature("Feature7")
    AddFeatureToFeatureModel(feature1,fmleft, true, true, true)
    AddFeatureToFeatureModel(feature2,feature1)
    AddFeatureToFeatureModel(feature3,feature2)
    AddFeatureToFeatureModel(feature4,feature2)
    AddFeatureToFeatureModel(feature5,feature3)
    AddFeatureToFeatureModel(feature6,feature4)
    AddFeatureToFeatureModel(feature7,feature6)

    val clone = deepCloneFeature(feature1)
    println(clone)
  }
}
