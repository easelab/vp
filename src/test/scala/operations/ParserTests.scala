package operations

import org.scalatest.FunSuite
import se.gu.vp.model._
import se.gu.vp.operations.JavaParser.JavaParserMain.{parseJavaFile, readInJavaAssetTree}
import se.gu.vp.operations.{AddAsset, AddFeatureModelToAsset, AddFeatureToFeatureModel}

import scala.util.matching.Regex

/**
Copyright [2019] [Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class ParserTests extends FunSuite{

   /* test("Java Parser test"){
      val file = new File("/Users/raz/Documents/University/RA Job/Wardah/VP Clean Clone/vp/CalculatorRootJava/Basic/Arithmetic.java")
      val source = io.Source.fromFile(file.getAbsolutePath, "ISO-8859-1")
      val filelines = source.getLines.toList
      source.close

      var fileAsset = Asset("a",FileType)
      parseJavaFile(file,null)
      println(fileAsset.assetType)
    }*/
/*
  test("Java Parser test"){
    // Correctly define the path to the file. Adjust the relative path as necessary.
    val relativePath = "../../../../../../CalculatorRootJava/Basic/Arithmetic.java"

    // Create a File object directly with the relative path. You don't need to call getAbsolutePath
    // since fromFile can handle File objects directly, and the File class can resolve relative paths.
    val file = new java.io.File(relativePath)

    // Use the file object directly without calling getAbsolutePath, because io.Source.fromFile
    // can accept a File object.
    val source = scala.io.Source.fromFile(file, "ISO-8859-1")
    val filelines = source.getLines.toList
    source.close()

    var fileAsset = Asset("a", FileType)
    // Pass the file object directly to parseJavaFile. Ensure that this method is expecting a File object.
    parseJavaFile(file, null)
    println(fileAsset.assetType)
  }*/

  test("Java Parser test"){
    // Construct the relative path from the current file location to Arithmetic.java
    val relativePath = "/Users/raz/Documents/University/RA Job/Wardah/VP Clean Clone/vp/CalculatorRootJava/Basic/Arithmetic.java"

    // Create the file object using the relative path
    val file = new java.io.File(relativePath)

    // Check if the file exists to prevent FileNotFoundException
    if (file.exists()) {
      val source = scala.io.Source.fromFile(file, "ISO-8859-1")
      val filelines = source.getLines.toList
      source.close()

      var fileAsset = Asset("a", FileType)
      parseJavaFile(file, null)
      println(fileAsset.assetType)
    } else {
      println(s"File not found: ${file.getCanonicalPath}")
    }
  }

  /*test("Test by Christoph"){
    val feature = new RootFeature("/Users/raz/Documents/University/RA Job/Wardah/VP Clean Clone/vp/CalculatorRootJava/Basic/Basic.java")
    var featureModel = new FeatureModel(feature)

    val asset = readInJavaAssetTree("/Users/raz/Documents/University/RA Job/Wardah/VP Clean Clone/vp/CalculatorRootJava/Basic")
    //SerialiseAssetTree(asset.get, "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\temp")
    val a = 1
  }*/
  test("Test by Christoph"){
    // Construct the relative path from the current file location to Basic.java
    val relativePathToFile = "/Users/raz/Documents/University/RA Job/Wardah/VP Clean Clone/vp/CalculatorRootJava/Basic/Basic.java"
    val relativePathToDir = "/Users/raz/Documents/University/RA Job/Wardah/VP Clean Clone/vp/CalculatorRootJava/Basic"

    // Create the RootFeature object using the relative path to Basic.java
    val feature = new RootFeature(relativePathToFile)
    var featureModel = new FeatureModel(feature)

    // Read in the Java asset tree from the Basic directory
    val asset = readInJavaAssetTree(relativePathToDir)
    //SerialiseAssetTree(asset.get, "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\temp")
    val a = 1
  }


  /*test("JsonFileParser.Scala test"){
    val feature1 = new RootFeature("timeout")
    val feature2 = Feature("inactivityTimeout")
    val feature3 = Feature("pingTimeout")

    val FM = new FeatureModel(feature1)
    AddFeatureToFeatureModel(feature2,FM, true, true, true)
    AddFeatureToFeatureModel(feature3,FM, true, true, true)

    val astroot = new Asset("/Users/raz/Documents/University/RA Job/Wardah/VP Clean Clone/vp/CalculatorRootJava/Basic/Arithmetic.java", VPRootType)
    val file = new File("/Users/raz/Documents/University/RA Job/Wardah/VP Clean Clone/vp/config.json")
    val repo = new Asset("Repo",RepositoryType)
    val fileasset = new Asset(file.getAbsolutePath,FileType)
    AddAsset(repo,astroot)
    AddAsset(fileasset,repo)
    AddFeatureModelToAsset(repo,FM)
  }*/

  /*test("JsonFileParser.Scala test"){
    val feature1 = new RootFeature("timeout")
    val feature2 = new RootFeature("inactivityTimeout")
    val feature3 = new RootFeature("pingTimeout")

    val FM = new FeatureModel(feature1)
    AddFeatureToFeatureModel(feature2, FM, true, true, true)
    AddFeatureToFeatureModel(feature3, FM, true, true, true)

    // Relative path from the current file location to Arithmetic.java
    val relativePathToArithmetic = "/Users/raz/Documents/University/RA Job/Wardah/VP Clean Clone/vp/CalculatorRootJava/Basic/Arithmetic.java"
    // Relative path from the current file location to config.json
    val relativePathToConfig = "/Users/raz/Documents/University/RA Job/Wardah/VP Clean Clone/vp/config.json"

    // Create the Asset object using the relative path to Arithmetic.java
    val astroot = new Asset(relativePathToArithmetic, VPRootType)
    // Create the File object using the relative path to config.json
    val file = new java.io.File(relativePathToConfig)
    val repo = new Asset("Repo", RepositoryType)

    // Check if the file exists to prevent FileNotFoundException
    if (file.exists()) {
      val fileasset = new Asset(file.getAbsolutePath, FileType)
      AddAsset(repo, astroot)
      AddAsset(fileasset, repo)
      AddFeatureModelToAsset(repo, FM)
    } else {
      println(s"File not found: ${file.getCanonicalPath}")
    }
  }*/

  test("JsonFileParser.Scala test") {
    // Assuming basePath is correctly defined to include or point to the vp directory
    val basePath = Configuration.basePath  // basePath should end with "/vp" if vp is the directory where Repo is located

    val feature1 = new RootFeature("timeout")
    val feature2 = new RootFeature("inactivityTimeout")
    val feature3 = new RootFeature("pingTimeout")

    val FM = new FeatureModel(feature1)
    AddFeatureToFeatureModel(feature2, FM, true, true, true)
    AddFeatureToFeatureModel(feature3, FM, true, true, true)

    // Construct paths by using basePath, ensuring it points to the correct location within the vp directory
    val pathToArithmetic = s"$basePath/CalculatorRootJava/Basic/Arithmetic.java"
    val pathToConfig = s"$basePath/config.json"

    // Create Asset objects using paths derived from basePath
    val astroot = new Asset(pathToArithmetic, VPRootType)
    val file = new java.io.File(pathToConfig)
    val repo = new Asset(s"$basePath/Repo", RepositoryType)  // Ensure Repo path is derived from basePath

    // Check if the config file exists to prevent FileNotFoundException
    if (file.exists()) {
      val fileasset = new Asset(file.getAbsolutePath, FileType)
      AddAsset(repo, astroot)
      AddAsset(fileasset, repo)
      AddFeatureModelToAsset(repo, FM)
    } else {
      println(s"File not found: ${file.getCanonicalPath}")
    }
  }



  test("RegexTester"){
    val beginannotation = new Regex("//\\s*[&$]\\s*[Bb]egin")
    val endannotation = new Regex("//\\s*[&$]\\s*[Ee]nd")
    val str1 = "//&begin hello"
    val str2 = "//$begin hi"
    val str3 = "// &begin jsdi"
    val str4 = "// $ begin"
    val str5 = "// * begin"

    println(beginannotation.findFirstIn(str1))
    println(beginannotation.findFirstIn(str2))
    println(beginannotation.findFirstIn(str3))
    println(beginannotation.findFirstIn(str4))
    println(beginannotation.findFirstIn(str5))
  }


}
