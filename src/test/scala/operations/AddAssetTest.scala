package operations

import org.scalatest.FunSuite
import se.gu.vp.model._
import se.gu.vp.operations.CalculatorSimulation.ASTModificationHelpers
import se.gu.vp.operations.JavaParser.JavaParserMain
import se.gu.vp.operations.Utilities._
import se.gu.vp.operations._
import vp.operations.AddExternalAsset

import java.io.File
import java.nio.file.Paths

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class AddAssetTest extends FunSuite {
  val basePath = Configuration.basePath
  test("Simple Addition") {
    println(" --------------------------------- Test Case 1 : Simple Addition --------------------------------------")
    println("Creating assets, root and repository ... ")
    val root = Asset(s"$basePath/CalculatorRootJava", VPRootType)
    val repo = Repository(s"$basePath/Repository")
    println("Adding Repository in Root")
    root.prettyprint
    AddAsset(repo,root)
    root.prettyprint
    println("Asserting that root contains repository ... ")
    print(astContainsAsset(repo,root))
    assert(astContainsAsset(repo,root)==true)
    assert(root.children.length == 1)
  }

  /*test("Addition with missing features") {
    println(" ---------------------- Test Case 2 : Addition with Missing Features -----------------------------")

    println("Creating assets, root and repository ... ")
    val root = Asset("CalculatorRootJava", VPRootType)
    val repo = Repository("Repository")
    println("Adding Repository in Root")
    val fm = new FeatureModel(new RootFeature("Blank"))
    println("Adding feature model to root")
    AddFeatureModelToAsset(root,fm)
    println("Mapping Feature A and B to repository")
    repo.presenceCondition = Feature("A") | Feature("B")
    println("Adding Repository in Root")
    AddAsset(repo,root)
    print("Asserting that root contains repository ... ")
    assert(astContainsAsset(repo,root)==true)
    println("Checking that root contains repository : " + astContainsAsset(repo,root))
    print("Checking that feature model contains the new features : ")
    assert(root.children.length == 1)
    assert(fm.rootfeature.UNASSIGNED.subfeatures.length==2)
    println(assert(root.children.length == 1))
  }*/

  test("Addition with Missing Features") {
    println(" ---------------------- Test Case 2 : Addition with Missing Features -----------------------------")

    // Use basePath for assets to ensure they are located outside the project directory
    println("Creating assets, root and repository ... ")
    val basePath = Configuration.basePath // Ensure this is defined in your configuration file or context
    val root = Asset(s"$basePath/CalculatorRootJava", VPRootType)
    val repo = Repository(s"$basePath/Repository")
    println("Adding Repository in Root")

    // Create a feature model and add it to the asset
    val fm = new FeatureModel(new RootFeature("Blank"))
    println("Adding feature model to root")
    AddFeatureModelToAsset(root, fm)

    // Set the presence condition for the repository
    println("Mapping Feature A and B to repository")
    repo.presenceCondition = Feature("A") | Feature("B")

    // Add the repository to the root asset
    println("Adding Repository in Root")
    AddAsset(repo, root)

    // Assert to check if the root contains the repository
    print("Asserting that root contains repository ... ")
    assert(astContainsAsset(repo, root) == true)
    println("Checking that root contains repository : " + astContainsAsset(repo, root))

    // Additional asserts to check the structure and content of the feature model and root
    print("Checking that feature model contains the new features : ")
    assert(root.children.length == 1)
    assert(fm.rootfeature.UNASSIGNED.subfeatures.length == 2)
    println(assert(root.children.length == 1))
  }

  /*test("Addition with present features") {
    val root = Asset("CalculatorRootJava",VPRootType)
    val repo = Repository("Repository")
    val fm = new FeatureModel(new RootFeature("Blank"))
    AddFeatureModelToAsset(root,fm)

    val feature=new RootFeature("Feature A")
    AddFeatureToFeatureModel(feature, fm, true, true, true)

    AddAsset(repo,root)
    MapAssetToFeature(repo,feature)
    assert(astContainsAsset(repo,root)==true)
    assert(root.children.length == 1)
    assert(featureModelContainsFeature(feature,fm)==true)
  }*/
  test("Addition with present features") {
    // Retrieve the base path from your configuration
    val basePath = Configuration.basePath

    // Create assets, root, and repository with basePath
    val root = Asset(s"$basePath/CalculatorRootJava", VPRootType)
    val repo = Repository(s"$basePath/Repository")

    // Create and add a feature model to the root
    val fm = new FeatureModel(new RootFeature("Blank"))
    AddFeatureModelToAsset(root, fm)

    // Create a feature and add it to the feature model
    val feature = new RootFeature("Feature A")
    AddFeatureToFeatureModel(feature, fm, true, true, true)

    // Add the repository to the root and map it to the feature
    AddAsset(repo, root)
    MapAssetToFeature(repo, feature)

    // Assertions to validate the structure and relationships
    assert(astContainsAsset(repo, root) == true, "Repo should be contained within root")
    assert(root.children.length == 1, "Root should have one child")
    assert(featureModelContainsFeature(feature, fm) == true, "Feature model should contain the feature")
  }

 /* test("Test by Christoph"){
    val root = Asset("CalculatorRootJava",VPRootType)
    val repo = Asset("Repository",RepositoryType)
    val folder = Asset("Folder",FolderType)
    val file = Asset("File",FileType)
    val method = Asset("Method",MethodType)

    val op1 = AddAsset(repo,root)
    val op2 = AddAsset(folder,repo)
    val test = AddAsset(file,folder)
    val AddAsset(a,p,i) = test
    val op3 = AddAsset(method,file)

    root.prettyprint

    val f1 = new RootFeature("f1")
    val fm = new FeatureModel(f1)
    val f2 = new Feature("f2")
    val f22 = new Feature("f2")
    val f3 = new Feature("f3")
    val f4 = new Feature("f4")
    val f5 = new RootFeature("f5")
    val fm2 = new FeatureModel(f5)

    AddFeatureToFeatureModel(f1, fm,versioning = true,runImmediately = true, createMetadata = false)
    AddFeatureToFeatureModel(f5, fm2, true, runImmediately = true, createMetadata = false)
    AddFeatureToFeatureModel(f3,f1, createMetadata = false)
    AddFeatureToFeatureModel(f2,f3, createMetadata = false)
    AddFeatureToFeatureModel(f4,f2, createMetadata = false)

    MapAssetToFeature(file,f2)

    // This block tests what happens, if two features in the same hierarchy mapped to the same asset are added to the same parent root via AddAsset
    // => both features are added to RootFeature.unassigned, but f8 still has the childreference to f9 (whose parent is not unassigned though)
    AddFeatureModelToAsset(repo, fm)
    val block = Asset("block", BlockType)
    val f8 = new Feature("f8")
    val f9 = new Feature("f9")
    AddFeatureToFeatureModel(f9, f8)
    block.presenceCondition = f9 | f8 | block.presenceCondition
    val hierarchyFeatAssetAddOp = AddAsset(block, method)

    val parenttest = new Asset("Folder", FolderType)
    JavaParserMain.readInFileTree(new File("/Users/raz/Documents/University/RA Job/Wardah/VP Clean Clone/vp/CalculatorRootJava/Advanced"),parenttest)
    ASTModificationHelpers.handleFeatureModel(new File("/Users/raz/Documents/University/RA Job/Wardah/VP Clean Clone/vp/CalculatorRootJava/Advanced/featuremodel.txt"),parenttest)
    AddExternalAsset(Paths.get("/Users/raz/Documents/University/RA Job/Wardah/VP Clean Clone/vp/CalculatorRootJava/Advanced/Advanced.java"), parentAsset = parenttest)

    println(f2 == f22)
    println(f2.equals(f22))
    println(f2.eq(f22))

    fm.prettyprint()
    fm2.prettyprint()
  }*/
  /*WORKS
  test("Test by Christoph - Debugging Feature Model Association") {
    val basePath = Configuration.basePath + "/Dummy"
    val root = Asset(s"$basePath/CalculatorRootJava", VPRootType)
    val repo = Asset(s"$basePath/Repository", RepositoryType)
    val folder = Asset(s"$basePath/Folder", FolderType)
    val file = Asset(s"$basePath/File", FileType)
    val method = Asset(s"$basePath/Method", MethodType)

    // Build asset hierarchy
    AddAsset(repo, root)
    AddAsset(folder, repo)
    AddAsset(file, folder)
    AddAsset(method, file)

    // Initialize feature model and root feature
    val f1 = new RootFeature("f1")
    val fm = new FeatureModel(f1)
    AddFeatureModelToAsset(repo, fm)

    // Define and add features
    val f2 = new Feature("f2") // Correctly defined before use
    val f3 = new Feature("f3")
    val f4 = new Feature("f4")
    // Add features to the feature model
    AddFeatureToFeatureModel(f1, fm, versioning = true, runImmediately = true, createMetadata = false)
    AddFeatureToFeatureModel(f2, f1, createMetadata = false) // Adding f2 to f1


    // Attempt to map asset to feature
    println("Attempting to map 'file' asset to 'f2' feature...")
    MapAssetToFeature(file, f2) // Now f2 is correctly defined before this point
  }*/

  // Example ScalaTest test case
  test("Test by Christoph - Adjusted for Dummy in basePath") {
    // Initial setup of assets
    val basePath = Configuration.basePath
    val root = Asset(s"$basePath/CalculatorRootJava", VPRootType)
    val repo = Asset(s"$basePath/Repository", RepositoryType)
    val folder = Asset(s"$basePath/Folder", FolderType)
    val file = Asset(s"$basePath/File", FileType)
    val method = Asset(s"$basePath/Method", MethodType)

    // Building the asset hierarchy
    AddAsset(repo, root)
    AddAsset(folder, repo)
    AddAsset(file, folder)
    AddAsset(method, file)

    // Initialize feature model and features
    val f1 = new RootFeature("f1")
    val fm = new FeatureModel(f1)
    val f2 = new Feature("f2")
    val f22 = new Feature("f2") // Note: Having two features with the same identifier might be problematic unless handled explicitly in your system
    val f3 = new Feature("f3")
    val f4 = new Feature("f4")
    val f5 = new RootFeature("f5")
    val fm2 = new FeatureModel(f5)

    // Populate the feature models
    AddFeatureToFeatureModel(f1, fm, versioning = true, runImmediately = true, createMetadata = false)
    AddFeatureToFeatureModel(f5, fm2, true, runImmediately = true, createMetadata = false)
    AddFeatureToFeatureModel(f3, f1, createMetadata = false)
    AddFeatureToFeatureModel(f2, f3, createMetadata = false)
    AddFeatureToFeatureModel(f4, f2, createMetadata = false)

    // Associate the feature model with the repo asset
    AddFeatureModelToAsset(repo, fm)

    // Verify feature model association
    println(s"Feature model associated with repo: ${repo.featureModel}")

    // Map assets to features after ensuring the feature model association
    MapAssetToFeature(file, f2)

    // Additional operations for feature models, assets, and mappings
    val block = Asset(s"$basePath/block", BlockType)
    val f8 = new Feature("f8")
    val f9 = new Feature("f9")
    AddFeatureToFeatureModel(f9, f8)
    block.presenceCondition = f9 | f8 | block.presenceCondition
    AddAsset(block, method)

    // Handling file tree and feature model for a specific directory
    val parentTest = new Asset(s"$basePath/Folder", FolderType)
    JavaParserMain.readInFileTree(new File(s"$basePath/Advanced"), parentTest)
    ASTModificationHelpers.handleFeatureModel(new File(s"$basePath/Advanced/featuremodel.txt"), parentTest)
    AddExternalAsset(Paths.get(s"$basePath/Advanced/Advanced.java"), parentAsset = parentTest)

    // Equality checks for features
    println(f2 == f22) // Expected: false, unless overridden to consider identifiers only
    println(f2.equals(f22)) // As above
    println(f2.eq(f22)) // Always false, as eq checks for reference equality

    // Pretty print the feature models to verify their structure
    fm.prettyprint()
    fm2.prettyprint()
  }


  /*test("Test from Scenarios: AddAssetScenario"){

      println(" --------------------- ADD ASSET --------------------------")
      //Test scenario
      val rootfeature1 = new RootFeature("A")
      val fm = new FeatureModel(rootfeature1)
      val rootfeature2 = new RootFeature("B")
      val rootfeature3 = new RootFeature("C")
      AddFeatureToFeatureModel(rootfeature1, fm, true, true, true)
      AddFeatureToFeatureModel(rootfeature2, fm, true, true, true)
      AddFeatureToFeatureModel(rootfeature3, fm, true, true, true)


      val root = Asset("CalculatorRootJava", VPRootType)
      val repo1 = Asset("repo 1", RepositoryType)
      val folder = Asset("folder", FolderType)
      val file = Asset("file", FileType)
      val classvp = Asset("class", ClassType)

      println("Here")
      AddFeatureModelToAsset(root, fm)

      repo1.presenceCondition = (rootfeature1 & rootfeature2) | !Feature("W")
      folder.presenceCondition = (rootfeature2 | !Feature("P"))

      AddAsset(repo1, root)
      AddAsset(folder, repo1)
      AddAsset(file, folder)
      AddAsset(classvp, file)
      root.prettyprint
      val mappedassets = rootfeature2.mappedAssets(root)
      println(mappedassets)
      println(isAncestor(file,folder))
      CloneAsset(file,folder)
      //CloneAsset(file,folder)
      println(Utilities.findClones(file))
      root.prettyprint
      println(Utilities.getAncestors(classvp))
      val a = getAncestors(file)
      println(a)

  }*/
  test("Test from Scenarios: AddAssetScenario") {
    println(" --------------------- ADD ASSET --------------------------")

    // Retrieve the base path from your configuration
    val basePath = Configuration.basePath

    // Test scenario setup with feature model
    val rootfeature1 = new RootFeature("A")
    val fm = new FeatureModel(rootfeature1)
    val rootfeature2 = new RootFeature("B")
    val rootfeature3 = new RootFeature("C")
    AddFeatureToFeatureModel(rootfeature1, fm, true, true, true)
    AddFeatureToFeatureModel(rootfeature2, fm, true, true, true)
    AddFeatureToFeatureModel(rootfeature3, fm, true, true, true)

    // Create assets with basePath to organize outside the project directory
    val root = Asset(s"$basePath/CalculatorRootJava", VPRootType)
    val repo1 = Asset(s"$basePath/repo1", RepositoryType)
    val folder = Asset(s"$basePath/folder", FolderType)
    val file = Asset(s"$basePath/file", FileType)
    val classvp = Asset(s"$basePath/class", ClassType)

    println("Here")
    AddFeatureModelToAsset(root, fm)

    // Define presence conditions for assets
    repo1.presenceCondition = (rootfeature1 & rootfeature2) | !Feature("W")
    folder.presenceCondition = (rootfeature2 | !Feature("P"))

    // Add assets to their respective parents to form the asset hierarchy
    AddAsset(repo1, root)
    AddAsset(folder, repo1)
    AddAsset(file, folder)
    AddAsset(classvp, file)

    // Print the asset hierarchy
    root.prettyprint

    // Operations involving feature mapping and cloning
    val mappedassets = rootfeature2.mappedAssets(root)
    println(mappedassets)
    println(isAncestor(file, folder))
    CloneAsset(file, folder)
    println(Utilities.findClones(file))
    root.prettyprint
    println(Utilities.getAncestors(classvp))
    val a = getAncestors(file)
    println(a)
  }

}
