package operations

import org.scalatest.FunSuite
import se.gu.vp.model._
import se.gu.vp.operations.Utilities
/**
Copyright [2019] [Thorsten Berger]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */
class ConvenienceMethod extends FunSuite {

  import Utilities._

 /* test("check missing features") {
    val file1 = File("file1")
    val folder1 = Folder("folder1", file1)

    val ast = VPRoot("root",
      Repository("Repository 1", folder1) ::
              Repository("Repository 2") ::
              Nil)
    ast.prettyprint
    fixParentPointers(ast)


  }*/
  test("check missing features with basePath") {
    // Assuming basePath is defined in your configuration for asset management
    val basePath = Configuration.basePath

    // Creating files and folders with a conceptual link to basePath
    // Note: The actual File and Folder classes here are placeholders and do not directly support basePath.
    // You would need to adapt or imagine these classes to understand or use basePath for actual file system operations.
    val file1Path = s"$basePath/file1"
    val folder1Path = s"$basePath/folder1"
    val file1 = File(file1Path) // Conceptually representing file at basePath
    val folder1 = Folder(folder1Path, file1) // Similarly for folder

    // Constructing the AST with repositories, imagining they are linked to locations defined by basePath
    val ast = VPRoot("root",
      Repository("Repository 1", folder1) ::
        Repository("Repository 2") ::
        Nil)

    // For illustrative purposes, let's assume prettyprint can output paths reflecting the conceptual basePath
    ast.prettyprint
    fixParentPointers(ast)

    // This test remains largely conceptual regarding basePath, as it primarily deals with an AST structure.
    // Adapting this to actually involve file system paths would require more context on how File and Folder are defined and used.
  }

}
