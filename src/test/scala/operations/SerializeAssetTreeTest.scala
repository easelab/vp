package operations

import org.scalatest.FunSuite
import se.gu.vp.operations.CalculatorSimulation.BasicCalculator.readInAssetTree
import se.gu.vp.operations.SerialiseAssetTree
/**
Copyright [2021] [Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */
class SerializeAssetTreeTest extends FunSuite {

   /*test("Test by Christoph") {
      // Check and print the current working directory
      val currentDir = new java.io.File(".").getCanonicalPath
      println(s"Current working directory: $currentDir")

      // Corrected path to CommitLog.txt in the CalculatorRootJava directory
      val commitLogPathCalculatorRootJava = "CalculatorRootJava/CommitLog.txt"
      val commitLogFileCalculatorRootJava = new java.io.File(commitLogPathCalculatorRootJava)

      // Corrected path to CommitLog.txt in the Basic directory
      val commitLogPathBasic = "CalculatorRootJava/Basic/CommitLog.txt"
      val commitLogFileBasic = new java.io.File(commitLogPathBasic)

      // Check if the file exists in either location
      if (commitLogFileCalculatorRootJava.exists() && commitLogFileBasic.exists()) {
         // If the file exists in both locations, proceed with the test
         val ast = readInAssetTree("CalculatorRootJava/Basic")
         ast match {
            case Some(astroot) =>
               astroot.prettyprint
               val path = "../test"

               SerialiseAssetTree(astroot, path)

            // ... rest of the test logic ...

            case None =>
               fail("Asset tree could not be read from 'CalculatorRootJava/Basic'")
         }
      } else {
         // If the file does not exist in either location, print out an error message and fail the test
         if (!commitLogFileCalculatorRootJava.exists()) {
            println(s"File not found in CalculatorRootJava: ${commitLogFileCalculatorRootJava.getCanonicalPath}")
         }
         if (!commitLogFileBasic.exists()) {
            println(s"File not found in Basic: ${commitLogFileBasic.getCanonicalPath}")
         }
         fail("Required file CommitLog.txt was not found in either location.")
      }
   }*/
     test("Test by Christoph targeting Dummy folder") {
        // Explicitly set the basePath to the Dummy folder location
        val basePath = "/Users/raz/Documents/University/RA Job/Wardah/Dummy"

        // Construct paths to CommitLog.txt within the Dummy/CalculatorRootJava directory
        val commitLogPathCalculatorRootJava = s"$basePath/CalculatorRootJava/CommitLog.txt"
        val commitLogFileCalculatorRootJava = new java.io.File(commitLogPathCalculatorRootJava)

        // Construct paths to CommitLog.txt within the Dummy/CalculatorRootJava/Basic directory
        val commitLogPathBasic = s"$basePath/CalculatorRootJava/Basic/CommitLog.txt"
        val commitLogFileBasic = new java.io.File(commitLogPathBasic)

        // Check if CommitLog.txt exists in either location
        if (commitLogFileCalculatorRootJava.exists() && commitLogFileBasic.exists()) {
           // Proceed with the test if files exist
           val ast = readInAssetTree(s"$basePath/CalculatorRootJava/Basic")
           ast match {
              case Some(astroot) =>
                 astroot.prettyprint
                 // Use basePath to define the correct output path within the Dummy folder
                 val outputPath = s"$basePath/test"
                 SerialiseAssetTree(astroot, outputPath)


              case None =>
                 fail("Asset tree could not be read from 'CalculatorRootJava/Basic'")
           }
        } else {
           // Handle cases where CommitLog.txt is missing in the expected locations
           if (!commitLogFileCalculatorRootJava.exists()) {
              println(s"File not found in CalculatorRootJava: ${commitLogFileCalculatorRootJava.getAbsolutePath}")
           }
           if (!commitLogFileBasic.exists()) {
              println(s"File not found in Basic: ${commitLogFileBasic.getAbsolutePath}")
           }
           fail("Required file CommitLog.txt was not found in either location.")
        }
     }

}
