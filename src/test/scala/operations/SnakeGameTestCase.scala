package operations

import se.gu.vp.model.{FeatureModel, RootFeature}
import se.gu.vp.operations.SerialiseAssetTree
import org.scalatest.FunSuite

/**
Copyright [2021] [Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */
class SnakeGameTestCase extends FunSuite{

  // Function to load the base path from a configuration file

/*test("SnakeGameTestCase"){
  val feature = new RootFeature("CalculatorRootJava")
  var featureModel = new FeatureModel(feature)

  val asset = se.gu.vp.operations.SnakeGame.Simulation.readInJavaAssetTree("C:\\Users\\khaled\\Snake")
  asset match{case _ => asset.get.prettyprint}
  SerialiseAssetTree(asset.get, "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TestTransplant_Original")
  val a = 1
}*/

 /* test("SnakeGameTestCase") {
    val feature = new RootFeature("CalculatorRootJava")
    var featureModel = new FeatureModel(feature)

    // Now using basePath to point to the 'Dummy' folder
    val relativePathToCalculatorRoot = s"$basePath/CalculatorRootJava"
    println(s"Looking for CommitLog.txt in: $relativePathToCalculatorRoot")

    //CheckiftheCommitLog.txtfileexistsintheSnakedirectory
    val commitLogFile=new java.io.File(relativePathToCalculatorRoot,"CommitLog.txt")
    if(commitLogFile.exists()){
      val asset=core.operations.SnakeGame.Simulation.readInJavaAssetTree(relativePathToCalculatorRoot)
      asset match {
        case Some(assetTree)=>
        assetTree.prettyprint
        //Definethecorrectrelativepathtotheoutputdirectory
        val outputPath="./path/to/output/directory"//Replacewithactualrelativepathtooutput
        SerialiseAssetTree(assetTree,outputPath)
        case None=>
          fail(s"Assettreecouldnotbereadfrom'$relativePathToCalculatorRoot'")
      }
    }else{
      fail(s"CommitLog.txtfilenotfoundat$relativePathToCalculatorRoot")
    }
  }*/

  /*test("SnakeGameTestCase") {
    val feature = new RootFeature("CalculatorRootJava")
    var featureModel = new FeatureModel(feature)

    // Relative path to the Snake folder
    val relativePathToSnake = "Snake"
    val absolutePathToSnake = new java.io.File(relativePathToSnake).getAbsolutePath
    println(s"Looking for CommitLog.txt in: $absolutePathToSnake")

    // Check if the CommitLog.txt file exists in the Snake directory
    val commitLogFile = new java.io.File(absolutePathToSnake, "CommitLog.txt")
    if (commitLogFile.exists()) {
      val asset = core.operations.SnakeGame.Simulation.readInJavaAssetTree(absolutePathToSnake)
      asset match {
        case Some(assetTree) =>
          assetTree.prettyprint
          // Define the correct relative path to the output directory
          val outputPath = "./path/to/output/directory" // Replace with actual relative path to output
          SerialiseAssetTree(assetTree, outputPath)
        case None =>
          fail(s"Asset tree could not be read from '$absolutePathToSnake'")
      }
    } else {
      fail(s"CommitLog.txt file not found at $absolutePathToSnake")
    }
  }*/

  // Directly using Configuration.basePath
  val basePath = Configuration.basePath

  test("SnakeGameTestCase") {
    // Assuming CalculatorRootJava is located under basePath/Dummy
    val calculatorRootPath = s"$basePath/Dummy/CalculatorRootJava"

    val feature = new RootFeature("CalculatorRootJava")
    var featureModel = new FeatureModel(feature)

    // Now, using basePath to construct the path to the Snake folder
    val relativePathToSnake = s"$basePath/Snake"
    val absolutePathToSnake = new java.io.File(relativePathToSnake).getAbsolutePath
    println(s"Looking for CommitLog.txt in: $absolutePathToSnake")

    // Check if the CommitLog.txt file exists in the Snake directory
    val commitLogFile = new java.io.File(absolutePathToSnake, "CommitLog.txt")
    if (commitLogFile.exists()) {
      val asset = se.gu.vp.operations.SnakeGame.Simulation.readInJavaAssetTree(absolutePathToSnake)
      asset match {
        case Some(assetTree) =>
          assetTree.prettyprint
          // Define the correct relative path to the output directory
          val outputPath = "./path/to/output/directory" // Adjust according to your needs
          SerialiseAssetTree(assetTree, outputPath)
        case None =>
          fail(s"Asset tree could not be read from '$absolutePathToSnake'")
      }
    } else {
      fail(s"CommitLog.txt file not found at $absolutePathToSnake")
    }
  }
}


