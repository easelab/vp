package operations

import org.scalatest.FunSuite
import se.gu.vp.model._
import se.gu.vp.operations.Utilities._
import se.gu.vp.operations._
import java.nio.file.Paths

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */
class PropagateToFeatureTest extends FunSuite {
 /* test("test1") {
  val feature1 = new RootFeature("feature1")
  val feature2 = Feature("feature2")
  val fmleft = new FeatureModel(feature1)
  AddFeatureToFeatureModel(feature2,feature1)

  val fmright = new FeatureModel(new RootFeature("fmright"))

  val root = Asset("CalculatorRootJava",VPRootType)
  val repo1 =  Asset("Repository 1",RepositoryType)
  val repo2 =  Asset("Repository 2",RepositoryType)
  val folder1 = Asset("Folder 1",FolderType)
  val file1=Asset("File 1",FileType)

  AddFeatureModelToAsset(repo1,fmleft)
  AddFeatureModelToAsset(repo2,fmright)

  AddAsset(repo1,root)
  AddAsset(repo2,root)
  AddAsset(folder1,repo1)
  AddAsset(file1,folder1)

  MapAssetToFeature(folder1,feature1)

  val sourcepath = Path(root,computeAssetPath(repo1) ++ computeFeaturePath(feature1))
  val targetpath = Path(root,computeAssetPath(repo2))

  CloneFeature(sourcepath,targetpath)

  val fcloneopt = findFeatureByName("feature1",fmright)
  val fclone = fcloneopt.get

  feature1.name = "feature1new"
  val folder5 = Folder("Folder5")
  val file7 = File("File7")
  val file8 = File("File8")

  AddAsset(folder5,repo1)
  AddAsset(file7,folder5)
  AddAsset(file8,folder5)
  MapAssetToFeature(folder5,feature1)

  val spath = Path(root,computeAssetPath(repo1) ++ computeFeaturePath(feature1))
  val clonepath = Path(root,computeAssetPath(repo2) ++ computeFeaturePath(fclone))
  val trace = FeatureTraceDatabase.traces
  PropagateToFeature(spath,clonepath)

  assert(featureClonedInFM(feature1,fmright)==true)
  assert(assetClonedInAST(folder5,repo2))
  assert(assetClonedInAST(file7,repo2))
  assert(assetClonedInAST(file8,repo2))
}*/

  test("test1") {
    // Assuming Configuration object's basePath is correctly set to point to the "Dummy" folder
    val basePathToDummy = Paths.get(Configuration.basePath)

    // Defining paths for each asset based on the basePath
    val rootPath = basePathToDummy.resolve("CalculatorRootJava")
    val repo1Path = basePathToDummy.resolve("Repository 1")
    val repo2Path = basePathToDummy.resolve("Repository 2")
    val folder1Path = repo1Path.resolve("Folder 1")
    val file1Path = folder1Path.resolve("File 1")
    val folder5Path = repo1Path.resolve("Folder5")
    val file7Path = folder5Path.resolve("File7")
    val file8Path = folder5Path.resolve("File8")

    // Define features and feature models
    val feature1 = new RootFeature("feature1")
    val feature2 = Feature("feature2")
    val fmleft = new FeatureModel(feature1)
    AddFeatureToFeatureModel(feature2,feature1)

    val fmright = new FeatureModel(new RootFeature("fmright"))

    // Instantiating assets with their respective paths converted to strings
    val root = Asset(rootPath.toString, VPRootType)
    val repo1 = Asset(repo1Path.toString, RepositoryType)
    val repo2 = Asset(repo2Path.toString, RepositoryType)
    val folder1 = Asset(folder1Path.toString, FolderType)
    val file1 = Asset(file1Path.toString, FileType)
    val folder5 = Asset(folder5Path.toString, FolderType)
    val file7 = Asset(file7Path.toString, FileType)
    val file8 = Asset(file8Path.toString, FileType)

    // Proceed with the asset manipulation logic
    AddFeatureModelToAsset(repo1, fmleft)
    AddFeatureModelToAsset(repo2, fmright)

    AddAsset(repo1, root)
    AddAsset(repo2, root)
    AddAsset(folder1, repo1)
    AddAsset(file1, folder1)

    MapAssetToFeature(folder1, feature1)

    // Operations related to cloning and propagation (Assuming computeAssetPath and computeFeaturePath are defined)
    CloneFeature(Path(root, computeAssetPath(repo1) ++ computeFeaturePath(feature1)), Path(root, computeAssetPath(repo2)))

    val fcloneopt = findFeatureByName("feature1", fmright)
    val fclone = fcloneopt.get

    feature1.name = "feature1new"

    AddAsset(folder5, repo1)
    AddAsset(file7, folder5)
    AddAsset(file8, folder5)
    MapAssetToFeature(folder5, feature1)

    PropagateToFeature(Path(root, computeAssetPath(repo1) ++ computeFeaturePath(feature1)), Path(root, computeAssetPath(repo2) ++ computeFeaturePath(fclone)))

    // Assertions
    assert(featureClonedInFM(feature1, fmright) == true)
    assert(assetClonedInAST(folder5, repo2))
    assert(assetClonedInAST(file7, repo2))
    assert(assetClonedInAST(file8, repo2))
  }

  // Define or import utility functions and classes such as computeAssetPath, computeFeaturePath, CloneFeature, etc.
}

