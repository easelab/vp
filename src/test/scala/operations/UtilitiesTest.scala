package operations

import org.scalatest.FunSuite
import se.gu.vp.operations.Replay.CommitHelpers.parseExcelFile
import se.gu.vp.operations.Utilities.getLastModifiedTime

import java.io.File
/**
Copyright [2021] [Thorsten Berger]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */
class UtilitiesTest extends FunSuite {
  test("CommitHelpers.Scala"){
    val file2 = new File("C:\\Experiment\\data.csv")
    val claferoperations = parseExcelFile(file2)
}
  test("MainSimulation.Scala"){
  }
  test("Get last modified time test"){
    //Testing versions in Scala
    Thread.sleep(1000)

    var file:File = new File("C:\\Users\\wardah\\Downloads")
    println("Last modified time is " + getLastModifiedTime(file))
  }
}

