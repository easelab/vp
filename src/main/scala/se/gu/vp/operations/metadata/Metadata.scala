package se.gu.vp.operations.metadata

import se.gu.vp.model.Relationship

/**
Copyright [2021] [Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

abstract class Metadata

// check, how to get typeclass back in?
abstract class OperationMetadata extends Metadata {
  var subOperations : Option[Seq[OperationMetadata]] = None
}
case class AddAssetMetadata(asset: AssetMetadata, parent: AssetMetadata, insertionPoint: Option[Int]) extends OperationMetadata
case class AddExternalAssetMetadata(pathToAsset: String, parent: AssetMetadata, assetName: Option[String]) extends OperationMetadata
case class AddFeatureModelToAssetMetadata() extends OperationMetadata
case class AddFeatureToFeatureModelMetadata(feature: FeatureMetadata, featuremodel: Option[FeatureModelMetadata], parent: Option[FeatureMetadata]) extends OperationMetadata
case class ChangeAssetMetadata(assetToChange: Option[AssetMetadata], assetsToChange: Option[List[AssetMetadata]]) extends OperationMetadata
case class CloneAssetMetadata(source: AssetMetadata, targetContainer: AssetMetadata, insertAfter: Option[AssetMetadata], cloneName: Option[String], deepCloneFeatures: Boolean) extends OperationMetadata
case class CloneRepositoryMetadata(source: AssetMetadata, cloneName: Option[String], targetContainer: Option[AssetMetadata]) extends OperationMetadata
case class CloneCodeAssetMetadata(source: AssetMetadata, targetContainer: AssetMetadata, insertAfter: Option[AssetMetadata], deepCloneFeatures: Boolean) extends OperationMetadata
case class CloneFeatureMetadata() extends OperationMetadata
case class IntegrateAssetMetadata() extends OperationMetadata
case class MakeFeatureOptionalMetadata() extends OperationMetadata
case class MapAssetToFeatureMetadata(asset: AssetMetadata, feat: FeatureMetadata) extends OperationMetadata
case class MoveAssetMetadata() extends OperationMetadata
case class MoveFeatureMetadata() extends OperationMetadata
case class PropagateAssetMetadata() extends OperationMetadata
case class PropagateFeatureMetadata() extends OperationMetadata
case class RemoveAssetMetadata(ast: AssetMetadata, assetToDelete: Option[AssetMetadata], assetsToDelete: Option[List[AssetMetadata]]) extends OperationMetadata
case class RemoveFeatureMetadata(feature: FeatureMetadata) extends OperationMetadata
case class RemoveFeatureFromFeatureModelMetadata(feature: FeatureMetadata) extends OperationMetadata
// TODO: Test implemented metadata case classes! (Best using VPBench)

abstract class AssetMetadata extends Metadata

case class ExternalAssetMetadata(name: String, assetType: String, children: List[AssetMetadata], presenceCondition: ExpressionMetadata, featureModel: Option[ExternalFeatureModelMetadata], content: Option[List[String]], versionNumber: Long) extends AssetMetadata
case class InternalAssetMetadata(path: Option[String], indexPath: Option[List[Int]] = None) extends AssetMetadata
case class SelectedExternalAssetMetadata(rootAsset: ExternalAssetMetadata, concreteAsset: InternalAssetMetadata) extends AssetMetadata

// TODO: Potentially integrate this? This is not required, but it might make parsing/comprehension easier?

// Is this necessary in this form?
abstract class ExpressionMetadata extends Metadata
case class UnaryExpressionMetadata(op: String, exp: ExpressionMetadata) extends ExpressionMetadata
case class BinaryExpressionMetadata(op: String, left: ExpressionMetadata, right: ExpressionMetadata) extends ExpressionMetadata
case class FeatureExpressionMetadata(featureMetadata: FeatureMetadata) extends ExpressionMetadata
case class TrueExpressionMetadata() extends ExpressionMetadata
case class FalseExpressionMetadata() extends ExpressionMetadata
// What is the use of identifier for expressions?
case class IdentifierExpressionMetadata(name: String) extends ExpressionMetadata

abstract class FeatureModelMetadata
case class InAssetFeatureModelMetadata(featureModelAsset: AssetMetadata) extends FeatureModelMetadata
case class ExternalFeatureModelMetadata(rootFeature: ExternalFeatureMetadata, name: String) extends FeatureModelMetadata

// potentially add more clarification here for the case, where a feature is referenced, that can not be found in the main assettree or the current asset-structure? need to identify these uniquely, too
abstract class FeatureMetadata extends Metadata
case class ExternalFeatureMetadata(name: String, complete: Boolean, optional: Boolean, dependsOn: Option[ExpressionMetadata], subfeatures: List[ExternalFeatureMetadata], versionNumber: Long, isRootFeature: Boolean) extends FeatureMetadata
// describe a feature in an asset tree, if its inside the main asset tree or another external one is clarified by the concrete type of AssetMetadata
case class InAssetFeatureMetadata(featureModelAsset: AssetMetadata, lpq: String) extends FeatureMetadata

// its not possible to reference elements in the same external asset tree otherwise -> use this metadata class to make this explicit
case class SelectedExternalFeatureInCurrentExternalAssetTreeMetadata(featureModelAsset: AssetMetadata, lpq: String) extends FeatureMetadata
// reference a feature in an external featuremodel without containing asset
case class SelectedExternalFeatureInFMMetadata(fm: ExternalFeatureModelMetadata, lpq: String) extends FeatureMetadata
// reference a feature beneath at least another feature, but no containing featuremodel
case class SelectedExternalSubFeatureMetadata(topFeature: ExternalFeatureMetadata, lpq: String) extends FeatureMetadata

// Reference a feature in the same external feature model definition
case class SelectedExternalFeatureInCurrentFeatureModelMetadata(lpq: String) extends FeatureMetadata
// Reference a feature in the same external feature tree definition
case class SelectedExternalFeatureInCurrentExternalFeatureTree(lpq: String) extends FeatureMetadata

case class AssetTraceMetadata(source: AssetMetadata, target: AssetMetadata, relationship: Relationship, versionAt: Long) extends Metadata

case class FeatureTraceMetadata(source: FeatureMetadata, target: FeatureMetadata, relationship: Relationship) extends Metadata

//trait CanContainFeature
//// not included
//trait InSameExternalAssetTree