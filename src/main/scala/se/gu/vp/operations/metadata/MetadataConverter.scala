package se.gu.vp.operations.metadata

import se.gu.vp.model._
import se.gu.vp.operations.Utilities.{findFileParentAsset, getLeastQualifiedPath, getTopFeature, isAncestor}

/**
Copyright [2021] [Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

object MetadataConverter {
  val inFileATypes = List(ClassType, MethodType, FieldType, BlockType)

  def findTopLevelElementUntilAsset(feature: Feature) : Object = {
    // If the feature is not embedded in a feature model -> return the highest ancestor-feature
    // Split this into two different statements, in case the rootfeature should be treated differently than other features
    if (!feature.getFeatureModelRoot.isDefined || !feature.getFeatureModelRoot.get.featureModel.isDefined) {
      return getTopFeature(feature)
    }

    // Return the containing feature model, if the fm is not embedded in a containing asset
    if (!feature.getFeatureModelRoot.get.featureModel.get.containingAsset.isDefined) {
      return feature.getFeatureModelRoot.get.featureModel.get
    }

    // Return the asset defining the feature
    feature.getFeatureModelRoot.get.featureModel.get.containingAsset.get
  }

  def convertFeatureInPresenceCondition(feature: Feature, asset: Asset) : FeatureMetadata = {
    if (feature.getFeatureModelRoot.isDefined
      && feature.getFeatureModelRoot.get.featureModel.isDefined
      && feature.getFeatureModelRoot.get.featureModel.get.containingAsset.isDefined) {
      val conAsset = feature.getFeatureModelRoot.get.featureModel.get.containingAsset.get
      // feature is defined in root asset tree
      if (conAsset.getRoot.isDefined) {
        val pathToAsset = getPathToAsset(conAsset)
        InAssetFeatureMetadata(InternalAssetMetadata(pathToAsset._1, pathToAsset._2), getLeastQualifiedPath(feature).get)
        // feature is defined in same external asset tree as to-be-described asset
      } else if (asset == conAsset || isAncestor(asset, conAsset)) {
        val pathToAsset = getPathToAsset(conAsset)
        SelectedExternalFeatureInCurrentExternalAssetTreeMetadata(InternalAssetMetadata(pathToAsset._1, pathToAsset._2), getLeastQualifiedPath(feature).get)
        // feature is defined in some other external asset tree or invalid in a way
      } else {
        // This does not recognize wrong inputs, such as cases, where the feature is defined in a featuremodel beneath the asset
        // This also includes the case, where the feature is part of a different external asset tree
        // TODO: How to handle this on the layer above? Set Metadata to None or abort?
        throw new Exception("Error during Metadata generation. Invalid parametrization.")
      }
      // feature is defined in a feature model, which is not part of any asset
    } else if (feature.getFeatureModelRoot.isDefined
      && feature.getFeatureModelRoot.get.featureModel.isDefined) {
      val fm = feature.getFeatureModelRoot.get.featureModel.get
      val fmMetadata = convertFeatureModelDownwardsToMetadata(fm)
      val featLPQ = getLeastQualifiedPath(feature)
      SelectedExternalFeatureInFMMetadata(fmMetadata, featLPQ.get)
      // feature is not part of any feature model
    } else if (feature.parent.isDefined) {
      val topFeature = getTopFeature(feature)
      // getLeastQualifiedPath was changed to not only work with elements beneath a rootfeature, but beneath any parent
      SelectedExternalSubFeatureMetadata(convertFeatureTreeToMetadata(topFeature), getLeastQualifiedPath(feature).get)
      // feature has subfeatures at most, but no parent elements
    } else {
      convertFeatureTreeToMetadata(feature)
    }
  }

  // Converts a Feature into FeatureMetadata
  def convertFeatureToMetadata(feat: Feature) : FeatureMetadata = {
    val topLevelElement = findTopLevelElementUntilAsset(feat)
    topLevelElement match {
      case topFeat: Feature
        if topFeat == feat  => convertFeatureTreeToMetadata(feat)
      case topFeat: Feature => SelectedExternalSubFeatureMetadata(convertFeatureTreeToMetadata(topFeat), getLeastQualifiedPath(feat).get)
      case fm: FeatureModel => SelectedExternalFeatureInFMMetadata(convertFeatureModelDownwardsToMetadata(fm), getLeastQualifiedPath(feat).get)
      case asset: Asset     => InAssetFeatureMetadata(convertAssetToMetadata(asset), getLeastQualifiedPath(feat).get)
      case _                => throw new Exception("Error during metadata creation. Unexpected return type of function findTopLevelElementUntilAsset.")
    }
  }

  def convertFeatureTreeToMetadata(feat: Feature) : ExternalFeatureMetadata = {
    val subfeatures = for (subf <- feat.subfeatures) yield {
      convertFeatureTreeToMetadata(subf)
    }

    val dependsOn = if (feat.dependsOn.isDefined) {
      Some(convertDependsOnToMetadata(feat.dependsOn.get, feat))
    } else { None }

    ExternalFeatureMetadata(feat.name, feat.complete, feat.optional, dependsOn, subfeatures, feat.versionNumber, feat.isInstanceOf[RootFeature])
  }

  def convertFeatureModelToMetadata(fm: FeatureModel) : FeatureModelMetadata = {
    fm.containingAsset.isDefined match {
      case true =>
        InAssetFeatureModelMetadata(convertAssetToMetadata(fm.containingAsset.get))
      case false =>
        convertFeatureModelDownwardsToMetadata(fm)
    }
  }

  def convertFeatureModelDownwardsToMetadata(fm: FeatureModel) : ExternalFeatureModelMetadata = {
    val rootFeature = convertFeatureTreeToMetadata(fm.rootfeature)
    ExternalFeatureModelMetadata(rootFeature, fm.name)
  }

  def convertAssetToMetadata(asset: Asset) : AssetMetadata = {
    val topAncestor = getTopParent(asset)
    topAncestor.assetType == VPRootType match {
      // Passed asset is part of the main asset tree
      case true =>
        val (path, indexpath) = getPathToAsset(asset)
        val internalMetadata = InternalAssetMetadata(path, indexpath)

        internalMetadata
      // Passed asset is top level asset
      case false
        if topAncestor == asset => convertAssetTreeToMetadata(asset)
      // Passed asset is non-top-level-node of an asset tree
      case false =>
        val (path, indexpath) = getPathToAsset(asset)
        val internalMetadata = InternalAssetMetadata(path, indexpath)

        SelectedExternalAssetMetadata(convertAssetTreeToMetadata(topAncestor), internalMetadata)
    }
  }

  // Needs to be converted in case the asset is not part of the main asset tree, otherwise its not possible to reconstruct it
  def convertAssetTreeToMetadata(asset: Asset) : ExternalAssetMetadata = {
    // Check, if metadata for given asset tree can be unique
    // -> Check: Occurrence of undefined features should be limited -> for now: a single not contained feature may only appear once
    //           Alternative: Only a single feature with the same featurename may be used -> could assume, that everything matching to value-equal unspecified features maps to the same one
    //           Should there actually be two different, but value-equal unspecified features: Not valid -> Add one of them to main asset tree first
    // TODO: Decide on an approach (or implement both) and implement it

    val children = for (child <- asset.children) yield {
      convertAssetTreeToMetadata(child)
    }

    val name = if (inFileATypes.contains(asset.assetType)) {
      asset.name
    } else {
      relativizeAssetPath(asset)
    }

    // Calculate presenceCondition
    val presenceCondition = convertPresenceConditionToMetadata(asset.presenceCondition, asset)

    val featureModel = if (asset.featureModel.isDefined) {
      Some(convertFeatureModelDownwardsToMetadata(asset.featureModel.get))
    } else {
      None
    }

    ExternalAssetMetadata(name, asset.assetType.toString, children, presenceCondition, featureModel, asset.content, asset.versionNumber)
  }

  def absolutizeAssetPath(rootAsset: Asset, path: String) : String = {
    val missingPath = rootAsset.name.split("\\\\").dropRight(1).mkString("\\")
    missingPath + "\\" + path
  }

  def getTopParent(asset: Asset) : Asset = {
    asset.parent match {
      case None => asset
      case Some(p) => getTopParent(p)
    }
  }

  def relativizeAssetPath(asset: Asset) : String = {
    // does not use getRoot, as an asset might not have an ancestor with type VPRootType
    val topPath = getTopParent(asset).name
    val filteredBeginLength = topPath.split("\\\\").dropRight(1).mkString("\\").length
    val dropAmount = if (filteredBeginLength == 0) {
      // in case nothing has to be extracted, the existence of an initial \ should not be assumed
      0
    } else {
      // + 1 to get rid of the \ at the beginning
      filteredBeginLength + 1
    }
    asset.name.drop(dropAmount)
  }

  def findIndexPath(asset: Asset) : List[Int] = {
    findReverseIndexPath(asset).reverse
  }

  def findReverseIndexPath(asset: Asset) : List[Int] = {
    asset.assetType match {
      case _
        if asset.parent.isEmpty => Nil
      case FileType => Nil
      case ClassType =>
        val index = asset.parent.get.children.indexOf(asset)
        index :: findReverseIndexPath(asset.parent.get)
      case BlockType =>
        val index = asset.parent.get.children.indexOf(asset)
        index :: findReverseIndexPath(asset.parent.get)
      case MethodType =>
        val index = asset.parent.get.children.indexOf(asset)
        index :: findReverseIndexPath(asset.parent.get)
      case FieldType =>
        val index = asset.parent.get.children.indexOf(asset)
        index :: findReverseIndexPath(asset.parent.get)
    }
  }

  def getPathToAsset(asset: Asset) : Tuple2[Option[String], Option[List[Int]]] = {
    asset.assetType match {
      case VPRootType => (Some(relativizeAssetPath(asset)), None)
      case RepositoryType => (Some(relativizeAssetPath(asset)), None)
      case FolderType => (Some(relativizeAssetPath(asset)), None)
      case FileType => (Some(relativizeAssetPath(asset)), None)
      case _ =>
        val filePath = if (findFileParentAsset(asset).isDefined) {
          Some(relativizeAssetPath(findFileParentAsset(asset).get))
        } else {
          None
        }
        val indexPath = findIndexPath(asset)
        (filePath, Some(indexPath))
    }
  }

  def convertPresenceConditionToMetadata(expr: Expression, asset: Asset) : ExpressionMetadata = {
    expr match {
      case _   : True       => TrueExpressionMetadata()
      case _   : False      => FalseExpressionMetadata()
      case f   : Feature    => FeatureExpressionMetadata(convertFeatureInPresenceCondition(f, asset))
      case neg : Negation   => UnaryExpressionMetadata("!", convertPresenceConditionToMetadata(neg.e, asset))
      case and : And        => BinaryExpressionMetadata("&", convertPresenceConditionToMetadata(and.l, asset), convertPresenceConditionToMetadata(and.r, asset))
      case or  : Or         => BinaryExpressionMetadata("|", convertPresenceConditionToMetadata(or.l, asset), convertPresenceConditionToMetadata(or.r, asset))
      case imp : Implies    => BinaryExpressionMetadata("=>", convertPresenceConditionToMetadata(imp.l, asset), convertPresenceConditionToMetadata(imp.r, asset))
      case id  : Identifier => IdentifierExpressionMetadata(id.name)
      case _                => throw new Exception("Something went wrong during metadata conversion. Unexpected type in expression.")
    }
  }

  def convertFeatureInDependsOn(dependentOnFeat: Feature, dependingFeat: Feature): FeatureMetadata = {
    val dependentOnTopElement = findTopLevelElementUntilAsset(dependentOnFeat)

    dependentOnTopElement match {
      case dependentOnAsset: Asset =>
        if (dependentOnAsset.getRoot.isDefined) {
          // Inside MAT
          InAssetFeatureMetadata(convertAssetToMetadata(dependentOnAsset), getLeastQualifiedPath(dependentOnFeat).get)
        } else {
          val dependingTopElement = findTopLevelElementUntilAsset(dependingFeat)
          dependingTopElement match {
            case dependingAsset: Asset
              // eq over ==, as topParent could also be inside FileAsset and could have the same name due to that
              if getTopParent(dependingAsset) eq getTopParent(dependentOnAsset) =>
                val pathToAsset = getPathToAsset(dependentOnAsset)
                SelectedExternalFeatureInCurrentExternalAssetTreeMetadata(InternalAssetMetadata(pathToAsset._1, pathToAsset._2), getLeastQualifiedPath(dependentOnFeat).get)
            case _ =>
                throw new Exception("Something went wrong during metadata conversion. A feature dependency on an external feature has to be part of the main asset tree or defined in the same structure.")
          }
        }
      case dependentOnFM: FeatureModel =>
        val dependingTopElement = findTopLevelElementUntilAsset(dependingFeat)
        dependingTopElement match {
          case dependingFM: FeatureModel
            if dependentOnFM eq dependingFM =>
              SelectedExternalFeatureInCurrentFeatureModelMetadata(getLeastQualifiedPath(dependentOnFeat).get)
          case _ =>
              throw new Exception("Something went wrong during metadata conversion. A feature dependency on an external feature has to be part of the main asset tree or defined in the same structure.")
        }
      case dependentOnFeature: Feature =>
        val dependingTopElement = findTopLevelElementUntilAsset(dependingFeat)
        dependingTopElement match {
          case dependingFeature: Feature
            if dependentOnFeature eq dependingFeature =>
              SelectedExternalFeatureInCurrentExternalFeatureTree(getLeastQualifiedPath(dependentOnFeat).get)
          case _ =>
              throw new Exception("Something went wrong during metadata conversion. A feature dependency on an external feature has to be part of the main asset tree or defined in the same structure.")
        }
      case _ =>
        throw new Exception("Something went wrong during metadata conversion. Unexpected datatype.")
    }
  }

  def convertDependsOnToMetadata(expr: Expression, feature: Feature) : ExpressionMetadata = {
    expr match {
      case _: True => TrueExpressionMetadata()
      case _: False => FalseExpressionMetadata()
      case f: Feature => FeatureExpressionMetadata(convertFeatureInDependsOn(f, feature))
      case neg: Negation => UnaryExpressionMetadata("!", convertDependsOnToMetadata(neg.e, feature))
      case and: And => BinaryExpressionMetadata("&", convertDependsOnToMetadata(and.l, feature), convertDependsOnToMetadata(and.r, feature))
      case or: Or => BinaryExpressionMetadata("|", convertDependsOnToMetadata(or.l, feature), convertDependsOnToMetadata(or.r, feature))
      case imp: Implies => BinaryExpressionMetadata("=>", convertDependsOnToMetadata(imp.l, feature), convertDependsOnToMetadata(imp.r, feature))
      case id: Identifier => IdentifierExpressionMetadata(id.name)
      case _ => throw new Exception("Something went wrong during metadata conversion. Unexpected type in expression.")
    }
  }
}
