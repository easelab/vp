package se.gu.vp.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model.ASTWellFormednessConstraints.containable
import se.gu.vp.model._
import se.gu.vp.operations.AssetCloneHelpers.addTrace
import se.gu.vp.operations.FeatureCloneHelpers.addFeatureTrace
import se.gu.vp.operations.Utilities._
import se.gu.vp.operations.metadata.{CloneAssetMetadata, CloneCodeAssetMetadata, CloneRepositoryMetadata, MetadataConverter}

import scala.collection.mutable

/**
Copyright [2019] [Wardah Mahmood, Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

// TODO: Check for repercussions elsewhere due to cloneName parameter!
// For now: deepCloneFeatures still defaults to same behaviour as before for RepositoryAssets
class CloneAsset(source: Asset, targetContainer: Asset, insertAfter:Option[Asset] = None, cloneName: Option[String] = None, deepCloneFeatures: Boolean = true) extends Operation with Logging {
  override def perform(): Boolean = {

    if(ASTWellFormednessConstraints.containable(source,targetContainer) && !assetClonedInAST(source,targetContainer)) {
      val operationListBuilder = mutable.ListBuffer[Operation]()

      source.assetType match {
        case RepositoryType => {
          operationListBuilder += CloneRepository(source, cloneName, Some(targetContainer))
          subOperations = Some(operationListBuilder.toList)
          storeSubOpMetadata
          true
        }
        case FolderType | FileType => {
          val clone = AssetCloneHelpers.deepCloneAsset(source, targetContainer, deepCloneFeatures, Some(targetContainer), cloneName)
          targetContainer.children = targetContainer.children ++ List(clone)
          updateVersion(targetContainer)
          storeSubOpMetadata
          true
        };
        //TODO -> integrate
        case ClassType | MethodType | BlockType | FieldType => {
          operationListBuilder += CloneCodeAsset(source, targetContainer, insertAfter, deepCloneFeatures = deepCloneFeatures)
          subOperations = Some(operationListBuilder.toList)
          storeSubOpMetadata
          true
        }
      }
    }
    else if(assetClonedInAST(source,targetContainer)){
      info("Asset " + source.name + " is already cloned in " + targetContainer.name)
      false
    }
    else{
      error("Clone not possible, ASTWellFormednessConstraints violated")
      false
    }
  }

  override def createMetadata(): Unit = {
    try {
      val sourceMD = MetadataConverter.convertAssetToMetadata(source)
      val targetContainerMD = MetadataConverter.convertAssetToMetadata(targetContainer)

      val insertAfterMD = if (insertAfter.isDefined) {
        Some(MetadataConverter.convertAssetToMetadata(insertAfter.get))
      } else {
        None
      }

      metadata = Some(CloneAssetMetadata(source = sourceMD, targetContainer = targetContainerMD, insertAfter = insertAfterMD, cloneName = cloneName, deepCloneFeatures = deepCloneFeatures))
    } catch {
      case e : Exception =>
        println("Something went wrong during metadata creation.")
    }
  }
}
object CloneAsset{
  def apply(source: Asset, targetContainer: Asset, insertAfter:Option[Asset] = None, cloneName: Option[String] = None, deepCloneFeature: Boolean = true, runImmediately: Boolean = true, createMetadata: Boolean = true): CloneAsset = {
    if (runImmediately && createMetadata) {
      new CloneAsset(source, targetContainer, insertAfter, cloneName, deepCloneFeature) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new CloneAsset(source, targetContainer, insertAfter, cloneName, deepCloneFeature) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new CloneAsset(source, targetContainer, insertAfter, cloneName, deepCloneFeature) with CreateMetadata
    } else {
      new CloneAsset(source, targetContainer, insertAfter, cloneName, deepCloneFeature)
    }
  }
}

case class Mapping(var asset:Asset,var featurename:String){}

class CloneRepository (source: Asset, cloneName: Option[String] = None, targetContainer: Option[Asset]=None) extends Operation with Logging {

  var mappinglist:List[Mapping]= Nil

  def deepCloneFeature(sourceFeature:Feature, parent:Option[Feature]=None):Feature={

    var clone = sourceFeature.doCloneFeature()

    if (sourceFeature.isInstanceOf[RootFeature]) {
      val rootFeature = clone.asInstanceOf[RootFeature]
      val unassignedSubFeature = transformFMToList(rootFeature)
        .filter(feat => feat.name == "UNASSIGNED")(0)
      rootFeature.UNASSIGNED = unassignedSubFeature
      clone = rootFeature
    }

    parent match{
      case None=>;
      case _=> clone.parent = parent
    }
    clone.subfeatures = Nil
    addFeatureTrace(sourceFeature,clone)
    for(subfeature <- sourceFeature.subfeatures){
      val childclone = deepCloneFeature(subfeature,Some(clone))
      clone.subfeatures =  clone.subfeatures ++ List(childclone)
    }

    clone
  }

  def deepCloneAsset(source:Asset,parent:Option[Asset] = None, cloneName:Option[String] = None):Asset= {

    val clone = source.doClone()

    // TODO: Ideally CloneRepository always needs a cloneName, but there could be repercussions with PropagateAsset as is atm -> First discuss a new error handling strategy (for bad parametrization)
    if (cloneName.isDefined) {
      changeAssetName(clone, cloneName.get)
    }

    parent match{
      case None=>;
      case _=> {
        clone.parent = parent
        clone.name = parent.get.name + "\\" + clone.name.split('\\').last
      }
    }
    clone.versionNumber = source.versionNumber
    clone.presenceCondition = True()
    clone.featureModel = None

    if(!source.mappedToFeatures.isEmpty){
      source.mappedToFeatures.foreach( mappedfeature => {
        val mapping =  Mapping(clone,mappedfeature.name)
        mappinglist =  mappinglist ++ List(mapping)
      })
    }
    clone.children = Nil
    addTrace(source,clone,source.versionNumber)
    for(child <- source.children){
      val childclone = deepCloneAsset(child,Some(clone))
      clone.children = clone.children ++ List(childclone)
    }
    clone
  }

  def replicateMappings(fm:FeatureModel) : List[MapAssetToFeature] = {
    for {
      mapping <- mappinglist
      feature = Utilities.findFeatureByName(mapping.featurename, fm)
      if feature.isDefined
    } yield {
      MapAssetToFeature(mapping.asset, feature.get)
    }
  }

  override def perform(): Boolean = {

    source.assetType match {
      case RepositoryType => {
        val operationListBuilder = mutable.ListBuffer[Operation]()

        var fork:Asset = null
        // Recursively clones assets and collects a list of assetToFeature-mappings

        // Set name of repository to cloneName
        // TODO: Ideally CloneRepository always needs a cloneName, but there could be repercussions with PropagateAsset as is atm -> First discuss a new error handling strategy (for bad parametrization)
        if (cloneName.isDefined)
          fork = deepCloneAsset(source,None,cloneName)
        else {
          fork = deepCloneAsset(source)

        }

        // TODO: This cloning above is intertwined with the adding here -> strategy between cloning subassets and adding assets here needs to fit together
        val addOp = targetContainer match {
          // Metadata does not need to be stored, as identifying the existing asset, that is cloned, already provides all information
          // TODO: how this is handled depends on further strategy with metadata creation! and storage of it
          case None => {
            if(containable(fork,source.getRoot().get)){
              fork.parent = Some(source.getRoot().get)
              source.getRoot().get.children = source.getRoot().get.children ++ List(fork)
            }
            //Removed AddAsset as it was versioning the root. Also, if the fork wasn't serialized
            // yet, it was giving an error of file not present when getting the last modified time
            // of the file to set versions.
          }
          case _ =>
            setNewNamesRecursively(fork, targetContainer.get.name)
            // Metadata does not need to be stored, as identifying the existing asset, that is cloned, already provides all information
            if(containable(fork,targetContainer.get)){
              fork.parent = targetContainer
              targetContainer.get.children = targetContainer.get.children ++ List(fork)
              getLastModifiedTime(targetContainer.get)
            }

        }

        // TODO: Does this assume repo to have a featuremodel? Shouldnt replicateMappings be called here anyways? Requires fms on other levels to be maintained along with a fm in Repo, this doesnt happen atm. fm is set to none in deepCloneAsset
        source.featureModel match {
          case None => ;
          case Some(fm) => {
            val clonedfm = fm.doCloneFeatureModel()
            val rootFeature = deepCloneFeature(clonedfm.rootfeature).asInstanceOf[RootFeature]
            rootFeature.name = fork.name.split('\\').last; /** Renaming feature model's root feature to repository's name */
            clonedfm.rootfeature = rootFeature
            rootFeature.featureModel = Some(clonedfm)
            rootFeature.UNASSIGNED = Utilities.findFeatureByName("UnAssigned", clonedfm).get

            // way of cloning here requires either storing a completely wrong fm first with wrong contained features before correctly setting it
            // or using an operation on an external element to alter it before adding it afterwards
            // otherwise use createMetadata = false to not store any metadata but only store information on the insertion of the featuremodel with the correct rootfeature afterwards
            operationListBuilder += AddFeatureModelToAsset(fork, clonedfm)
            // replicateMappings maps the asset clones to the feature clones
            replicateMappings(clonedfm).foreach(op => operationListBuilder += op)
          }
        }
        subOperations = Some(operationListBuilder.toList)
      }
      case _=> false;
    }
    storeSubOpMetadata
    true
  }

  override def createMetadata(): Unit = {
    try {
      val targetContainerMD = if (targetContainer.isDefined) {
        Some(MetadataConverter.convertAssetToMetadata(targetContainer.get))
      } else { None }

      val createdMetadata = CloneRepositoryMetadata(source = MetadataConverter.convertAssetToMetadata(source), cloneName = cloneName, targetContainer = targetContainerMD)
      metadata = Some(createdMetadata)
    } catch {
      case e : Exception =>
        println("Something went wrong during metadata creation.")
    }
  }
}

class CloneCodeAsset(source: Asset, targetContainer: Asset, insertAfter:Option[Asset], deepCloneFeatures: Boolean) extends Operation with Logging {

  override def perform(): Boolean = {
    if(containable(source,targetContainer)){
      if (insertAfter.isDefined && !targetContainer.children.contains(insertAfter.get)) {
        println("Error - insertAfter does not exist")
        false
      } else {
        val operationListBuilder = mutable.ListBuffer[Operation]()

        val sourceclone = Utilities.deepclone(source)
        setParentageBetween(targetContainer, sourceclone, insertAfter)
        setNewNamesAfterCloning(sourceclone,targetContainer)
        if (deepCloneFeatures) {
          // TODO: Shouldnt this be inspected starting from the new source clone, as that can also contain a feature model? Otherwise support for multiple fms in the future?
          val targetfm = Utilities.getImmediateAncestorFeatureModel(Some(targetContainer))
          targetfm match {
            case None => ;
            case _ => {
              for (elem <- source.mappedToFeatures) {
                val featureName = elem.name.split("-").last
                // TODO: Shouldnt use lastName, but lpq. How to handle with multiple potential fms?
                Utilities.findFeatureByLastName(featureName, targetfm.get) match {
                  case None => {
                    // Cloning feature(s) and adding them to the target FM + mapping them to the cloned asset
                    // TODO: What happens here, if an asset is cloned that maps to two features in the same hierarchy? Both are cloned, but subfeature of mapped topfeature and mapped subfeature are now two different features
                    val featureclone = Utilities.deepCloneFeature(elem)
                    operationListBuilder += AddFeatureToFeatureModel(featureclone, targetfm.get.rootfeature.UNASSIGNED)
                    operationListBuilder += MapAssetToFeature(sourceclone, featureclone)
                  }
                  case _ => {
                    operationListBuilder += MapAssetToFeature(sourceclone, Utilities.findFeatureByLastName(featureName, targetfm.get).get)
                  }
                }
              }
            }
          }
        }

        updateVersion(targetContainer)
        subOperations = Some(operationListBuilder.toList)
      }
    } else {
      println("Error - Source cannot be inserted into target.")
    }

    storeSubOpMetadata
    true
  }

  override def createMetadata(): Unit = {
    try {
      val sourceMD = MetadataConverter.convertAssetToMetadata(source)
      val targetContainerMD = MetadataConverter.convertAssetToMetadata(targetContainer)

      val insertAfterMD = if (insertAfter.isDefined) {
        Some(MetadataConverter.convertAssetToMetadata(insertAfter.get))
      } else {
        None
      }

      metadata = Some(CloneCodeAssetMetadata(source = sourceMD, targetContainer = targetContainerMD, insertAfter = insertAfterMD, deepCloneFeatures = deepCloneFeatures))
    } catch {
      case e : Exception =>
        println("Something went wrong during metadata creation.")
    }
  }
}
object CloneCodeAsset{
  def apply(source: Asset, targetContainer: Asset, insertAfter: Option[Asset], deepCloneFeatures: Boolean = true, runImmediately: Boolean = true, createMetadata: Boolean = true): CloneCodeAsset = {
    if (runImmediately && createMetadata) {
      new CloneCodeAsset(source, targetContainer, insertAfter, deepCloneFeatures) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new CloneCodeAsset(source, targetContainer, insertAfter, deepCloneFeatures) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new CloneCodeAsset(source, targetContainer, insertAfter, deepCloneFeatures) with CreateMetadata
    } else {
      new CloneCodeAsset(source, targetContainer, insertAfter, deepCloneFeatures)
    }
  }
}

object CloneRepository{

  def apply(source: Asset, cloneName: Option[String] = None, targetContainer: Option[Asset] = None, runImmediately: Boolean = true, createMetadata: Boolean = true): CloneRepository = {
    if (runImmediately && createMetadata) {
      new CloneRepository(source, cloneName, targetContainer) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new CloneRepository(source, cloneName, targetContainer) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new CloneRepository(source, cloneName, targetContainer) with CreateMetadata
    } else {
      new CloneRepository(source, cloneName, targetContainer)
    }
  }
}