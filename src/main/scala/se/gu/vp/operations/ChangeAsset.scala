package se.gu.vp.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model._
import se.gu.vp.operations.metadata.{ChangeAssetMetadata, MetadataConverter}

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

/*
* Author: Wardah Mahmood. Date: 08-02-2019
* At the moment, change is an abstract concept, which can be implemented concretely,
* but the general idea is to update something in an asset, which essentially updates
* the asset's version number, along with all it's ancestors uptil the root. The version
* update which is a recursive method, is incorporated in multiple other operations,
* like add asset, clone asset, remove asset, change asset and propagate asset. This is
* stored in the tracelinks and used when propagating changes to determine if the source
* is ahead of the target, and if the target is unchaged or not. In the end, since all
* changes result in a version update up till the root, the root has the highest version
* number at any given time
* */

class ChangeAsset(assetToChange:Option[Asset] = None , assetsToChange:Option[List[Asset]] = None) extends Operation with Logging {
  override def perform(): Boolean = {
    if(assetToChange!=None) {
      updateVersion(assetToChange.get)
      true
    }
    if(assetsToChange!=None)
    {
      for(asset <- assetsToChange.get){
          updateVersion(asset)
      }
    }
    storeSubOpMetadata
    true
  }

  override def createMetadata(): Unit = {
    try {
      val assetToChangeMD = if (assetToChange.isDefined) {
        Some(MetadataConverter.convertAssetToMetadata(assetToChange.get))
      } else { None }
      val assetsToChangeMD = if (assetsToChange.isDefined) {
        val listMD = for (asset <- assetsToChange.get) yield {
          MetadataConverter.convertAssetToMetadata(asset)
        }
        Some(listMD)
      } else { None }
      val createdMetadata = ChangeAssetMetadata(assetToChangeMD, assetsToChangeMD)

      metadata = Some(createdMetadata)
    } catch {
      case e: Exception =>
        println("Something went wrong during metadata creation.")
    }
  }
}
object ChangeAsset{
  def apply(assetToChange: Asset, runImmediately: Boolean = true, createMetadata: Boolean = true): ChangeAsset = {
    if (runImmediately && createMetadata) {
      new ChangeAsset(assetToChange = Some(assetToChange)) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new ChangeAsset(assetToChange = Some(assetToChange)) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new ChangeAsset(assetToChange = Some(assetToChange)) with CreateMetadata
    } else {
      new ChangeAsset(assetToChange = Some(assetToChange))
    }
  }

  def apply(assetsToChange: List[Asset], runImmediately: Boolean, createMetadata: Boolean): ChangeAsset = {
    if (runImmediately && createMetadata) {
      new ChangeAsset(assetsToChange = Some(assetsToChange)) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new ChangeAsset(assetsToChange = Some(assetsToChange)) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new ChangeAsset(assetsToChange = Some(assetsToChange)) with CreateMetadata
    } else {
      new ChangeAsset(assetsToChange = Some(assetsToChange))
    }
  }
}