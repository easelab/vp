package se.gu.vp.operations.Replay

import se.gu.vp.model.{Expression, True}

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class DataHolder {

}
case class commit(var commitid:String,var merge:String,var author:String,var summary:List[String]=Nil){}

case class PendingMappings(var assetname:String,var featureName:String){}
case class FeatureFile(var filename:String, var filecontent:List[String]){}

case class ClaferCommit(var repository:String,var commitid:String,var merge:String,var author:String,var date:String, var time:String,var summary:List[String]=Nil){}

case class ClaferFeatureOperation(var sourcerepo:String, var targetrepo:String,var operation:String,var feature:String,var sourcecommit:String,var targetcommit:String){}

class Annotation(var featurename : String , var beginat : Int ,var endat : Int )
class TempBlock(var name:String, var block:Boolean, var parent:Option[TempBlock] = None, var childern:List[TempBlock],var start:Int, var end:Int, var presenceCondition: Expression = True())
