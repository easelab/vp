package se.gu.vp.operations.Replay

import scala.util.matching.Regex

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class RegexTester {

}
object RegexTester{

  def test(): Unit ={
    val pattern = new Regex("//\\s*[&$]\\s*[Bb]egin")
    val str1 = "\\&begin"
    val str2 = "\\$begin"
    val str3 = "\\ &begin"
    val str4 = "\\ $ begin"

    println((pattern findAllIn str1).mkString(","))
    println((pattern findAllIn str2).mkString(","))
    println((pattern findAllIn str3).mkString(","))
    println((pattern findAllIn str4).mkString(","))
  }
//Test moved to ParserTests.Scala under RegexTester
}
