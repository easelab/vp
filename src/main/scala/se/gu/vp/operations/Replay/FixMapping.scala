package se.gu.vp.operations.Replay

import java.io.File

import se.gu.vp.model.{Asset, Feature, FeatureModel}
import se.gu.vp.operations.Replay.FeatureRelatedHelpers.{findFeatureByName, pendingmappings}
import se.gu.vp.operations.{MapAssetToFeature, Utilities}

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class FixMapping {

}
object FixMapping{
  def fixFolderMappings(astroot:Asset,mappingsfile:File,featureModel: Option[FeatureModel]):Unit= {

    println("Fixing Folder Mappings After Modifying and Reloading Folder ...")
    val source = io.Source.fromFile(mappingsfile.getAbsolutePath, "ISO-8859-1")
    val lines = source.getLines.toList
    source.close

    val parentname = mappingsfile.getParent
    val parentasset = Utilities.getassetbyname(parentname, astroot)

    if (parentasset != None) {
      for (line <- lines) {
        featureModel match {
          case None => {
            val pendingmapping = new PendingMappings(parentasset.get.name,line.trim)
            pendingmappings = pendingmapping :: pendingmappings
            println("Pending mapping added between folder " + parentasset.get + " and " + line.trim)
          }
          case _ => {
            val feature = findFeatureByName(line.trim, featureModel.get)
            feature match {
              case None => {
                println("Mapped feature not found in target repository ... Adding in unassigned features")
                val addedfeature = Feature(line.trim)
                MapAssetToFeature(parentasset.get, addedfeature)
              }
              case _ => {
                MapAssetToFeature(parentasset.get, feature.get)
              }
            }
          }
        }
      }
    }
    else println("Folder to map missing")
  }
  def handleFileMappings(astroot:Asset,mappingsfile:File,featureModel: Option[FeatureModel]):Unit= {
    println("Fixing File Mappings After Modifying and Reloading Folder ...")
    val source = io.Source.fromFile(mappingsfile.getAbsolutePath, "ISO-8859-1")
    val lines = source.getLines.toList
    source.close

    val parentname = mappingsfile.getParent
    val parentasset = Utilities.getassetbyname(parentname,astroot)
    if(parentasset!=None) {
      for (line <- lines) {
        if (line.trim.endsWith(".js")) {
          val temp = line.trim()
          val splits = temp.split(" ")
          for (split <- splits) {

            val assetname = parentname + "\\" + split.trim
            val featurename = lines(lines.indexOf(line) + 1).trim

            featureModel match {
              case None => {
                pendingmappings = new PendingMappings(assetname, featurename) :: pendingmappings
              }
              case _ => {
                val asset = Utilities.getassetbyname(assetname, astroot)
                val feature = findFeatureByName(featurename, featureModel.get)
                if (asset != None & !feature.isEmpty) {
                  MapAssetToFeature(asset.get, feature.get)
                }
                else if (asset == None)
                  println("File mapping unsuccessful "+ assetname + " asset missing")
                else if (feature == None) {
                  val newfeature = Feature(featurename)
                  MapAssetToFeature(asset.get,newfeature)

                }
              }
            }
          }
        }
      }
    }
  }
}