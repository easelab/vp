package se.gu.vp.operations.Replay
import java.io.File

import se.gu.vp.model._
import se.gu.vp.operations._

import scala.util.control.Breaks._

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class FeatureRelatedHelpers {
}
object FeatureRelatedHelpers {

  var pendingmappings:List[PendingMappings] = Nil

  def indentation(str:String):Int={
    var indentation = 0
    breakable(
      for(i<-0 to str.length-1){
        if(str(i)==' ') indentation+=1
        else if(str(i)=='\t') indentation+=4
        else break()
      }
    )
    if(indentation%4==1)
      indentation = indentation - (indentation%4)
    return indentation
  }

  def getfeaturepath(feature:Feature):Any={
    feature.parent match{
      case None=>return feature.name+"-";
      case _=>{
        return getfeaturepath(feature.parent.get) + feature.parent.get.name
      }
    }
  }
  def featurepath(feature:String,featurefile:List[String]):Any= {
    var featurename = feature.trim
    if (featurename.startsWith("or ")) featurename = featurename.split(" ").last

    if (indentation(feature) == 0)
       return featurename
    else {
      for (i <- (0 to featurefile.indexOf(feature) - 1).reverse) {
        if (indentation(featurefile(i)) < indentation(feature)) {
            return featurepath(featurefile(i), featurefile) + "-" + featurename
          }
        }
    }
  }

  def findFeatureByName(name:String,featureModel: FeatureModel):Option[Feature]={
     val feature = (Utilities.transformFMToList(featureModel)).find(p=>p.name.endsWith("-" + name) | p.name == name)
    if(!feature.isEmpty){
      val foundfeature = feature.get
      return Some(foundfeature)
    }
    else None
  }



  def unmapFiles(parentFolder:Asset):Unit= {
     for (child <- parentFolder.children) {
       println("Unmapping File "+child +" from "+child.presenceCondition)
       child.presenceCondition = True()
      }
  }

  def unmapFolder(annotatedFolder:Asset):Unit= {
    println("Unmapping Folder "+annotatedFolder+" from "+annotatedFolder.presenceCondition)
      annotatedFolder.presenceCondition = True()
  }

  def executeFeatureCommits(astroot:Asset, repositoryfile:File, featurecommits:List[ClaferCommit]):Unit={

}
}
