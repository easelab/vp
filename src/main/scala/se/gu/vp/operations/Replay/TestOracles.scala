package se.gu.vp.operations.Replay

import java.io.File

import se.gu.vp.model._
import se.gu.vp.operations.Utilities

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class TestOracles {

}
object TestOracles{
  def featurePresentinTarget(featurename:String,targetfm:FeatureModel):Boolean={
    if(FeatureRelatedHelpers.findFeatureByName(featurename,targetfm)!=None) true
    else false
  }
  def assetpresentintarget(file:File,targetast:Asset):Boolean={
    if(targetast.assetType == RepositoryType){
      val asset = (Utilities.transformASTToList(targetast)).find(p=>p.name.endsWith(file.getName))
      if(!asset.isEmpty)
        return true
      else false
    }
    else{
      println("Please enter a repository as target")
      false
    }
  }

}