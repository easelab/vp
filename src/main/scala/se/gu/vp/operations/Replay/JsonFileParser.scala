package se.gu.vp.operations.Replay

import java.io.{File, FileWriter}

import se.gu.vp.model._
import se.gu.vp.operations._

import scala.util.matching.Regex

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class JsonFileParser {
}
object JsonFileParser{
  def dropIndex[T](list: List[T], idx: Int): List[T] = {
    list.zipWithIndex.filter(_._2 != idx).map(_._1)
  }
  def preprocessing(featureline:String):String={

    var processed = featureline.replace("[","")
    processed = processed.replace("]","")
    processed = processed.replace("/","")
    processed = processed.replace("&"," ")
    processed = processed.replace("$","")
    processed = processed.replace("\t","")
    processed = processed.replace("{","")
    processed = processed.replace("}","")
    //processed = processed.replace(",","")
    processed
  }

  def getAnnotations(file:File,featuremodel:Option[FeatureModel]=None):List[Annotation]= {

    var fileannotations: List[Annotation] = Nil
    val source = io.Source.fromFile(file.getAbsolutePath, "ISO-8859-1")
    val filelines = source.getLines.toList
    source.close

    val beginannotation = new Regex("//\\s*[&$]\\s*[Bb]egin")
    val endingannotation = new Regex("//\\s*[&$]\\s*[Ee]nd")
    for (i <- 0 to filelines.length - 1) {
      if(beginannotation.findFirstIn(filelines(i))!=None){
        val fromindex = i
        val featureline = preprocessing(filelines(i))
        val intermediate = featureline.split(" ")
        val annotation = intermediate.slice(intermediate.indexOf("begin") + 1, intermediate.length).mkString("")
        if (!annotation.contains(",")) {
          val remaininglines = filelines.slice(i, filelines.length)
          val ending = remaininglines.find(p => endingannotation.findFirstIn(p)!=None & p.trim.endsWith(annotation.trim))
          var toindex = 0;
          if (ending == None) {
            toindex = filelines.length - 1
          }
          else {
            toindex = i + remaininglines.indexOf(ending.get)
          }
          fileannotations = new Annotation(annotation.trim, fromindex, toindex) :: fileannotations
        }
        else {
          val featurenames = annotation.split(",")
          featurenames.foreach(feature => {
            val remaininglines = filelines.slice(i, filelines.length)
            val ending = remaininglines.find(p => endingannotation.findFirstIn(p)!=None & p.trim.endsWith(annotation.trim))
            var toindex = 0;
            if (ending == None) {
              toindex = filelines.length - 1
            }
            else {
              toindex = i + remaininglines.indexOf(ending.get)
            }
            fileannotations = new Annotation(feature.trim, fromindex, toindex) :: fileannotations
          })
        }
      }
      else if(filelines(i).contains("//&line") | filelines(i).contains("//line")){
        val featureline = preprocessing(filelines(i))
        val intermediate = featureline.split(" ")
        val annotation = intermediate.slice(intermediate.indexOf("line")+1,intermediate.length).mkString("")
        if(!annotation.contains(",")) {
          fileannotations = new Annotation(annotation.trim,i,i) :: fileannotations
        }
        else{
          val featurenames = annotation.split(",")
          featurenames.foreach(feature =>  {
            fileannotations = new Annotation(feature.trim,i,i) :: fileannotations
          })
        }
      }
    }
    fileannotations
  }
  def splitBlock2Ways(block:TempBlock,start1:Int,end1:Int,start2:Int,end2:Int,feature:Feature,blocknumber:Int,bw:FileWriter):List[TempBlock]= {

    val block1 = new TempBlock("block", true, block.parent, Nil, start1, end1, True())
    block1.presenceCondition = block.presenceCondition
    if (blocknumber == 1) {
      block1.presenceCondition = block.presenceCondition | feature
      bw.write("MapAssetToFeature - code asset " + block1.name + " mapped to feature " + feature + "\n")
    }

    val block2 = new TempBlock("block", true, block.parent, Nil, start2,end2)
    block2.presenceCondition = block.presenceCondition
    if (blocknumber == 2) {
      block2.presenceCondition = block.presenceCondition | feature
      bw.write("MapAssetToFeature - code asset " + block2.name + " mapped to feature " + feature + "\n")
    }

    block.parent match {
      case None=>;
      case _=>{
        block.parent.get.childern = dropIndex(block.parent.get.childern,block.parent.get.childern.indexOf(block))
        block.parent.get.childern = block1 :: block2 :: block.parent.get.childern
        block.parent = None
      }
    }

    return block1 :: block2 :: Nil
  }
  def splitBlock3Ways(block:TempBlock,start1:Int,end1:Int,start2:Int,end2:Int,start3:Int,end3:Int,feature:Feature, bw:FileWriter):List[TempBlock]={

    val block1 = new TempBlock("block", true, block.parent, Nil,start1, end1 )
    block1.presenceCondition = block.presenceCondition

    val block2 = new TempBlock("block", true, block.parent, Nil, start2, end2)
    block2.presenceCondition = block.presenceCondition | feature
    bw.write("MapAssetToFeature - code asset "+block2.name+" mapped to feature "+feature+"\n")

    val block3 = new TempBlock("block", true, block.parent, Nil, start3, end3)
    block3.presenceCondition = block.presenceCondition

    block.parent match {
      case None=>;
      case _=>{
        block.parent.get.childern = dropIndex (block.parent.get.childern, block.parent.get.childern.indexOf (block) )
        block.parent.get.childern = block1 :: block2 :: block3 :: block.parent.get.childern
        block.parent = None
      }
    }
    return block1 :: block2 :: block3 :: Nil
  }

  def addAnnotations(blocklist:List[TempBlock],annotations:List[Annotation],featureModel: FeatureModel, bw:FileWriter):List[TempBlock]={

    var newblocklist:List[TempBlock] = blocklist

    for(annotation <- annotations.reverse) {
      var feature = FeatureRelatedHelpers.findFeatureByName(annotation.featurename, featureModel)
      if(feature == None){
        val newfeature = Feature(annotation.featurename)
        feature = Some(newfeature)
      }
      if (feature != None) {
        for (block <- newblocklist) {
          val annotationrange = annotation.beginat to annotation.endat
          val blockrange = block.start to block.end
          val common = blockrange.intersect(annotationrange)
          if (!common.isEmpty) {

            if (common.reverse.last == block.start & common.last == block.end) {
              block.presenceCondition = block.presenceCondition | feature.get
            }
            else if(common.reverse.last == block.start & common.last > block.end){
              block.presenceCondition = block.presenceCondition | feature.get
            }
            else if (common.reverse.last > block.start & common.last < block.end) {
              val newblocks = splitBlock3Ways(block, block.start, annotation.beginat - 1, annotation.beginat, annotation.endat, annotation.endat + 1, block.end,feature.get,bw)
              newblocklist = dropIndex(newblocklist,newblocklist.indexOf(block))
              newblocklist = newblocks(0) :: newblocks(1) :: newblocks(2) :: newblocklist
            }
            else if (common.reverse.last == block.start & common.last < block.end) {
              val newblocks = splitBlock2Ways(block, block.start, common.last, annotation.endat + 1, block.end,feature.get,1,bw)
              newblocklist = dropIndex(newblocklist,newblocklist.indexOf(block))
              newblocklist = newblocks(0) :: newblocks(1) :: newblocklist
            }
            else if (common.reverse.last > block.start & common.last == block.end) {
              val newblocks = splitBlock2Ways(block,block.start, annotation.beginat - 1, annotation.beginat, block.end,feature.get,2,bw)
              newblocklist = dropIndex(newblocklist,newblocklist.indexOf(block))
              newblocklist = newblocks(0) :: newblocks(1) :: newblocklist
            }
          }
        }
      }
      else println(annotation.featurename + "Feature Doesn't Exist")
    }
    newblocklist
  }

  def transformBlock(tempBlock: TempBlock, lines:List[String], parentAsset:Asset):Unit={
    val block = Asset(tempBlock.name,BlockType,Nil,tempBlock.presenceCondition,None,Some(lines.slice(tempBlock.start, tempBlock.end)))
    AddAsset(block,parentAsset)
  }

  def apply(): JsonFileParser = new JsonFileParser()

  def addJsonFile(file: File, astroot:Asset, featuremodel: FeatureModel, bw:FileWriter):Unit={

    val source = io.Source.fromFile(file.getAbsolutePath, "ISO-8859-1")
    val filelines = source.getLines.toList
    source.close

    val singleBlock = new TempBlock("block",true,None,Nil,0,filelines.length-1,True())
    var blocks:List[TempBlock] = Nil;
    blocks = singleBlock :: blocks

    val fileasset = Utilities.getassetbyname(file.getAbsolutePath,astroot)
    val annotations = getAnnotations(file)
    val assetList:List[TempBlock] = singleBlock :: Nil

    fileasset match {
      case None => println("File doesn't exist")
      case _ =>{
        val annotatedAssets:List[TempBlock] = addAnnotations(blocks,annotations,featuremodel,bw)
        annotatedAssets.foreach(c => transformBlock(c,filelines,fileasset.get))
        println("Number of annotations in " + file +" are " + annotations.distinct.size)
      }
    }
  }
}