package se.gu.vp.operations.Replay
import java.io.{File, FileWriter}
import java.util.Calendar

import se.gu.vp.model._
import se.gu.vp.operations.Experimentation.CommandExecutor
import se.gu.vp.operations.Replay.CommitHelpers.parseExcelFile
import se.gu.vp.operations.Replay.FeatureRelatedHelpers.{featurepath, findFeatureByName, indentation, pendingmappings}
import se.gu.vp.operations.Replay.StateComparators.currentRepositoryState
import se.gu.vp.operations.Utilities.{computeAssetPath, computeFeaturePath}
import se.gu.vp.operations._
import se.gu.vp.operations.Utilities._

import scala.collection.mutable.HashMap

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class ASTModificationHelpers {

}

object ASTModificationHelpers{

  val operationcount =  HashMap[String, Int]()
  val operations = List("AddAsset","RemoveAsset","ChangeAsset","MoveAsset","RenameAsset","CloneAsset",
                    "PropagateAsset","AddFeature","RemoveFeature","MoveFeature","AddFeatureModelToAsset",
                    "MapAssetToFeature","MakeFeatureOptional","CloneFeature","PropagateFeature","UnmapFeature")

  val operationfirstoccurence = HashMap[String,Int]()

  def getRepository(asset:Asset):Option[Asset] =
    asset match {
    case Asset( _, RepositoryType,_,_,_,_) => Some(asset)
    case _ => asset.parent match {
      case None => None
      case _ => getRepository(asset.parent.get)
    }
  }
  def getPartialPath(asset:Asset):Option[String]={
    getRepository(asset) match{
      case None=> None;
      case _=>{
        Some(asset.name.diff(getRepository(asset).get.name))
      }
    }
  }
  def handleFeatureModel(file:File,asset:Asset,bw:FileWriter, featureModel: Option[FeatureModel] = None):FeatureModel={

    var allfeatures:List[String] = Nil

    val source = io.Source.fromFile(file.getAbsolutePath, "ISO-8859-1")
    val lines = source.getLines.toList
    source.close

    val rootfeature = new RootFeature(lines(0).trim)
    var featuremodel = FeatureModel(rootfeature)
    allfeatures = lines(0).trim :: allfeatures

    if(featureModel != None) {
      featuremodel = featureModel.get
    }

    for (line <- lines) {
      val indent = indentation(line.replaceAll("\\s+$", ""))
      var featurename = line.trim
      if(featurename.startsWith("or ")){
        featurename = featurename.split(" ").last
      }
      val featurequalifiedpath = featurepath(line, lines).toString.trim

      val featurealreadyexists = Utilities.findFeatureByName(featurequalifiedpath, featuremodel)
      featurealreadyexists match {
        case None => {
          if (findFeatureByName(featurename, featuremodel) != None) {

            val fff= findFeatureByName(featurename,featuremodel)
            val newparentname = featurequalifiedpath.slice(0,featurequalifiedpath.lastIndexOf("-"))
            val newparentfeature = Utilities.findFeatureByName(newparentname, featuremodel)

            if(newparentfeature!=None) {
              val existingfeature = findFeatureByName(featurename, featuremodel).get
              if(existingfeature.parent!=newparentfeature)
                bw.write("Move Feature - Feature "+ existingfeature +" moved from "+existingfeature.parent+" to "+newparentfeature.get+"\n")
                MoveFeature((findFeatureByName(featurename, featuremodel).get), newparentfeature.get)
              findFeatureByName(featurename,featuremodel).get.rename(featurequalifiedpath)
            }
            println(featurename + " already exists as a sub-feature of some other feature. Feature Moving")
            allfeatures = featurequalifiedpath :: allfeatures
            var count = operationcount(getRepository(asset).get+"-"+"MoveFeature")
            count = count + 1
            operationcount += ((getRepository(asset).get+"-"+"MoveFeature") -> count)
          }
          else {
            indent match {
              case 0 => ;
              case _ => {
                if (featurequalifiedpath != "") {
                  val parentname = featurequalifiedpath.slice(0,featurequalifiedpath.lastIndexOf("-"))
                  val parentfeature = Utilities.findFeatureByName(parentname, featuremodel)

                  parentfeature match {
                    case None => ;
                    case _ => {
                      val feature = Feature(featurequalifiedpath)
                      AddFeatureToFeatureModel(feature, parentfeature.get)
                      bw.write("AddFeatureToFeatureModel - Feature added : "+feature.name+"\n")
                      var count = operationcount(getRepository(asset).get+"-"+"AddFeature")
                      count = count + 1
                      operationcount += ((getRepository(asset).get+"-"+"AddFeature") -> count)
                      allfeatures = featurequalifiedpath :: allfeatures
                    }
                  }
                }
              }
            }
          }
        }
        case _ =>{
          allfeatures = featurequalifiedpath :: allfeatures
        }
      }
    }
    for(featuremodelfeature <- Utilities.transformFMToList(featuremodel)) {
      if (!allfeatures.contains(featuremodelfeature.name) & featuremodelfeature != featuremodel.rootfeature.UNASSIGNED) {
        val root = asset.getRoot()
        root match {
          case None => ;
          case _ => {
            val path = computeFeaturePath(featuremodelfeature)
            val featurepath = Path(root.get, computeAssetPath(asset) ++ computeFeaturePath(featuremodelfeature))
            bw.write("RemoveFeature - Feature "+featuremodelfeature+" removed from feature model"+"\n")
            println("Feature "+featuremodelfeature.name+" removed")
            RemoveFeature(featurepath)
            var count = operationcount(getRepository(asset).get+"-"+"RemoveFeature")
            count = count + 1
            operationcount += ((getRepository(asset).get+"-"+"RemoveFeature") -> count)
            //remove subfeatures
          }
        }
      }
    }
    val fm = featuremodel
    if(asset.featureModel == None) {
      AddFeatureModelToAsset(asset,fm)
      bw.write("AddFeatureModelToAsset - Added Feature Model to "+asset+"\n")
        var count = operationcount(getRepository(asset).get+"-"+"AddFeatureModelToAsset")
        count = count + 1
        operationcount += ((getRepository(asset).get+"-"+"AddFeatureModelToAsset") -> count)
      }

    return fm
  }
  def handleFolderMappings(astroot:Asset,mappingsfile:File,bw:FileWriter,featureModel: Option[FeatureModel]):Unit= {

    val source = io.Source.fromFile(mappingsfile.getAbsolutePath, "ISO-8859-1")
    val lines = source.getLines.toList
    source.close

    val parentname = mappingsfile.getParent
    val parentasset = Utilities.getassetbyname(parentname, astroot)

    if (parentasset != None) {
      for (line <- lines) {
        featureModel match {
          case None => {
            val pendingmapping = new PendingMappings(parentasset.get.name,line.trim)
            pendingmappings = pendingmapping :: pendingmappings
            println("Pending mapping added between folder " + parentasset.get + " and " + line.trim)
          }
          case _ => {
            val feature = findFeatureByName(line.trim, featureModel.get)
            feature match {
              case None => {
                var count1 = operationcount(getRepository(parentasset.get).get+"-"+"AddFeature")
                count1 = count1 + 1
                operationcount += ((getRepository(parentasset.get).get+"-"+"AddFeature") -> count1)
                println("Mapped feature not found in target repository ... Adding in unassigned features")
                val addedfeature = Feature(line.trim)
                MapAssetToFeature(parentasset.get, addedfeature)
                bw.write("MapAssetToFeature - Folder "+parentasset.get+" mapped to feature "+addedfeature+"\n")
                var count = operationcount(getRepository(parentasset.get).get+"-"+"MapAssetToFeature")
                count = count + 1
                operationcount += ((getRepository(parentasset.get).get+"-"+"MapAssetToFeature") -> count)
              }
              case _ => {
                MapAssetToFeature(parentasset.get, feature.get)
                bw.write("MapAssetToFeature - Folder "+parentasset.get+" mapped to feature "+feature.get+"\n")
                var count = operationcount(getRepository(parentasset.get).get+"-"+"MapAssetToFeature")
                count = count + 1
                operationcount += ((getRepository(parentasset.get).get+"-"+"MapAssetToFeature") -> count)
              }
            }
          }
        }
      }
    }
    else println("Folder to map missing")
  }
  def ignoredFile(file:File):Boolean={
    if(file.getAbsolutePath.contains(".git") | file.getAbsolutePath.contains("CommitsGraph.txt")) true
    else false
  }

  def handleFileMappings(astroot:Asset,mappingsfile:File,bw:FileWriter,featureModel: Option[FeatureModel]):Unit= {
    val source = io.Source.fromFile(mappingsfile.getAbsolutePath, "ISO-8859-1")
    val lines = source.getLines.toList
    source.close

    val parentname = mappingsfile.getParent
    val parentasset = Utilities.getassetbyname(parentname,astroot)
    if(parentasset!=None) {
      for (line <- lines) {
        if (line.trim.endsWith(".js")) {
          val temp = line.trim()
          val splits = temp.split(" ")
          for (split <- splits) {

            val assetname = parentname + "\\" + split.trim
            val featurename = lines(lines.indexOf(line) + 1).trim

            featureModel match {
              case None => {
                pendingmappings = new PendingMappings(assetname, featurename) :: pendingmappings
              }
              case _ => {
                val asset = Utilities.getassetbyname(assetname, astroot)
                val feature = findFeatureByName(featurename, featureModel.get)
                if (asset != None & !feature.isEmpty) {
                  MapAssetToFeature(asset.get, feature.get)
                  bw.write("MapAssetToFeature - File "+asset.get+" mapped to feature "+feature.get+"\n")
                  var count = operationcount(getRepository(parentasset.get).get+"-"+"MapAssetToFeature")
                  count = count + 1
                  operationcount += ((getRepository(parentasset.get).get+"-"+"MapAssetToFeature") -> count)
                }
                else if (asset == None)
                  println("File mapping unsuccessful "+ assetname + " asset missing")
                else if (feature == None) {
                  val newfeature = Feature(featurename)
                  var count1 = operationcount(getRepository(asset.get).get+"-"+"AddFeature")
                  count1 = count1 + 1
                  operationcount += ((getRepository(asset.get).get+"-"+"AddFeature") -> count1)
                  MapAssetToFeature(asset.get,newfeature)
                  var count = operationcount(getRepository(asset.get).get+"-"+"MapAssetToFeature")
                  count = count + 1
                  operationcount += ((getRepository(asset.get).get+"-"+"MapAssetToFeature") -> count)
                  bw.write("MapAssetToFeature - File "+asset.get+" mapped to feature "+newfeature+"\n")
                }
              }
            }
          }
        }
      }
    }
  }

  def InitialAddition(astroot:Asset, assetname:String, bw:FileWriter):Unit={

    val file = new File(assetname)
    if(file.exists() & !ignoredFile(file)) {
      if (file.isDirectory) {
        val parentname = file.getParentFile.getAbsolutePath
        val parentasset = Utilities.getassetbyname(parentname, astroot)
        if (Utilities.getassetbyname(file.getAbsolutePath, astroot) == None & parentasset != None) {
          AssetHelpers.createFolder(file, parentasset.get)

          val folderasset = Utilities.getassetbyname(file.getAbsolutePath,astroot).get
          val partialpath = getPartialPath(folderasset)
          var count = operationcount(getRepository(folderasset).get+"-"+"AddAsset")
          count = count + 1
          operationcount += ((getRepository(folderasset).get+"-"+"AddAsset") -> count)
          bw.write("AddAsset - Folder "+ folderasset +" added to "+folderasset.parent.get+"\n")
        }
        else if (parentasset==None) println("Parent asset missing for "+file.getAbsolutePath)
      }
      else if (file.isFile) {
        val parentname = file.getParentFile.getAbsolutePath
        val parentasset = Utilities.getassetbyname(parentname, astroot)
        if (Utilities.getassetbyname(file.getAbsolutePath, astroot) == None & parentasset != None) {
          AssetHelpers.createFile(file, parentasset.get)

          val fileasset = Utilities.getassetbyname(file.getAbsolutePath,astroot).get
          val partialpath = getPartialPath(fileasset)
          var count = operationcount(getRepository(fileasset).get+"-"+"AddAsset")
          count = count + 1
          operationcount += ((getRepository(fileasset).get+"-"+"AddAsset") -> count)
          bw.write("AddAsset - File "+ fileasset +" added to "+fileasset.parent.get+"\n")

          if (file.getName.toString.endsWith("js") & ASTModificationHelpers.nonprojectFile(file.getAbsolutePath) == false) {
            addCodeFile(file,astroot)
          }
          else if (!file.getName.endsWith(".js") | ASTModificationHelpers.nonprojectFile(file.getAbsolutePath) == true) {
            addNonCodeFile(file,astroot)
          }
        }
        else if (parentasset==None) println("Parent asset missing for "+file.getAbsolutePath)
      }
    }
  }

  def loadInitialState(operationalfile:File,astroot:Asset,bw:FileWriter):Unit= {
    val assets = currentRepositoryState(operationalfile)
    assets.foreach(asset => InitialAddition(astroot,asset,bw))
  }

  def nonprojectFile(filename:String):Boolean={
    if(filename.contains("jquery") | filename.contains("\\ace\\") | filename.contains("\\chosen\\"))
      true
    else false
  }

  def addNonCodeFile( file:File , astroot:Asset ):Unit={
    val source = io.Source.fromFile(file.getAbsolutePath, "ISO-8859-1")
    val countlines = source.getLines.size
    val block = new Asset("block",BlockType,Nil,True(), None, Some(source.getLines.toList))
    source.close

    val parent = Utilities.getassetbyname( file.getAbsolutePath , astroot )
    if(parent != None){
      AddAsset(block, parent.get)
    }
  }
  def addCodeFile(file: File,astroot:Asset):Unit={

    val source = io.Source.fromFile(file.getAbsolutePath, "ISO-8859-1")
    val filelines = source.getLines.toList
    source.close

    val fileasset = Utilities.getassetbyname(file.getAbsolutePath,astroot)
    fileasset match {
      case None => println("File doesn't exist")
      case _ =>{
        CodeFileParser.addCodeFile(file:File, fileasset.get,astroot)
      }
    }
  }
  def fileAddition(astroot:Asset, assetname:String,bw:FileWriter):Unit={

    val file = new File(assetname)
      if(file.exists() & !ignoredFile(file)) {
        if (file.isDirectory) {
          val parentname = file.getParentFile.getAbsolutePath
          val parentasset = Utilities.getassetbyname(parentname, astroot)
          if (Utilities.getassetbyname(file.getAbsolutePath, astroot) == None & parentasset != None) {
            AssetHelpers.createFolder(file, parentasset.get)

            val folderasset = Utilities.getassetbyname(file.getAbsolutePath,astroot).get
            val partialpath = getPartialPath(folderasset)
            val alreadyExisting = Utilities.transformASTToList(astroot).filterNot(p=>p.assetType==VPRootType).filterNot(p=>p.assetType==RepositoryType).filter(p=>getPartialPath(p).get == partialpath.get).filter(p=>p!=folderasset)
              bw.write("AddAsset - Folder "+ folderasset +" added to "+folderasset.parent.get+"\n")
              var count = operationcount(getRepository(folderasset).get+"-"+"AddAsset")
              count = count + 1
              operationcount += ((getRepository(folderasset).get+"-"+"AddAsset") -> count)

            if(file.getAbsolutePath.endsWith("commons")){
              loadInitialState(file, astroot,bw)
            }
          }
          else if (parentasset==None) println("Parent asset missing for "+file.getAbsolutePath)
        }
        else if (file.isFile) {
          val parentname = file.getParentFile.getAbsolutePath
          val parentasset = Utilities.getassetbyname(parentname, astroot)
          if (Utilities.getassetbyname(file.getAbsolutePath, astroot) == None & parentasset != None) {
            AssetHelpers.createFile(file, parentasset.get)

            val fileasset = Utilities.getassetbyname(file.getAbsolutePath,astroot).get
            val partialpath = getPartialPath(fileasset)

              bw.write("AddAsset - File "+ fileasset +" added to "+fileasset.parent.get+"\n")
              var count = operationcount(getRepository(fileasset).get+"-"+"AddAsset")
              count = count + 1
              operationcount += ((getRepository(fileasset).get+"-"+"AddAsset") -> count)

              if (file.getName.toString.endsWith("js") & nonprojectFile(file.getAbsolutePath) == false) {
                val featuremodel = Utilities.getImmediateAncestorFeatureModel(parentasset)
                featuremodel match {
                  case None=>addCodeFile(file,astroot)
                  case _=>{
                      JavaScriptFileParser.addAnnotatedFile(file, astroot, featuremodel.get, bw)
                  }
                }
              }
              else if (file.getName.toString.endsWith("json") & nonprojectFile(file.getAbsolutePath) == false) {
                val featuremodel = Utilities.getImmediateAncestorFeatureModel(parentasset)
                featuremodel match {
                  case None=>addNonCodeFile(file,astroot)
                  case _=>{
                    JsonFileParser.addJsonFile(file, astroot, featuremodel.get, bw)
                  }
                }
              }
              else if (!file.getName.endsWith(".js") | nonprojectFile(file.getAbsolutePath) == true) {
                addNonCodeFile(file,astroot)
              }
            }
            else if (parentasset==None) println("Parent asset missing for "+file.getAbsolutePath)
        }
      }
  }

  def fixMappings(file:File,astroot:Asset):Unit= {
    val featurefiles = currentRepositoryState(file).filter(p => isFeatureFile(p))
    for (featurefile <- featurefiles) {
      if (featurefiles.endsWith(".vp-folder")) {
        val ffile = new File(featurefile)
        val asset = Utilities.getassetbyname(ffile.getParentFile.getAbsolutePath, astroot)
        if (asset != None) {
          val closestfm = Utilities.getImmediateAncestorFeatureModel(asset)
          FixMapping.fixFolderMappings(astroot, ffile, closestfm)
        }
      }
      else if (featurefiles.endsWith(".vp-files")) {
        val ffile = new File(featurefile)
        val asset = Utilities.getassetbyname(ffile.getParentFile.getAbsolutePath, astroot)
        if (asset != None) {
          val closestfm = Utilities.getImmediateAncestorFeatureModel(asset)
          FixMapping.handleFileMappings(astroot, ffile, closestfm)
        }
      }
    }
  }
  def fileModification(astroot:Asset, assettomodify:String,bw:FileWriter):Unit= {

    val modificationasset = Utilities.getassetbyname(assettomodify,astroot)
    modificationasset match {
      case None=>;
      case _=>{
        if (CommitHelpers.isClone(modificationasset.get) != None) {
          val trace = CommitHelpers.isClone(modificationasset.get).get
          if (trace.source.versionNumber > modificationasset.get.versionNumber) {
            bw.write("PropagateAsset - changes propagated to "+modificationasset+"\n"+" from "+trace.source+"\n")
            bw.write("Version of source = "+trace.source.versionNumber+"\n")
            var count = operationcount (getRepository (trace.source).get + "-" + "PropagateAsset")
            count = count + 1
            operationcount += ((getRepository (trace.source).get + "-" + "PropagateAsset") -> count)
            if(modificationasset.get.mappedToFeatures.size>0){
              bw.write("PropagateFeature - Asset changes might be related to feature propagation")
              var count = operationcount (getRepository (trace.source).get + "-" + "PropagateFeature")
              count = count + 1
              operationcount += ((getRepository (trace.source).get + "-" + "PropagateFeature") -> count)
            }
          }
        }
      }
    }
    val file = new File(assettomodify)
    if (file.exists()) {

      if (file.isDirectory) {
        val filetomodify = Utilities.getassetbyname(file.getAbsolutePath, astroot)
        if(filetomodify!=None){
          ChangeAsset(filetomodify.get)
          filetomodify.get.children.foreach(c=> RemoveAsset(astroot,c))
          bw.write("ChangeAsset - Folder modified ... Reloading "+filetomodify.get+"\n")
          loadInitialState(file,astroot,bw)
          fixMappings(file,astroot)
        }
      }
      else if (file.isFile) {

        val filetomodify = Utilities.getassetbyname(file.getAbsolutePath, astroot)
        val parentname = file.getParentFile.getAbsolutePath
        val parentasset = Utilities.getassetbyname(parentname,astroot)
        if(parentasset!=None & filetomodify != None){

          if(filetomodify!=None) ChangeAsset(filetomodify.get)
          bw.write("ChangeAsset - File modified ... Reloading sub-assets of "+filetomodify.get+"\n")
          filetomodify.get.children.foreach(c=>c.parent = None)
          filetomodify.get.children = Nil

          if (file.getName.toString.endsWith("js") & nonprojectFile(file.getAbsolutePath) == false) {
            val featuremodel = Utilities.getImmediateAncestorFeatureModel(parentasset)
            if(featuremodel!=None){
              JavaScriptFileParser.addAnnotatedFile (file, astroot, featuremodel.get, bw)
            }
            else addCodeFile(file, astroot)
          }
          else if (file.getName.toString.endsWith("json") & nonprojectFile(file.getAbsolutePath) == false) {
            val featuremodel = Utilities.getImmediateAncestorFeatureModel(parentasset)
            if(featuremodel!=None){
              JsonFileParser.addJsonFile(file, astroot, featuremodel.get, bw)
            }
            else addNonCodeFile(file, astroot)
          }
          else if (!file.getName.endsWith(".js") | nonprojectFile(file.getAbsolutePath) == true) {
            addNonCodeFile(file, astroot)
        }
      }
        else if(parentasset==None){
          fileAddition(astroot,parentname,bw)
          loadInitialState(file.getParentFile,astroot,bw)
        }
        else{
          fileAddition(astroot,file.getAbsolutePath,bw)
        }
    }
      val filetomodify = Utilities.getassetbyname(file.getAbsolutePath, astroot)
      filetomodify match {
        case None => ;
        case _ => {
          var count = operationcount(getRepository(filetomodify.get).get + "-" + "ChangeAsset")
          count = count + 1
          operationcount += ((getRepository(filetomodify.get).get + "-" + "ChangeAsset") -> count)
        }
      }
    }
  }
  def modifyFeatureMapping(astroot:Asset,file:File,bw:FileWriter):Unit={
    if(file.getAbsolutePath.endsWith(".vp-folder")){
      val asset = Utilities.getassetbyname(file.getParentFile.getAbsolutePath,astroot)
      if(asset!=None) {
        val closestfm = Utilities.getImmediateAncestorFeatureModel(asset)
        handleFolderMappings(astroot,file,bw,closestfm)
      }
    }
    else if(file.getAbsolutePath.endsWith(".vp-files")){
      val asset = Utilities.getassetbyname(file.getParentFile.getAbsolutePath,astroot)
      if(asset!=None) {
        val closestfm = Utilities.getImmediateAncestorFeatureModel(asset)
        handleFileMappings(astroot,file,bw,closestfm)
      }
    }
  }

  def addFeatureFile(astroot:Asset,file:File,bw:FileWriter):Unit={
    if(file.getAbsolutePath.endsWith(".vp-project") | file.getAbsolutePath.endsWith("FeatureModel.cfr")){
        val asset = Utilities.getassetbyname(file.getParentFile.getAbsolutePath,astroot)
        if(asset!=None) handleFeatureModel(file,asset.get,bw)
    }
    else if(file.getAbsolutePath.endsWith(".vp-folder")){
      val asset = Utilities.getassetbyname(file.getParentFile.getAbsolutePath,astroot)
      if(asset!=None) {
        val closestfm = Utilities.getImmediateAncestorFeatureModel(asset)
        handleFolderMappings(astroot,file,bw,closestfm)
      }
    }
    else if(file.getAbsolutePath.endsWith(".vp-files")){
      val asset = Utilities.getassetbyname(file.getParentFile.getAbsolutePath,astroot)
      if(asset!=None) {
        val closestfm = Utilities.getImmediateAncestorFeatureModel(asset)
        handleFileMappings(astroot,file,bw,closestfm)
      }
    }
  }

  def fileDeletion(astroot:Asset, filename:String, bw:FileWriter):Unit= {
    val file = new File(filename)
    val filetodelete = Utilities.getassetbyname(filename, astroot)
    if(filetodelete!=None) {
      var count = operationcount(getRepository(filetodelete.get).get+"-"+"RemoveAsset")
      count = count + 1
      operationcount += ((getRepository(filetodelete.get).get+"-"+"RemoveAsset") -> count)
      bw.write("RemoveAsset - Asset Deleted : "+filetodelete.get+"\n")
      if(filetodelete.get.mappedToFeatures.size > 0){
        var count = operationcount(getRepository(filetodelete.get).get+"-"+"UnmapFeature")
        count = count + filetodelete.get.mappedToFeatures.size
        operationcount += ((getRepository(filetodelete.get).get+"-"+"UnmapFeature") -> count)
        }
      RemoveAsset(astroot,filetodelete.get)
    }
  }
  def removeFeatureModel(astroot:Asset, asset:String):Unit={
    val fmassetpath = asset.slice(0,asset.lastIndexOf("\\"))
    val fmasset = Utilities.getassetbyname(fmassetpath,astroot)
    if(fmasset.isDefined && fmasset.get.featureModel.isDefined) {
      val fm = fmasset.get.featureModel.get
      fm.containingAsset = None
      fmasset.get.featureModel = None
    }
  }

  def unmapFiles(astroot:Asset, asset:String,bw:FileWriter):Unit= {

    val folderpath = asset.slice(0,asset.lastIndexOf("\\"))
    val parentasset = Utilities.getassetbyname(folderpath,astroot)
    if(parentasset!=None) {
      for (child <- parentasset.get.children) {

        var count = operationcount(getRepository(child).get+"-"+"UnmapFeature")
        count = count + child.mappedToFeatures.size
        operationcount += ((getRepository(child).get+"-"+"UnmapFeature") -> count)
        if(child.mappedToFeatures.size>0) {
          bw.write("Unmapping File " + child + " from " + child.presenceCondition+"\n")
          println("Unmapping File " + child + " from " + child.presenceCondition)
        }
      }
    }
    else println("Folder also deleted with mapping file .vp-files")
  }

  def unmapFolder(astroot:Asset, asset:String,bw:FileWriter):Unit= {
    val folderpath = asset.slice(0,asset.lastIndexOf("\\"))
    val parentasset = Utilities.getassetbyname(folderpath,astroot)
    if(parentasset!=None) {
      var count = operationcount(getRepository(parentasset.get).get+"-"+"UnmapFeature")
      count = count + parentasset.get.mappedToFeatures.size
      operationcount += ((getRepository(parentasset.get).get+"-"+"UnmapFeature") -> count)
        println("Unmapping Folder " + parentasset.get + " from " + parentasset.get.presenceCondition)
      if(parentasset.get.mappedToFeatures.size>0)
        bw.write("Unmapping Folder "+parentasset.get+" from "+parentasset.get.presenceCondition+"\n")
    }
    else println("Folder also deleted along wit mapping file .vp-folder")
  }

  def fileRenaming(astroot:Asset, repositoryname:String, source:String, destination:String,bw:FileWriter):Unit={

    val sourcepath = astroot.name + "\\" + repositoryname + "\\" + source.replace("/","""\""")
    val destinationpath = astroot.name + "\\" + repositoryname + "\\" + destination.replace("/","""\""")
    val sourcefile = new File(sourcepath)
    val destinationfile = new File(destinationpath)
    fileAddition(astroot,repositoryname,bw)
  }

  def getCommit(commits:List[ClaferCommit],commitid:String):ClaferCommit={
    val commit = commits.find(p=>p.commitid == commitid)
    if(!commit.isEmpty)
      return commit.get
    else null
  }

  def isFeatureModel(filename:String):Boolean={
    if(filename.trim.endsWith(".vp-project") | filename.trim.endsWith("FeatureModel.cfr"))
      true
    else false
  }

  def isFeatureFile(filename:String):Boolean={
    if(filename.trim.endsWith(".vp-project") | filename.trim.endsWith("FeatureModel.cfr") | filename.trim.endsWith(".vp-folder") | filename.trim.endsWith(".vp-files"))
      true
    else false
  }
  def firstCommit(commit:ClaferCommit,commits:List[ClaferCommit]):Boolean={
    val repocommits = commits.filter(_.repository==commit.repository)
    if(repocommits.indexOf(commit)==0)
      true
    else false
  }

  def getName(path:String):String={
    return path.slice(path.lastIndexOf("\\")+1,path.length)
  }
  def writeasttofile(astroot:Asset, bw:FileWriter){
    bw.write( getName(astroot.name) + " Version :" + astroot.versionNumber +"\n")
    astroot.children foreach(c=>writeasttofile(c,bw,1) )
  }

  def writeasttofile(child:Asset, bw:FileWriter,nestingLevel: Int){
    bw.write(("  ".toString * nestingLevel) +  getName(child.name) + " Version :" + child.versionNumber +"\n")
    child.children foreach(c=>writeasttofile(c,bw,nestingLevel+1) )
  }

  def traceExists(source:Asset,target:Asset):Boolean={
    for(trace<-TraceDatabase.traces){
      if(trace.source==source && trace.target == target){
        true
      }
    }
    false
  }
  def getRepoPath(asset:Asset):String={
   return asset.name diff(getRepository(asset).get.name)
  }

  def loadAST(astroot:Asset, originalpath:String, astcommits:List[ClaferCommit]):Unit={

    val start = Calendar.getInstance().getTime()
    val featureoperationsfile = new File("C:\\Experiment\\data.csv")
    val featureoperations = parseExcelFile(featureoperationsfile)

    val rootexperiment = new File("C:\\Experiment\\ExperimentData\\")
    val commandexecutor0 = new CommandExecutor
    commandexecutor0.cleanDirectory(rootexperiment.getAbsolutePath)
    if(!rootexperiment.exists())  rootexperiment.mkdir()

    for(astcommit <- astcommits) {

      val repositoryexp = new File(rootexperiment.getAbsolutePath+File.separator+astcommit.repository+File.separator)
      if(!repositoryexp.exists()) repositoryexp.mkdir()

      val filename = new File(astroot.name + """\""" + astcommit.repository)
      if (Utilities.getassetbyname(filename.getAbsolutePath, astroot) == None) {
        AssetHelpers.createRepository(filename, astroot)
        //Initializing count of every operation as 0 for every repository
        //1 for add asset
        for(operation<-operations){
          operationcount += ((filename+"-"+operation) -> 0)
        }
        operationcount += ((filename+"-"+operations(0)) -> 1)
      }
      val dataaddedassets = operationcount(getRepository(Utilities.getassetbyname(filename.getAbsolutePath, astroot).get).get+"-"+"AddAsset")
      val dataremovedassets = operationcount(getRepository(Utilities.getassetbyname(filename.getAbsolutePath, astroot).get).get+"-"+"RemoveAsset")
      val datachangedassets = operationcount(getRepository(Utilities.getassetbyname(filename.getAbsolutePath, astroot).get).get+"-"+"ChangeAsset")
      val datamovedassets = operationcount(getRepository(Utilities.getassetbyname(filename.getAbsolutePath, astroot).get).get+"-"+"MoveAsset")
      val datarenamedassets = operationcount(getRepository(Utilities.getassetbyname(filename.getAbsolutePath, astroot).get).get+"-"+"RenameAsset")
      val dataclonedassets = operationcount(getRepository(Utilities.getassetbyname(filename.getAbsolutePath, astroot).get).get+"-"+"CloneAsset")
      val datapropagateassets = operationcount(getRepository(Utilities.getassetbyname(filename.getAbsolutePath, astroot).get).get+"-"+"PropagateAsset")
      val dataaddedfeatures = operationcount(getRepository(Utilities.getassetbyname(filename.getAbsolutePath, astroot).get).get+"-"+"AddFeature")
      val dataremovedfeatures = operationcount(getRepository(Utilities.getassetbyname(filename.getAbsolutePath, astroot).get).get+"-"+"RemoveFeature")
      val datamovedfeatures = operationcount(getRepository(Utilities.getassetbyname(filename.getAbsolutePath, astroot).get).get+"-"+"MoveFeature")
      val dataaddfeaturetofm = operationcount(getRepository(Utilities.getassetbyname(filename.getAbsolutePath, astroot).get).get+"-"+"AddFeatureModelToAsset")
      val datamappatof = operationcount(getRepository(Utilities.getassetbyname(filename.getAbsolutePath, astroot).get).get+"-"+"MapAssetToFeature")
      val dataclonedfeature = operationcount(getRepository(Utilities.getassetbyname(filename.getAbsolutePath, astroot).get).get+"-"+"CloneFeature")
      val datapropagatefeature = operationcount(getRepository(Utilities.getassetbyname(filename.getAbsolutePath, astroot).get).get+"-"+"PropagateFeature")
      val dataunmap = operationcount(getRepository(Utilities.getassetbyname(filename.getAbsolutePath, astroot).get).get+"-"+"UnmapFeature")

      val originalrepository = new File(originalpath + File.separator + astcommit.repository + "\\")
      val operationalfile = new File(astroot.name.trim + "\\" + astcommit.repository + "\\")
      if (!operationalfile.exists()) operationalfile.mkdir()
      val commandexecutor1 = new CommandExecutor
      commandexecutor1.cleanDirectory(operationalfile.getAbsolutePath)
      commandexecutor1.copyDirectory(originalrepository.getAbsolutePath, astroot.name.trim + "\\" + astcommit.repository + "\\")

      val commandexecutor = new CommandExecutor()
      if (operationalfile.exists()) {
        commandexecutor.gitHardReset(operationalfile, astcommit.commitid.trim)
      }
      else {
        println("Error - Repository File Doesn't Exist")
      }

      val commitindex = astcommits.filter(_.repository ==astcommit.repository).indexOf(astcommit)
      println(" \n======= Current Commit "+ (commitindex+1) + " - " + astcommit.repository + " - " + astcommit.commitid + " =========\n")

      //Making a directory for logging output for each commit.
      val commitdirectoryexp = new File(repositoryexp.getAbsolutePath+File.separator+(commitindex+1)+"-" + astroot.versionNumber + "-" + astcommit.commitid+File.separator)
      if(!commitdirectoryexp.exists()) commitdirectoryexp.mkdir()

      val astprintfile = new File(commitdirectoryexp.getAbsolutePath+File.separator+"ASTprint.txt")
      val bwast = new FileWriter(astprintfile,true)
      val commitdetailedlog = new File(commitdirectoryexp.getAbsolutePath+File.separator+"commitdetaillog.txt")
      val cdet = new FileWriter(commitdetailedlog,true)

      //Problem: develoeprs could already add an annotated file or a feature model in the first commit
      if (commitindex==0) {
        cdet.write("First Commit for "+astcommit.repository+"\n")
        loadInitialState(operationalfile, astroot,cdet)
        if(operationalfile.getAbsolutePath.endsWith("ClaferToolsUICommonPlatform")){
          val repoasset = Utilities.getassetbyname(operationalfile.getAbsolutePath,astroot)
          if(repoasset!=None){
            val fmfile = new File("C:\\Experiment\\.vp-project")
            handleFeatureModel(fmfile, repoasset.get,cdet)
          }
        }
      }
      else {
        val previouscommit = astcommits(astcommits.indexOf(astcommit) - 1)
        if (previouscommit.repository == astcommit.repository) {
          val commandexecutor = new CommandExecutor()
          val path = new File(astroot.name.trim + "\\" + astcommit.repository)
          if (path.exists()) {
            commandexecutor.getDiffFromTwoCommits(path.getAbsolutePath, previouscommit.commitid, astcommit.commitid)
          }
          else {
            println("Problem Here ... ")
          }

          val diffFile = new File("C:\\Experiment\\diff.txt")
          if (diffFile.exists()) {

            val addedassets = DiffOperations.getAddedAssets(diffFile).sortBy(_.length)
            val modifiedassets = DiffOperations.getModifiedAssets(diffFile)
            val deletedassets = DiffOperations.getDeletedAssets(diffFile)
            val renamedassets = DiffOperations.getRenamedAssets(diffFile)
            val addedafterrename = renamedassets.values

            //Feature Model addition
            addedassets.foreach(addedasset => {
              if (isFeatureModel(addedasset)) {
                val featuremodelfilepath = operationalfile.getAbsolutePath + File.separator + addedasset
                val featuremodelfile = new File(featuremodelfilepath)
                if (featuremodelfile.exists()) {
                  val assetwithfm = Utilities.getassetbyname(featuremodelfile.getParentFile.getAbsolutePath, astroot)
                  if (assetwithfm != None) {
                    if (assetwithfm.get.featureModel != None) {
                      handleFeatureModel(featuremodelfile, assetwithfm.get,cdet,assetwithfm.get.featureModel)
                      println("Feature Model Added")
                    }
                    else {
                      handleFeatureModel(featuremodelfile, assetwithfm.get,cdet)
                    }
                  }
                  else
                    println("Folder containing feature model absent")
                }
              }
            })

            //Feature Model Modification
            modifiedassets.foreach(modifiedasset => {
              if (isFeatureModel(modifiedasset)) {
                val featuremodelfilepath = operationalfile.getAbsolutePath + File.separator + modifiedasset
                val featuremodelfile = new File(featuremodelfilepath)
                if (featuremodelfile.exists()) {
                  val assetwithfm = Utilities.getassetbyname(featuremodelfile.getParentFile.getAbsolutePath, astroot)
                  val featuremodel = assetwithfm.get.featureModel
                  if (assetwithfm != None)
                    handleFeatureModel(featuremodelfile, assetwithfm.get,cdet, featuremodel)
                  else println("Folder containing feature model absent")
                }
              }
            })

            //Asset Addition
            addedassets.foreach(addedasset => {
              if (!isFeatureFile(addedasset)) {
                val assetstoadd = StateComparators.getAllAssets(addedasset)
                for (assettoadd <- assetstoadd) {
                  val filepath = operationalfile.getAbsolutePath + File.separator + assettoadd.replace("/", "\\")
                  fileAddition(astroot, filepath, cdet)
                }
              }
            })

            //AssetAddition After Renaming
            addedafterrename.foreach(addedasset => {
              if (!isFeatureFile(addedasset)) {
                val assetstoadd = StateComparators.getAllAssets(addedasset)
                for (assettoadd <- assetstoadd) {
                  val filepath = operationalfile.getAbsolutePath + File.separator + assettoadd.replace("/", "\\")
                  fileAddition(astroot, filepath,cdet)

                }
              }
            })
            //Count renames and move assets
            for((source,destination)<-renamedassets){
              val assetname = operationalfile.getAbsolutePath + File.separator + destination.replace("/", "\\")
              val asset = Utilities.getassetbyname(assetname,astroot)
              val sourceparent = source.slice(0,source.lastIndexOf("/")).trim
              val destinationparent = destination.slice(0,destination.lastIndexOf("/")).trim
              if(asset!=None) {
                if (sourceparent == destinationparent) {
                  cdet.write("RenameAsset - Asset "+ source +" renamed to "+destination+"\n")
                  var count = operationcount(getRepository(asset.get).get+"-"+"RenameAsset")
                  count = count + 1
                  operationcount += ((getRepository(asset.get).get+"-"+"RenameAsset") -> count)
                }
                else {
                  cdet.write("MoveAsset - Asset "+ source +" moved to "+ destinationparent +"\n")
                  var count = operationcount(getRepository(asset.get).get+"-"+"MoveAsset")
                  count = count + 1
                  operationcount += ((getRepository(asset.get).get+"-"+"MoveAsset") -> count)
                }
              }
              else{
                println("Renamed asset does not exist " + assetname)
              }
            }

            //Asset Modification
            modifiedassets.foreach(modifiedasset => {
              if (!isFeatureFile(modifiedasset)) {
                val assettomodify = operationalfile.getAbsolutePath + File.separator + modifiedasset
                fileModification(astroot, assettomodify,cdet)
              }
            })

            //Adding Mappings for Files and Folders
            addedassets.foreach(addedasset => {
              if (isFeatureFile(addedasset) & !isFeatureModel(addedasset)) {
                val mappingfilepath = operationalfile.getAbsolutePath + File.separator + addedasset
                val mappingfile = new File(mappingfilepath)
                if (mappingfile.exists()) addFeatureFile(astroot, mappingfile,cdet)
                else println("Mapping File Doesn't Exist " + mappingfilepath)
              }
            })

            //Modifying Mappings for Files and Folders
            modifiedassets.foreach(modifiedasset => {
              if (isFeatureFile(modifiedasset) & !isFeatureModel(modifiedasset)) {
                val assettomodify = operationalfile.getAbsolutePath + File.separator + modifiedasset
                val modifiedfile = new File(assettomodify)
                if (modifiedfile.exists()) addFeatureFile(astroot, modifiedfile,cdet)
              }
            })

            //Deleting Assets
            deletedassets.foreach(deletedasset => {
              if (!isFeatureFile(deletedasset)) {
                val deletedassetpath = operationalfile.getAbsolutePath + File.separator + deletedasset
                fileDeletion(astroot, deletedassetpath,cdet)
              }
            })
          }
          else println("Diff File Doesn't Exist")
        }
      }


      if(CommitHelpers.isTarget(featureoperations,astcommit)!=None){
        println("This commit is a target commit")
        val commits = CommitHelpers.isTarget(featureoperations,astcommit).get
        for(commit <- commits){
          if(commit.operation.contains("Cloning")){
            val commitfeature = commit.feature
            val assetwithfm = Utilities.getassetbyname(filename.getAbsolutePath,astroot)
            assetwithfm match{
              case None=>println("Target asset with Feature Model not found");
              case _=>{
                assetwithfm.get.featureModel match{
                  case None=>
                    println("Target asset found but doesn't have Feature Model");
                  case _=>{
                    val featurefound = findFeatureByLastName(commitfeature,assetwithfm.get.featureModel.get)
                    featurefound match{
                      case None=>println("Cloned Feature Not Found");
                      case _=>{
                        val sourcereponame = astroot.name+ """\""" +commit.sourcerepo
                        val sourcerepo = Utilities.getassetbyname(sourcereponame,astroot)
                        if(sourcerepo!=None){
                         if(sourcerepo.get.featureModel!=None){
                          val sourcefeature = findFeatureByLastName(commitfeature,sourcerepo.get.featureModel.get)
                           if(sourcefeature!=None){

                             cdet.write("Feature " + sourcefeature.get + " cloned from "+sourcerepo.get+" to "+assetwithfm.get+"\n")
                             cdet.write("Mapped assets of "+sourcefeature.get+" in source "+sourcefeature.get.mappedAssets(sourcerepo.get)+"\n")
                             cdet.write("Mapped assets of "+featurefound.get+" in destination "+featurefound.get.mappedAssets(assetwithfm.get)+"\n")
                             cdet.write("Adding trace between "+sourcefeature.get+" and "+featurefound.get+"\n")

                             var count1 = operationcount(sourcerepo.get+"-"+"CloneFeature")
                             count1 = count1 + 1
                             operationcount += ((sourcerepo.get+"-"+"CloneFeature") -> count1)

                             var count2 = operationcount(assetwithfm.get+"-"+"CloneFeature")
                             count2 = count2 + 1
                             operationcount += ((assetwithfm.get+"-"+"CloneFeature") -> count2)

                             println("Feature " + sourcefeature.get + " cloned from "+sourcerepo.get+" to "+assetwithfm.get)
                             println("Mapped assets of "+sourcefeature.get+" in source "+sourcefeature.get.mappedAssets(sourcerepo.get))
                             println("Mapped assets of "+featurefound.get+" in destination "+featurefound.get.mappedAssets(assetwithfm.get))
                             println("Adding trace ... ")
                             FeatureTraceDatabase.traces = FeatureTrace(sourcefeature.get,featurefound.get,CloneOf, sourcefeature.get.versionNumber) :: FeatureTraceDatabase.traces

                             for(mappedassettosource <- sourcefeature.get.mappedAssets(sourcerepo.get)){

                                 val targetasset = Utilities.transformASTToList(assetwithfm.get).filter(p=>getName(p.name)== getName(mappedassettosource.name))
                                 if(!targetasset.isEmpty){
                                   var count4 = operationcount(assetwithfm.get+"-"+"CloneAsset")
                                   count4 = count4 + 1
                                   operationcount += ((assetwithfm.get+"-"+"CloneAsset") -> count4)
                                   TraceDatabase.traces = Trace(mappedassettosource,targetasset(0),CloneOf,mappedassettosource.versionNumber) :: TraceDatabase.traces
                                   println("Asset Cloned correpsonding to cloned feature "+sourcefeature.get.name)
                                   println("Source : " + mappedassettosource + " Target :"+targetasset(0))

                                   cdet.write("Asset Cloned correpsonding to cloned feature "+sourcefeature.get.name)
                                   cdet.write("Source : " + mappedassettosource + " Target :"+targetasset(0))
                                 }
                                 }
                               }
                           }
                           else {
                             println("Source of target not found "+sourcereponame)
                           }
                         }
                        }
                      }
                    }
                  }
              }
            }
          }
        }
      }

      val currentrepository = Utilities.getassetbyname(filename.getAbsolutePath, astroot).get
      val one = operationcount(getRepository(currentrepository).get+"-"+"AddAsset") - dataaddedassets
      val two = operationcount(getRepository(currentrepository).get+"-"+"RemoveAsset") - dataremovedassets
      val three = operationcount(getRepository(currentrepository).get+"-"+"ChangeAsset") - datachangedassets
      val four = operationcount(getRepository(currentrepository).get+"-"+"MoveAsset") - datamovedassets
      val five = operationcount(getRepository(currentrepository).get+"-"+"RenameAsset") - datarenamedassets
      val six = operationcount(getRepository(currentrepository).get+"-"+"CloneAsset") - dataclonedassets
      val seven = operationcount(getRepository(currentrepository).get+"-"+"PropagateAsset") - datapropagateassets
      val eight = operationcount(getRepository(currentrepository).get+"-"+"AddFeature") - dataaddedfeatures
      val nine = operationcount(getRepository(currentrepository).get+"-"+"RemoveFeature") - dataremovedfeatures
      val ten = operationcount(getRepository(currentrepository).get+"-"+"MoveFeature") - datamovedfeatures
      val eleven = operationcount(getRepository(currentrepository).get+"-"+"AddFeatureModelToAsset") - dataaddfeaturetofm
      val twelve = operationcount(getRepository(currentrepository).get+"-"+"MapAssetToFeature") - datamappatof
      val thirteen = operationcount(getRepository(currentrepository).get+"-"+"CloneFeature") - dataclonedfeature
      val forteen = operationcount(getRepository(currentrepository).get+"-"+"PropagateFeature") - datapropagatefeature
      val fifteen = operationcount(getRepository(currentrepository).get+"-"+"UnmapFeature") - dataunmap

      print("Commit Summary : ")
      if(one>0){
        print("Added Assets = "+one+" ")
        if(!operationfirstoccurence.isDefinedAt(getRepository(currentrepository).get+"-"+"AddAsset"))
          operationfirstoccurence+= ((getRepository(currentrepository).get+"-"+"AddAsset") -> (commitindex+1))
      }
      if(two>0) {
        print("Removed Assets = "+two+" ")
        if(!operationfirstoccurence.isDefinedAt(getRepository(currentrepository).get+"-"+"RemoveAsset"))
          operationfirstoccurence+= ((getRepository(currentrepository).get+"-"+"RemoveAsset") -> (commitindex+1))
      }
      if(three>0) {
        print("Modified Assets = "+three+" ")
        if(!operationfirstoccurence.isDefinedAt(getRepository(currentrepository).get+"-"+"ChangeAsset"))
          operationfirstoccurence+= ((getRepository(currentrepository).get+"-"+"ChangeAsset") -> (commitindex+1))
      }
      if(four>0) {
        print("Moved Assets = "+four+" ")
        if(!operationfirstoccurence.isDefinedAt(getRepository(currentrepository).get+"-"+"MoveAsset"))
          operationfirstoccurence+= ((getRepository(currentrepository).get+"-"+"MoveAsset") -> (commitindex+1))
      }
      if(five>0) {
        print("Renamed Assets = "+five+" ")
        if(!operationfirstoccurence.isDefinedAt(getRepository(currentrepository).get+"-"+"RenameAsset"))
          operationfirstoccurence+= ((getRepository(currentrepository).get+"-"+"RenameAsset") -> (commitindex+1))
      }
      if(six>0) {
        print("Cloned Assets = "+six+" ")
        if(!operationfirstoccurence.isDefinedAt(getRepository(currentrepository).get+"-"+"CloneAsset"))
          operationfirstoccurence+= ((getRepository(currentrepository).get+"-"+"CloneAsset") -> (commitindex+1))
      }
      if(seven>0) {
        print("Propagated Assets = "+seven+" ")
        if(!operationfirstoccurence.isDefinedAt(getRepository(currentrepository).get+"-"+"PropagateAsset"))
          operationfirstoccurence+= ((getRepository(currentrepository).get+"-"+"PropagateAsset") -> (commitindex+1))
      }
      if(eight>0) {
        print("Added Features = "+eight+" ")
        if(!operationfirstoccurence.isDefinedAt(getRepository(currentrepository).get+"-"+"AddFeature"))
          operationfirstoccurence+= ((getRepository(currentrepository).get+"-"+"AddFeature") -> (commitindex+1))
      }
      if(nine>0) {
        print("Removed Features = "+nine+" ")
        if(!operationfirstoccurence.isDefinedAt(getRepository(currentrepository).get+"-"+"RemoveFeature"))
          operationfirstoccurence+= ((getRepository(currentrepository).get+"-"+"RemoveFeature") -> (commitindex+1))
      }
      if(ten>0) {
        print("Moved Features = "+ten+" ")
        if(!operationfirstoccurence.isDefinedAt(getRepository(currentrepository).get+"-"+"MoveFeature"))
          operationfirstoccurence+= ((getRepository(currentrepository).get+"-"+"MoveFeature") -> (commitindex+1))
      }
      if(eleven>0) {
        print("Add FeatureModel To Asset = "+eleven+" ")
        if(!operationfirstoccurence.isDefinedAt(getRepository(currentrepository).get+"-"+"AddFeatureModelToAsset"))
          operationfirstoccurence+= ((getRepository(currentrepository).get+"-"+"AddFeatureModelToAsset") -> (commitindex+1))
      }
      if(twelve>0) {
        print("Map Asset to Feature = "+twelve+" ")
        if(!operationfirstoccurence.isDefinedAt(getRepository(currentrepository).get+"-"+"MapAssetToFeature"))
          operationfirstoccurence+= ((getRepository(currentrepository).get+"-"+"MapAssetToFeature") -> (commitindex+1))
      }
      if(thirteen>0) {
        print("Cloned Features = "+thirteen+" ")
        if(!operationfirstoccurence.isDefinedAt(getRepository(currentrepository).get+"-"+"CloneFeature"))
          operationfirstoccurence+= ((getRepository(currentrepository).get+"-"+"CloneFeature") -> (commitindex+1))
      }
      if(forteen>0) {
        print("Propagated Features = "+forteen+" ")
        if(!operationfirstoccurence.isDefinedAt(getRepository(currentrepository).get+"-"+"PropagateFeature"))
          operationfirstoccurence+= ((getRepository(currentrepository).get+"-"+"PropagateFeature") -> (commitindex+1))
      }
      if(fifteen>0) print("Unmapped Features = "+fifteen+" ")


      writeasttofile(astroot,bwast)
      bwast.close()
      cdet.close()
      println("")
    }

    println("\n=========== Repository Metrics ===========\n")
    operationcount.foreach(c=>println(c))
      println("\n=========== Order of Appearance ===========\n")
      operationfirstoccurence.foreach(c=>println(c))

    for(trace<-FeatureTraceDatabase.traces){
      println("Feature "+trace.source+" Mapped Assets : " + trace.source.mappedAssets(astroot))
      println("")
    }

    //Count Annotations

    println("\n =========== Annotation Count For All Repositories ===========\n")
    val repositories = Utilities.transformASTToList(astroot).filter(p=>p.assetType==RepositoryType)
    for(repository <- repositories){
      var annotationsum = 0
      val allcodeblocks = Utilities.transformASTToList(repository).filter(p=>p.assetType==BlockType | p.assetType==MethodType)
      for(block <- allcodeblocks){
        annotationsum = annotationsum + block.mappedToFeatures.size
      }
      println("Number of annotations in "+repository + " = "+annotationsum)
    }

    for(repo<-repositories){
      val fm = repo.featureModel
      if(repo.featureModel!=None){
        val mappedfeatures = Utilities.transformFMToList(fm.get).filter(p=>p.mappedAssets(repo).size>0)
        println("Mapped Features in "+repo+ " are "+mappedfeatures)
      }
    }
  }
  }