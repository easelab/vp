package se.gu.vp.operations.Replay
import java.io.File

import se.gu.vp.model.{Asset, Trace, TraceDatabase}

import scala.io.Codec

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class CommitHelpers {

}
object CommitHelpers {

  def isClone(asset:Asset):Option[Trace]={
    val latesttrace = TraceDatabase.traces.find(p=>p.target == asset)
    if(!latesttrace.isEmpty) Some(latesttrace.get)
    else None
  }
  def isSource(operations:List[ClaferFeatureOperation],commit:ClaferCommit):Option[List[ClaferFeatureOperation]]={
      val commitissource = operations.filter(_.sourcerepo == commit.repository).filter(_.sourcecommit == commit.commitid)
      if(commitissource.isEmpty)
        None
      else Some(commitissource)
  }

  def isTarget(operations:List[ClaferFeatureOperation],commit:ClaferCommit):Option[List[ClaferFeatureOperation]]={
    var commitisdestination = operations.filter(_.targetrepo == commit.repository).filter(_.targetcommit == commit.commitid)
    if(commitisdestination.isEmpty)
      None
    else Some(commitisdestination)
  }

  def parseExcelFile(file:File):List[ClaferFeatureOperation]={
    var claferoperations : List[ClaferFeatureOperation] = Nil
    val bufferedSource = io.Source.fromFile("C:\\Experiment\\data.csv")(Codec("utf-8"))

    for (line <- bufferedSource.getLines.drop(1)) {
      val splits = line.split(";")
      var source:String = ""
      var target:String = ""
      splits(1) match{
        case "Visualizer" => source = "ClaferMooVisualizer"
        case "IDE" => source = "ClaferIDE"
        case "Configurator" => source = "ClaferConfigurator"
        case "Platform" => source = "ClaferToolsUICommonPlatform"
      }
      splits(2) match{
        case "Visualizer" => target = "ClaferMooVisualizer"
        case "IDE" => target = "ClaferIDE"
        case "Configurator" => target = "ClaferConfigurator"
        case "Platform" => target = "ClaferToolsUICommonPlatform"
      }
      val op: ClaferFeatureOperation = new ClaferFeatureOperation(source,target, splits(3), splits(4), splits(5), splits(6))
      claferoperations = op :: claferoperations
    }
    bufferedSource.close
    claferoperations
  }

  def loadgraphCommits(file:File):List[commit]={

    var commits:List[commit] = Nil
    val bufferedSource = io.Source.fromFile(file.getAbsolutePath, "ISO-8859-1")
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close()

    var commitid:String = null
    var merge:String = null
    var author:String = null
    var data:List[String] = Nil
    var firstcommit=false

    for(lineindex<-lines){

      var line = lineindex.replace("|","")
      line = line.replace("/","")
      line = line.replace("\\","")
      line = line.replace("*","")

      if(line.trim.startsWith("commit")){
        if(firstcommit==true) {
          val commit = new commit(commitid,merge, author,data)
          commits = commit :: commits
          data = Nil
          merge = null
        }
        commitid = line.split(" ").last
        firstcommit = true
      }
      else if(line.trim.startsWith("Author")) author = line.split(" ").last
      else if(line.trim.startsWith("Merge:")){ merge = line.trim}
      else if(!line.startsWith(" ") & !line.startsWith("Date") & !line.trim.equals("")){
        data = line :: data
      }
    }
    val commit = new commit(commitid,merge, author, data)
    commits = commit :: commits
    println("Number of commits is "+commits.length)
    for(com<-commits) com.summary = com.summary.reverse
    commits
  }

  def loadExtendedCommits(file:File):List[ClaferCommit]={

    var commits:List[ClaferCommit] = Nil
    val bufferedSource = io.Source.fromFile(file.getAbsolutePath, "ISO-8859-1")
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close()

    var commitid:String = null
    var merge:String = null
    var author:String = null
    var date:String = null
    var time:String = null
    var data:List[String] = Nil
    var firstcommit=false

    for(lineindex<-lines){

      var line = lineindex.replace("|","")
      line = line.replace("/","")
      line = line.replace("\\","")
      line = line.replace("*","")

      if(line.trim.startsWith("commit")){
        if(firstcommit==true) {
          val commit = new ClaferCommit(file.getParentFile.getName,commitid,merge,author,date,time,data)
          commits = commit :: commits
          data = Nil
          merge = null
        }
        commitid = line.split(" ").last
        firstcommit = true
      }
      else if(line.trim.startsWith("Author")) author = line.split(" ").last
      else if(line.trim.startsWith("Merge:")){ merge = line.trim}
      else if(line.trim.startsWith("Date:")){
        val temp = line.trim.replace("   "," ")
        val split = temp.split(" ")
        date = split(1).replace("-","")
        time = split(2).replace(":","")
      }
      else if(!line.startsWith(" ") & !line.startsWith("Date") & !line.trim.equals("")){
        data = line :: data
      }
    }
    val commit = new ClaferCommit(file.getParentFile.getName,commitid,merge,author,date,time,data)
    commits = commit :: commits
    println("Total number of commits is "+commits.length)
    for(com<-commits) com.summary = com.summary.reverse
    commits
  }
}