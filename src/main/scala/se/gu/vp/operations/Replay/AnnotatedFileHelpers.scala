package se.gu.vp.operations.Replay

import java.io.{File, FileWriter}

import se.gu.vp.model._
import se.gu.vp.operations.AddAsset

import scala.util.control.Breaks.{break, breakable}
import scala.util.matching.Regex

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class AnnotatedFileHelpers{

}

object AnnotatedFileHelpers {

  def dropIndex[T](list: List[T], idx: Int): List[T] = {
    list.zipWithIndex.filter(_._2 != idx).map(_._1)
  }
  def countFrequency(string: String, char: Char): Int = {
    var frequency = 0
    for (i <- 0 to string.length - 1) {
      if (string.charAt(i) == char) frequency = frequency + 1
    }
    return frequency
  }

  def findFunctionEndIndex(fromindex: Int, filelines: List[String]): Int = {
    var countleft = 0
    var countright = 0
    var toindex = 0
    breakable {
      for (i <- fromindex to filelines.length - 1) {
        countleft += countFrequency(filelines(i), '{')
        countright += countFrequency(filelines(i), '}')

        if (countright == countleft & i > fromindex) {
          toindex = i
          return toindex
        }
        else toindex = filelines.length - 1
      }
    }
    return toindex
  }

  def preprocessing(featureline:String):String={

    var processed = featureline.replace("[","")
    processed = processed.replace("]","")
    processed = processed.replace("/","")
    processed = processed.replace("&"," ")
    processed = processed.replace("$","")
    processed = processed.replace("\t","")
    processed = processed.replace("{","")
    processed = processed.replace("}","")
    //processed = processed.replace(",","")
    processed
  }
  def getAnnotations(file:File,featuremodel:Option[FeatureModel]=None):List[Annotation]= {

    var fileannotations: List[Annotation] = Nil

    val source = io.Source.fromFile(file.getAbsolutePath, "ISO-8859-1")
    val filelines = source.getLines.toList
    source.close

    val beginannotation = new Regex("//\\s*[&$]\\s*[Bb]egin")
    val endingannotation = new Regex("//\\s*[&$]\\s*[Ee]nd")
    for (i <- 0 to filelines.length - 1) {
      if(beginannotation.findFirstIn(filelines(i))!=None){
        val fromindex = i
        val featureline = preprocessing(filelines(i))
        val intermediate = featureline.split(" ")
        val annotation = intermediate.slice(intermediate.indexOf("begin") + 1, intermediate.length).mkString("")
        if (!annotation.contains(",")) {
          val remaininglines = filelines.slice(i, filelines.length)
          val ending = remaininglines.find(p => endingannotation.findFirstIn(p)!=None & p.trim.contains(annotation.trim))
          //val ending = remaininglines.find(p => p.contains("//") & p.contains("end") & p.contains(annotation.trim))
          var toindex = 0;
          if (ending == None) {
            toindex = filelines.length - 1
          }
          else {
            toindex = i + remaininglines.indexOf(ending.get)
          }
          fileannotations = new Annotation(annotation.trim, fromindex, toindex) :: fileannotations
        }
        else {
          val featurenames = annotation.split(",")
          featurenames.foreach(feature => {
            val remaininglines = filelines.slice(i, filelines.length)
            val ending = remaininglines.find(p => endingannotation.findFirstIn(p)!=None & p.trim.endsWith(annotation.trim))
            //val ending = remaininglines.find(p => p.contains("//") & p.contains("end") & p.contains(feature.trim))
            var toindex = 0;
            if (ending == None) {
              toindex = filelines.length - 1
            }
            else {
              toindex = i + remaininglines.indexOf(ending.get)
            }
            fileannotations = new Annotation(feature.trim, fromindex, toindex) :: fileannotations
          })
        }
      }
      else if(filelines(i).contains("//&line") | filelines(i).contains("//line")){
        val featureline = preprocessing(filelines(i))
        val intermediate = featureline.split(" ")
        val annotation = intermediate.slice(intermediate.indexOf("line")+1,intermediate.length).mkString("")
        if(!annotation.contains(",")) {
          fileannotations = new Annotation(annotation.trim,i,i) :: fileannotations
        }
        else{
          val featurenames = annotation.split(",")
          featurenames.foreach(feature =>  {
            fileannotations = new Annotation(feature.trim,i,i) :: fileannotations
          })
        }
      }
    }
    fileannotations.reverse
  }

  def getAllAssets(lines:List[String]):List[TempBlock]={

    var blocklist:List[TempBlock] = Nil
    var sequenceNumber = 0
    var i = 0
    var currentindex = 0

    while (i < lines.length) {
      i = currentindex
      breakable {
        while (i < lines.length) {
          if (!lines(i).trim.startsWith("function")) i += 1
          else break()
        }
      }

        if (i > currentindex) {
          //Block found before method or end of file
          sequenceNumber += 1
          val endingindex = i - 1
          val block = new TempBlock("block", true, None, Nil, currentindex, endingindex, True())
          blocklist = block :: blocklist
          currentindex = i
        }
        else {
          //Method found
           if (i<lines.length && lines(i).trim.startsWith("function")) {

            val endingindex = findFunctionEndIndex(i, lines)
            sequenceNumber += 1
            val method = new TempBlock("method", false, None, Nil, currentindex, currentindex, True())
            blocklist = method :: blocklist
            sequenceNumber += 1

            val block = new TempBlock("block", true, None, Nil, currentindex + 1, endingindex, True())
            method.childern = block :: method.childern
            block.parent = Some(method)
            blocklist = block :: blocklist
            currentindex = endingindex + 1
          }
        }
      }
    blocklist
  }

  def splitBlock2Ways(block:TempBlock,start1:Int,end1:Int,start2:Int,end2:Int,feature:Feature,blocknumber:Int,bw:FileWriter):List[TempBlock]= {

    val block1 = new TempBlock("block", true, block.parent, Nil, start1, end1, True())
    block1.presenceCondition = block.presenceCondition
    if (blocknumber == 1) {
      block1.presenceCondition = block.presenceCondition | feature
      bw.write("MapAssetToFeature - code asset " + block1.name + " mapped to feature " + feature + "\n")
    }

    val block2 = new TempBlock("block", true, block.parent, Nil, start2,end2)
    block2.presenceCondition = block.presenceCondition
    if (blocknumber == 2) {
      block2.presenceCondition = block.presenceCondition | feature
      bw.write("MapAssetToFeature - code asset " + block2.name + " mapped to feature " + feature + "\n")
    }
    block.parent match{
      case None=>;
      case _ => {
        block.parent.get.childern = dropIndex(block.parent.get.childern,block.parent.get.childern.indexOf(block))
        block.parent.get.childern = block1 :: block2 :: block.parent.get.childern
        block.parent = None
      }
    }

    return block1 :: block2 :: Nil
  }
  def splitBlock3Ways(block:TempBlock,start1:Int,end1:Int,start2:Int,end2:Int,start3:Int,end3:Int,feature:Feature, bw:FileWriter):List[TempBlock]={

    val block1 = new TempBlock("block", true, block.parent, Nil,start1, end1 )
    block1.presenceCondition = block.presenceCondition

    val block2 = new TempBlock("block", true, block.parent, Nil, start2, end2)
    block2.presenceCondition = block.presenceCondition | feature
    bw.write("MapAssetToFeature - code asset "+block2.name+" mapped to feature "+feature+"\n")

    val block3 = new TempBlock("block", true, block.parent, Nil, start3, end3)
    block3.presenceCondition = block.presenceCondition

    block.parent match{
      case None =>; //next todo: see if return should only be here
      case _=>{
        block.parent.get.childern = dropIndex(block.parent.get.childern,block.parent.get.childern.indexOf(block))
        block.parent.get.childern = block1 :: block2 :: block3 :: block.parent.get.childern
        block.parent = None
     }
    }
    return block1 :: block2 :: block3 :: Nil
  }

  def addAnnotations(blocklist:List[TempBlock],functions:List[TempBlock],annotations:List[Annotation],featureModel: FeatureModel, bw:FileWriter):List[TempBlock]={

    var newblocklist:List[TempBlock] = blocklist
    val newmethodlist:List[TempBlock] = functions

    for(annotation <- annotations.reverse) {
      var feature = FeatureRelatedHelpers.findFeatureByName(annotation.featurename, featureModel)
      if(feature == None){
        val newfeature = Feature(annotation.featurename)
        feature = Some(newfeature)
      }
      if (feature != None) {
        for (block <- newblocklist) {
          val annotationrange = annotation.beginat to annotation.endat
          val blockrange = block.start to block.end
          val common = blockrange.intersect(annotationrange)
          if (!common.isEmpty) {

            if (common.reverse.last == block.start & common.last == block.end) {
              block.presenceCondition = block.presenceCondition | feature.get
              //bw.write("MapAssetToFeature - code asset "+block.name+" mapped to feature "+feature.get+"\n")
            }
            else if(common.reverse.last == block.start & common.last > block.end){
              block.presenceCondition = block.presenceCondition | feature.get
              //bw.write("MapAssetToFeature - code asset "+block.name+" mapped to feature "+feature.get+"\n")
            }
            else if (common.reverse.last > block.start & common.last < block.end) {
              val newblocks = splitBlock3Ways(block, block.start, annotation.beginat - 1, annotation.beginat, annotation.endat, annotation.endat + 1, block.end,feature.get,bw)
              newblocklist = dropIndex(newblocklist,newblocklist.indexOf(block))
              newblocklist = newblocks(0) :: newblocks(1) :: newblocks(2) :: newblocklist
            }
            else if (common.reverse.last == block.start & common.last < block.end) {
              val newblocks = splitBlock2Ways(block, block.start, common.last, annotation.endat + 1, block.end,feature.get,1,bw)
              newblocklist = dropIndex(newblocklist,newblocklist.indexOf(block))
              newblocklist = newblocks(0) :: newblocks(1) :: newblocklist
            }
            else if (common.reverse.last > block.start & common.last == block.end) {
              val newblocks = splitBlock2Ways(block,block.start, annotation.beginat - 1, annotation.beginat, block.end,feature.get,2,bw)
              newblocklist = dropIndex(newblocklist,newblocklist.indexOf(block))
              newblocklist = newblocks(0) :: newblocks(1) :: newblocklist
            }
          }
        }
      }
      else println(annotation.featurename + "Feature Doesn't Exist")
    }
    for(annotation <- annotations) {
      var feature = FeatureRelatedHelpers.findFeatureByName(annotation.featurename, featureModel)
      if(feature == None){
        val newfeature = Feature(annotation.featurename)
        feature = Some(newfeature)
      }
      if (feature != None) {
        for(function <- functions){
          val annotationrange = annotation.beginat to annotation.endat
          val functionrange = function.start to function.end
          val common = functionrange.intersect(annotationrange)
          if (!common.isEmpty) {
            if (common.reverse.last <= function.start & common.last >= function.end) {
              function.presenceCondition = function.presenceCondition | feature.get
              bw.write("MapAssetToFeature - code asset "+function.name+" mapped to feature "+feature.get+"\n")
            }
          }
        }
      }
      else println(annotation.featurename + "Feature Doesn't Exist")
    }
    val newassetlist = newblocklist ++ newmethodlist
    newassetlist
  }
  def transformMethod(tempBlock: TempBlock, lines:List[String], fileAsset:Asset):Unit={

    val method = Asset(tempBlock.name, MethodType, Nil, tempBlock.presenceCondition, None, Some(lines.slice(tempBlock.start, tempBlock.end+1)))
    AddAsset(method, fileAsset)
    tempBlock.childern.foreach(c=>transformBlock(c,lines,method))
  }
  def transformBlock(tempBlock: TempBlock, lines:List[String], parentAsset:Asset):Unit={
    val block = Asset(tempBlock.name,BlockType,Nil,tempBlock.presenceCondition,None,Some(lines.slice(tempBlock.start, tempBlock.end+1)))
    AddAsset(block,parentAsset)
  }

  def apply(): AnnotatedFileHelpers = new AnnotatedFileHelpers()
}