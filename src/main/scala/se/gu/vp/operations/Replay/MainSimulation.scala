package se.gu.vp.operations.Replay

import java.io.{BufferedWriter, File, FileWriter}

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class MainSimulation {
}
object MainSimulation{
  def runMainSimulation():Unit={
    val errorfile = new File("C:\\Experiment\\errorfile.txt")
    val bw = new BufferedWriter(new FileWriter(errorfile))

    val progressfile = new File("C:\\Experiment\\progress.txt")
    val writeprogress = new BufferedWriter(new FileWriter(progressfile))

    val repositories:List[String] = List("ClaferConfigurator","ClaferMooVisualizer","ClaferIDE","ClaferToolsUICommonPlatform")

    val rootfilepath = "C:\\Experiment\\RepositoryCopies"
    val originalpath = "C:\\Experiment\\Original"

    val rootfile = new File(rootfilepath)
    if(!rootfile.exists()){rootfile.mkdir()}
    if(rootfile.exists()) {
      val astroot = AssetHelpers.createRoot(rootfile)

      var allcommits:List[ClaferCommit] = Nil
      for(repository <- repositories){
        val repositoryfile = new File(originalpath+File.separator+repository)
        if (repositoryfile.exists()) {

          var commits: List[ClaferCommit] = Nil
          var simulationcommits: List[ClaferCommit] = Nil

          val logfile = new File(repositoryfile.getAbsolutePath+File.separator+ "CommitsGraph.txt")
          commits = CommitHelpers.loadExtendedCommits(logfile)

          simulationcommits = commits.filter(_.author.contains("w6ji"))
          println("Number of simulation commits for " + repository + " is " + simulationcommits.length)

          allcommits = simulationcommits ++ allcommits
        }
        else{
          bw.write("Program terminated. Repository Folder Missing")
        }
      }

      val sortedcommits = allcommits.sortWith(_.date<_.date)
      for (elem <- sortedcommits) {
        println(elem.commitid + "   "+elem.date+ "   "+elem.time + "   " + elem.repository +"   "+ elem.merge)
      }
      ASTModificationHelpers.loadAST(astroot,originalpath,sortedcommits)

    } else
    {
      bw.write("Program terminated. Root Folder Missing")
    }
    bw.close()
  }
  //Test removed to UtilitiesTest.Scala

}