package se.gu.vp.operations.Replay

import java.io.{File, FileWriter}

import se.gu.vp.model.{Asset, FeatureModel, FileType, MethodType}
import se.gu.vp.operations.CalculatorSimulation.BasicCalculator.{containsLineAnnotation, dropIndex, isAnnotation, replaceLineAnnotation}
import se.gu.vp.operations.{AddAsset, Utilities}

import scala.io.Source

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class JavaScriptFileParser{

}
object JavaScriptFileParser{

  def addAnnotatedFile(file:File, astroot:Asset, featureModel: FeatureModel, bw:FileWriter):Unit={

    if(file.exists()) {

      val fileasset:Asset = Asset(file.getAbsolutePath, FileType)
        val target = Utilities.getassetbyname(file.getParentFile.getAbsolutePath,astroot)
        target match{
            case None=>{
                println("Parent file for the javascript file to add not found")
            }
            case _=>{
                AddAsset( fileasset , target.get )
                val featuremodel = Utilities.getImmediateAncestorFeatureModel(target)

              featuremodel match{
                case  None => CodeFileParser.addCodeFile(file,fileasset,astroot)
                case _=>{
                  val source = Source.fromFile(file.getAbsolutePath)
                  val lines = source.getLines.toList
                  source.close

                  val annotations = AnnotatedFileHelpers.getAnnotations(file)

                  val assetList = AnnotatedFileHelpers.getAllAssets(lines).sortBy(_.start)
                  val methodList = assetList.filter(_.block==false)
                  val blockList = assetList.filter(_.block==true)

                  val annotatedAssets = AnnotatedFileHelpers.addAnnotations(blockList, methodList, annotations, featuremodel.get, bw).sortBy(_.start)

                  annotatedAssets.foreach(c=>{
                    if(c.block == true && c.parent == None) {
                      AnnotatedFileHelpers.transformBlock(c,lines,fileasset)
                    }
                    else if (c.block == false && c.parent == None){
                      AnnotatedFileHelpers.transformMethod(c,lines,fileasset)
                    }
                  })
                  fileasset.children = fileasset.children.reverse
                  fileasset.children.foreach(c=>{
                    if(c.assetType==MethodType){
                      c.children=c.children.reverse
                    }
                  })
                    println("File added")
                }
              }
            }
        }
    }
  }
  def removeAnnotationBlocks(fileAsset:Option[Asset]):Unit={

    //Removing blocks that are themselves annotations
    fileAsset match {
      case None => ;
      case _ => {
        fileAsset.get.children.foreach(c => {
          if (c.content.get.length == 1 && isAnnotation(c.content.get(0))) {
            fileAsset.get.children = dropIndex(fileAsset.get.children, fileAsset.get.children.indexOf(c))
            c.parent = None
          }
          else if(c.content.get.length == 1 && containsLineAnnotation(c.content.get(0))) {
            val modifiedLine = replaceLineAnnotation(c.content.get(0)) :: Nil
            c.content = Some(modifiedLine)
          }
          else{
            var newList:List[String] = Nil;
            for(i<-0 to c.content.get.length-1){
              if(!isAnnotation(c.content.get(i))){
                newList = c.content.get(i) :: newList
              }
            }
            c.content = Some(newList.reverse)
          }
        }
        )
      }
    }
  }
}
