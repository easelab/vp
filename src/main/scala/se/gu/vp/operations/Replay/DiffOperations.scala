package se.gu.vp.operations.Replay

import java.io.File

import scala.collection.mutable

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class DiffOperations {

}
object DiffOperations{

  def getAddedAssets(file:File):List[String] = {
    var addedassets:List[String] = Nil
    val bufferedSource = io.Source.fromFile(file.getAbsolutePath, "ISO-8859-1")
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close()

    for(modification <- lines){
      if(modification.trim.startsWith("A")){
        val addedasset = modification.split("\t").last.replace("/","\\")
        addedassets = addedasset :: addedassets
      }
    }
    addedassets
  }
  def getModifiedAssets(file:File):List[String] = {
    var modifiedassets:List[String] = Nil
    val bufferedSource = io.Source.fromFile(file.getAbsolutePath, "ISO-8859-1")
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close()

    for(modification <- lines){
      if(modification.trim.startsWith("M")){
        val modifiedasset = modification.split("\t").last.replace("/","\\")
        modifiedassets = modifiedasset :: modifiedassets
      }
    }
    modifiedassets
  }
  def getDeletedAssets(file:File):List[String] = {
    var deletedassets:List[String] = Nil
    val bufferedSource = io.Source.fromFile(file.getAbsolutePath, "ISO-8859-1")
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close()

    for(modification <- lines){
      if(modification.trim.startsWith("D")){
        val deletedasset = modification.split("\t").last.replace("/","\\")
        deletedassets = deletedasset :: deletedassets
      }
    }
    deletedassets
  }
  def getRenamedAssets(file:File):mutable.HashMap[String,String] = {
    val renamedassets = mutable.HashMap[String, String]()

    val bufferedSource = io.Source.fromFile(file.getAbsolutePath, "ISO-8859-1")
    val lines = (for (line <- bufferedSource.getLines()) yield line).toList
    bufferedSource.close()

    for(modification <- lines){
      if(modification.trim.startsWith("R")){
        val splits = modification.split("\t")
        val source = splits(1).replace("/","\\")
        val destination = splits.last.replace("/","\\")
        renamedassets += (source -> destination)
      }
    }
    renamedassets
  }
}
