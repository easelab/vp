package se.gu.vp.operations.Replay

import java.io.File
import java.util.regex.Pattern

import se.gu.vp.model.{Asset, FileType, FolderType, RepositoryType}
import se.gu.vp.operations.Replay.ASTModificationHelpers._
import se.gu.vp.operations.Utilities

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class StateComparators {

}
object StateComparators{

  def walkTree(file: File): Iterable[File] = {
    val children = new Iterable[File] {
      def iterator = if (file.isDirectory) file.listFiles.iterator else Iterator.empty
    }
    Seq(file) ++: children.flatMap(walkTree(_))
  }
  def currentASTState(astroot:Asset):List[String]={
    Utilities.transformASTToList(astroot).filter(p=>p.assetType == FolderType | p.assetType == RepositoryType | p.assetType == FileType).map(_.name)
  }
  def currentRepositoryState(file:File):List[String]={
    walkTree(file).map(_.getAbsolutePath).toList
  }
  def addedFolders(file:File,astroot:Asset):List[String]={
    walkTree(file).filter(_.isDirectory).map(_.getAbsolutePath).toList.filterNot(currentASTState(astroot).contains(_))
  }
  def AddedAssets(astroot:Asset,repository:File):List[String]={
    currentRepositoryState(repository).filterNot(currentASTState(astroot).contains(_))
  }
  def deletedAssets(astroot:Asset,repository:File):List[String]={
    val currentstate = Utilities.transformASTToList(astroot).filter(p=>p.assetType==RepositoryType | p.assetType==FileType | p.assetType==FolderType).map(_.name)
    currentstate.filterNot(currentRepositoryState(repository).contains(_))
  }
  def commonFiles(astroot:Asset,repository:File):List[String]={
    val currentstate = Utilities.transformASTToList(astroot).filter(p=>p.assetType==FileType).map(_.name)
    currentstate.intersect(currentRepositoryState(repository))
  }

  def getAllAssets(path:String):List[String]={

    var names:List[String] = Nil
    val assets = path.split(Pattern.quote("\\")).toList
    for(k<-0 to assets.length-1){
      val assetname =  assets.slice(0,k+1).mkString(File.separator)
      names = assetname :: names
    }
    names.reverse
  }
  def getAllConfigurations(path:String):List[String]={

    var names:List[String] = Nil
    val assets = path.split("\\").toList
    for(k<-0 to assets.length-1){
      val assetname =  assets.slice(0,k+1).mkString("\\")
      names = assetname :: names
    }
    names.reverse
  }
  def featureModelAdded(addedassets:List[String]):String={
    addedassets.find(p=>p.endsWith(".vp-project") | p.endsWith("FeatureModel.cfr")).getOrElse(null)
  }
  def featureModelModified(modifiedassets:List[String]):String={
    modifiedassets.find(p=>p.endsWith(".vp-project") | p.endsWith("FeatureModel.cfr")).getOrElse(null)
  }
}