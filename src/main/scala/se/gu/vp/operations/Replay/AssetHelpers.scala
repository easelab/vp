package se.gu.vp.operations.Replay

import java.io.File

import se.gu.vp.model._
import se.gu.vp.operations._

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class AssetHelpers {

}
object AssetHelpers{
  def createRoot(file:File):Asset={

        val root = Asset(file.getAbsolutePath, VPRootType)
        root.versionNumber = 1
        return root
  }
  def createRepository(file: File, parent:Asset):Unit={
    val repository = Asset(file.getAbsolutePath, RepositoryType)
    AddAsset(repository,parent)
  }
  def createFolder(file: File, parent:Asset):Unit={
      val folder = Asset(file.getAbsolutePath, FolderType)
      AddAsset (folder, parent)

  }
  def createFile(file: File, parent:Asset):Unit = {
    val newfile = Asset(file.getAbsolutePath, FileType)
    AddAsset (newfile, parent)
  }
  def createBlock(name:String, parent:Asset):Unit = {
  }
}