package se.gu.vp.operations.Replay

import java.io.File
import se.gu.vp.model.Asset
import scala.io.Source

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class CodeFileParser {

}
object CodeFileParser{

  var filestoignore = ("jquery-1.8.2.js")

  def addCodeFile(file:File, fileasset:Asset, astroot:Asset):Unit={

    val source = Source.fromFile(file.getAbsolutePath)
    val lines = source.getLines.toList
    source.close

    println(file.getAbsolutePath)

    val assetList = AnnotatedFileHelpers.getAllAssets(lines).sortBy(_.start)
    val methodList:List[TempBlock] = assetList.filter(_.block==false)
    val blockList:List[TempBlock] = assetList.filter(_.block==true)

    methodList.foreach(c => AnnotatedFileHelpers.transformMethod(c,lines,fileasset))
    blockList.foreach(c=>AnnotatedFileHelpers.transformBlock(c,lines,fileasset))
  }
}
