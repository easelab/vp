package se.gu.vp.operations

import java.time.LocalDateTime

import se.gu.vp.model.{Asset, Feature}
import se.gu.vp.operations.metadata.OperationMetadata

/**
Copyright [2018] [Thorsten Berger, Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

abstract class Operation{

  var subOperations : Option[Seq[Operation]] = None
  var metadata : Option[OperationMetadata] = None

  def perform(): Boolean
  def createMetadata(): Unit

  def storeSubOpMetadata = {
    if (metadata.isDefined && subOperations.isDefined) {
      val subOpMD = for {
        op <- subOperations.get
        if op.metadata.isDefined
      } yield {
        op.metadata.get
      }
      metadata.get.subOperations = Some(subOpMD)
    }
  }

  def getCurrentTimeStamp(): Long = {
    val timestamp = LocalDateTime.now()
    return Utilities.getTimeStamp(timestamp.toString).toLong
  }

  def getLastModifiedTime(asset:Asset): Unit = {
    asset.versionNumber = Utilities.getLastModifiedTime(asset)
  }

  def updateVersion(asset:Asset): Unit = {
    asset.versionNumber = getCurrentTimeStamp()
  }
  def updateFeatureVersion(feature:Feature): Unit = {
    feature.versionNumber = getCurrentTimeStamp()
  }

}
abstract class Query[T]{

  // show list of clones
  def query(): T
}