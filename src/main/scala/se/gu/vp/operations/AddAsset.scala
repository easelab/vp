package se.gu.vp.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model._
import se.gu.vp.operations.metadata.{AddAssetMetadata, MetadataConverter}

import scala.collection.mutable

/**
Copyright [2018] [Wardah Mahmood, Christoph Derks, Thorsten Berger]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class AddAsset(val asset: Asset, val parent: Asset, val insertionPoint: Option[Int] = None) extends Operation with Logging {

  import Utilities._
  import se.gu.vp.model.ASTWellFormednessConstraints._

  override def perform(): Boolean = {

    // Checking AST Well-Formedness constraints to disallow additions that invalidate the AST
    if( containable(asset, parent) && (!insertionPoint.isDefined || insertionPoint.get <= parent.children.length)) {
      val operationListBuilder = mutable.ListBuffer[Operation]()

      //Establishing parent child relationships between asset and target container
      asset.parent = Some(parent)
      parent.children = if (insertionPoint.isDefined) {
        val preceding = parent.children.take(insertionPoint.get)
        val post = parent.children.drop(insertionPoint.get)
        preceding ++ (asset :: post)
      } else {
        parent.children ++ List(asset)
      }

      getLastModifiedTime(asset)
      getLastModifiedTime(asset.parent.get)

      //Traversing through the asset's presence condition to find the mapped features
      //Finding the closest ancestor with a feature model and checking if the feature
      //model contains the features mapped to the assets
      val closestAncestor = getImmediateAncestorFeatureModel( Some(asset) )
      closestAncestor match {
        case Some( fm ) => //Find the features from the presence condition not contained in the feature model
          // Should this use the actual feature reference or the name? Name could not work because of lpq..
          val missingImplementation = asset.missingFeatures(fm)

          missingImplementation.foreach(
            m => {
              // Add every missing feature in the target feature model's unassigned features
              m.parent = Option(closestAncestor.get.rootfeature.UNASSIGNED)
              closestAncestor.get.rootfeature.UNASSIGNED.subfeatures = m :: closestAncestor.get.rootfeature.UNASSIGNED.subfeatures
            }
          )
          subOperations = Some(operationListBuilder.toList)
        case _ => ;
      }
      storeSubOpMetadata
      true
    } else {
      error(asset.name + "  - Addition unsuccessful. Incompatible source and target")
      false
    }
  }

  override def createMetadata(): Unit = {
    try {
      val createdMetadata = AddAssetMetadata(asset = MetadataConverter.convertAssetToMetadata(asset), parent = MetadataConverter.convertAssetToMetadata(parent), insertionPoint = insertionPoint)
      metadata = Some(createdMetadata)
    } catch {
      case e : Exception =>
        println("Something went wrong during metadata creation.")
    }
  }
}

object AddAsset {
  def apply(asset: Asset, parent: Asset, insertionPoint: Option[Int] = None, runImmediately: Boolean = true, createMetadata: Boolean = true): AddAsset = {
    if (runImmediately && createMetadata) {
      new AddAsset(asset, parent, insertionPoint) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new AddAsset(asset, parent, insertionPoint) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new AddAsset(asset, parent, insertionPoint) with CreateMetadata
    } else {
      new AddAsset(asset, parent, insertionPoint)
    }
  }
//  // Option where Transactions are not required?
  def unapply(arg: AddAsset): Option[(Asset, Asset, Option[Int])] = Some(arg.asset, arg.parent, arg.insertionPoint)

}