package se.gu.vp.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model.{Asset, FeatureModel}

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class AddFeatureModelToAsset(asset:Asset, featuremodel:FeatureModel) extends Operation with Logging {
  override def perform(): Boolean = {
    //Adding a feature model to an asset
    asset.featureModel = Some(featuremodel)
    featuremodel.containingAsset = Some(asset)

    //info(this.getClass.getName + " - Added feature model to : "+asset.name)
    //Updating asset and root's version number after adding feature model
    //as this is an extended for of change Asset
    updateVersion(asset)

    //info(this.getClass.getName + " - Asset changed : "+asset.name)
    storeSubOpMetadata
    true
  }

  override def createMetadata(): Unit = {
    true
  }
}
object AddFeatureModelToAsset{
  def apply(asset: Asset, featuremodel: FeatureModel, runImmediately: Boolean = true, createMetadata: Boolean = true): AddFeatureModelToAsset = {
    if (runImmediately && createMetadata) {
      new AddFeatureModelToAsset(asset, featuremodel) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new AddFeatureModelToAsset(asset, featuremodel) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new AddFeatureModelToAsset(asset, featuremodel) with CreateMetadata
    } else {
      new AddFeatureModelToAsset(asset, featuremodel)
    }
  }
}