package se.gu.vp.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model.{Asset, Feature}
import se.gu.vp.operations.Utilities._
import se.gu.vp.operations.metadata.{MapAssetToFeatureMetadata, MetadataConverter}

import scala.collection.mutable

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class MapAssetToFeature(val asset:Asset, val feature:Feature) extends Operation with Logging {

  def perform(): Boolean = {
    var closestfm = getImmediateAncestorFeatureModel( Some(asset) )
    closestfm match {
      case None => error("No ancestor with feature model found")
      case _ => {
        val operationListBuilder = mutable.ListBuffer[Operation]()

        // TODO: Shouldnt this mapping here occur on the actual present feature in case its a perfect copy? Can there be problems with multiple identical-looking objects here?
        if (featureModelContainsFeature(feature, closestfm.get)) {
          if(!asset.isMappedToFeature(feature)) {
            // Something like this?
            asset.presenceCondition = asset.presenceCondition | feature
          }
        }
        else {
          feature.parent = Option(closestfm.get.rootfeature.UNASSIGNED)
          closestfm.get.rootfeature.UNASSIGNED.subfeatures = feature :: closestfm.get.rootfeature.UNASSIGNED.subfeatures
          if (!asset.isMappedToFeature(feature)) asset.presenceCondition = asset.presenceCondition | feature
        }
        updateVersion(asset)
        updateFeatureVersion(feature)
        subOperations = Some(operationListBuilder.toList)
      }
    }
    storeSubOpMetadata
    true
  }

  override def createMetadata(): Unit = {
    try {
      val assetMD = MetadataConverter.convertAssetToMetadata(asset)
      val featMD = MetadataConverter.convertFeatureToMetadata(feature)

      metadata = Some(MapAssetToFeatureMetadata(assetMD, featMD))
    } catch {
      case e : Exception =>
        println("Something went wrong during metadata creation.")
    }
  }
}
object MapAssetToFeature {
  def apply(asset:Asset, feature:Feature, runImmediately: Boolean = true, createMetadata: Boolean = true): MapAssetToFeature = {
    if (runImmediately && createMetadata) {
      new MapAssetToFeature(asset, feature) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new MapAssetToFeature(asset, feature) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new MapAssetToFeature(asset, feature) with CreateMetadata
    } else {
      new MapAssetToFeature(asset, feature)
    }
  }

  def unapply(arg: MapAssetToFeature): Option[(Asset, Feature)] = Some(arg.asset, arg.feature)
}