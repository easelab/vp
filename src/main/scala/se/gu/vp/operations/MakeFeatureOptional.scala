package se.gu.vp.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model.Feature

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class MakeFeatureOptional (feature:Feature) extends Operation with Logging {
  override def perform(): Boolean = {
    feature.optional=true
    updateFeatureVersion(feature)
    storeSubOpMetadata
    true
  }

  override def createMetadata(): Unit = {
    true
  }
}
object MakeFeatureOptional {
  def apply(feature: Feature, runImmediately: Boolean = true, createMetadata: Boolean = true): MakeFeatureOptional = {
    if (runImmediately && createMetadata) {
      new MakeFeatureOptional(feature) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new MakeFeatureOptional(feature) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new MakeFeatureOptional(feature) with CreateMetadata
    } else {
      new MakeFeatureOptional(feature)
    }
  }
}