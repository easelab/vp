package se.gu.vp.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model._
import se.gu.vp.operations.Utilities._

import scala.collection.mutable

/**
Copyright [2019] [Zhichuan Li]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

/*
* Author: Zhichuan Li
* integrate assert
* Only contains implementation for asset with MethodType or BlockType
* Other asset types plan to do in the future
* */

class IntegrateAsset(source: Asset, target: Asset, feature: Feature) extends Operation with Logging {
  override def perform(): Boolean = {
    if(ASTWellFormednessConstraints.containable(source,target)) {
      val operationListBuilder = mutable.ListBuffer[Operation]()

      source.assetType match {
        case BlockType => {
          /**
            * Add the target asset to the parent of source asset with feature presence.
            * */
          operationListBuilder += AddAsset(target, source.parent.get)
          operationListBuilder += MapAssetToFeature(target, feature)
        }
        case MethodType =>{
          for(src <- transformToList(source)) {
            for(tar <- transformToList(target)){
              //TODO: how to compare?
            }
          }
        }
        case _ => ;
      }

      subOperations = Some(operationListBuilder.toList)
    }
    else{
      error("Integrate not possible, ASTWellFormednessConstraints violated")
      false
    }
    storeSubOpMetadata
    true
  }

  override def createMetadata(): Unit = {
    true
  }
}

object IntegrateAsset {
  def apply(source: Asset, target: Asset, feature: Feature, runImmediately: Boolean = true, createMetadata: Boolean = true): IntegrateAsset = {
    if (runImmediately && createMetadata) {
      new IntegrateAsset(source, target, feature) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new IntegrateAsset(source, target, feature) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new IntegrateAsset(source, target, feature) with CreateMetadata
    } else {
      new IntegrateAsset(source, target, feature)
    }
  }
}
