package se.gu.vp.operations

import java.io.{File, FileWriter}

import se.gu.vp.model._
import se.gu.vp.operations.Utilities.{extractFeatures, getLeastQualifiedPath, transformFMToList}
import se.gu.vp.operations.cli.TraceLoader

/**
Copyright [2021] [Christoph Derks, Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class SerialiseAssetTree(asset: Asset, path: String) {
  perform()

  def printfm(features: List[Feature], parentFeatureName: String, indent: Int) : String = {
    features match {
      case Nil => ""
      case ::(head, tl) =>
        val featurePrint = "\t" * indent + head.name.stripPrefix(parentFeatureName + "-") + '\n'
        val subfeaturePrint = printfm(head.subfeatures, head.name, indent + 1)
        featurePrint + subfeaturePrint + printfm(tl, parentFeatureName, indent)
    }
  }

  def prettyprint(fm: FeatureModel,path: String) : Unit = {

    //Only writing feature models that have features (other than the root feature and unassigned feature)
   if(transformFMToList(fm).size>2) {
     val file = new File(path + "\\.feature-model")
     file.createNewFile()
     val writer = new FileWriter(file.getAbsolutePath)

     val fmprint = printfm(List(fm.rootfeature), "", 0)

     // write everything to file
     writer.write(fmprint)
     writer.close()
   }
  }


  def calculateCode(assetList: List[Asset], parentLevelActiveFeatures: List[Feature], activeFeatures: List[Feature] = Nil) : String =
  {
    assetList match {
      case Nil =>
        // close final annotations
        val finalAnnotations = for {
          actFeat <- activeFeatures
          if !parentLevelActiveFeatures.contains(actFeat)
        } yield {
          s"//&end ${getLeastQualifiedPath(actFeat).get}\n"
        }
        finalAnnotations.mkString
      case asset :: as =>
        // even if no content is set an asset theoretically might still be mapped to a feature
        val assetCode = asset.content match {
          case None => ""
          case Some(code) =>
            // TODO: possibly language specific
            code.mkString("\n")
        }

        // re-add annotations
        val annotatedFeatures = extractFeatures(asset.presenceCondition)

        val code = asset.children.length match {
          case 0 => s"$assetCode\n"
          case _ =>
            val childrenCode = calculateCode(asset.children, annotatedFeatures)
            s"$assetCode\n$childrenCode"
        }

        // add feature ending annotations from previous codeblock
        val endAnnotations = for {
            actFeat <- activeFeatures
            if !annotatedFeatures.contains(actFeat)
            if !parentLevelActiveFeatures.contains(actFeat)
          } yield {
            s"//&end ${getLeastQualifiedPath(actFeat).get}\n"
          }

        // add feature begin annotations for current codeblock
        val beginAnnotations = for {
          annFeat <- annotatedFeatures
          if !activeFeatures.contains(annFeat)
          if !parentLevelActiveFeatures.contains(annFeat)
        } yield {
          s"//&begin ${getLeastQualifiedPath(annFeat).get}\n"
        }

        val annotatedCode = s"${endAnnotations.mkString}${beginAnnotations.mkString}$code"
        // reverse is called on annotatedFeatures to close annotations in the right order
        annotatedCode + calculateCode(assetList = as, parentLevelActiveFeatures = parentLevelActiveFeatures, activeFeatures = annotatedFeatures.reverse)
    }
  }

  // TODO: perhaps do a rootlevel asset check initially and delete contents of path in that case to ensure that there are no unused folders and files in the dir
  // assumes path is at the level of current level asset
  def prettyprint(asset:Asset, path: String, parentLevelActiveFeatures: List[Feature] = Nil):Boolean = {
    val featureMappings = extractFeatures(asset.presenceCondition)

    if(asset.assetType == VPRootType) {
      TraceLoader.writeTracesToFile(asset.name) /** Not writing to the correct path */
    }
    if (asset.assetType == FileType)
      {
        val fileName = asset.name.split('\\').last

        // choose dependent on convention
        val file = new File(path+"\\"+fileName)
        file.createNewFile()
        val fWriter = new FileWriter(file.getAbsolutePath)

        val fileContent = if (asset.content.isDefined) {
          asset.content.get.mkString("\n")
        } else {
          ""
        }
        val code = calculateCode(assetList = asset.children, featureMappings)

        // write everything to file
        fWriter.write(fileContent + code)
        fWriter.close()
        true
      }
    else {
      // asset is minimum a folder
      // choose dependent on convention

      if(asset.assetType == RepositoryType){
      }

      val folder = new File(path+"\\"+asset.name.split('\\').last)
      folder.mkdir()
      if(asset.featureModel.isDefined)
        {
          prettyprint(asset.featureModel.get, folder.getAbsolutePath)
        }

      for (child <- asset.children) {
        prettyprint(child, folder.getAbsolutePath, featureMappings)
      }
      var mappings:List[String] = Nil
      asset.children.foreach(subasset =>{
        if(subasset.assetType==FileType && !subasset.mappedToFeatures.isEmpty){

          var featureList:String = ""

          subasset.mappedToFeatures.foreach(feature =>{
            featureList = feature.name + " " + featureList
          })
          featureList = featureList.trim
          featureList = featureList.replace(" ",",")
          mappings = (subasset.name.split('\\').last + ":" + featureList) :: mappings
        }
      })

      if(!mappings.isEmpty){
        val featureToFileMapping = new File(folder + "\\" + ".feature-to-file")
        val f2fWriter = new FileWriter(featureToFileMapping, false)
        mappings.foreach(mapping =>{
          f2fWriter.write(mapping.split(":").reverse.last +"\n")
          f2fWriter.write(mapping.split(":").last+"\n")
        })
        f2fWriter.close
      }

      if(!asset.mappedToFeatures.isEmpty){
        val featureToFolderMapping = new File(folder + "\\" + ".feature-to-folder")
        val f2fWriter = new FileWriter(featureToFolderMapping, false)

        asset.mappedToFeatures.foreach( C => f2fWriter.write(C.name + "\n"))

        f2fWriter.close
      }
      true
    }
  }

   def perform(): Boolean = {
    val dir = new File(path)
    dir.exists() match {
      case true  =>
      case false => dir.mkdir()
    }
     println("\n *** Serializing asset tree ...")
    prettyprint(asset,path)
     println("\n *** Serialization done successfully!")
    true
  }
}

object SerialiseAssetTree {

  def apply (asset: Asset, path: String): SerialiseAssetTree = new SerialiseAssetTree(asset,path)

}
