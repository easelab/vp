package se.gu.vp.operations.Experimentation;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 Copyright [2020] [Wardah Mahmood]

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

public class CommandExecutor {

    public void getStatus(String path,String commitid) throws IOException {
        ProcessBuilder builder = new ProcessBuilder("C:\\Program Files\\Git\\bin\\bash.exe", "-c", String.format("git diff-tree --no-commit-id --name-only -r "+commitid));

        builder.directory(new File(path));
        //start process
        builder.redirectOutput(new File("C:\\Experiment\\status.txt"));
        final Process process = builder.start();

        try (var reader = new BufferedReader(
                new InputStreamReader(process.getInputStream()))) {

            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        }
    }
    public void getDiffFromTwoCommits(String path, String commitA, String commitB) throws IOException, InterruptedException {
        ProcessBuilder builder = new ProcessBuilder("C:\\Program Files\\Git\\bin\\bash.exe", "-c", String.format("git diff --name-status %s %s", commitA, commitB));
        builder.directory(new File(path));
        //start process
        builder.redirectOutput(new File("C:\\Experiment\\diff.txt"));
        final Process process = builder.start();
        process.waitFor();
        try (var reader = new BufferedReader(
                new InputStreamReader(process.getInputStream()))) {

            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        }
    }
    public void copyDirectory(String path1, String path2) throws IOException, InterruptedException {
        File srcDir = new File(path1);
        File destDir = new File(path2);
        try {
            FileUtils.copyDirectory(srcDir, destDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void cleanDirectory(String path1) throws IOException, InterruptedException {
        File DelDir = new File(path1);

        try {
            for(File file : DelDir.listFiles()){
                System.gc();
                FileUtils.forceDelete(file);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void gitHardReset(File directory,String commit) throws IOException {
        String command = "git reset --hard "+commit.trim();
        ProcessBuilder builder = new ProcessBuilder("C:\\Program Files\\Git\\bin\\bash.exe","-c", command);
        System.out.println(directory);
        builder.directory(directory);
        final Process process = builder.start();
        try {
            process.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        process.destroy();
    }
}