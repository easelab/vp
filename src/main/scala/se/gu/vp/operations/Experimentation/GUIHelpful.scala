package se.gu.vp.operations.Experimentation

import java.awt.event.{KeyEvent, KeyListener}

/**
Copyright [2022] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

object KeyListenerExample {
}

class KeyListenerExample extends KeyListener {
  override def keyTyped(e: KeyEvent): Unit = {
    System.out.println("The key Typed was: " + e.getKeyChar)
  }

  override def keyPressed(e: KeyEvent): Unit = {
    if (e.isActionKey) System.exit(0)
    System.out.println("The key Pressed was: " + e.getKeyChar)
  }

  override def keyReleased(e: KeyEvent): Unit = {
    System.out.println("The key Released was: " + e.getKeyChar)
  }
}

class GUIHelpful {

}
