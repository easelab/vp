package se.gu.vp.operations.Experimentation;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Optional;

/**
 Copyright [2020] [Wardah Mahmood]

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

public class AllOpenProcesses {

    public static void test(){

        try {
            String process;
            Process p = Runtime.getRuntime().exec
                    (System.getenv("windir") +"\\system32\\"+"tasklist.exe");
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((process = input.readLine()) != null) {
                System.out.println(process); // <-- Print all Process here line
                // by line
            }
            input.close();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    private static String processDetails(ProcessHandle process) {
        return String.format("%8d %8s %10s %26s %-40s",
                process.pid(),
                (process.parent().map(ProcessHandle::pid)).toString(),
                (process.info().user()).toString(),
                (process.info().startInstant()).toString(),
                (process.info().commandLine())).toString();
    }

    private static String text(Optional<?> optional) {
        return optional.map(Object::toString).orElse("-");
    }
}
