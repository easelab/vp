package se.gu.vp.operations

import se.gu.vp.model._

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class FeatureCloneHelpers {

}
object FeatureCloneHelpers{
  def addUnassignedFeature(feature:Feature,featuremodel:FeatureModel):Unit={
  feature.parent = Option(featuremodel.rootfeature.UNASSIGNED)
  featuremodel.rootfeature.UNASSIGNED.subfeatures =  featuremodel.rootfeature.UNASSIGNED.subfeatures ++ List(feature)
}
  def addFeatureTrace(source:Feature,clone:Feature):Unit={
    FeatureTraceDatabase.traces = FeatureTrace(source, clone, CloneOf, source.versionNumber) :: FeatureTraceDatabase.traces
  }
  def deepCloneFeature(sourceFeature:Feature,parent:Option[Feature]=None):Feature={
    val clone = sourceFeature.doCloneFeature()
    parent match{
      case None=>;
      case _=> clone.parent = parent
    }
    clone.subfeatures = Nil
    addFeatureTrace(sourceFeature,clone)
    for(subfeature <- sourceFeature.subfeatures){
      val childclone = deepCloneFeature(subfeature,Some(clone))
      clone.subfeatures =  clone.subfeatures ++ List(childclone)
    }
    clone
  }
}