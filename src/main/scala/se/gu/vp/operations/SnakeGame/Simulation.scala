package se.gu.vp.operations.SnakeGame

import java.io.{File, FileWriter}

import se.gu.vp.model._
import se.gu.vp.operations.AddAsset
import se.gu.vp.operations.CalculatorSimulation.ASTModificationHelpers.handleFeatureModel
import se.gu.vp.operations.CalculatorSimulation.BasicCalculator.getListOfSubDirectories
import se.gu.vp.operations.JavaParser.JavaParserMain.parseJavaFile
import se.gu.vp.operations.Replay.AssetHelpers
import se.gu.vp.operations.Utilities.{getImmediateAncestorFeatureModel, getassetbyname}

import scala.io.Source

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class Simulation {

}
object Simulation{

  def readInFileTree(currentPath: File, parentAsset: Asset) : Unit = {
    val assetType =
      if (currentPath.isDirectory)
      { FolderType }
      else
      { FileType }
    if(!currentPath.getAbsolutePath.contains(".git")) {
      assetType match {
        case FolderType =>
          println(currentPath.getAbsolutePath)

          val folderAsset = Asset(currentPath.getAbsolutePath, assetType)
          AddAsset(folderAsset, parentAsset)
          currentPath.listFiles
            .foreach(file => readInFileTree(file, folderAsset))
        case FileType => {
          println(currentPath.getAbsolutePath)
          if (currentPath.getAbsolutePath.endsWith(".feature-model")) handleFeatureModel(currentPath, parentAsset)
          else if (currentPath.getAbsolutePath.endsWith(".java")) {
            val fileAsset = parseJavaFile(currentPath, getImmediateAncestorFeatureModel(Some(parentAsset)).get)
            AddAsset(fileAsset, parentAsset)
          }
          else {
            val fileAsset = Asset(currentPath.getAbsolutePath, assetType, content = Some(Source.fromFile(currentPath, "ISO-8859-1").getLines.toList))
            AddAsset(fileAsset, parentAsset)
          }
        }
      }
    }
  }

  def readInJavaAssetTree(rootFilePath: String) : Option[Asset] = {
    val commitlog = new File(rootFilePath + File.separator + "CommitLog.txt")
    val astprintfile = new File(rootFilePath + File.separator + "ASTprint.txt")
    val bwast = new FileWriter(commitlog, true)

    val rootfile = new File(rootFilePath)
    if (rootfile.exists()) {
      val astroot = AssetHelpers.createRoot(rootfile)
      getListOfSubDirectories(rootFilePath).foreach(c => {
        val repofile = new File(rootFilePath + "//" + c)
        if (repofile.exists()) {
          AssetHelpers.createRepository(repofile, astroot)
          val repositoryasset = getassetbyname(repofile.getAbsolutePath, astroot).get
          repofile.listFiles.foreach(file => readInFileTree(file, repositoryasset))
        }
      })
      Some(astroot)
    } else {
      None
    }
  }
//Main removed. Moved to SnakeGameTestCase called "SnakeGameTestCase"

}
