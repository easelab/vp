package se.gu.vp.operations

import java.time.LocalDateTime

import se.gu.vp.model._
import se.gu.vp.operations.Utilities.{setNewNamesRecursively, setParentageBetween}

import scala.collection.mutable
import scala.util.control.Breaks.{break, breakable}

/**
Copyright [2019] [Wardah Mahmood, Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class ASTSliceHelpers {

}
object ASTSliceHelpers{

  def getCurrentTimeStamp(): Long = {
    val timestamp = LocalDateTime.now()
    return Utilities.getTimeStamp(timestamp.toString).toLong
  }
  def cloneASTSlice(asset:Asset, target:Asset,feature:Feature):List[Operation]={

    val listBuilder = mutable.ListBuffer[Operation]()
    val assetClone = tryGetAssetClone(asset, target)

    /** Cloning blocks is a bit different. If a mapped asset is a code block,
      * and it is not yet cloned, then, we see the parent of the block. If it is a file or a class,
      * we clone it right away. If it is a method, then we see if the new mapping
      * was the only change that occured. If so, we propagate the entire method.
      * If the parent is a method not yet cloned, we clone the method.
      * */

    if(assetClone.isEmpty) {
      //If the asset is not already cloned, we clone it
      val ancestors = Utilities.getAncestors(asset).filterNot(_.assetType == VPRootType)
      if (ancestors.length == 1) { // Repository is not cloned into target, asset is immediately cloned without slicing
        // TODO: insertAfter does not yet exist for CloneFeature -> after which asset should the slice be inserted?
        listBuilder += CloneAsset(source = asset, targetContainer = target, insertAfter = None, cloneName = None, deepCloneFeature = false) //returned. To be added in sub-assets
        val clone = target.children.find(ch => TraceDatabase.traces.find(t => t.target == ch).isDefined).get
        listBuilder += MapAssetToFeature(clone,feature)
        target.versionNumber = getCurrentTimeStamp()
      }
      else {
        var cloned = false
        for (i <- (0 to ancestors.length - 1).reverse) {
          if (!cloned) {
            val ancestor = ancestors(i)
            val ancestorCloneOpt = tryGetAssetClone(ancestor, target)
            if (ancestorCloneOpt.isDefined) {
              val ancestorClone = ancestorCloneOpt.get
              cloned = true
              val (slice, operations) = performSlice(asset, ancestors(i), ancestorClone)
              operations.foreach(op => listBuilder += op)

              ancestorClone.versionNumber = getCurrentTimeStamp()
              // Get assetClone using slice-property, i.e. until the assetClone there can only be a single child on each layer
              val assetclone = tryGetAssetClone(asset,target).get
              listBuilder += MapAssetToFeature(assetclone,feature)
            }
          }
        }
        if (cloned == false) {
          /**If no common ancestor is found, we create a slice till the
          repository asset, not including the repository asset itself */
          val (slice, operations) = performSlice(asset, ancestors.reverse.last, target)
          operations.foreach(op => listBuilder += op)

          target.versionNumber = getCurrentTimeStamp()
          val assetclone = tryGetAssetClone(asset,target).get
          listBuilder += MapAssetToFeature(assetclone,feature)
        }
      }
    }
    else{
      listBuilder += MapAssetToFeature(assetClone.get, feature)
    }
    listBuilder.toList
  }

  // TODO: Improve, if not aiming to insert at first position at all times
  def findSuitableInsertionPoint(sourceAsset: Asset, cloneType: AssetType, sourceParent: Asset, targetParent: Asset) : Option[Asset] = {
    cloneType match {
      case (VPRootType | RepositoryType | FieldType) => ;
      case (FolderType | FileType | ClassType) => None
      case MethodType => {
        var siblings:List[Asset] = Nil;
        breakable(
          sourceParent.children.foreach(child => {
            if(child != sourceAsset)
              siblings = child :: siblings
            else break()
          }))
        /** Finding the closest sibling that was cloned in the target parent */
        for(sibling <- siblings){
          if(tryGetAssetClone(sibling,targetParent) != None){
            return tryGetAssetClone(sibling,targetParent)
          }
        }
        return Some(targetParent.children(targetParent.children.length-2))
        /** Earlier, we were returning targetParent.children.last, but that was nt flying well.
          * In serliazation, it was written after the class ends. If I give it the index of the second
          * last child, it is still inserted at the last index, meaning the ending bracket
          * is the last child
          *
          */
      }
      case BlockType =>{

        sourceParent.assetType match{
          case FileType => if(sourceParent.children.length>1) return Some(targetParent.children(0)) //Added this so the asset is added after the package statement
          case ClassType => return Some(targetParent.children.head)
          case MethodType =>{
            var siblings:List[Asset] = Nil;
            breakable(
              sourceParent.children.foreach(child => {
                if(child != sourceAsset) siblings = child :: siblings
                else break()
              }))
            /** Finding the closest sibling that was cloned in the target parent */
            for(sibling <- siblings){
              if(tryGetAssetClone(sibling,targetParent) != None){
                return tryGetAssetClone(sibling,targetParent)
              }
            }
          }
        }
        return None
      }
    }
    None
  }

  def performSlice(asset:Asset, upto:Asset, parent: Asset):(Asset, List[Operation])={

    val operations = mutable.ListBuffer[Operation]()
    val ancestors = Utilities.getAncestors(asset).reverse
    var clones:List[Asset] = Nil
    breakable {
      for (ancestor <- ancestors) {
        if (ancestor != upto) {
          val clone = doPartialClone(ancestor)
          clones = clone :: clones
        }
        else break()
      }
    }

    // currently defaults to none -> first position in parent
    val cloneType = clones.length match {
      case 0 => asset.assetType
      case _ => clones(0).assetType
    }

    val insertAfter = findSuitableInsertionPoint(sourceAsset = asset, cloneType = cloneType, sourceParent = upto, targetParent = parent)

    if(clones.length==0){
      operations += CloneAsset(source = asset, targetContainer = parent, insertAfter = insertAfter, cloneName = None, deepCloneFeature = false)
      val deepclone = upto.children.find(ch => TraceDatabase.traces.find(t => t.source == asset).isDefined).get
      return (deepclone, operations.toList)
    }
    if (clones.length==1){
      val slice = clones(0)
      //TODO: No insertAfter exists here, yet
      setNewNamesRecursively(slice, parent.name)
      // insertAfter is set here because now we insert the slice at the correct position into the parent
      setParentageBetween(parent, slice, insertAfter)

      // TODO: Issue: both, upto and clones(0) would be wrong here, because the new parent would be clones(0), but the fm-providing element upto -> does not matter in this case, because targetasset is only required in case deepCloneFeatures = true and that is not the case for CloningFeatures
      // insertAfter = None here because this adds the actual clone to the slice, which contains only one element anyways
      operations += CloneAsset(source = asset, targetContainer = slice, insertAfter = None, cloneName = None, deepCloneFeature = false)
      // CloneAsset already sets parent in both directions for asset and clones(0)
      (slice, operations.toList)
    }
    else{
      for (i <- 0 to clones.length-2){
        clones(i).children =  clones(i).children ++ List(clones(i+1))
        clones(i+1).parent = Some(clones(i))
      }

      val slice = clones(0)
      setNewNamesRecursively(slice, parent.name)
      setParentageBetween(parent, slice, insertAfter)

      operations += CloneAsset(source = asset, targetContainer = clones.last, insertAfter = None, cloneName = None, deepCloneFeature = false)
      (slice, operations.toList)
    }
  }

  def doPartialClone(asset: Asset):Asset={
    val clone = asset.doClone()
    clone.presenceCondition = True()
    clone.parent = None
    clone.children = Nil
    clone.featureModel = None
    AssetCloneHelpers.addTrace(asset,clone,asset.versionNumber)
    clone
  }

  def tryGetAssetClone(asset: Asset, targetAST: Asset) : Option[Asset] = {
    for(trace <- TraceDatabase.traces){
      if(trace.source == asset & Utilities.astContainsAsset(trace.target,targetAST))
        return Some(trace.target)
      else if(trace.target == asset & Utilities.astContainsAsset(trace.source,targetAST))
        return Some(trace.source)
    }
    None
  }
}