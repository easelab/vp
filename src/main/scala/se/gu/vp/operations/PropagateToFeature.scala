package se.gu.vp.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model._
import se.gu.vp.operations.ASTSliceHelpers.{cloneASTSlice, tryGetAssetClone}
import se.gu.vp.operations.Utilities._

import scala.collection.mutable

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class PropagateToFeature(sourceFeature:Path, targetFeature:Path) extends Operation with Logging {
  def getMostRecentFeatureClone(feature:Feature,clone:Feature): Option[FeatureTrace]={
    for(trace <- FeatureTraceDatabase.traces){
      if(trace.source == feature && trace.target == clone){
        return Some(trace)
      }
    }
    None
  }
  def featureClonedInTarget(feature:Feature,featuremodel:FeatureModel): Boolean ={
    for(trace<-FeatureTraceDatabase.traces){
      if(trace.source == feature && featureModelContainsFeature(trace.target,featuremodel)){
        return true
      }
    }
    false
  }
  def getFeatureClone(feature:Feature,featuremodel:FeatureModel): Option[FeatureTrace] ={
    for(trace<-FeatureTraceDatabase.traces){
      if(trace.source == feature && featureModelContainsFeature(trace.target,featuremodel)) return Some(trace)
      else if(trace.target == feature && featureModelContainsFeature(trace.source,featuremodel)) return Some(trace)
    }
    None
  }
  def getUnClonedAssets(feature:Feature,sourceast:Asset,clone:Feature,targetast:Asset):List[Asset]={

    val sourcemappedassets = feature.mappedAssets(sourceast)
    var unclonedassets:List[Asset] = Nil
    sourcemappedassets.foreach(c=>{
      var found = false
      val sourcemappedassetclones = findClones(c)
      for(mappedclone<-sourcemappedassetclones){
        if(astContainsAsset(mappedclone,targetast)){
          found = true
        }
      }
      if(found==false){//Found an uncloned asset
        unclonedassets = unclonedassets ++ List(c)
      }
    })
    unclonedassets
  }
  def detectChange(feature:Feature,clone:Feature):Boolean ={
    val mostrecentclonetrace = getMostRecentFeatureClone(feature,clone)
    mostrecentclonetrace match{
      case None => false;
      case _=> {
        if(feature.name!=clone.name || feature.optional!=clone.optional || feature.dependsOn!=clone.dependsOn){
          return true
        }
        else return false
      }
    }
  }
def propagateFeature(sourceFeature:Path, targetFeature:Path):Boolean = {

  val operationListBuilder = mutable.ListBuffer[Operation]()

  val s_assetpath = sourceFeature.path.collect { case a: AssetPathElement => a }
  val sourceassetpath = resolveAssetPath(sourceFeature.root,s_assetpath)
  val featuremodel = sourceassetpath.last.featureModel.get
  val s_featurepath = sourceFeature.path.collect { case a: FeaturePathElement => a }
  val sourcefeature = resolveFeaturePath(featuremodel,s_featurepath)

  val t_assetpath = targetFeature.path.collect { case a: AssetPathElement => a }
  val targetassetpath = resolveAssetPath(targetFeature.root,t_assetpath)
  val t_featurepath = targetFeature.path.collect { case a: FeaturePathElement => a }
  val targetfeaturemodel = targetassetpath.last.featureModel.get
  val targetfeature = resolveFeaturePath(targetfeaturemodel,t_featurepath)

  println("Found feature " + sourcefeature.get.name)
  println("Propagating changes in feature ...")
  println("Propagating changes in assets mapped to the feature " + sourcefeature.get.name)

  if(detectChange(sourcefeature.get,targetfeature.get)){
    targetfeature.get.name = sourcefeature.get.name
    targetfeature.get.optional = sourcefeature.get.optional
  }

  val mappedSourceAssets = sourcefeature.get.mappedAssets(sourceassetpath.last)

  /**
    * Propagating changes in the already cloned assets to the target
    */
  mappedSourceAssets.foreach(c=>{
    if(assetClonedInAST(c,targetassetpath.last)){
      val assetClone = tryGetAssetClone(c,targetassetpath.last)
      if(assetClone.isDefined)
           PropagateToAsset2(c,assetClone.get)
    }
  })

  /**
    * Cloning the (newly mapped) assets that are not cloned in the target. Starting with
    * high-level assets to avoid cone-duplication (a newly mapped file can have a newly
    * mapped function mapped to it, that we don't want to clone twice.
    */
  /**Cloning folders and files*/
  mappedSourceAssets.foreach(c=>{
    if(!assetClonedInAST(c,targetassetpath.last)) {
      if(!isCodeAsset(c)) {
        println("Asset is not cloned " + c)
        cloneASTSlice(c, targetassetpath.last, targetfeature.get).foreach(op => operationListBuilder += op)
      }
    }
  })

  /**Cloning classes*/
  mappedSourceAssets.foreach(c=>{
    if(!assetClonedInAST(c,targetassetpath.last) && c.assetType == ClassType) {
      println("Asset is not cloned " + c)
      cloneASTSlice(c, targetassetpath.last, targetfeature.get).foreach(op => operationListBuilder += op)
    }
  })

  /**Cloning methods*/
  mappedSourceAssets.foreach(c=>{
    if(!assetClonedInAST(c,targetassetpath.last) && c.assetType == MethodType) {
      println("Asset is not cloned " + c)
      cloneASTSlice(c, targetassetpath.last, targetfeature.get).foreach(op => operationListBuilder += op)
    }
  })

  /**Cloning blocks*/
  mappedSourceAssets.foreach(c=>{
    if(!assetClonedInAST(c,targetassetpath.last) && c.assetType == BlockType) {
      val ancestorMethod = tryGetAssetClone(c.parent.get,targetassetpath.last)
      if(ancestorMethod.isDefined){
        ancestorMethod.get.children.foreach(child => RemoveAsset(ancestorMethod.get,child))
        c.parent.get.children.reverse.foreach(child => CloneAsset(child,ancestorMethod.get))
      }
    }
  })

  for(s <- sourcefeature.get.subfeatures){
      if(featureClonedInTarget(s,targetfeaturemodel)){
        val featureclonetrace = getFeatureClone(s,targetfeaturemodel)
        val featureclone = featureclonetrace.get.target

        val spath = Path(sourceFeature.root, s_assetpath ++ computeFeaturePath(s))
        val tpath = Path(targetFeature.root, t_assetpath ++ computeFeaturePath(featureclone))

        propagateFeature(spath,tpath)
          //compute new paths and propagate
      }
    else{
        //create new clone
        val spath = Path(sourceFeature.root, s_assetpath ++ computeFeaturePath(s))
        operationListBuilder += CloneFeature(spath,targetFeature)
      }
  }
  subOperations = Some(operationListBuilder.toList)
  storeSubOpMetadata
  true
}
  override def perform(): Boolean = {

propagateFeature(sourceFeature,targetFeature)
    true
  }

  override def createMetadata(): Unit = {
    true
  }
}
object PropagateToFeature{
  def apply(sourceFeature: Path, targetFeature: Path, runImmediately: Boolean = true, createMetadata: Boolean = true): PropagateToFeature = {
    if (runImmediately && createMetadata) {
      new PropagateToFeature(sourceFeature, targetFeature) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new PropagateToFeature(sourceFeature, targetFeature) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new PropagateToFeature(sourceFeature, targetFeature) with CreateMetadata
    } else {
      new PropagateToFeature(sourceFeature, targetFeature)
    }
  }
}
