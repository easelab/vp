package se.gu.vp.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model._
import se.gu.vp.operations.JavaParser.JavaParserMain.isEmptyCodeBlock
import se.gu.vp.operations.Utilities._


/**
Copyright [2022] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class DiffAssets (assetone:Asset, assettwo:Asset) extends Operation with Logging {

  perform()

  def getShortName(asset:Asset):String =
    asset.name.split('\\').last.filter(!_.isDigit).replace("-","")

  def findDiff(assetone:Asset, assettwo:Asset):Unit = {

    println("\nFinding differences between source " +getShortName(assetone) + " and clone " + getShortName(assettwo) + "\n")

    transformASTToList(assetone).foreach(asset => {/**traversing variant 1 (left) first to see if all assets are cloned to variant 2*/
      if (findCloneInVariant(asset,assettwo)!=None) {
        val assetClone = findCloneInVariant(asset,assettwo).get /**There should be only one clone in the target, we put the check that the asset is not already cloned in CloneFeature*/
        val mappedFeaturesSource = asset.mappedToFeatures
        mappedFeaturesSource.foreach(sourceFeature => {
          if(assetMappedToFeatureClone(sourceFeature,assetClone) == false) {
            asset.assetType match {
              case BlockType =>{
                if(asset.parent!=None && !assetMappedToFeatureClone(sourceFeature,asset.parent.get)) {
                  asset.parent.get.assetType match{
                    case FileType =>  print("Difference found in a block in file " + getShortName(findImmediateParentFile(asset).get) + ". ")
                    case MethodType  => print("Difference found in a block in file " + getShortName(findImmediateParentFile(asset).get) + ". ")
                    case ClassType  => print("Difference found in a block in class " + getShortName(findImmediateParentClass(asset).get) + ". ")
                    case _=>;
                  }

                  println("The block is mapped to feature " + sourceFeature + " in variant " + getShortName(assetone) + " but not in variant " + getShortName(assettwo)+".")
                  asset.printContent
                }
              }
              case MethodType =>{
                print("Difference found in method " + getShortName(asset) + "() in file " + getShortName(findImmediateParentFile(asset).get) + ". ")
                println("The method is mapped to feature " + sourceFeature + " in variant " + getShortName(assetone) + " but not in variant " + getShortName(assettwo)+".")
              }
              case ClassType =>{
                print("Difference found in a class " + getShortName(asset) + " in file " + getShortName(findImmediateParentFile(asset).get) + ". ")
                println("The class is mapped to feature " + sourceFeature + " in variant " + getShortName(assetone) + " but not in variant " + getShortName(assettwo)+".")
              }
              case _ =>{
                print("Difference found in a the asset " + getShortName(asset) + ". ")
                println("The asset is mapped to feature " + sourceFeature + " in variant " + getShortName(assetone) + " but not in variant " + getShortName(assettwo)+".")
              }
            }
          }
        })
      }
      else{
        //Asset is not cloned at all. See if it is mapped to a feature not cloned in the second variant
        assettwo.featureModel match{
          case None=>;
          case _=>{
            asset.mappedToFeatures.foreach(feature=>{
              if(!featureClonedInFM(feature,getImmediateAncestorFeatureModel(Some(assettwo)).get)){
                asset.assetType match {
                  case BlockType => {
                    if(asset.content!=None && !isEmptyCodeBlock(asset)) {
                      if (!asset.parent.get.mappedToFeatures.contains(feature)) {
                        println("***\n")

                        asset.parent.get.assetType match {
                          case FileType => print("A block in file " + getShortName(findImmediateParentFile(asset).get) + " is not cloned in " + getShortName(assettwo) + ". ")
                          case ClassType => print("A block in " + getShortName(asset.parent.get) + " (class " + getShortName(findImmediateParentClass(asset).get) + ") is not cloned in " + getShortName(assettwo) + ". ")
                          case MethodType => print("A block in method " + getShortName(asset.parent.get) + "(), (file " + getShortName(findImmediateParentFile(asset).get) + ") is not cloned in " + getShortName(assettwo) + ". ")
                          case _=>;
                        }
                        println("Block is mapped to feature " + feature.name+".")
                        asset.printContent
                        println()
                      }
                    }
                  }
                  case MethodType =>{
                    print("Method " + getShortName(asset) +"() in file " + getShortName(findImmediateParentFile(asset).get) + " is not cloned in " + getShortName(assettwo) + ". ")
                    println("The method is mapped to feature " + feature.name+".")
                  }
                  case _=>{
                    print("Asset " + getShortName(asset) + " of type " + asset.assetType +" is not cloned in " + getShortName(assettwo) + ". ")
                    println("Asset is mapped to feature " + feature.name+".")
                  }
                }
              }
            })
          }
        }
      }
    }
    )
  }

  override def perform(): Boolean = {

    if(aIsCloneOfb(assetone,assettwo)) {

      findDiff(assetone,assettwo)
      findDiff(assettwo,assetone)

    }
    else{
      println(getShortName(assetone) + " and " + getShortName(assettwo) + " are not clones of each other. No traces found in .trace-db.")
    }
    storeSubOpMetadata
    true
  }
  override def createMetadata(): Unit = {}
}
object DiffAssets{
  def apply(assetone: Asset, assettwo: Asset): DiffAssets = new DiffAssets(assetone, assettwo)
}