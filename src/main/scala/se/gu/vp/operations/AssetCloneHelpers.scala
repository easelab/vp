package se.gu.vp.operations

import se.gu.vp.model._
import se.gu.vp.operations.Utilities.{changeAssetName, findFeatureClones, setNewNamesAfterCloning}

/**
Copyright [2019] [Wardah Mahmood, Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class AssetCloneHelpers {

}
object AssetCloneHelpers{

  def alreadyCloned(feature:Feature,featureModel:FeatureModel):Boolean={
    var clones = findFeatureClones(feature)
    clones.foreach(c=> {
      if (Utilities.transformFMToList(featureModel).contains(c))
        return true
    })
    false
  }
  def getLatestClone(source:Asset,target:Asset):Option[Trace] ={
    for(trace <- TraceDatabase.traces){
      if( trace.source == source && trace.target == target) return Some(trace) //removing return caused None to return
      else if(trace.target == source && trace.source == target) return Some(trace)
    }
    None
  }

  def addTrace(source:Asset,target:Asset,version:Long) : Unit = {
    TraceDatabase.traces = Trace(source,target,CloneOf,version) :: TraceDatabase.traces
  }

  def findFeatureByName(feature:Feature,featuremodel:FeatureModel) : Feature = {
    val featuremodelfeatures = Utilities.transformFMToList(featuremodel)
    for(fmfeature <- featuremodelfeatures){
      if(fmfeature.name == feature.name)
        return  fmfeature
    }
    null
  }
  def getFeatureClone(feature:Feature,featuremodel:FeatureModel) : Feature = {
    for(trace <- FeatureTraceDatabase.traces){
      if(trace.source == feature & Utilities.featureModelContainsFeature(trace.target,featuremodel))
        return trace.target
    }
    null
  }
  def featurePresentInTarget(feature:Feature,featuremodel:FeatureModel) : Boolean = {
    var found = false
    val featuremodelfeatures = Utilities.transformFMToList(featuremodel)
    for(fmfeature <- featuremodelfeatures){
      if(fmfeature.name == feature.name)
        found = true
    }
    found
  }
  def featureAlreadyCloned(feature:Feature,featuremodel:FeatureModel) : Boolean = {
     var found = false
      for(trace <- FeatureTraceDatabase.traces){
        if(trace.source == feature & Utilities.featureModelContainsFeature(trace.target,featuremodel))
          found = true
      }
    found
  }

  //targetasset is set once even when going down recursively -> this gives the target feature model starting from the initial target parent
  //targetParent is set to the freshly created parent, when iterating over the source's children
  def deepCloneAsset(source:Asset, targetasset:Asset, deepCloneFeature: Boolean, targetParent:Option[Asset]=None, cloneName: Option[String]=None) : Asset = {
    val clone = source.doClone()

    if (cloneName.isDefined) {
      changeAssetName(clone, cloneName.get)
    }
    setNewNamesAfterCloning(clone,targetasset)

    targetParent match {
      case None=>;
      case _=>
        clone.parent = targetParent
        if (List(FolderType, FileType).contains(clone.assetType)) {
          clone.name = targetParent.get.name + "\\" + clone.name.split('\\').last
        }
    }

    clone.presenceCondition = True()

    if (deepCloneFeature) {
      val targetfeaturemodel = Utilities.getImmediateAncestorFeatureModel(Some(targetasset))
      targetfeaturemodel match {
        case None => ;
        case _ => {
          for (mappedfeature <- source.mappedToFeatures) {
            if (!featurePresentInTarget(mappedfeature, targetfeaturemodel.get) & !featureAlreadyCloned(mappedfeature, targetfeaturemodel.get)) {
              val featureclone = FeatureCloneHelpers.deepCloneFeature(mappedfeature)
              FeatureCloneHelpers.addUnassignedFeature(featureclone, targetfeaturemodel.get)
              MapAssetToFeature(clone, featureclone)
            }
            else if (featureAlreadyCloned(mappedfeature, targetfeaturemodel.get) == true) {
              val featureclone = getFeatureClone(mappedfeature, targetfeaturemodel.get)
              MapAssetToFeature(clone, featureclone)
            }
            else if (featurePresentInTarget(mappedfeature, targetfeaturemodel.get) == true) {
              val feature = findFeatureByName(mappedfeature, targetfeaturemodel.get)
              MapAssetToFeature(clone, feature)
            }
          }
        }
      }
    }

    clone.featureModel = None
    clone.children = Nil
    addTrace(source,clone,source.versionNumber)

    for(child <- source.children){
      val childclone = deepCloneAsset(child, targetasset, deepCloneFeature, Some(clone))
      clone.children =  clone.children ++ List(childclone)
    }
    clone
  }
}
