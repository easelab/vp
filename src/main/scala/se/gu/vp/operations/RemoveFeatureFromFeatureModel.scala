package se.gu.vp.operations
import se.gu.vp.commons.Logging
import se.gu.vp.model._
import se.gu.vp.operations.metadata.{MetadataConverter, RemoveFeatureFromFeatureModelMetadata}

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */
class RemoveFeatureFromFeatureModel(featuretodelete:Feature) extends Operation with Logging {
  override def perform(): Boolean = {

    updateFeatureVersion(featuretodelete.parent.get)
    featuretodelete.parent.get.subfeatures = dropIndex (featuretodelete.parent.get.subfeatures, featuretodelete.parent.get.subfeatures.indexOf (featuretodelete) )
    featuretodelete.parent = None
    info ("Feature " + featuretodelete + " Deleted from Feature Model")
    storeSubOpMetadata
    false
  }
  def dropIndex[T](list:List[T],idx:Int):List[T]={
    list.zipWithIndex.filter(_._2 != idx).map(_._1)
  }

  override def createMetadata(): Unit = {
    try {
      val featureMD = MetadataConverter.convertFeatureToMetadata(featuretodelete)

      metadata = Some(RemoveFeatureFromFeatureModelMetadata(featureMD))
    } catch {
      case e : Exception =>
        println("Something went wrong during metadata creation.")
    }
  }
}

object RemoveFeatureFromFeatureModel{
  def apply(featuretodelete: Feature, runImmediately: Boolean = true, createMetadata: Boolean = true): RemoveFeatureFromFeatureModel = {
    if (runImmediately && createMetadata) {
      new RemoveFeatureFromFeatureModel(featuretodelete) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new RemoveFeatureFromFeatureModel(featuretodelete) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new RemoveFeatureFromFeatureModel(featuretodelete) with CreateMetadata
    } else {
      new RemoveFeatureFromFeatureModel(featuretodelete)
    }
  }
}