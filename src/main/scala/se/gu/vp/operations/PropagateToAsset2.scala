package se.gu.vp.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model._
import se.gu.vp.operations.Utilities._

import scala.collection.mutable

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

/*
* Author: Wardah Mahmood. Date: 08-02-2019
* Propagation of an asset involves finding a tracelink in the traces,
* using that trace to determine if the source has infact changed,
* and the target hasn't, and then if all these are true, locating the asset.
* Once the asset is located, it is removed from the container, and the updated
* source is recloned and it's tracelink is added again.
* */

//optional parameter to replace content
//Make propagation bidirectional
class PropagateToAsset2(source:Asset, target:Asset) extends Operation with Logging {

  def propagateNewMappings(source:Asset, target:Asset):Unit={

    val operationListBuilder = mutable.ListBuffer[Operation]()

    val sourcefeatures = source.mappedToFeatures
    val closestAncestor = getImmediateAncestorFeatureModel( Some(target) )
    closestAncestor match {
      case Some( fm ) =>
        sourcefeatures.foreach(
          m => {
            if (!AssetCloneHelpers.alreadyCloned(m, fm)) {
              val fclone = FeatureCloneHelpers.deepCloneFeature(m)
              FeatureCloneHelpers.addUnassignedFeature(fclone, closestAncestor.get)
              operationListBuilder += MapAssetToFeature(target, fclone)
            }
            else {
              operationListBuilder += MapAssetToFeature(target,AssetCloneHelpers.getFeatureClone(m,closestAncestor.get))
            }
          })
      case _ => ;
    }
  }


  def propagate(source:Asset,target:Asset):Boolean={

    val operationListBuilder = mutable.ListBuffer[Operation]()

    val latestTrace = AssetCloneHelpers.getLatestClone(source,target) //bi-directional + don't need to check asset type

    latestTrace match{
      case None =>
        operationListBuilder += CloneAsset(source,target.parent.get);
      case Some(trace) => {

        //The source and target have a clone relationship
        if(Utilities.getLastModifiedTime(source) > trace.versionAt) {

          source.assetType match {
            case RepositoryType => {
              //Add all sub-folders and sub-files
              //Should we propagate addition of a feature model?
              source.children.foreach(c => {
                if (!assetClonedInAST(c, target)) {
                  CloneAsset(c, target)
                }
              })
            }
            case FolderType =>{
          //Add all sub-folders and sub-files
          source.children.foreach(c => {
            if (!assetClonedInAST(c, target)) {
              CloneAsset(c, target)
            }
          })
          propagateNewMappings(source,target)
         }
            /**
              * Change propagation should be deep. For repository,
              * invoke change propagation of sub-folders and files.
              * Also, the detection algorithm for changes needs to
              * be a bit more sophisticated.
              */
            case FileType=>{
              target.children.foreach(c => RemoveAsset(target,c))
              source.children.foreach(c => CloneAsset(c,target))
            }
            case ClassType=>{
              target.children.foreach(c => RemoveAsset(target,c))
              source.children.foreach(c => CloneAsset(c,target))
            }
            case MethodType=>{
              target.content = source.content
              target.children.foreach(c => RemoveAsset(target,c))
              source.children.foreach(c => CloneAsset(c,target))
            }
            case BlockType=>{
              /* This is for the experiment only. If there is a change in
              a block, we won't be able to identify it, so we replace the
              content of the parent method
              */
              target.parent match{
                case None=>;
                case _=>{
                  target.parent.get.children.foreach(c => RemoveAsset(target.parent.get,c))
                  source.children.foreach(c => CloneAsset(c,target))
                }
              }

            }
          }
          updateVersion(target)
          AssetCloneHelpers.addTrace(source,target,source.versionNumber)


          //recursive unless code asset
          for(sourcesubasset <- source.children){
            val subassetclonetrace = findMostRecentCloneTrace(sourcesubasset,target)
            subassetclonetrace match {
              case Some(trace) => {
                propagate(sourcesubasset, trace.target)
              }
              case None => ;//already cloned above
            }
          }
        }
      }
    }
    subOperations = Some(operationListBuilder.toList)
    storeSubOpMetadata
    true
  }
  override def perform(): Boolean = {
    propagate(source,target)
    true
  }

  override def createMetadata(): Unit = {
    true
  }
}
object PropagateToAsset2{
  def apply(source: Asset, target: Asset, runImmediately: Boolean = true, createMetadata: Boolean = true): PropagateToAsset2 = {
    if (runImmediately && createMetadata) {
      new PropagateToAsset2(source, target) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new PropagateToAsset2(source, target) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new PropagateToAsset2(source, target) with CreateMetadata
    } else {
      new PropagateToAsset2(source, target)
    }
  }
}