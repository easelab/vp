package se.gu.vp.operations.JavaParser

import java.io.File
import java.util.regex.Pattern

import se.gu.vp.model._
import se.gu.vp.operations.CalculatorSimulation.ASTModificationHelpers.handleFeatureModel
import se.gu.vp.operations.CalculatorSimulation.BasicCalculator.{getListOfSubDirectories, isAnnotation}
import se.gu.vp.operations.Replay.FeatureRelatedHelpers.findFeatureByName
import se.gu.vp.operations.Replay.{AnnotatedFileHelpers, Annotation, AssetHelpers}
import se.gu.vp.operations.Utilities.{getHashCode, getImmediateAncestorFeatureModel, getassetbyname, transformASTToList}
import se.gu.vp.operations._

import scala.io.Source
import scala.util.control.Breaks.{break, breakable}

/**
Copyright [2020] [Wardah Mahmood, Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

//Add JSON + JS parsing here
//Rename
object JavaParserMain{

  def readInFileTree(currentPath: File, parentAsset: Asset) : Unit = {
    val assetType =
      if (currentPath.isDirectory)
      { FolderType }
      else
      { FileType }

    assetType match {
      case FolderType =>
        val folderAsset = Asset(currentPath.getAbsolutePath, assetType)
        AddAsset(folderAsset, parentAsset)
        currentPath.listFiles
          .foreach(file => readInFileTree(file, folderAsset))
      case FileType
        if currentPath.getAbsolutePath.endsWith("featuremodel.txt") =>
        handleFeatureModel(currentPath, parentAsset)
      case FileType
        if currentPath.getAbsolutePath.endsWith(".java") =>
        val fileAsset = parseJavaFile(currentPath, getImmediateAncestorFeatureModel(Some(parentAsset)).get)
        AddAsset(fileAsset, parentAsset)
      case FileType =>
        val source = Source.fromFile(currentPath)
        val lines = source.getLines.toList
        source.close

        val fileAsset = Asset(currentPath.getAbsolutePath, assetType, content = Some(lines))
        AddAsset(fileAsset, parentAsset)
    }
  }


  def readInJavaAssetTree(rootFilePath: String) : Option[Asset] = {
    val commitlog = new File(rootFilePath + File.separator + "CommitLog.txt")
    val astprintfile = new File(rootFilePath + File.separator + "ASTprint.txt")

    val rootfile = new File(rootFilePath)
    val result = if (rootfile.exists()) {
      val astroot = AssetHelpers.createRoot(rootfile)
      getListOfSubDirectories(rootFilePath).foreach(c => {
        val repofile = new File(rootFilePath + "//" + c)
        if (repofile.exists()) {
          AssetHelpers.createRepository(repofile, astroot)
          val repositoryasset = getassetbyname(repofile.getAbsolutePath, astroot).get
          repofile.listFiles.foreach(file => readInFileTree(file, repositoryasset))
        }
      })
      Some(astroot)
    } else {
      None
    }
    result
  }

  var currentIndex:Int = 0
  var loopVariable:Int = 0
  var startingIndex:Int = 0
  class Temp(var start:Int, var end:Int, var mappedFeatures:List[Feature] = Nil)
  class Comment(var start:Int, var end:Int, var startColumn:Int, var endColumn:Int)

  def loadComments(fileLines:List[String]):List[Comment]={

    var comments:List[Comment] = Nil
    val singleline = "//"
    val multilineStart = "/\\*"
    val multilineEnd = "\\*/"
    val filePathRegex = "\".*//"

    val patternSingleLine = Pattern.compile(singleline)
    val patternMultilineStart = Pattern.compile(multilineStart)
    val patternMultilineEnd = Pattern.compile(multilineEnd)
    val patternFilePath= Pattern.compile(filePathRegex)

    var marker:Int = 0

    for(loop<-marker to fileLines.length-1){
      breakable {
        val matcher1 = patternSingleLine.matcher(fileLines(loop))
        val matched1 = matcher1.find()

        val matcher2 = patternFilePath.matcher(fileLines(loop))
        val matched2 = matcher2.find()

        val matcher3 = patternMultilineStart.matcher(fileLines(loop))
        val matched3 = matcher3.find()

        if (matched1 && !matched2) {
          comments = comments ++ List(new Comment(loop,loop,matcher1.start(),fileLines(loop).length))
          marker += 1
          break()
        }
        else if(matched3) {
          marker = loop
          breakable {
            while (marker <= fileLines.length - 1) {

              val matcher4 = patternMultilineEnd.matcher(fileLines(marker))
              var matched4 = matcher4.find()
              if (matched4 == false) {
                val matcher4 = patternMultilineEnd.matcher(fileLines(marker))
                matched4 = matcher4.find()
                marker += 1
              }
              else {
                comments = comments ++ List(new Comment(loop,marker,matcher3.start(),matcher4.start()))
                break()
              }
            }
          }
          break()
        }
      }
    }
    comments
  }

  def insideComment(comments:List[Comment],bracketRow:Int,bracketColumn:Int):Boolean = {

    var isComment = false
    breakable(
      for(comment <- comments){
        if((comment.start to comment.end).contains(bracketRow)){
          if((comment.startColumn to comment.endColumn).contains(bracketColumn)) {
            isComment = true
            break()
          }
        }
      })
    isComment
  }

  def dropIndex[T](list: List[T], idx: Int): List[T] = {
    list.zipWithIndex.filter(_._2 != idx).map(_._1)
  }

  def countFrequency(string: String, char: Char, row:Int,comments:List[Comment]): Int = {
    var frequency = 0
    for (column <- 0 to string.length - 1) {
      if (string.charAt(column) == char) {
        if(!insideComment(comments,row,column))
          frequency = frequency + 1
      }
    }
    return frequency
  }

  def findEndingIndex(fromindex: Int, filelines: List[String], comments:List[Comment]): Int = {
    var countleft = 0
    var countright = 0
    var toindex = 0
    breakable {
      for (i <- fromindex to filelines.length - 1) {
        countleft += countFrequency(filelines(i), '{',i,comments)
        countright += countFrequency(filelines(i), '}',i,comments)
        if (countright == countleft & i > fromindex) {
          toindex = i
          return toindex
        }
        else toindex = filelines.length - 1
      }
    }
    return toindex
  }

  def annotateNonBlockAssets (asset:Asset, annotations: List[Annotation], start:Int, end:Int, featureModel: FeatureModel):Unit={
    for(annotation <- annotations){
      var feature = Utilities.findFeatureByName(annotation.featurename,featureModel)
      if(feature == None) {
        val newfeature = Feature(annotation.featurename)
        AddFeatureToFeatureModel(newfeature,featureModel, true, true, true)
        feature = Some(newfeature)
      }
      if(annotation.beginat <= start && annotation.endat >= end) {
        asset.presenceCondition = feature.get & asset.presenceCondition
      }
    }
  }

  def addAnnotations(filelines:List[String], annotations:List[Annotation], absStart:Int, absEnd:Int, featureModel: FeatureModel):List[Asset] = {

    val block = new Temp(absStart,absEnd,Nil)
    var blockList:List[Temp] = block :: Nil

    for(annotation <- annotations){
      for(block <- blockList) {
        var feature = findFeatureByName(annotation.featurename, featureModel)
        if (feature == None) {
          val newfeature = Feature(annotation.featurename)
          // UNASSIGNED as default, but would it be useful to do a name-based search to find a more suitable parent? Or can there be cases, where you dont want to use that as a parent?
          AddFeatureToFeatureModel(newfeature, featureModel.rootfeature.UNASSIGNED)

          feature = Some(newfeature)
        }
        val annotationrange = annotation.beginat to annotation.endat
        val blockrange = block.start to block.end
        val common = blockrange.intersect(annotationrange)
        if (!common.isEmpty) {
          //Case 1: Whole block is annotated. No splitting, just mapping
          if (annotation.beginat <= block.start && annotation.endat >= block.end) {
            if(!block.mappedFeatures.contains(feature)) block.mappedFeatures = feature.get :: block.mappedFeatures
          }
          //case 2: Some part is annotated. Annotation begins outside, ends inside
          else if (annotation.beginat <= block.start && annotation.endat < block.end) {

            val block1 = new Temp(block.start,annotation.endat,block.mappedFeatures)
            if(!block1.mappedFeatures.contains(feature)) block1.mappedFeatures = feature.get :: block1.mappedFeatures
            val block2 = new Temp(annotation.endat+1,block.end, block.mappedFeatures)

            blockList = dropIndex(blockList, blockList.indexOf(block))
            blockList = block2 :: block1 :: blockList
          }

          //case 3: Some part is annotated. annotation begins inside, ends outside
          else if (annotation.beginat > block.start && annotation.endat >= block.end) {

            val block1 = new Temp(block.start,annotation.beginat - 1,block.mappedFeatures)
            val block2 = new Temp(annotation.beginat, block.end, block.mappedFeatures)
            if(!block2.mappedFeatures.contains(feature)) block2.mappedFeatures = feature.get :: block2.mappedFeatures

            blockList = dropIndex(blockList, blockList.indexOf(block))
            blockList = block2 :: block1 :: blockList

          }
          //case 4: Part of the block is annotated. Annotation begins and ends inside
          else if (annotation.beginat > block.start && annotation.endat < block.end) {

            val block1 = new Temp(block.start,annotation.beginat - 1,block.mappedFeatures)
            val block2 = new Temp(annotation.beginat, annotation.endat, block.mappedFeatures)
            if(!block2.mappedFeatures.contains(feature)) block2.mappedFeatures = feature.get :: block2.mappedFeatures
            val block3 = new Temp(annotation.endat+1, block.end, block.mappedFeatures)

            blockList = dropIndex(blockList, blockList.indexOf(block))
            blockList = block3 :: block2 :: block1 :: blockList
          }
        }
      }
    }
    var assetList:List[Asset] = Nil
    blockList.foreach(c=>{
      val blockToAdd = Asset("block",BlockType)
      val content = filelines.slice(c.start,c.end+1)
      blockToAdd.content = Some(content)
      for (elem <- c.mappedFeatures) {
        blockToAdd.presenceCondition = elem & blockToAdd.presenceCondition
      }

      var newList:List[String] = Nil;
      for(i<-0 to blockToAdd.content.get.length-1){
        if(!isAnnotation(blockToAdd.content.get(i))){
          newList = blockToAdd.content.get(i) :: newList
        }
      }
      blockToAdd.content = Some(newList.reverse)
      blockToAdd.name = blockToAdd.name + getHashCode(blockToAdd)
      assetList = blockToAdd :: assetList
    })
    assetList
  }

  def removeAnnotations(asset:Asset):Unit = {

    val beginAnnotation = "//\\s*[&$]\\s*[Bb]egin\\s*\\[+.*\\]+"
    val endAnnotation = "//\\s*[&$]\\s*[Ee]nd\\s*\\[+.*\\]+"
    val lineAnnotation = "//\\s*[&$]\\s*[Ll]ine\\s*\\[+.*\\]+"

    var newContent:List[String] = Nil
    var newContentString = ""
    asset.content match{
      case None=>;
      case _=>{
        asset.content.get.reverse.foreach(c=>{
          newContentString = c.replaceAll(beginAnnotation,"")
          newContentString = newContentString.replaceAll(endAnnotation,"")
          newContentString = newContentString.replaceAll(lineAnnotation,"")
          newContent =  newContentString :: newContent
        })
      }
    }
    asset.content = Some(newContent)
  }

  def cleanCode(asset:Asset):Unit = {
    if(asset.assetType == BlockType) {
      print("Hello")
      asset.assetType match {
        case BlockType => {}
          asset.content match{
            case None=>;
            case _=>{
              if(asset.content.get.size == 1){
                if(asset.content.get(0).trim=="") {
                  RemoveAsset(asset.getRoot().get,asset)
                }
              }
            }
          }
      }
    }
  }

  def parseJavaFile(file:File, featureModel: FeatureModel):Asset={

    loopVariable = 0
    currentIndex = 0
    startingIndex = 0

    val filesource = io.Source.fromFile(file.getAbsolutePath, "ISO-8859-1")
    val filelines = filesource.getLines.toList
    filesource.close

    val annotations = AnnotatedFileHelpers.getAnnotations(file)
    val comments = loadComments(filelines)

    val fileAsset = Asset(file.getAbsolutePath,FileType)

    val classRegex = ("\\s*(public|private|protected)*\\s*class\\s+(\\w+)(<.*>)?\\s*((extends\\s+\\w+(<.*>)?)|(implements\\s+\\w+(<.*>)?( ,\\w+(<.*>)?)*))?\\s*").r
    while(loopVariable<filelines.length-1 && currentIndex < filelines.length-1){
      loopVariable = currentIndex
      breakable(
        if (classRegex.findFirstIn(filelines(loopVariable)) == None) {
          breakable(
            while(loopVariable<filelines.length-1){
              if (loopVariable<filelines.length-1 && classRegex.findFirstIn(filelines(loopVariable)) == None) {
                loopVariable+=1
              }
              else{
                val annotatedAssets:List[Asset] = addAnnotations(filelines,annotations,currentIndex,loopVariable - 1, featureModel)
                annotatedAssets.foreach(c => AddAsset(c,fileAsset))
                currentIndex = loopVariable
                break()
              }
            }
          )
          break()
        }
        else{
          val className = filelines(loopVariable).split("class ")(1).split("( |\\{)")(0)
          val classAsset = Asset(className, ClassType)
          AddAsset(classAsset, fileAsset)
          val content:List[String] = filelines(loopVariable) :: Nil
          classAsset.content = Some(content)
          val classEnd = findEndingIndex(loopVariable, filelines, comments)
          annotateNonBlockAssets(classAsset,annotations,loopVariable,classEnd,featureModel)

          var j:Int = loopVariable + 1
          val methodRegex = ("\\s*(?:(?:public|private|protected|static|final|native|synchronized|abstract|transient)+\\s+)+[$_\\w<>\\[\\]\\s]*\\s+[\\$_\\w]+\\([^\\)]*\\)?\\s*\\{?[^\\}]*\\}?").r
          val constructorRegex = ("\\s*"+ classAsset.name +"\\s*\\(.*\\)\\s*\\{").r;

          while(j<=classEnd) {

            breakable(
              if (methodRegex.findFirstIn(filelines(j)) != None) {

                val methodEnd = findEndingIndex(j, filelines, comments)
                val methodName = filelines(j).split("\\(")(0).split(" ").last
                var methodcontent:List[String] = filelines(j) :: Nil
                val methodAsset = Asset(methodName, MethodType)
                methodAsset.content = Some(methodcontent)
                methodAsset.name = (methodName + getHashCode(methodAsset)).trim
                AddAsset(methodAsset, classAsset)
                annotateNonBlockAssets(methodAsset,annotations,j,methodEnd,featureModel)

                val annotatedAssets:List[Asset] = addAnnotations(filelines,annotations,j + 1, methodEnd ,featureModel)

                annotatedAssets.foreach(c => AddAsset(c,methodAsset))

                startingIndex = methodEnd + 1
                j = startingIndex
                break()
              }
              //constructor
              else if (constructorRegex.findFirstIn(filelines(j)) != None) {
                val methodEnd = findEndingIndex(j, filelines, comments)
                val methodName = filelines(j).split("\\(")(0).split(" ").last
                val methodAsset = Asset(methodName, MethodType)
                val methodcontent:List[String] = filelines(j) :: Nil
                methodAsset.content = Some(methodcontent)
                methodAsset.name = (methodName + getHashCode(methodAsset)).trim
                AddAsset(methodAsset, classAsset)
                annotateNonBlockAssets(methodAsset,annotations,j,methodEnd,featureModel)


                val annotatedAssets:List[Asset] = addAnnotations(filelines,annotations, j + 1, methodEnd,featureModel)
                annotatedAssets.foreach(c => AddAsset(c,methodAsset))

                startingIndex = methodEnd + 1
                j = startingIndex
                break()
              }
              else {
                var k: Int = j
                val methodRegex = ("\\s*(?:(?:public|private|protected|static|final|native|synchronized|abstract|transient)+\\s+)+[$_\\w<>\\[\\]\\s]*\\s+[\\$_\\w]+\\([^\\)]*\\)?\\s*\\{?[^\\}]*\\}?").r
                val constructorRegex = ("\\s*" + classAsset.name + "\\s*\\(.*\\)\\s*\\{").r;

                while (k <= classEnd) {
                  if (methodRegex.findFirstIn(filelines(k)) == None && constructorRegex.findFirstIn(filelines(k)) == None) {
                    k += 1
                  }
                  else {
                    val annotatedAssets:List[Asset] = addAnnotations(filelines,annotations,j, k - 1, featureModel)
                    annotatedAssets.foreach(c => AddAsset(c,classAsset))
                    startingIndex = k
                    j = startingIndex
                    break()
                  }
                }
                val annotatedAssets:List[Asset] = addAnnotations(filelines,annotations,j, k - 1, featureModel)
                annotatedAssets.foreach(c => AddAsset(c,classAsset))
                startingIndex = k //Do I need this
                j = startingIndex //Do I need this
                break()
              })
          }
          currentIndex = classEnd + 1
          break()
        }
      )
    }

    if(currentIndex < filelines.length - 1) {
      val blockAsset = Asset("block", BlockType)
      val content = filelines.slice(currentIndex, filelines.length)
      blockAsset.content = Some(content)
      blockAsset.name = blockAsset.name + getHashCode(blockAsset)
      AddAsset(blockAsset, fileAsset)
    }

    (fileAsset :: Utilities.transformToList(fileAsset)).foreach(c=>removeAnnotations(c))
    // cleanCode calls RemoveAsset, which currently requires the asset to already be part of an AssetTree including Root (getRoot.get is called, which is None)

    //Removing empty blocks from code
    for(asset <- transformASTToList(fileAsset).filter(p=>p!=fileAsset)) {
      if (isEmptyCodeBlock(asset)) {
        asset.parent.get.children = dropIndex(asset.parent.get.children, asset.parent.get.children.indexOf(asset))
        asset.parent = None
      }
    }
    /** Setting new name recursively for code assets */
    transformASTToList(fileAsset).filter(p=>p!=fileAsset).foreach(asset => {
      asset.name = asset.parent.get.name + "\\" + asset.name.split('\\').last
    })
    fileAsset
  }

  def isEmptyCodeBlock(asset:Asset):Boolean={
    asset.assetType match{
      case BlockType =>{
        asset.content match{
          case None => return true;
          case _=>{
            var empty:Boolean = true
            asset.content.get.foreach(c=>if(!c.trim.equals("")) {
              empty = false
            })
            return empty
          }
        }
      }
      case _=> return false
    }
  }

  //Main removed, now in ParserTests called "Test by Christoph"
}