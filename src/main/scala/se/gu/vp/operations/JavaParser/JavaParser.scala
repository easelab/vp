package se.gu.vp.operations.JavaParser
import se.gu.vp.model._
import se.gu.vp.operations.AddAsset
import se.gu.vp.operations.Replay.AnnotatedFileHelpers.countFrequency

import scala.util.control.Breaks.{break, breakable}

/**
Copyright [2020] [Wardah Mahmood, Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

object JavaParser {

  def findEndingIndex(fromindex: Int, filelines: List[String]): Int = {
    var countleft = 0
    var countright = 0
    var toindex = 0
    breakable {
      for (i <- fromindex to filelines.length - 1) {
        countleft += countFrequency(filelines(i), '{')
        countright += countFrequency(filelines(i), '}')
        if (countright == countleft & i > fromindex) {
          toindex = i
          return toindex
        }
        else toindex = filelines.length - 1
      }
    }
    return toindex
  }

  def getGlobalCode(startingIndex:Int,lines:List[String],fileAsset:Asset):Int={
    var i:Int = startingIndex
    val classRegex = ("\\s*(public|private|protected)*\\s*class\\s+(\\w+)\\s+((extends\\s+\\w+)|(implements\\s+\\w+( ,\\w+)*))?\\s*\\{").r
    breakable{
      while(i<lines.length-1){
        if (classRegex.findFirstIn(lines(i)) == None) {
          i+=1
        }
        else{
          println("I am here")
          val blockasset = Asset("block",BlockType)
          val content = lines.slice(startingIndex,i)
          blockasset.content = Some(content)
          AddAsset(blockasset,fileAsset)
          loadJavaFile(i,lines,fileAsset)
          break()
          return i
        }
      }
    }
    val blockasset = Asset("block",BlockType)
    val content = lines.slice(startingIndex,i)
    blockasset.content = Some(content)
    loadJavaFile(i,lines,fileAsset)
    break()
    return i
  }


  def loadClass(start:Int, end:Int, lines:List[String], classAsset:Asset, fileAsset:Asset):Unit = {


    var classlines = lines.slice(start,end+1)
    var startingIndex:Int = 0
    val methodRegex = ("\\s*(?:(?:public|private|protected|static|final|native|synchronized|abstract|transient)+\\s+)+[$_\\w<>\\[\\]\\s]*\\s+[\\$_\\w]+\\([^\\)]*\\)?\\s*\\{?[^\\}]*\\}?").r
    val constructorRegex = ("\\s*"+ classAsset.name +"\\s*\\(.*\\)\\s*\\{").r;

    for (i <- startingIndex to classlines.length - 1) {
      //method found
      breakable(
        if (methodRegex.findFirstIn(classlines(i)) != None) {

          val end = findEndingIndex(i, classlines)
          val methodName = classlines(i).split("\\(")(0).split(" ").last
          println("Method name is " + methodName)
          val methodAsset = Asset(methodName, MethodType)
          AddAsset(methodAsset, classAsset)

          val content = classlines.slice(i + 1, end + 1)
          val blockAsset = Asset("block", BlockType)
          blockAsset.content = Some(content)
          AddAsset(blockAsset, methodAsset)
          startingIndex = end
          break()
        }
        //constructor
        else if (constructorRegex.findFirstIn(classlines(i)) != None) {
          val end = findEndingIndex(i, classlines)
          val methodName = classlines(i).split("\\(")(0).split(" ").last
          println("Method name is " + methodName)
          val methodAsset = Asset(methodName, MethodType)
          AddAsset(methodAsset, classAsset)

          val content = classlines.slice(i + 1, end + 1)
          val blockAsset = Asset("block", BlockType)
          blockAsset.content = Some(content)
          AddAsset(blockAsset, methodAsset)
          startingIndex = end
          break()
        }
        else {
          startingIndex = getGlobalClassCode(i, lines, classAsset)
          break()
        })
    }
    loadJavaFile(end,lines,fileAsset)
  }

  def getGlobalClassCode(startingIndex:Int, lines:List[String],classAsset:Asset):Int={
    var i:Int = startingIndex
    val methodRegex = ("\\s*(?:(?:public|private|protected|static|final|native|synchronized|abstract|transient)+\\s+)+[$_\\w<>\\[\\]\\s]*\\s+[\\$_\\w]+\\([^\\)]*\\)?\\s*\\{?[^\\}]*\\}?").r
    val constructorRegex = ("\\s*"+ classAsset.name +"\\s*\\(.*\\)\\s*\\{").r;

    while(i<lines.length-1){
      if (methodRegex.findFirstIn(lines(i)) == None && constructorRegex.findFirstIn(lines(i)) == None) {
        i+=1
      }
      else{
        val content = lines.slice(startingIndex, i + 1)
        val blockAsset = Asset("block", BlockType)
        blockAsset.content = Some(content)
        AddAsset(blockAsset, classAsset)
        return i
      }
    }
    return i
  }

  def loadJavaFile(startingIndex:Int, filelines:List[String],fileAsset:Asset):Unit = {

    var currentIndex = startingIndex
      val classRegex = ("\\s*(public|private|protected)*\\s*class\\s+(\\w+)\\s+((extends\\s+\\w+)|(implements\\s+\\w+( ,\\w+)*))?\\s*\\{").r
    for(i<-currentIndex to filelines.length-1) {
     breakable(
      if (classRegex.findFirstIn(filelines(i)) == None) {
         getGlobalCode(i, filelines, fileAsset)
        break()
      }
      else {
        val className = filelines(i).split("class ")(1).split(" ")(0)
        val classAsset = Asset(className, ClassType)
        AddAsset(classAsset, fileAsset)
        val end = findEndingIndex(i, filelines)
        loadClass(i, end, filelines, classAsset, fileAsset)
        break()
      })
  }
  }
}
