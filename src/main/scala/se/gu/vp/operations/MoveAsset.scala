package se.gu.vp.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model._

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */
// TODO: "insertAfter" or "indexing"
class MoveAsset (asset:Asset,targetasset:Asset) extends Operation with Logging {

  def dropIndex[T](list: List[T], idx: Int): List[T] = {
    list.zipWithIndex.filter(_._2 != idx).map(_._1)
  }

  override def perform(): Boolean = {
      asset.parent match {
        case None => {
          asset.parent = Some(targetasset)
          asset.parent.get.children = asset.parent.get.children ++ List(asset)
          updateVersion(asset.parent.get)
        }
        case _ => {
          asset.parent.get.children = dropIndex(asset.parent.get.children, asset.parent.get.children.indexOf(asset))
          updateVersion(asset.parent.get)
          asset.parent = Some(targetasset)
          asset.parent.get.children = asset.parent.get.children ++ List(asset)
          updateVersion(asset.parent.get)
        }
    }
    storeSubOpMetadata
    true
  }

  override def createMetadata(): Unit = {
    true
  }
}
object MoveAsset{
  def apply(asset: Asset, targetasset: Asset, runImmediately: Boolean = true, createMetadata: Boolean = true): MoveAsset = {
    if (runImmediately && createMetadata) {
      new MoveAsset(asset, targetasset) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new MoveAsset(asset, targetasset) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new MoveAsset(asset, targetasset) with CreateMetadata
    } else {
      new MoveAsset(asset, targetasset)
    }
  }
}