package se.gu.vp.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model._
import se.gu.vp.operations.Utilities._

import scala.collection.mutable

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

* Propagation of an asset involves finding a tracelink in the traces,
* using that trace to determine if the source has infact changed,
* and the target hasn't, and then if all these are true, locating the asset.
* Once the asset is located, it is removed from the container, and the updated
* source is recloned and it's tracelink is added again.
* */

//optional parameter to replace content
class PropagateToAsset(source:Asset, target:Asset) extends Operation with Logging {
  def propagate(source:Asset,target:Asset):Boolean={
    val operationListBuilder = mutable.ListBuffer[Operation]()

    val latestTrace = AssetCloneHelpers.getLatestClone(source,target)
    latestTrace match{
      case None =>
        operationListBuilder += CloneAsset(source,target.parent.get);
      case Some(trace) => {
          if(Utilities.getLastModifiedTime(source) > trace.versionAt) {
            target.name = source.name
            target.content = source.content
            updateVersion(target)

            val sourcefeatures = source.mappedToFeatures
            val closestAncestor = getImmediateAncestorFeatureModel( Some(target) )
            closestAncestor match {
              case Some( fm ) =>
                sourcefeatures.foreach(
                  m => {
                    if (!AssetCloneHelpers.alreadyCloned(m, closestAncestor.get)) {
                      val fclone = FeatureCloneHelpers.deepCloneFeature(m)
                      FeatureCloneHelpers.addUnassignedFeature(fclone, closestAncestor.get)
                      operationListBuilder += MapAssetToFeature(target, fclone)
                    }
                    else {
                      // MapAssetToFeature doesnt do anything, when the mapping already exists
                      operationListBuilder += MapAssetToFeature(target,AssetCloneHelpers.getFeatureClone(m,closestAncestor.get))
                    }
                  })
              case _ => ;
            }
            AssetCloneHelpers.addTrace(source,target,source.versionNumber)

            for(sourcesubasset <- source.children){
              val subassetclonetrace = findMostRecentCloneTrace(sourcesubasset,target)
              subassetclonetrace match {
                case Some(trace) => propagate(sourcesubasset, trace.target)
                case None => operationListBuilder += CloneAsset(sourcesubasset,target, None)
              }
            }
          }
        }
      }
    subOperations = Some(operationListBuilder.toList)
    storeSubOpMetadata
    true
  }
  override def perform(): Boolean = {
    val clone = propagate(source,target)
    true
  }

  override def createMetadata(): Unit = {
    true
  }
}
object PropagateToAsset{
  def apply(source: Asset, target: Asset, runImmediately: Boolean = true, createMetadata: Boolean = true): PropagateToAsset = {
    if (runImmediately && createMetadata) {
      new PropagateToAsset(source, target) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new PropagateToAsset(source, target) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new PropagateToAsset(source, target) with CreateMetadata
    } else {
      new PropagateToAsset(source, target)
    }
  }
}