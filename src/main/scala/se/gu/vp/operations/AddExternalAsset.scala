package vp.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model.{Asset, FileType, FolderType}
import se.gu.vp.operations.JavaParser.JavaParserMain.parseJavaFile
import se.gu.vp.operations.Utilities.{getImmediateAncestorFeatureModel, getassetbyname}
import se.gu.vp.operations.metadata.{AddExternalAssetMetadata, MetadataConverter}
import se.gu.vp.operations.{AddAsset, CreateMetadata, Operation, RunImmediately}

import java.io.File
import java.nio.file.Path
import scala.collection.mutable
import scala.io.Source

/**
Copyright [2020] [Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

// At the moment Java-specific -> due to it calling Java-parsing functionality explicitly
// assetName: Can be set extra to rename the created asset. This is used, e.g. for renaming the root project folder beneath a repository
class AddExternalAsset(val pathToAsset: Path, val parentAsset: Option[Asset] = None, val assetName: Option[String] = None, val rootAsset: Option[Asset] = None, val parentName: Option[String] = None) extends Operation with Logging {

  var newAsset : Option[Asset] = None

  override def perform(): Boolean = {
    // Dont read in everything -> just what is required -> only the concrete elements with which this is called
    //// bwast writes to CommitLog.txt -> sense to it? How to handle here?
    val operationListBuilder = mutable.ListBuffer[Operation]()

    // TODO: VP strategy for handling errors in parametrization?
    val parent = if (parentAsset.isDefined) {
      parentAsset.get
    } else {
      getassetbyname(parentName.get, rootAsset.get).get
    }

    // TODO: Check for feature models, that are conceptually part of the to-be-imported asset (and switch parametrization to it in case)
    val createdAsset = if (pathToAsset.toFile.isFile) {
      if(pathToAsset.getFileName.toString.endsWith(".java")) {
        parseJavaFile(pathToAsset.toFile, getImmediateAncestorFeatureModel(Some(parent)).get)
      } else {
        Asset(pathToAsset.toString, FileType, content = Some(Source.fromFile(new File(pathToAsset.toString), "ISO-8859-1").getLines.toList))
      }
    } else {
      Asset(pathToAsset.toString, FolderType)
    }

    val fileName = if(assetName.isDefined) {
      assetName.get
    } else {
      createdAsset.name.split('\\').last
    }
    createdAsset.name = parent.name + "\\" + fileName

    // CreateMetadata is set to false as the new asset is basically already specified by the path in this case
    operationListBuilder += AddAsset(createdAsset, parent, createMetadata = false)
    newAsset = Some(createdAsset)

    subOperations = Some(operationListBuilder.toList)
    storeSubOpMetadata
    true
  }

  override def createMetadata() : Unit = {
    // Should not work in case of lazy evaluations, that depend on each other, as the parent asset will not be able to be found using getassetbyname, before previous operation is executed
    // But lazy evaluations are not used currently, when introducing them -> take care of metadata
    try {
      val parent = if (parentAsset.isDefined) {
        parentAsset.get
      } else {
        getassetbyname(parentName.get, rootAsset.get).get
      }

      val createdMetadata = AddExternalAssetMetadata(pathToAsset = pathToAsset.toString, parent = MetadataConverter.convertAssetToMetadata(parent), assetName = assetName)
      metadata = Some(createdMetadata)
    } catch {
      case e : Exception =>
        println("Something went wrong during metadata creation.")
    }
  }
}

object AddExternalAsset {
  def apply(pathToAsset: Path, parentAsset: Asset, assetName: Option[String] = None, runImmediately: Boolean = true, createMetadata: Boolean = true): AddExternalAsset = {
    if (runImmediately && createMetadata) {
      new AddExternalAsset(pathToAsset = pathToAsset, parentAsset = Some(parentAsset), assetName = assetName) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new AddExternalAsset(pathToAsset = pathToAsset, parentAsset = Some(parentAsset), assetName = assetName) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new AddExternalAsset(pathToAsset = pathToAsset, parentAsset = Some(parentAsset), assetName = assetName) with CreateMetadata
    } else {
      new AddExternalAsset(pathToAsset = pathToAsset, parentAsset = Some(parentAsset), assetName = assetName)
    }
  }
  def apply(pathToAsset: Path, rootAsset: Asset, parentAsset: String, assetName: Option[String], runImmediately: Boolean, createMetadata: Boolean): AddExternalAsset = {
    if (runImmediately && createMetadata) {
      new AddExternalAsset(pathToAsset = pathToAsset, assetName = assetName, rootAsset = Some(rootAsset), parentName = Some(parentAsset)) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new AddExternalAsset(pathToAsset = pathToAsset, assetName = assetName, rootAsset = Some(rootAsset), parentName = Some(parentAsset)) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new AddExternalAsset(pathToAsset = pathToAsset, assetName = assetName, rootAsset = Some(rootAsset), parentName = Some(parentAsset)) with CreateMetadata
    } else {
      new AddExternalAsset(pathToAsset = pathToAsset, assetName = assetName, rootAsset = Some(rootAsset), parentName = Some(parentAsset))
    }
  }
}