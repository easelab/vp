package se.gu.vp.operations.cli

import java.io.File

import se.gu.vp.model._
import se.gu.vp.operations.JavaParser.JavaParserMain.parseJavaFile
import se.gu.vp.operations.Replay.AssetHelpers
import se.gu.vp.operations.Utilities.{findAssetByName, findFeatureByName, getImmediateAncestorFeatureModel, getassetbyname}
import se.gu.vp.operations._

import scala.io.Source

/**
Copyright [2021] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class LoadAT() {

}
object LoadAT{

  def getFilesAndFolders(f: File): Array[File] = {
    val these = f.listFiles
    these ++ these.filter(_.isDirectory).flatMap(getFilesAndFolders)
  }

  def getShortName(asset:Asset):String =
    asset.name.split('\\').last.filter(!_.isDigit).replace("-","")

  def isIgnorableFile(file:File):Boolean = {

    //java, json, javascript, text
    //Distinguish file type
    val filesToIgnore:List[String] = List(".trace-db", "out" ,"LICENSE",".gitattributes","README.md",".feature-model",".classpath",".project",".git",".idea",".gitignore",".feature-to-file",".feature-to-folder")
    if(filesToIgnore.contains(file.getName) || file.getAbsolutePath.endsWith(".iml")) return true
    else return false
  }

  //Case 1: VP is being integrated for the first time.
  //We create the root and repositories and so on, and load the feature models
  // and all assets from scratch.

  def loadingFirstTime(rootfilepath:String):Asset = {

    println("\n *** Initializing loading asset tree...\n")
    val rootfile = new File(rootfilepath)
    //Step 1: Load root
    val astroot = AssetHelpers.createRoot(rootfile)

    println("Root is " + rootfilepath)
    //step 2: Load repositories and folders
    getFilesAndFolders(rootfile).foreach(f = c => {

      if(!isIgnorableFile(c)) {
        if (c.isDirectory) {
          val parentAsset = Utilities.getassetbyname(c.getParentFile.getAbsolutePath, astroot)
          parentAsset match {
            case None => ;
            case _ => {
              parentAsset.get.assetType match {
                case VPRootType => {
                  AssetHelpers.createRepository(c, parentAsset.get)
                  println("Variant loaded ... " + c.getName)
                }
                case _ => AssetHelpers.createFolder(c, parentAsset.get)
              }
            }
          }
        }
      }
    })

    //Step 3: Load feature models
    getFilesAndFolders(rootfile).foreach(f = c => {
      if (c.isFile & (c.getAbsolutePath.endsWith("featuremodel.txt") || c.getAbsolutePath.endsWith(".feature-model"))) {
        val fmcontainer = Utilities.getassetbyname(c.getParentFile.getAbsolutePath, astroot)
        fmcontainer match{
          case None=>
          case _ => {
            val fm = LoadFeatureModel.loadFeatureModel(c)
            AddFeatureModelToAsset(fmcontainer.get,fm)
          }
        }
      }
    })

    //Step 4: Load files
    getFilesAndFolders(rootfile).foreach(f = c => {
      if (!isIgnorableFile(c) && c.isFile) {

        val parentAsset = getassetbyname(c.getParentFile.getAbsolutePath, astroot)

        if (c.getAbsolutePath.endsWith(".java")) {
          parentAsset match {
            case None =>
            case _ => {
              val fm = getImmediateAncestorFeatureModel(parentAsset)
              fm match{
                case None =>{
                  val featureModel = FeatureModel(new RootFeature(parentAsset.get.getRepository().get.name))
                  val repoAsset = parentAsset.get.getRepository()
                  AddFeatureModelToAsset(repoAsset.get,featureModel)
                  val fileAsset = parseJavaFile(c, featureModel)
                  AddAsset(fileAsset, parentAsset.get)
                }
                case _=>{
                  val fileAsset = parseJavaFile(c, fm.get)
                  AddAsset(fileAsset, parentAsset.get)
                }
              }

            }
          }
        }
        else {
          parentAsset match {
            case None =>
            case _ => {
              val fileAsset = Asset(c.getAbsolutePath, FileType, content = Some(Source.fromFile(c, "ISO-8859-1").getLines.toList))
              AddAsset(fileAsset, parentAsset.get)
            }
          }
        }
      }
    })

    //println("Loading feature mappings ...  " )


    //Step 5: Load feature to file and feature to folder mappings
    getFilesAndFolders(rootfile).foreach(f = c => {
      if (c.isFile & c.getAbsolutePath.endsWith(".feature-to-file") && !c.getAbsolutePath.contains("\\out\\")) {

        val parentFolderPath = c.getParentFile.getAbsolutePath

        val filesource = io.Source.fromFile(c.getAbsolutePath, "ISO-8859-1")
        val filelines = filesource.getLines.toList
        filesource.close

        for (line <- filelines) {

          if (filelines.indexOf(line) % 2 == 0 && filelines.indexOf(line) < filelines.length) {
            var assetList = List[Asset]()
            var featureList = List[Feature]()
            line.trim.split(",").foreach(c => {
              val asset = findAssetByName(parentFolderPath + "\\" + c, astroot)
              if (asset != None) assetList = asset.get :: assetList
            })

            val parentAsset = findAssetByName(parentFolderPath, astroot)
            if (parentAsset != None) {
              val closestFM = getImmediateAncestorFeatureModel(parentAsset)
              if (closestFM != None) {
                val features = filelines(filelines.indexOf(line) + 1).split(",")
                features.foreach(c => {
                  val feature = findFeatureByName(c.trim, closestFM.get)
                  if (feature != None) featureList = feature.get :: featureList
                  else {
                    val newFeature = Feature(c.trim)
                    AddFeatureToFeatureModel(newFeature, closestFM.get.rootfeature.UNASSIGNED)
                    featureList = newFeature :: featureList
                    ///current time stamp
                  }

                })
              }
                //No feature model in the repository
                //create an empty feature model and assign it to the repository asset
              else {
                val featureModel = FeatureModel(new RootFeature(parentAsset.get.getRepository().get.name))
                val repoAsset = parentAsset.get.getRepository()
                AddFeatureModelToAsset(repoAsset.get, featureModel)

                val features = filelines(filelines.indexOf(line) + 1).split(",")
                features.foreach(c => {
                  var feature = findFeatureByName(c.trim, closestFM.get)
                  if (feature != None) featureList = feature.get :: featureList
                  else {
                    val newFeature = Feature(c.trim)
                    AddFeatureToFeatureModel(newFeature, closestFM.get.rootfeature.UNASSIGNED)
                    featureList = newFeature :: featureList
                    ///current time stamp
                  }
                })
              }
              assetList.foreach(asset => {
                featureList.foreach(feature => MapAssetToFeature(asset, feature))
              })
            }
          }
        }
      }
      else if (c.isFile & c.getAbsolutePath.endsWith(".feature-to-folder") && !c.getAbsolutePath.contains("\\out\\")) {


        val assetToMap = findAssetByName(c.getParentFile.getAbsolutePath, astroot)
          if (assetToMap != None) {
            val filesource = io.Source.fromFile(c.getAbsolutePath, "ISO-8859-1")
            val filelines = filesource.getLines.toList
            filesource.close

            for (line <- filelines) {
              val closestFM = getImmediateAncestorFeatureModel(assetToMap)
              if (closestFM != None) {
                val feature = findFeatureByName(line.trim, closestFM.get)
                if (feature != None) MapAssetToFeature(assetToMap.get, feature.get)
                else{
                  val newFeature = Feature(line.trim)
                  AddFeatureToFeatureModel(newFeature,closestFM.get.rootfeature.UNASSIGNED)
                  MapAssetToFeature(assetToMap.get, newFeature)
                }
              }
              else{
                val featureModel = FeatureModel(new RootFeature(assetToMap.get.getRepository().get.name))
                val repoAsset = assetToMap.get.getRepository()
                AddFeatureModelToAsset(repoAsset.get, featureModel)
                val newfeature = Feature(line.trim)
                AddFeatureToFeatureModel(newfeature,featureModel.rootfeature.UNASSIGNED)
                MapAssetToFeature(assetToMap.get, newfeature)
              }
            }
          }
        }
    })

    TraceLoader.loadTraces(astroot)
    println("\n *** Asset tree loading successful!")
    astroot
  }
}