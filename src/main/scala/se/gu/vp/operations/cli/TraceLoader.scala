package se.gu.vp.operations.cli

import java.io.{File, FileWriter}

import se.gu.vp.model._
import se.gu.vp.operations.Utilities

/**
Copyright [2022] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class TraceLoader {
}
object TraceLoader {

  var lastModifiedTimes = Map[String,Long]()
  var lastAccessedTimes = Map[String,Long]()

  def getTimeStamp(value: String): String = {
    var timeStamp = value.replace("T", " ").split("\\.").reverse.last
    timeStamp = timeStamp.replace("-", "").replace(":", "").replace(" ", "")
    timeStamp = timeStamp.split("\\+").reverse.last
    timeStamp
  }

  def getFilesAndFolders(f: File): Array[File] = {
    val these = f.listFiles
    these ++ these.filter(_.isDirectory).flatMap(getFilesAndFolders)
  }

  def addTrace(source:Asset,target:Asset,version:Int) : Unit = {
    TraceDatabase.traces = Trace(source,target,CloneOf,version) :: TraceDatabase.traces
  }

  def getSmallName(asset:Asset, astRootPath:String):String = {
    var shortName =  asset.name.replace(astRootPath + "\\","")
    shortName
  }

  def writeTracesToFile(rootPath:String):Unit ={

    var atb = TraceDatabase.traces
    var ftb  = FeatureTraceDatabase.traces

    val fileWriter = new FileWriter(new File(rootPath+"\\.trace-db"), false)
    TraceDatabase.traces.foreach(C => {
      fileWriter.write(getSmallName(C.source,rootPath) + ";" + getSmallName(C.target, rootPath)  + ";" + C.source.versionNumber +"\n")
    })

    FeatureTraceDatabase.traces.foreach(C => {

      var sourceRoot = C.source.getFeatureModelRoot()
      if(sourceRoot!=None){
        val sourceFM = sourceRoot.get.featureModel
        val sourceAsset = sourceFM.get.containingAsset

        val targetRoot = C.target.getFeatureModelRoot()
        if(targetRoot!=None){
          val targetFM = targetRoot.get.featureModel
          val targetAsset = targetFM.get.containingAsset

          fileWriter.write(getSmallName(sourceAsset.get, rootPath) + "::" + C.source.name + ";" +
            getSmallName(targetAsset.get, rootPath) + "::" + C.target.name + ";" +
                            C.source.versionNumber +"\n")
        }
      }
    })
    fileWriter.close()
  }

  def loadTracesFromFile(astRoot:Asset):Unit = {

    val traceFile:File = new File(astRoot.name + "\\.trace-db")

    if(traceFile.exists()){

      println("Trace Database found at " + Utilities.getShortName(astRoot) + ". Loading ...")
      val filesource = io.Source.fromFile(traceFile.getAbsolutePath, "ISO-8859-1")
      val filelines = filesource.getLines.toList
      filesource.close

      for(line <- filelines if(!line.trim.equals(""))) {
        val source = line.trim.split(";").reverse.last
        val target = line.trim.split(";")(1)
        val version = line.trim.split(";").last.toLong


        if (!source.contains("::")) {
        val sourceAsset = Utilities.findAssetByName(astRoot.name + "\\" + source, astRoot)
        val targetAsset = Utilities.findAssetByName(astRoot.name + "\\" + target, astRoot)

        if (sourceAsset != None && targetAsset != None)
          TraceDatabase.traces = Trace(sourceAsset.get, targetAsset.get, CloneOf, version) :: TraceDatabase.traces
        }
        else {
          val sourceAsset = Utilities.getassetbyname(astRoot.name + "\\" + source.split("::").head, astRoot)
          val targetAsset = Utilities.getassetbyname(astRoot.name + "\\" + target.split("::").head, astRoot)

          if (sourceAsset != None && targetAsset != None) {
            if (sourceAsset.get.featureModel != None && targetAsset.get.featureModel != None) {

              val sourceFeature = Utilities.findFeatureByName(source.split("::").last, sourceAsset.get.featureModel.get)
              val targetFeature = Utilities.findFeatureByName(target.split("::").last, targetAsset.get.featureModel.get)

              if (sourceFeature != None && targetFeature != None) FeatureTraceDatabase.traces = FeatureTrace(sourceFeature.get, targetFeature.get, CloneOf, version) :: FeatureTraceDatabase.traces

            }
          }
        }
      }
    }
  }

  def loadTraces(astRoot:Asset):Unit={
    val traceFile:File = new File(astRoot.name + "\\.trace-db")
    if(traceFile.exists()) loadTracesFromFile(astRoot)
    else {
      println("No trace database file detected. Creating .trace-db at " + astRoot.name + "...")
      writeTracesToFile(astRoot.name)
    }
  }
}
