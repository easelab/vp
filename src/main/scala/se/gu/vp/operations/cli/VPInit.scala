package se.gu.vp.operations.cli

import java.io.File

import se.gu.vp.cli.Utilities.getShortRootPath
//import se.gu.vp.operations.Utilities.{getLastModifiedTime, transformASTToList}
import se.gu.vp.operations.SerialiseAssetTree
import se.gu.vp.operations.cli.LoadAT._

/**
Copyright [2022] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class VPInit {

}
object VPInit {
  def start(source: File): Unit = {
    // from Khaled: the existence is checked in InitCommand
    if (source.exists()) {
      if (new File(source.getAbsolutePath + "\\assetTree.txt").exists()) {
      }
      else {
        val assetTree = loadingFirstTime(source.getAbsolutePath)


        SerialiseAssetTree(assetTree,getShortRootPath())
      }
    }
  }
}