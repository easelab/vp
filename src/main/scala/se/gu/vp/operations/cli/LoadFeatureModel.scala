package se.gu.vp.operations.cli

import java.io.File

import se.gu.vp.model.{Feature, FeatureModel, RootFeature}
import se.gu.vp.operations.AddFeatureToFeatureModel
import se.gu.vp.operations.Replay.FeatureRelatedHelpers.indentation
import se.gu.vp.operations.Utilities.transformFMToList

import scala.collection.mutable.HashMap

/**
Copyright [2021] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

object LoadFeatureModel {

  def isNumeric(str: String): Boolean = str.matches("[-+]?\\d+(\\.\\d+)?")

  def findDuplicateFeatures(featureName: String, featuremodel: FeatureModel): Option[List[Feature]] = {
    var duplicateFeatures: List[Feature] = Nil
    for (feature <- transformFMToList(featuremodel)) {
      if (feature.name.equals(featureName)) {
        duplicateFeatures = feature :: duplicateFeatures
      }
    }
    if (duplicateFeatures.isEmpty) None
    else Some(duplicateFeatures)
  }

  def getFeatureName(line: String): String = {
    var featurename = line.trim
    if (isNumeric(featurename.replace("\\)","").split(" \\(v").last)) {
      if (featurename.startsWith("or")) featurename = featurename.split(" ")(featurename.split(" ").length - 2)
      else featurename = featurename.split(" ").reverse.last
    }
    else featurename = featurename.split(" ").reverse.last
    featurename
  }

  def getFQPFromFile(index: Int, indent: Int, FMLines: List[String]): String = {
    var featurePath = getFeatureName(FMLines(index).trim)
    if(featurePath.contains("::")) featurePath = featurePath.split("::").last
    var currentIndent = indent;
    for (i <- (0 to index - 1).reverse) {
      if (indentation(FMLines(i)) < currentIndent) {
        featurePath = getFeatureName(FMLines(i).trim) + "::" + featurePath
        currentIndent = indentation(FMLines(i))
      }
    }
    featurePath
  }

  def getFQPFromFM(path: String, feature: Feature): String = {
    var featurePath = path
    feature.parent match {
      case None => feature.name.split("::").last + featurePath
      case _ => {
        featurePath = getFQPFromFM(featurePath, (feature.parent.get)) + "::" + feature.name.split("::").last
        featurePath
      }
    }
  }

  //finding the parent feature: the feature that has the same fully qualified
  // path in the file and feature model
  def getParentFeature(index: Int, indent: Int, FMLines: List[String], featureModel: FeatureModel): Option[Feature] = {
    for (feature <- transformFMToList(featureModel)) {
      val pathfromFM = getFQPFromFM("", feature)
      var pathfromFile = getFQPFromFile(index, indent, FMLines)
      pathfromFile = pathfromFile.substring(0, pathfromFile.lastIndexOf("::"))
      if (pathfromFM.equals(pathfromFile)) {
        return Some(feature)
      }
    }
    None
  }

  def hasVersion(nameString: String): Option[Long] = {
    var temp = nameString.trim.replace(")","").split(" \\(v")
    if (isNumeric(temp.last)) {
      println(temp.last)
      return Some(temp.last.toLong)
    }
    else None
  }

  def getLPQ(featureName: String, parentFeature: Option[Feature], featureModel: FeatureModel): String = {
    var lpq = featureName;
    if (!findDuplicateFeatures(lpq, featureModel).isEmpty) {
      parentFeature match {
        case None => lpq
        case _ => {
          lpq = parentFeature.get.name + "::" + lpq
          getLPQ(lpq, parentFeature.get.parent, featureModel)
        }
      }
    }
    else return lpq
  }

  def loadFeatureModel(file: File): FeatureModel = {

    val lines = io.Source.fromFile(file.getAbsolutePath, "ISO-8859-1").getLines.toList
    //creating feature model + adding root feature
    //Also checking if root feature is versioned
    val featuremodel = new FeatureModel(new RootFeature(getFeatureName(lines(0).trim)))
    hasVersion(lines(0)) match {
      case Some(version) => featuremodel.rootfeature.versionNumber = version
      case None => featuremodel.rootfeature.versionNumber = 1;
    }
    for (line <- lines) {
      val indent = indentation(line.replaceAll("\\s+$", ""))
      val featurename = getFeatureName(line.trim)

      indent match {
        case 0 => ;
        case _ => {
          if(!featurename.endsWith("UnAssigned")){
            val feature = Feature(featurename)
            val parentFeature = getParentFeature(lines.indexOf(line), indentation(line), lines, featuremodel)
            parentFeature match {
              case None => ;
              case _ => {
                hasVersion(line) match {
                  case None => {
                    AddFeatureToFeatureModel(feature, parentFeature.get)
                  }
                  case Some(version) => {
                    feature.versionNumber = version
                    AddFeatureToFeatureModel(feature, parentFeature.get, false)

                  }
                }
              }
            }
          }
        }
        //Problem: The last feature's lqp is not calculated
      }
    }
    //Computing lpqs for duplicate features

    transformFMToList(featuremodel).foreach(f => {

      val duplicateFeatures = findDuplicateFeatures(f.name, featuremodel)
      if(!f.name.endsWith("UnAssigned")){
        duplicateFeatures match {
          case None => ;
          case _ => {
            if (duplicateFeatures.get.size > 1) {
              var hash: HashMap[String, String] = HashMap.empty
              duplicateFeatures.get.foreach(f => {
                hash += (getFQPFromFM("", f) -> getLPQ(f.name, f.parent, featuremodel))
              })
              for ((key, value) <- hash) {
                transformFMToList(featuremodel).foreach(f => {
                  if (getFQPFromFM("", f).equals(key)) f.name = value
                })
              }
            }
          }
        }
      }

    }
    )
    featuremodel
  }
}
