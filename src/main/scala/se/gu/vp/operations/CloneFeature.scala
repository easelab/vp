package se.gu.vp.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model._
import se.gu.vp.operations.ASTSliceHelpers.{cloneASTSlice, tryGetAssetClone}

import scala.collection.immutable.Stream.Empty
import scala.collection.mutable

/**
Copyright [2019] [Wardah Mahmood, Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class CloneFeature(val sourceFeature:Path, val targetParentFeature:Path) extends Operation with Logging {
  def getFeatureClone(feature:Feature,featuremodel:FeatureModel): Option[Feature] ={
    for(trace <- FeatureTraceDatabase.traces) {
        if(trace.source == feature && Utilities.featureModelContainsFeature(trace.target,featuremodel)){
          return Some(trace.target)
        }
      }
    None
  }
  import Utilities._

  def cloneFeature(sourceFeature:Path, targetFeature:Path):Boolean = {

    val operationListBuilder = mutable.ListBuffer[Operation]()

    val s_assetpath = sourceFeature.path.collect { case a: AssetPathElement => a }
    val sourceassetpath = resolveAssetPath(sourceFeature.root,s_assetpath)
    val featuremodel = sourceassetpath.last.featureModel.get
    val s_featurepath = sourceFeature.path.collect { case a: FeaturePathElement => a }
    val sourcefeature = resolveFeaturePath(featuremodel,s_featurepath)

    val t_assetpath = targetFeature.path.collect { case a: AssetPathElement => a }
    val targetassetpath = resolveAssetPath(targetFeature.root,t_assetpath)
    val t_featurepath = targetFeature.path.collect { case a: FeaturePathElement => a }
    val targetfeaturemodel = targetassetpath.last.featureModel.get

    if( sourcefeature != None && !featureClonedInFM( sourcefeature.get , targetfeaturemodel )){
      val featureclone = sourcefeature.get.doCloneFeature()
      featureclone.subfeatures = Nil

      t_featurepath match {
        case Empty => {
          operationListBuilder += AddFeatureToFeatureModel(featureclone, targetfeaturemodel, true, true, true)
        }
        case _ => {
          val targetfeature = resolveFeaturePath (targetfeaturemodel, t_featurepath)
          operationListBuilder += AddFeatureToFeatureModel (featureclone, targetfeature.get)
        }
      }
      println("Found feature " + sourcefeature.get.name)
      println("Cloning feature ...")
      println("Finding and cloning mapped assets to " + sourcefeature.get.name)

      FeatureTraceDatabase.traces = FeatureTrace(sourcefeature.get, featureclone, CloneOf, sourcefeature.get.versionNumber) :: FeatureTraceDatabase.traces

      val mappedsourceassets = sourcefeature.get.mappedAssets(sourceassetpath.last)

      /** Cloning of mapped assets should start with high-level assets
        * E.g., If a method is cloned, and so is the containing block,
        * if we start with cloning the block first, the block will be
        * cloned twice, once as a mapped asset itself, once as a child
        * of a mapped asset (i.e., the method).
         */

      for(mappedasset <- mappedsourceassets){
        if(!isCodeAsset(mappedasset)) cloneASTSlice(mappedasset,targetassetpath.last,featureclone).foreach(op => operationListBuilder += op)
      }

      for(mappedAsset <- mappedsourceassets) {
        if (mappedAsset.assetType == MethodType)
          cloneASTSlice(mappedAsset, targetassetpath.last, featureclone).foreach(op => operationListBuilder += op)
      }

      for(mappedAsset <- mappedsourceassets){
         if(mappedAsset.assetType == BlockType && !mappedAsset.parent.get.mappedToFeatures.contains(sourcefeature.get)){
          /** asset's parent is not mapped to the same feature. Otherwise the asset would be cloned as a part of deep clone of the parent asset */
          mappedAsset.parent.get.assetType match{
            case FileType => cloneASTSlice(mappedAsset,targetassetpath.last,featureclone).foreach(op => operationListBuilder += op)
            case ClassType => cloneASTSlice(mappedAsset,targetassetpath.last,featureclone).foreach(op => operationListBuilder += op)
            case MethodType => {
              val ancestorMethod = tryGetAssetClone(mappedAsset.parent.get,mappedAsset.getRoot().get)
              if(ancestorMethod.isDefined){
                ancestorMethod.get.children.foreach(child => RemoveAsset(ancestorMethod.get,child))
                mappedAsset.parent.get.children.reverse.foreach(child => CloneAsset(child,ancestorMethod.get))
              }
            }
          }
        }
      }

      sourcefeature.get.subfeatures.foreach (
        s => {
          val spath = Path(sourceFeature.root, s_assetpath ++ computeFeaturePath(s))
          val tpath = Path(targetFeature.root, t_assetpath ++ computeFeaturePath(featureclone))
          cloneFeature(spath,tpath)
        }
      )
      subOperations = Some(operationListBuilder.toList)
      true
    }

    else if (featureClonedInFM( sourcefeature.get , targetfeaturemodel ))
      info("Feature " + sourcefeature.get.name + "already cloned in the target feature model")

    true
  }


  override def perform(): Boolean = {
      cloneFeature(sourceFeature,targetParentFeature)
      storeSubOpMetadata
      true
    }

  override def createMetadata(): Unit = {
    true
  }
}
object CloneFeature{
  def apply(sourceFeature: Path, targetFeature: Path, runImmediately: Boolean = true, createMetadata: Boolean = true): CloneFeature = {
    if (runImmediately && createMetadata) {
      new CloneFeature(sourceFeature, targetFeature) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new CloneFeature(sourceFeature, targetFeature) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new CloneFeature(sourceFeature, targetFeature) with CreateMetadata
    } else {
      new CloneFeature(sourceFeature, targetFeature)
    }
  }
}