package se.gu.vp.operations.CalculatorSimulation

import java.io.File

import se.gu.vp.model._
import se.gu.vp.operations.Replay.ASTModificationHelpers.{getRepository, operationcount}
import se.gu.vp.operations.Replay.FeatureRelatedHelpers.{featurepath, findFeatureByName, indentation}
import se.gu.vp.operations.Utilities.{computeAssetPath, computeFeaturePath}
import se.gu.vp.operations._

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class ASTModificationHelpers {

}
object ASTModificationHelpers{
  def handleFeatureModel(file:File,asset:Asset, featureModel: Option[FeatureModel] = None):FeatureModel={

    var allfeatures:List[String] = Nil
    val source = io.Source.fromFile(file.getAbsolutePath, "ISO-8859-1")
    val lines = source.getLines.toList
    source.close

    val rootfeature = new RootFeature(lines(0).trim)
    var featuremodel = FeatureModel(rootfeature)
    allfeatures = lines(0).trim :: allfeatures

    if(featureModel != None) {
      featuremodel = featureModel.get
    }

    for (line <- lines) {
      val indent = indentation(line.replaceAll("\\s+$", ""))
      var featurename = line.trim
      if(featurename.startsWith("or ")){
        featurename = featurename.split(" ").last
      }
      val featurequalifiedpath = featurepath(line, lines).toString.trim

      val featurealreadyexists = Utilities.findFeatureByName(featurequalifiedpath, featuremodel)
      featurealreadyexists match {
        case None => {
          if (findFeatureByName(featurename, featuremodel) != None) {

            val fff= findFeatureByName(featurename,featuremodel)
            val newparentname = featurequalifiedpath.slice(0,featurequalifiedpath.lastIndexOf("-"))
            val newparentfeature = Utilities.findFeatureByName(newparentname, featuremodel)

            if(newparentfeature!=None) {
              val existingfeature = findFeatureByName(featurename, featuremodel).get
              if(existingfeature.parent!=newparentfeature)
              MoveFeature((findFeatureByName(featurename, featuremodel).get), newparentfeature.get)
              findFeatureByName(featurename,featuremodel).get.rename(featurequalifiedpath)
            }
            println(featurename + " already exists as a sub-feature of some other feature. Feature Moving")
            allfeatures = featurequalifiedpath :: allfeatures
            var count = operationcount(getRepository(asset).get+"-"+"MoveFeature")
            count = count + 1
            operationcount += ((getRepository(asset).get+"-"+"MoveFeature") -> count)
          }
          else {
            indent match {
              case 0 => ;
              case _ => {
                if (featurequalifiedpath != "") {
                  val parentname = featurequalifiedpath.slice(0,featurequalifiedpath.lastIndexOf("-"))
                  val parentfeature = Utilities.findFeatureByName(parentname, featuremodel)

                  parentfeature match {
                    case None => ;
                    case _ => {
                      val feature = Feature(featurequalifiedpath)
                      AddFeatureToFeatureModel(feature, parentfeature.get)
                      allfeatures = featurequalifiedpath :: allfeatures
                    }
                  }
                }
              }
            }
          }
        }
        case _ =>{
          allfeatures = featurequalifiedpath :: allfeatures
        }
      }
    }
    for(featuremodelfeature <- Utilities.transformFMToList(featuremodel)) {
      if (!allfeatures.contains(featuremodelfeature.name) & featuremodelfeature != featuremodel.rootfeature.UNASSIGNED) {
        val root = asset.getRoot()
        root match {
          case None => ;
          case _ => {
            val path = computeFeaturePath(featuremodelfeature)
            val featurepath = Path(root.get, computeAssetPath(asset) ++ computeFeaturePath(featuremodelfeature))
            println("Feature "+featuremodelfeature.name+" removed")
            RemoveFeature(featurepath)
            //remove subfeatures
          }
        }
      }
    }
    if(asset.featureModel == None) {
      AddFeatureModelToAsset(asset,featuremodel)
    }
    return featuremodel
  }
}
