package se.gu.vp.operations.CalculatorSimulation

import java.io.{File, FileWriter}

import se.gu.vp.model.Asset
import se.gu.vp.operations.CalculatorSimulation.ASTModificationHelpers.handleFeatureModel
import se.gu.vp.operations.Replay.{AssetHelpers, JavaScriptFileParser}

import scala.util.matching.Regex
//import se.gu.vp.operations.Replay.{AnnotatedFileHelpers, AssetHelpers}
import se.gu.vp.operations.Utilities

/**
Copyright [2020] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class BasicCalculator {
}

object BasicCalculator {

  def dropIndex[T](list: List[T], idx: Int): List[T] = {
    list.zipWithIndex.filter(_._2 != idx).map(_._1)
  }

  def removeAnnotation(block:Asset):Unit={
    block.content match{
      case None=>;
      case _=>{
        for(i<- 0 to block.content.get.length-1){
            if(isAnnotation(block.content.get(i))){
              block.content = Some(dropIndex(block.content.get,i))
            }
        }
      }
    }
  }
  def containsLineAnnotation(line:String):Boolean={
    val lineannotation = new Regex("//\\s*[&$]\\s*[Ll]ine")
    if(lineannotation.findFirstIn(line)!=None)
    true
    else false
  }

  def replaceLineAnnotation(line:String):String={
    var modifiedLine = line.split("//").reverse.last
    modifiedLine
  }

  def isAnnotation(line:String):Boolean={
    val beginannotation = new Regex("//\\s*[&$]\\s*[Bb]egin")
    val endingannotation = new Regex("//\\s*[&$]\\s*[Ee]nd")
    if(beginannotation.findFirstIn(line)!=None || endingannotation.findFirstIn(line)!=None) true
    else false
  }
  def removeAnnotatedBlocks():Unit={

  }
  def getListOfSubDirectories(directoryName: String): Array[String] = {
    (new File(directoryName))
      .listFiles
      .filter(_.isDirectory)
      .map(_.getName)
  }

  def getListOfSubFiles(directoryName: String): Array[String] = {
    new File(directoryName)
      .listFiles
      .filter(_.isFile)
      .map(_.getAbsolutePath)
  }

  // still a bit calculator-example specific
  def readInAssetTree(rootfilepath: String) : Option[Asset] = {
    val commitlog = new File(rootfilepath + File.separator + "CommitLog.txt")
    val astprintfile = new File(rootfilepath + File.separator + "ASTprint.txt")
    val bwast = new FileWriter(commitlog, true)

    val rootfile = new File(rootfilepath)

    if (rootfile.exists()) {
      val astroot = AssetHelpers.createRoot(rootfile)
      getListOfSubDirectories(rootfilepath).foreach(c => {
        val repofile = new File(rootfilepath + "//" + c)
        if (repofile.exists()) {
          AssetHelpers.createRepository(repofile, astroot)
          val fmfile = new File(rootfilepath + "/" + c + "/featuremodel.txt")
          val repositoryasset = Utilities.getassetbyname(repofile.getAbsolutePath, astroot)
          handleFeatureModel(fmfile, repositoryasset.get)
          repositoryasset.get.featureModel.get.prettyprint
          getListOfSubFiles(repofile.getAbsolutePath)
            .filter(filename => !filename.endsWith("featuremodel.txt"))
            .foreach(filename => {
          if (new File(filename).exists()) {
            val file = new File(filename)
            JavaScriptFileParser.addAnnotatedFile(file, astroot, repositoryasset.get.featureModel.get, bwast)
            val fileAsset = Utilities.getassetbyname(file.getAbsolutePath, astroot)

            JavaScriptFileParser.removeAnnotationBlocks(fileAsset)
          }
          }
          )
        }
      })
      return Some(astroot)
    }
    None
  }
  //Test moved to SerializeAssetTreeTest. Name: Test by Christoph
}