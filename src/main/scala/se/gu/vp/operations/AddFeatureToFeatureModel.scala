package se.gu.vp.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model.{Feature, FeatureModel, RootFeature}
import se.gu.vp.operations.metadata.{AddFeatureToFeatureModelMetadata, MetadataConverter}

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class AddFeatureToFeatureModel( feature:Feature , featuremodel:Option[FeatureModel] = None , parent:Option[Feature] = None,  val versioning: Option[Boolean] = None) extends Operation with Logging {
  override def perform(): Boolean = {

    if(featuremodel != None) {
      if (feature.isInstanceOf[RootFeature]) {
        featuremodel.get.rootfeature = feature.asInstanceOf[RootFeature]
        feature.asInstanceOf[RootFeature].featureModel = featuremodel

        feature.parent = None
        if (versioning == None || versioning.get == true) updateFeatureVersion(feature)
        true
      }
      else{
        error("Feature to add is not an instance of RootFeature. Change the type or add to another feature instaed of feature model ")
      }
    }

    else if(parent!=None){
      // TODO:
      parent.get.subfeatures = parent.get.subfeatures ++ List(feature)
      feature.parent = parent
      if(versioning == None || versioning.get == true) {
        updateFeatureVersion(parent.get)
        updateFeatureVersion(feature)
      }
    }
    storeSubOpMetadata
    true
  }

  override def createMetadata(): Unit = {
    try {
      val featMD = MetadataConverter.convertFeatureToMetadata(feature)
      val fmMD = if (featuremodel.isDefined) {
        Some(MetadataConverter.convertFeatureModelToMetadata(featuremodel.get))
      } else { None }
      val parentMD = if (parent.isDefined) {
        Some(MetadataConverter.convertFeatureToMetadata(parent.get))
      } else { None }
      val createdMetadata = AddFeatureToFeatureModelMetadata(featMD, fmMD, parentMD)

      metadata = Some(createdMetadata)
    } catch {
      case e: Exception =>
        println("Something went wrong during metadata creation.")
    }
  }
}
object AddFeatureToFeatureModel {

  def apply(feature: Feature, featuremodel: FeatureModel, versioning:Boolean, runImmediately: Boolean, createMetadata: Boolean): AddFeatureToFeatureModel = {
    if (runImmediately && createMetadata) {
      new AddFeatureToFeatureModel(feature = feature, featuremodel = Some(featuremodel), versioning = Some(versioning)) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new AddFeatureToFeatureModel(feature = feature, featuremodel = Some(featuremodel), versioning = Some(versioning)) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new AddFeatureToFeatureModel(feature = feature, featuremodel = Some(featuremodel), versioning = Some(versioning)) with CreateMetadata
    } else {
      new AddFeatureToFeatureModel(feature = feature, featuremodel = Some(featuremodel), versioning = Some(versioning))
    }
  }

  def apply(feature: Feature, parent: Feature, versioning:Boolean = true, autoAppendName: Boolean = false, runImmediately: Boolean = true, createMetadata: Boolean = true): AddFeatureToFeatureModel = {
    // UnAssigned is not adjusted namewise
    // this probably requires some more thinking and/or comparison with other similar adjustments
    // TODO: this information might need to be saved as well.. -> perhaps pass as another argument to the function
    if (autoAppendName) {
      feature.name = parent.name + "-" + feature.name
    }

    if (runImmediately && createMetadata) {
      new AddFeatureToFeatureModel(feature = feature, parent = Some(parent), versioning = Some(versioning)) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new AddFeatureToFeatureModel(feature = feature, parent = Some(parent), versioning = Some(versioning)) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new AddFeatureToFeatureModel(feature = feature, parent = Some(parent), versioning = Some(versioning)) with CreateMetadata
    } else {
      new AddFeatureToFeatureModel(feature = feature, parent = Some(parent), versioning = Some(versioning))
    }
  }
}