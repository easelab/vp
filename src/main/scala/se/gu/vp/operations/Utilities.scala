package se.gu.vp.operations


import java.io.File
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.{Files, Path, Paths}
import java.time.ZoneId

import org.kiama.rewriting.Rewriter
import se.gu.vp.model._

/**
Copyright [2019] [Wardah Mahmood, Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

object Utilities {

  def fixParentPointers(ast: Asset) {
    ast.children foreach { c =>
      c.parent = Some(ast)
      fixParentPointers(c)
    }
  }

  def getImmediateAncestorFeatureModel(asset: Option[Asset]): Option[FeatureModel] =
    asset match {
      case Some(Asset(_, _, _, _, Some(fm), _)) => Some(fm)
      case None => None
      case _ => getImmediateAncestorFeatureModel(asset.get.parent)

    }

  def featureContainsFeature(feature: Feature, targetfeature: Feature): Boolean = {
    if (targetfeature.subfeatures.contains(feature))
      return true
    else
      for (subfeatures <- targetfeature.subfeatures) {
        featureContainsFeature(feature, subfeatures)
      }
    return false
  }

  def featureModelContainsFeature(feature: Feature, featuremodel: FeatureModel): Boolean = {
    return transformFMToList(featuremodel).contains(feature)
  }

  def getFeatureByNameFromFeature(featureName: String, targetfeature: Feature): Option[Feature] = {
    val list: List[Feature] = targetfeature :: flattenSubFeatures(targetfeature.subfeatures)
    list.find(_.name.equalsIgnoreCase(featureName))
  }

  import scala.util.control.Breaks._

  def findFeatureByName(featureName: String, featuremodel: FeatureModel): Option[Feature] = {
    for (feature <- transformFMToList(featuremodel)) {
      if (feature.name.trim == featureName.trim) {
        return Some(feature)
      }
    }
    None
  }

  def astContainsAsset(asset: Asset, ast: Asset) =
    (ast :: flat(ast.children)) contains asset

  import Rewriter._

  //TODO this method does not work becaue it doesn't match anything...it returns all asets in the ast thus far...created an alternative method...findAsetByName..see below
  def astContainsAssetByName(assetname: String, ast: Asset) =
    collects {
      case a@Asset(assetname, _, _, _, _, _) if (a.name.equalsIgnoreCase(assetname)) => a
    }(ast)

  def findCloneInVariant(asset:Asset, target:Asset):Option[Asset] = {
    val clones = findClones(asset)
    clones.foreach(c=>if(astcontainsassetbyname(c.name,target)) return Some(c))
    None
  }

  def assetMappedToFeatureClone(feature:Feature, asset:Asset):Boolean = {
    asset.mappedToFeatures.foreach(c => {
      if(aIsCloneOfb(feature,c))
        return true
    })
    return false
  }

  def findAssetByName(assetName: String, root: Asset): Option[Asset] = {
    val list: List[Asset] = root :: flat(root.children)
    list.find(_.name.equalsIgnoreCase(assetName))

  }

  // Function is also called in Replay.ASTModificationHelpers. Does this change mess something up there?
  def findFeatureByLastName(featurename: String, featureModel: FeatureModel): Option[Feature] = {
    transformFMToList(featureModel).find(elem => elem.name.endsWith("-" + featurename) || elem.name.endsWith(featurename))
  }

  def astcontainsassetbyname(assetname: String, ast: Asset): Boolean = {
    if (transformASTToList(ast).find(_.name.equalsIgnoreCase(assetname)).isEmpty)
      false
    else true
  }


  def getassetbyname(assetname: String, ast: Asset): Option[Asset] = {
    val list: List[Asset] = ast :: flat(ast.children)
    list.find(_.name.equalsIgnoreCase(assetname))
  }

  /**
    * TODO, implement
    * @param path
    * @param ast
    * @return
    */
  def astContainsAssetByLeastPartiallyQualifiedPath(path: List[String], ast: Asset) =
    throw new RuntimeException("TBD")

  /**
    * TODO: not clear what this method does, can you explain or give it a more intuitive name?
    * I rewrote the implementation to make it more robust, inferring the meaning
    *
    * @param asset
    * @return
    */
  def findMissingFeatures(asset: Asset): Set[Feature] =
    getImmediateAncestorFeatureModel(Some(asset)) match {
      case Some(fm) => asset.mappedToFeatures -- fm.allFeatures
      case None => Set empty
    }

  def flat(ls: List[Asset]): List[Asset] = ls flatten {
    case t: Asset => t :: flat(t.children)
    case c => List(c)
  }

  def flattenSubFeatures(ls: List[Feature]): List[Feature] = ls flatten {
    case t: Feature => t :: flattenSubFeatures(t.subfeatures)
    case c => List(c)
  }

  def transformToList(ast: Asset): List[Asset] = {
    val flatten: List[Asset] = flat(ast.children)
    return flatten
  }

  def transformASTToList(ast: Asset): List[Asset] = ast :: flat(ast.children)

  def transformFMToList(fm: FeatureModel): List[Feature] = transformFMToList(fm.rootfeature)

  def transformFMToList(rf: RootFeature): List[Feature] = rf :: flattenSubFeatures(rf.subfeatures)

  def transformFeatToList(f: Feature): List[Feature] = f :: flattenSubFeatures(f.subfeatures)

  def cloneExists(source: Asset, target: Asset): Boolean = {
    TraceDatabase.traces.foreach(c => if (c.source == source && c.target == target) return true)
    false
  }

  def isAncestor(child: Asset, parent: Asset): Boolean = {
    child.parent match {
      case None => false
      case Some(elem)
        if elem == parent => true
      case _ => isAncestor(child.parent.get, parent)
    }
  }

  def findClones(asset: Asset): List[Asset] = {
    var clones: List[Asset] = Nil
    val sourceclones = TraceDatabase.traces.filter( p => p.source == asset || p.target == asset )
    for (trace <- sourceclones) {
      val source = trace.source
      val target = trace.target
      if(source == asset && !clones.contains(target)) clones = target :: clones
      else if(target == asset && !clones.contains(source)) clones = source :: clones
    }
    return clones
  }

  def findMostRecentCloneTrace(source: Asset, targetContainer: Asset): Option[Trace] = {
    for (trace <- TraceDatabase.traces) {
      if (trace.source == source && trace.target.parent.get == targetContainer)
        return Some(trace)
    }
    None
  }


  def findMostRecentFeatureCloneTrace(source: Feature, target: Feature): Option[FeatureTrace] = {
    for (trace <- FeatureTraceDatabase.traces) {
      if (trace.source == source && trace.target == target)
        return Some(trace)
    }
    None
  }

  def findFeatureClones(feature: Feature): List[Feature] = {
    var clones: List[Feature] = Nil
    val featureclones = FeatureTraceDatabase.traces.filter(_.source == feature)
    for (traces <- featureclones)
      clones = traces.target :: clones
    return clones
  }

  def assetClonedInAST(asset: Asset, target: Asset): Boolean = {
    val clones = findClones(asset)
    clones.foreach(c => {
      if (astContainsAsset(c, target))
        return true
    })
    false
  }

  def getHashCode(asset:Asset): String = {
    asset.content match {
      case None => return ""
      case _ => return asset.content.get.hashCode().toString;
    }
  }

  def assetPropagatedInAST(asset: Asset, target: Asset): Boolean = {
    for (trace <- TraceDatabase.traces) {
      if (trace.source == asset & trace.target == target & trace.versionAt == asset.versionNumber)
        return true
    }
    return false
  }

  def getDateFromVersion(versionNumber:Long):String = {
    return versionNumber.toString.slice(0,4) + "/" +
              versionNumber.toString.slice(4,6) + "/" +
      versionNumber.toString.slice(6,8)
  }

  def getTimeFromVersion(versionNumber:Long):String = {
    return versionNumber.toString.slice(8,10) + ":" +
      versionNumber.toString.slice(10,12) + "::" +
      versionNumber.toString.slice(12,14)
  }


  def featureClonedInFM(feature: Feature, FM: FeatureModel): Boolean = {
    var found = false
    val clones = findFeatureClones(feature)
    for (clone <- clones) {
      if (featureModelContainsFeature(clone, FM)) {
        found = true
        return true
      }
    }
    /** To discuss with Christoph -> Earlier, it only checked if feature A was "cloned"
      * in feature model, but not if it in itself is a "clone" of a feature in the
      * feature model, which could very well be the case.*/
    if(found == false){
      transformFMToList(FM).foreach(feat => {
        if(aIsCloneOfb(feat,feature)) found = true
      })
    }
    return found
  }

  def getClosestCommonAncestor(ast: Asset, targetasset: Asset): Any = {
    ast.parent match {
      case Some(parent) => {
        for (assetclone <- findClones(parent))
          if (isAncestor(assetclone, targetasset))
            return assetclone
      }
      case None => None
      case _ => getClosestCommonAncestor(ast.parent.get, targetasset)
    }
  }

  //Descending order - root,repo,folder...
  def getAncestors(asset: Asset): List[Asset] = {
    var ancestors: List[Asset] = Nil
    var ast = asset.parent
    while (ast != None) {
      ancestors = ast.get :: ancestors
      ast = ast.get.parent
    }
    //Ancestors in top down order (repository, folder, file,..)
    ancestors
  }
  def getShortName(asset:Asset):String =
    asset.name.split('\\').last.filter(!_.isDigit).replace("-","")

  def addclones(asset1: Asset, asset2: Asset): Unit = {
    asset1.parent = Some(asset2)
    asset2.children = asset1 :: asset2.children
  }

  def cloneAssetStructure(asset: Asset, container: Asset, assettoadd: Asset): Asset = {
    var list: List[Asset] = Nil
    val assetlist: List[Asset] = transformASTToList(container)
    for (c <- 1 to assetlist.length - 1) {
      if (astContainsAsset(asset, assetlist(c)) && !list.contains(assetlist(c)) && assetlist(c) != asset) {
        val clone = assetlist(c).doClone()
        clone.parent = None
        clone.children = Nil
        clone.presenceCondition = True()
        TraceDatabase.traces = Trace(assetlist(c), clone, CloneOf, assetlist(c).versionNumber) :: TraceDatabase.traces
        list = list ++ List(clone)
      }
    }
    if (list.isEmpty) return assettoadd
    else if (list.length == 1) {
      addclones(assettoadd, list(0))
      return list(0)
    }
    else {
      for (i <- 0 to list.length - 2) {
        addclones(list(i), list(i + 1))
      }
      addclones(assettoadd, list.last)
      return list.last
    }
  }

  def aIsCloneOfb(asset1: Asset, clone: Asset): Boolean = {
    var found = false
    for (trace <- TraceDatabase.traces) {
      if (trace.source == asset1 && trace.target == clone)
        found = true
      else if (trace.target == asset1 && trace.source == clone)
        found = true
    }
    return found
  }

  def aIsCloneOfb(feature1: Feature, feature2: Feature): Boolean = {
    for (trace <- FeatureTraceDatabase.traces) {
      if (trace.source == feature1 && trace.target == feature2)
        return true
      else if (trace.source == feature2 && trace.target == feature1)
        return true
    }
    false
  }

  def resolveAssetPath(astroot: Asset, path: Seq[AssetPathElement]): Seq[Asset] = {
    val ast = transformASTToList(astroot)
    var assetsequence: Seq[Asset] = Nil

    var firstindex = 0
    var secondindex = 0

    for (j <- firstindex to path.length - 1) {
      for (index <- secondindex to ast.length - 1) {
        if (path(j).assetname == ast(index).name && path(j).assettype == ast(index).assetType) {
          assetsequence = assetsequence :+ ast(index)
          firstindex = j
          secondindex = index
        }
      }
    }
    return assetsequence
  }

  def resolveFeaturePath(fm: FeatureModel, path: Seq[FeaturePathElement]): Option[Feature] = {

    var temp = fm.rootfeature.asInstanceOf[Feature]
    breakable {
      for (c <- 1 to path.length - 1) {
        val element = temp.subfeatures.filter(_.name == path(c).featurename)
        if (!element.isEmpty) {
          temp = element(0)
        }
        else {
          println("Error - Feature Not Found")
          break()
          None
        }
      }
    }
    return Some(temp)
  }

  //Checking if the target already has a clone of the mapped asset
  //Relevant for feature cloning when assets mapped to the feature
  //are added in the target AST
  def targetContainsClone(asset: Asset, targetAST: Asset): Boolean = {
    for (clone <- findClones(asset)) {
      if (astContainsAsset(clone, targetAST)) {
        return true
      }
    }
    false
  }

  def deepclone(asset: Asset): Asset = {
    val clone = asset.doClone()
    clone.children = Nil
    clone.presenceCondition = True()
    clone.versionNumber = asset.versionNumber
    TraceDatabase.traces = Trace(asset, clone, CloneOf, asset.versionNumber) :: TraceDatabase.traces
    asset.children.foreach(c => {
      clone.children = clone.children ++ List(deepclone(c))
    })
    clone.children.foreach(asset => asset.parent = Some(clone))
    return clone
  }

  def deepCloneFeature(feature: Feature): Feature = {
    val clone = feature.doCloneFeature()
    clone.name = feature.name
    clone.subfeatures = Nil
    FeatureTraceDatabase.traces = FeatureTrace(feature, clone, CloneOf, feature.versionNumber) :: FeatureTraceDatabase.traces
    feature.subfeatures.foreach(s => {
      clone.subfeatures = clone.subfeatures ++ List(deepCloneFeature(s))
    }
    )
    clone.subfeatures.foreach(feat => feat.parent = Some(clone))
    return clone
  }

  // not used anywhere, just called itself
  def removeAssetMappingRecursively(asset: Asset, feature: Feature): Boolean = {
    if (asset.isMappedToFeature(feature)) {
      val mappedfeatures = asset.mappedToFeatures
      println("Mapped features in the beginning are " + mappedfeatures)
      asset.presenceCondition = True()
      for (f <- mappedfeatures)
        if (f != feature) MapAssetToFeature(asset, f)
      println("Mapped features after deletion are " + mappedfeatures)
    }
    asset.children.foreach(c => removeAssetMappingRecursively(c, feature))
    true
  }

  def unmapFeature(asset: Asset, feature: Feature): Boolean = {
    if (asset.isMappedToFeature(feature)) {
      val mappedfeatures = asset.mappedToFeatures
      asset.presenceCondition = True()
      for (f <- mappedfeatures)
        if (f != feature) asset.presenceCondition = asset.presenceCondition | f
    }
    true
  }

  def computeAssetPath(asset: Asset): Seq[AssetPathElement] = {
    val ancestors = getAncestors(asset)
    var assetpath: Seq[AssetPathElement] = Nil
    for (ancestor <- ancestors) {
      assetpath = assetpath :+ new AssetPathElement(ancestor.name, ancestor.assetType)
    }
    assetpath = assetpath :+ new AssetPathElement(asset.name, asset.assetType)
    return assetpath
  }

  def getTimeStamp(value: String): String = {
    var timeStamp = value.replace("T", " ").split("\\.").reverse.last
    timeStamp = timeStamp.replace("-", "").replace(":", "").replace(" ", "")
    timeStamp = timeStamp.split("\\+").reverse.last
    timeStamp
  }

  def computeFeaturePath(feature: Feature): Seq[FeaturePathElement] = {
    var featurepath: Seq[FeaturePathElement] = Nil
    var feat = feature
    while (feat.parent != None) {
      featurepath = featurepath :+ new FeaturePathElement(feat.name)
      feat = feat.parent.get
    }
    featurepath = featurepath :+ new FeaturePathElement(feat.name)
    return featurepath.reverse
  }

  def findImmediateParentClass(asset:Asset):Option[Asset]={
    asset.assetType match{
      case ClassType => Some(asset)
      case FolderType | FolderType | RepositoryType => None
      case _=> {
        asset.parent match{
          case None => None
          case _ => findImmediateParentClass(asset.parent.get);
        }

      }
    }
  }

  def findImmediateParentFile(asset:Asset):Option[Asset] ={
    asset.assetType match{
      case FileType => Some(asset)
      case FolderType => None
      case RepositoryType =>None
      case _=> {
        asset.parent match{
          case None => None
          case _ => findImmediateParentFile(asset.parent.get);
        }

      }
    }

  }

  def getLastModifiedTime(file:File): Long = {
    val path: Path = Paths.get(file.getAbsolutePath)
      val attr = Files.readAttributes(path, classOf[BasicFileAttributes])
      return getTimeStamp(attr.lastModifiedTime.toInstant().atZone(ZoneId.systemDefault()).toString).toLong
  }

  def isCodeAsset(asset:Asset) : Boolean = {
    if(asset.assetType == FileType || asset.assetType == FolderType || asset.assetType == RepositoryType || asset.assetType == VPRootType){
      return false
    }
    else return true
  }

  def getLastModifiedTime(asset:Asset): Long = {

    if(!isCodeAsset(asset)){
      getLastModifiedTime(new File(asset.name))
    }
    else{
      findImmediateParentFile(asset) match{
        case None=>0
        case _=> getLastModifiedTime(new File(findImmediateParentFile(asset).get.name))
      }
    }
  }


  /*
  def getLastModifiedTime(asset:Asset): Long = {
    // Your base directory where the `vp` folder is located
    val baseDirectory = "CalculatorRootJava"

    // Construct the full path by combining baseDirectory with the asset's name
    val fullPath = baseDirectory

    // Now use the fullPath to get the last modified time
    if(!isCodeAsset(asset)){
      getLastModifiedTime(new File(fullPath))
    }
    else{
      findImmediateParentFile(asset) match {
        case None => 0
        case Some(parentAsset) => getLastModifiedTime(new File(baseDirectory + parentAsset.name))
      }
    }
  }
 */
  def getAllFeatures(asset:Asset):Set[Feature]={
    var set = scala.collection.immutable.Set[Feature]()
    for (child <- transformASTToList(asset)) {
      for (feature <- child.mappedToFeatures) {
        set += feature
      }
    }
    set
  }

  // extracts all Features existent somewhere in an Expression
  // intended use is to get all features referenced by the code to readd annotations
  // this assumes positive use of the feature annotations originally (only annotate feature if it is implemented in the following block)
  def extractFeatures[T <: Expression](exp: T): List[Feature] = {
    exp match {
      case _: True => Nil
      case feat: Feature => feat :: Nil
      case or: Or => extractFeatures(or.left) ::: extractFeatures(or.right)
      case and: And => extractFeatures(and.left) ::: extractFeatures(and.right)
      case imp: Implies => extractFeatures(imp.left) ::: extractFeatures(imp.right)
      case neg: Negation => extractFeatures(neg.expr)
      case _ => Nil //default: handle different?
    }
  }

  def getLeastQualifiedPath(feat: Feature, unqualifiedPath: String = ""): Option[String] = {
    // "::" in FAXE-Paper, VP uses "-"
    val pathCandidate = if (unqualifiedPath == "") {
      feat.name.split("::").last
    } else {
      feat.name.split("::").last + "-" + unqualifiedPath
    }

    val test = transformFeatToList(getTopFeature(feat))
    val test2 = test.map(feat => feat.name)
    val test3 = test2.filter(feat => feat.endsWith("::"+pathCandidate) || feat.equals(pathCandidate))
    val isLeastQualifiedPath = test3.length == 1

    if (isLeastQualifiedPath) {
      Some(pathCandidate)
    } else {
      feat.parent match {
        case None => None
        case Some(parent) =>
          getLeastQualifiedPath(parent, pathCandidate)
      }
    }
  }

  def addToOptionalSeq[T](optList: Option[Seq[T]], elem: T) = {
    if (optList.isDefined) {
      Some(optList.get :+ elem)
    } else {
      Some(Seq(elem))
    }
  }

  def findFileParentAsset(asset: Asset): Option[Asset] = {
    asset.assetType match {
      case FileType => Some(asset)
      case VPRootType => None
      case _ => asset.parent match {
        case None => None
        case Some(parent) => findFileParentAsset(parent)
      }
    }
  }

  def getTopFeature(feat: Feature): Feature = {
    feat.parent match {
      case None => feat
      case Some(f) => getTopFeature(f)
    }
  }

  def changeAssetName(asset: Asset, name: String) = {
    val pathElementsToAsset = asset.name.split('\\')
    if (pathElementsToAsset.length > 1) {
      asset.name = pathElementsToAsset.dropRight(1).mkString("\\") + "\\" + name
    } else {
      asset.name = name
    }
  }

  def isFeatureBeneathRootAsset(feat: Feature): Boolean = {
    feat.getFeatureModelRoot.isDefined &&
      feat.getFeatureModelRoot.get.featureModel.isDefined &&
      feat.getFeatureModelRoot.get.featureModel.get.containingAsset.isDefined &&
      feat.getFeatureModelRoot.get.featureModel.get.containingAsset.get.getRoot.isDefined
  }

  def setNewNamesAfterCloning(asset:Asset, parentAsset:Asset):Unit = {
    transformASTToList(asset).foreach(a => a.name = parentAsset.name + "\\" + a.name.split('\\').last)
  }

  def setNewNamesRecursively(asset: Asset, parentName: String): Unit = {
    if (!List(ClassType, MethodType, FieldType, BlockType).contains(asset.assetType)) {
      asset.name = parentName + "\\" + asset.name.split('\\').last
      asset.children
        // only Folder- and FileType, as names of codelevel-assets should not be changed
        .filter(child => List(FolderType, FileType).contains(child.assetType))
        .foreach(child => setNewNamesRecursively(child, asset.name))
    }
  }

  def setParentageBetween(parent: Asset, child: Asset, insertAfter: Option[Asset]) = {
    if (insertAfter.isDefined) {
      if (parent.children.contains(insertAfter.get)) {
        val insertionIndex = parent.children.indexOf(insertAfter.get) + 1
        parent.children = parent.children.take(insertionIndex) ++ (child :: parent.children.drop(insertionIndex))
      } else {
        println("Error - insertAfter does not exist within parent.")
      }
    } else {
      parent.children = child :: parent.children
    }

    child.parent = Some(parent)
  }
}