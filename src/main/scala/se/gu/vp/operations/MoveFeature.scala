package se.gu.vp.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model.Feature

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class MoveFeature(feature:Feature,targetfeature:Feature) extends Operation with Logging {

  def dropIndex[T](list: List[T], idx: Int): List[T] = {
    list.zipWithIndex.filter(_._2 != idx).map(_._1)
  }

  override def perform(): Boolean = {

    if (feature.parent != None) {
      // incase the feature is a root feature
      feature.parent.get.subfeatures = dropIndex(feature.parent.get.subfeatures, feature.parent.get.subfeatures.indexOf(feature))
      updateFeatureVersion(feature.parent.get)
    }
    feature.parent = Some(targetfeature)
    feature.parent.get.subfeatures = feature.parent.get.subfeatures ++ List(feature)
    updateFeatureVersion(feature.parent.get)

    if(feature.getFeatureModelRoot() == targetfeature.getFeatureModelRoot()){
      //Nothing
    }
    else{
      // Find clones and map
    }
    storeSubOpMetadata
    true
  }

  override def createMetadata(): Unit = {
    true
  }
}
object MoveFeature{
  def apply(feature: Feature, targetfeature: Feature, runImmediately: Boolean = true, createMetadata: Boolean = true): MoveFeature = {
    if (runImmediately && createMetadata) {
      new MoveFeature(feature, targetfeature) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new MoveFeature(feature, targetfeature) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new MoveFeature(feature, targetfeature) with CreateMetadata
    } else {
      new MoveFeature(feature, targetfeature)
    }
  }
}