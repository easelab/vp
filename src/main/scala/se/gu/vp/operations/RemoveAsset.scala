package se.gu.vp.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model.Asset
import se.gu.vp.operations.metadata.{MetadataConverter, RemoveAssetMetadata}

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class RemoveAsset(ast:Asset, assetToDelete: Option[Asset] = None, assetsToDelete: Option[List[Asset]] = None) extends Operation with Logging {

  def dropIndex[T](list: List[T], idx: Int): List[T] = {
    list.zipWithIndex.filter(_._2 != idx).map(_._1)
  }

  override def perform(): Boolean = {
    // TODO: Features are not deleted by deleting assets, even if no assets remain to map to the feature
    var versionedAfterDelete = false
    if(assetToDelete!=None) {
      if (Utilities.astContainsAsset(assetToDelete.get, ast)) {
        val assettodelete = assetToDelete.get
        val parent = assettodelete.parent.get
        if (versionedAfterDelete == false) {
          updateVersion(parent)
          versionedAfterDelete = true
        }
        parent.children = dropIndex(parent.children,parent.children.indexOf(assettodelete))
        assettodelete.parent = None
      }
    }
      if(assetsToDelete!=None){
        for(assettodelete <- assetsToDelete.get){
          val asset= assettodelete
          val parent = asset.parent.get
          if(Utilities.astContainsAsset(assettodelete,ast)){
            if (versionedAfterDelete == false) {
              versionedAfterDelete = true
            }
            // Version number should be set for each parent
            updateVersion(parent)
            parent.children = dropIndex(parent.children,parent.children.indexOf(asset))
            asset.parent = None
          }
      }
    }
    storeSubOpMetadata
    versionedAfterDelete
  }

  override def createMetadata(): Unit = {
    try {
      val astMD = MetadataConverter.convertAssetToMetadata(ast)
      val assetToDeleteMD = if (assetToDelete.isDefined) {
        Some(MetadataConverter.convertAssetToMetadata(assetToDelete.get))
      } else { None }
      val assetsToDeleteMD = if (assetsToDelete.isDefined) {
        val listMD = for (asset <- assetsToDelete.get) yield {
          MetadataConverter.convertAssetToMetadata(asset)
        }
        Some(listMD)
      } else { None }

      metadata = Some(RemoveAssetMetadata(astMD, assetToDeleteMD, assetsToDeleteMD))
    } catch {
      case e : Exception =>
        println("Something went wrong during metadata creation.")
    }
  }
}
object RemoveAsset{
  def apply(root: Asset, assettodelete: Asset, runImmediately: Boolean = true, createMetadata: Boolean = true): RemoveAsset = {
    if (runImmediately && createMetadata) {
      new RemoveAsset(ast = root, assetToDelete = Some(assettodelete)) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new RemoveAsset(ast = root, assetToDelete = Some(assettodelete)) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new RemoveAsset(ast = root, assetToDelete = Some(assettodelete)) with CreateMetadata
    } else {
      new RemoveAsset(ast = root, assetToDelete = Some(assettodelete))
    }
  }

  def apply(root: Asset, assetsToDelete: List[Asset], runImmediately: Boolean, createMetadata: Boolean): RemoveAsset = {
    if (runImmediately && createMetadata) {
      new RemoveAsset(ast = root, assetsToDelete = Some(assetsToDelete)) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new RemoveAsset(ast = root, assetsToDelete = Some(assetsToDelete)) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new RemoveAsset(ast = root, assetsToDelete = Some(assetsToDelete)) with CreateMetadata
    } else {
      new RemoveAsset(ast = root, assetsToDelete = Some(assetsToDelete))
    }
  }
}