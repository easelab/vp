package se.gu.vp.operations

import se.gu.vp.commons.Logging
import se.gu.vp.model._
import Utilities._
import se.gu.vp.operations.metadata.{MetadataConverter, RemoveFeatureMetadata}

import scala.collection.mutable

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

class RemoveFeature(val FeaturePath:Path) extends Operation with Logging {

  def dropIndex[T](list: List[T], idx: Int): List[T] = {
    list.zipWithIndex.filter(_._2 != idx).map(_._1)
  }

  override def perform(): Boolean = {
    val operationListBuilder = mutable.ListBuffer[Operation]()

    val s_assetpath = FeaturePath.path.collect { case a: AssetPathElement => a }
    val sourceassetpath = resolveAssetPath(FeaturePath.root, s_assetpath)
    val featureModel = sourceassetpath.last.featureModel.get
    val s_featurepath = FeaturePath.path.collect { case a: FeaturePathElement => a }
    val featureToDelete = resolveFeaturePath(featureModel, s_featurepath)

    featureToDelete match {
      case None=> info("Feature might already moved to another location");
      case _=>{
        // call recursively for subfeatures?
    featureToDelete.get.subfeatures.foreach (f => f.parent = None)
    featureToDelete.get.subfeatures = Nil

    if (featureModelContainsFeature (featureToDelete.get, featureModel) ) {
    info ("Removing Asset Mappings in from the AST")
    val astroot = sourceassetpath.last.getRoot ()
    val mappedassets = featureToDelete.get.mappedAssets (sourceassetpath.last)

    for (mappedasset <- mappedassets) {
      val size = mappedasset.mappedToFeatures.size
      if (mappedasset.mappedToFeatures.size > 1) {
        Utilities.unmapFeature (mappedasset, featureToDelete.get)
        updateVersion(mappedasset)
      }
      else {
        operationListBuilder += RemoveAsset (astroot.get, mappedasset)
      }
    }

    featureToDelete.get.parent match {
    case None => {
      if(featureToDelete.get.isInstanceOf[RootFeature]){
        featureToDelete.get.asInstanceOf[RootFeature].featureModel.get.rootfeature = null
      }
    }
    case _ => {
      updateFeatureVersion(featureToDelete.get.parent.get)
      featureToDelete.get.parent.get.subfeatures = dropIndex (featureToDelete.get.parent.get.subfeatures, featureToDelete.get.parent.get.subfeatures.indexOf (featureToDelete.get) )
      featureToDelete.get.parent = None
      info ("Feature " + featureToDelete.get + " Deleted from Feature Model")
    }
    }
    }
    else error ("Feature " + featureModel + " not contained in feature model")
    }
  }
    subOperations = Some(operationListBuilder.toList)
    storeSubOpMetadata
    true
    }

  override def createMetadata(): Unit = {
    try {
      val s_assetpath = FeaturePath.path.collect { case a: AssetPathElement => a }
      val sourceassetpath = resolveAssetPath(FeaturePath.root, s_assetpath)
      val featureModel = sourceassetpath.last.featureModel.get
      val s_featurepath = FeaturePath.path.collect { case a: FeaturePathElement => a }
      val featureToDelete = resolveFeaturePath(featureModel, s_featurepath).get

      val featureMD = MetadataConverter.convertFeatureToMetadata(featureToDelete)

      metadata = Some(RemoveFeatureMetadata(featureMD))
    } catch {
      case e : Exception =>
        println("Something went wrong during metadata creation.")
    }
  }
}
object RemoveFeature{
  def apply(FeaturePath: Path, runImmediately: Boolean = true, createMetadata: Boolean = true): RemoveFeature = {
    if (runImmediately && createMetadata) {
      new RemoveFeature(FeaturePath) with CreateMetadata with RunImmediately
    } else if (runImmediately && !createMetadata) {
      new RemoveFeature(FeaturePath) with RunImmediately
    } else if (!runImmediately && createMetadata) {
      new RemoveFeature(FeaturePath) with CreateMetadata
    } else {
      new RemoveFeature(FeaturePath)
    }
  }
}