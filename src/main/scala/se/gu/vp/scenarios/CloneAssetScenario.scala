package se.gu.vp.scenarios

import se.gu.vp.model._
import se.gu.vp.operations._

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

/*
* Author: Wardah Mahmood. Date: 08-02-2019
* Simple scenarios to test every operator
* */
object CloneAssetScenario{

  def mainMethod():Unit = {
    
    //Test scenario
    val rootfeature1 = new RootFeature("A")
    val fm = new FeatureModel(rootfeature1)
    val rootfeature2 = new Feature("B")
    val rootfeature3 = new Feature("C")
    AddFeatureToFeatureModel(rootfeature1, fm, true, true, true)
    AddFeatureToFeatureModel(rootfeature2, fm, true, true, true)
    AddFeatureToFeatureModel(rootfeature3, fm, true, true, true)

    val fm2 = new FeatureModel(new RootFeature("Blank"))

    val root = Asset("root", VPRootType)

    val repo1 = Asset("repo 1", RepositoryType)
    val folder = Asset("folder", FolderType)
    val file = Asset("file", FileType)
    val classvp = Asset("class", ClassType)
    val codeblock1 = Asset("block1", BlockType)
    val codeblock2 = Asset("block2", BlockType)
    val method1 = Asset("method1", MethodType)
    val method2 = Asset("method2", MethodType)
    val mcblock11 = Asset("mcblock11", BlockType)
    val mcblock12 = Asset("mcblock12", BlockType)
    val mcblock21 = Asset("mcblock21", BlockType)
    val mcblock22 = Asset("mcblock22", BlockType)

    val repo2 = Asset("repo 2", RepositoryType)

    AddFeatureModelToAsset(repo1, fm)
    AddFeatureModelToAsset(repo2, fm2)

    AddAsset(repo1, root)
    AddAsset(folder, repo1)
    AddAsset(file, folder)
    AddAsset(classvp, file)
    AddAsset(repo2,root)

    AddAsset(codeblock2, file)
    AddAsset(codeblock1, file)
    AddAsset(method2, classvp)
    AddAsset(method1, classvp)
    AddAsset(mcblock12, method1)
    AddAsset(mcblock11, method1)
    AddAsset(mcblock22, method2)
    AddAsset(mcblock21, method2)

    MapAssetToFeature(repo1,rootfeature1)
    MapAssetToFeature(folder,rootfeature2)
    MapAssetToFeature(file,rootfeature3)
    MapAssetToFeature(classvp,rootfeature3)

    println("Before Cloning ----------")
    root.prettyprint

    CloneAsset(folder,repo2)

    println("After Cloning ----------")
    root.prettyprint
  }
}