package se.gu.vp.scenarios

import se.gu.vp.model._
import se.gu.vp.operations.Utilities.{computeAssetPath, computeFeaturePath}
import se.gu.vp.operations._

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

/*
* Author: Wardah Mahmood. Date: 08-02-2019
* Simple scenarios to test every operator
* */
object CloneFeatureScenario{

  def mainMethod():Unit = {
    //Test scenario
    val feature1 = new RootFeature("feature1")
    val fmleft = new FeatureModel(feature1)
    val feature2 = Feature("feature2")
    val feature3 = Feature("feature3")
    val feature4 = Feature("feature4")
    val feature5 = Feature("feature5")
    val feature6 = Feature("feature6")
    val feature7 = Feature("feature3")
    AddFeatureToFeatureModel(feature1,fmleft, true, true, true)
    AddFeatureToFeatureModel(feature2,fmleft, true, true, true)
    AddFeatureToFeatureModel(feature3,feature1)
    AddFeatureToFeatureModel(feature4,feature2)
    AddFeatureToFeatureModel(feature5,feature1)
    AddFeatureToFeatureModel(feature6,feature2)
    AddFeatureToFeatureModel(feature7,feature5)

    val fmright = new FeatureModel(new RootFeature("FmLeftRoot"))

    val root = Asset("Root",VPRootType)
    val repo1 =  Asset("Repository 1",RepositoryType)
    val repo2 =  Asset("Repository 2",RepositoryType)
    val folder1 = Asset("Folder 1",FolderType)
    val folder2 = Asset("Folder 2",FolderType)
    val folder3 = Asset("Folder 3",FolderType)
    val folder4 = Asset("Folder 4",FolderType)
    val file1=Asset("File 1",FileType)
    val file2=Asset("File 2",FileType)
    val file3=Asset("File 3",FileType)
    val file4=Asset("File 4",FileType)
    val file5=Asset("File 5",FileType)
    val file6=Asset("File 6",FileType)
    AddFeatureModelToAsset(repo1,fmleft)
    AddFeatureModelToAsset(repo2,fmright)

    AddAsset(repo1,root)
    AddAsset(repo2,root)
    AddAsset(folder1,repo1)
    AddAsset(folder2,repo1)
    AddAsset(folder3,repo2)
    AddAsset(folder4,repo2)
    AddAsset(file1,folder1)
    AddAsset(file2,folder1)
    AddAsset(file3,folder2)
    AddAsset(file4,folder3)
    AddAsset(file5,folder3)
    AddAsset(file6,folder4)

    MapAssetToFeature(folder2,feature1)
    MapAssetToFeature(folder2,feature3)
    MapAssetToFeature(folder2,feature4)

    MapAssetToFeature(folder1,feature1)

    MapAssetToFeature(file1,feature1)

    MapAssetToFeature(file2,feature3)
    MapAssetToFeature(file2,feature5)

    val sourcepath = Path(root,computeAssetPath(repo1) ++ computeFeaturePath(feature1))
    val targetpath = Path(root,computeAssetPath(repo2))
    println(sourcepath)

    println("AST In the Beginning --------------")
    root.prettyprint
    CloneFeature(sourcepath,targetpath)
    println("AST Afterwards --------------")
    root.prettyprint
    fmright.prettyprint()
  }
}