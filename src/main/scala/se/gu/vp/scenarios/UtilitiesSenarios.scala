package se.gu.vp.scenarios

import se.gu.vp.model._
import se.gu.vp.operations._

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

/*
* Author: Wardah Mahmood. Date: 08-02-2019
* Simple scenarios to test every operator
* */
object UtilitiesSenarios{

  def mainMethod():Unit = {
    println(" --------------------- UTILITIES --------------------------")
    import Utilities._
    val root = Asset("Root",VPRootType)
    val r1= Asset("Rep1",RepositoryType)
    val r2= Asset("Rep2",RepositoryType)
    val f1=Asset("Fol1",FolderType)
    val f2=Asset("Fol2",FolderType)
    val fl2=Asset("Fl2",FolderType)
    val fl1=Asset("Fl1",FileType)
    val c1 = Asset("C1",ClassType)
    AddAsset(r1,root)
    AddAsset(f1,r1)
    AddAsset(fl1,f1)
    AddAsset(c1,fl1)
    AddAsset(r2,root)
    AddAsset(f2,r2)
    AddAsset(fl2,f2)


    assert(astContainsAsset(f1,root) == true)
    assert(astContainsAsset(root,f1) == false)
    val ft1=new RootFeature("F1")
    val ft2=Feature("F2")
    val fm=new FeatureModel(ft1)
    AddFeatureToFeatureModel(ft2,ft1)
    AddFeatureModelToAsset(root,fm)
    assert(getImmediateAncestorFeatureModel(Some(f1))!=None)
    CloneAsset(r1,root, cloneName = Some("r1Clone"))
    root.prettyprint

    val p1 = AssetPathElement("Rep1",RepositoryType)
    val p2 = AssetPathElement("Fol1",FolderType)
    var path:Seq[AssetPathElement] = Nil
    path = path :+ p1 :+ p2
    resolveAssetPath(root,path)

    val ftt1 =  new RootFeature("F1")
    val ftt2 =  Feature("F2")
    val ftt3 =  Feature("F3")

    val fm2 = new FeatureModel(ftt1)
    val fp1 = FeaturePathElement("F1")
    val fp2 = FeaturePathElement("F2")
    val fp3 = FeaturePathElement("F3")
    AddFeatureToFeatureModel(ftt2,ftt1)
    AddFeatureToFeatureModel(ftt3,ftt2)
    var fpath:Seq[FeaturePathElement] = Nil
    fpath = fpath :+ fp1 :+ fp2 :+ fp3
    resolveFeaturePath(fm2,fpath)

    println(featureModelContainsFeature(ftt3,fm2))
    println(getImmediateAncestorFeatureModel(Some(c1)).get)
    println(transformFMToList(fm2))
    MoveFeature(ftt3,ftt1)
    println(transformFMToList(fm2))
  }
}