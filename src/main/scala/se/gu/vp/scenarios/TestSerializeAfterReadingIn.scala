package vp.scenarios

import se.gu.vp.operations.JavaParser.JavaParserMain.readInJavaAssetTree
import se.gu.vp.operations.SerialiseAssetTree

/**
Copyright [2021] [Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

object TestSerializeAfterReadingIn {

  def mainMethod():Unit = {
    val readInPath = "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\CalculatorRoot"

    val ast = readInJavaAssetTree(readInPath)
    ast match {
      case None =>
      case Some(astroot) =>

        SerialiseAssetTree(astroot, "D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\TestTransplant_Original")
    }
  }
}
