package se.gu.vp.scenarios

import se.gu.vp.model
import se.gu.vp.model._
import se.gu.vp.operations._

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

/*
* Author: Wardah Mahmood. Date: 08-02-2019
* Simple scenarios to test every operator
* */
object AllOperatorsExampleScenario{

  def mainMethod():Unit = {

    val folder1 = Folder("folder1")

    val myAst =
      VPRoot( "root",
        Repository( "Repository 1", folder1 ) ::
        Repository("Repository 2" ) ::
        Nil )

    // let's map folder 1 to featureA
    folder1.presenceCondition = Feature( "featureA" )
    // we could also map it to an expression over multiple features
    // this all works because Feature is an Expression
    folder1.presenceCondition = Feature( "featureA" ) & Feature("featureB")
    folder1.presenceCondition = ( Feature( "featureA" ) & Feature("featureB") ) | Feature("featureC")

    myAst.prettyprint
    

    myAst.prettyprint
  }


  def mainMethod2():Unit = {
    println(" --------------------- ADD ASSET --------------------------")

    val rootfeature1 = new RootFeature("Play Game")
    val subfeature11=new Feature("Run Forward")
    val subfeature12=new Feature("Run Backward")
    val subfeature13=new Feature("Slidedown")

    AddFeatureToFeatureModel(subfeature11,rootfeature1)
    AddFeatureToFeatureModel(subfeature12,rootfeature1)
    AddFeatureToFeatureModel(subfeature13,rootfeature1)

    val rootfeature2 = new Feature("Kill Enemies")
    val subfeature21=new Feature("Jump On Enemies")
    val subfeature22=new Feature("Shoot enemies")
    AddFeatureToFeatureModel(subfeature21,rootfeature2)
    AddFeatureToFeatureModel(subfeature22,rootfeature2)


    val rootfeature3 = new Feature("Collect Coins")
    val subfeature31=new Feature("Score Update")
    val subfeature32=new Feature("Reward Player")
    AddFeatureToFeatureModel(subfeature31,rootfeature3)
    AddFeatureToFeatureModel(subfeature32,rootfeature3)

    val rootfeature4 = new Feature("Dodge Obstacles")
    val subfeature41=new Feature("Jump Over Obstacles")
    AddFeatureToFeatureModel(subfeature41,rootfeature4)

    val fm = new FeatureModel(rootfeature1)

    AddFeatureToFeatureModel(rootfeature1,fm, true, true, true)
    AddFeatureToFeatureModel(rootfeature2,fm, true, true, true)
    AddFeatureToFeatureModel(rootfeature3,fm, true, true, true)
    AddFeatureToFeatureModel(rootfeature4,fm, true, true, true)
    // always prefer val over var
    val root=new Asset("Root",VPRootType)
    val r1=new Asset("Repository 1",RepositoryType)
    val r2 = new Asset("Repository 2",RepositoryType)
    val f1=new Asset("Folder 1",FolderType)
    f1.presenceCondition = (rootfeature3 | !Feature("P"))
    val f2=new Asset("Folder 2",FolderType)
    val file1=new Asset("File 1",FileType)
    val file2=new Asset("File 2",FileType)
    val class1=new Asset("Class 1",model.ClassType)
    class1.presenceCondition =(rootfeature1 & rootfeature2) | !Feature("W")
    val class2=new Asset("Class 2",model.ClassType)
    val field1=new Asset("Field A",FieldType)
    val field2=new Asset("Field B",FieldType)
    val m1=new Asset("Method X",MethodType)
    val m2=new Asset("Method Y",MethodType)

    AddFeatureModelToAsset(root,fm)

    AddAsset(r1,root)
    AddAsset(r2,root)
    AddAsset(f1,r1)
    AddAsset(f2,r1)
    AddAsset(file1,f1)
    AddAsset(file2,f2)
    AddAsset(class1,file1)
    AddAsset(class2,file2)
    AddAsset(m1,class1)
    AddAsset(m2,class2)
    AddAsset(field1,class1)
    AddAsset(field2,class2)
    root.prettyprint
    println(" --------------------- CLONE ASSET --------------------------")
    val clone= CloneAsset(file1,f2)
    root.prettyprint

    println(" --------------------- REMOVE ASSET --------------------------")
    val remove= RemoveAsset(root,m1)
    root.prettyprint

    println(" --------------------- CHANGE ASSET --------------------------")
    ChangeAsset(file1)
    root.prettyprint

    //After discussion with Daniel, we decided the PropagateAsset should
    //should have source and clone in the arguments, but it is difficult
    //to do since we do not have the clone in the memory here. So we would
    //have to implement the getClone separately, which is easy but not
    //very significant at this stage.So I am changing it back to just like
    //how is is done in clone, that is, by giving the source, and the
    //container of the target parent
    println(" --------------------- PROPAGATE ASSET --------------------------")
    val propOp = PropagateToAsset(file1,f2)
    root.prettyprint

    println(" --------------------- Feature Model --------------------------")

  }
}