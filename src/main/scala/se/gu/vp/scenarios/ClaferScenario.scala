package se.gu.vp.scenarios

import java.io.File

import se.gu.vp.model._
import se.gu.vp.operations.Utilities._
import se.gu.vp.operations._

import scala.collection.mutable.ListBuffer
import scala.io.Codec

/**
Copyright [2019] [Wardah Mahmood, Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

object ClaferScenario {
  var repoFolder: Option[Asset] = None;


  // Failing
  def mainMethod():Unit = {

    //add root
    val root = new Asset("Root", VPRootType)


    // @TODO I think you put it under src/main/resources and then you don't need to hardcode the path (check sbt documentation)
    val dir = new File("src/main/scala/se/gu/vp/scenarios").getAbsolutePath;
    println(dir)
    val bufferedSource = io.Source.fromFile(dir + "/ClaferMooVisualizer.csv")(Codec("utf-8"))
    var claferOperations = ListBuffer[ClaferOperation]()
    var commitIds = ListBuffer[String]()
    var createdAssets: List[Asset] = Nil
    var createdFeatures: List[Feature] = Nil
    //list of all features added so far
    var featureModelAssets: List[Asset] = Nil //for assets with feature models

    for (line <- bufferedSource.getLines.drop(1)) {

      val Array(repo,claferOP, vpOperation, assetName, assetType, immediateParent, commitHash, assetFeatures, target, commitLink, commitMessage) = line.split(";").map(_.trim)

      val op: ClaferOperation = new ClaferOperation(repo,claferOP, vpOperation, assetName, assetType, immediateParent, commitHash, assetFeatures, target, commitLink, commitMessage)
      claferOperations += op
      commitIds += commitHash

    }


    bufferedSource.close

    println("==========ALL CLAFER OPERATIONS==========")

    val distinctCommitIds = commitIds.distinct
    printf("DISTINCT COMMITS: %d\n", distinctCommitIds.size - 1) //subtract one due to the first NONE commit when repo is created
    distinctCommitIds.foreach {
      println
    }

    //get assets per commit
    for (commitId <- distinctCommitIds) {

      val commitOperations = claferOperations.filter(_.commitHash.equalsIgnoreCase(commitId))
      val groupedAssets = ListBuffer[Map[Asset, Asset]]()
      val changedAssets = ListBuffer[Asset]()
      val removedAssets = ListBuffer[Asset]()
      val removedFeatures = ListBuffer[Feature]()

      println("=====COMMIT========")
      println(commitId)

      //=====ASSET OPERATIONS=========
      for (operation <- commitOperations) {
        if (!operation.assetType.equalsIgnoreCase("Feature")) {
          //asset related import
          if (operation.claferOP.startsWith("add")) {
            val childAsset = createAsset(operation)
            if (operation.assetType.equalsIgnoreCase("REPOSITORY")) {
              groupedAssets += Map(childAsset -> root)
            } else {
              val parentOp = claferOperations.find(_.assetName.equalsIgnoreCase(operation.immediateParent))

              val parentAsset = createAsset(parentOp.get)
              groupedAssets += Map(childAsset -> parentAsset)
            }
          }
          else if (operation.claferOP.startsWith("change")) {
            val asset = findAssetByName(operation.assetName, root)
            if (asset != None) {
              changedAssets += asset.get
            }
          }
          else if (operation.claferOP.startsWith("delete")) {
            val asset = findAssetByName(operation.assetName, root)
            if (asset != None) {
              removedAssets += asset.get
            }
          }
          else if (operation.claferOP.startsWith("move")) {
            val asset = findAssetByName(operation.assetName, root)
            val targetParentAsset = findAssetByName(operation.target, root)
            if (asset != None && targetParentAsset != None) {
              MoveAsset(asset.get, targetParentAsset.get)
            }
          } else if (operation.claferOP.startsWith("rename")) {
            val asset = findAssetByName(operation.assetName, root)
            if (asset != None) {
              asset.get.rename(operation.target)
            }
          }
        }
      }

      if (changedAssets.size > 0) {
        ChangeAsset(changedAssets.toList, true, true)
      }
      if (removedAssets.size > 0) {
        RemoveAsset(root, removedAssets.toList, true, true)
      }

      //===FEATURES===========IF ANY

      val featureOperations = commitOperations.filter(_.assetType.contains("Feature"))
      for (operation <- featureOperations) {
        if (operation.assetType.equalsIgnoreCase("Feature") || operation.assetType.equalsIgnoreCase("FeatureModel")) {
          //feature related import
          if (operation.claferOP.startsWith("add")) {
            val parentOp = claferOperations.find(_.assetName.equalsIgnoreCase(operation.immediateParent))
            if (operation.assetType.equalsIgnoreCase("FeatureModel")) {
              val featureModelAsset = createAsset(parentOp.get) //feature model asset should be parent of the .vp-project file
              AddFeatureModelToAsset(featureModelAsset, FeatureModel(new RootFeature("Blank")))
              featureModelAssets = featureModelAsset :: featureModelAssets //all feature model assets for the repository

            } else {
              val currentFeatureModelAsset = featureModelAssets.find(_.name.equalsIgnoreCase(operation.immediateParent))
              if (!currentFeatureModelAsset.equals(None)) {
                val feature = new Feature(operation.assetName)
                AddFeatureToFeatureModel(feature, currentFeatureModelAsset.get.featureModel.get, true, true, true)
                createdFeatures = feature :: createdFeatures
              } else {
                val parentFeature = createFeature(parentOp.get)
                val childFeature = createFeature(operation)
                AddFeatureToFeatureModel(childFeature, parentFeature)
              }
            }

          }
          else if (operation.claferOP.startsWith("remove")) {

            val currentFeatureModelAsset = findAssetByName(getFeatureModelName(operation), root)

            val feature = findFeatureByName(operation.assetName, currentFeatureModelAsset.get.featureModel.get)
            if (feature != None) {
              val path = Path(root, computeAssetPath(currentFeatureModelAsset.get) ++ computeFeaturePath(feature.get))
              RemoveFeature(path)
            }
          }
          else if (operation.claferOP.startsWith("move")) {

            val currentFeatureModelAsset = findAssetByName(getFeatureModelName(operation), root)

            val feature = findFeatureByName(operation.assetName, currentFeatureModelAsset.get.featureModel.get)
            val targetParentFeature = findFeatureByName(operation.target, currentFeatureModelAsset.get.featureModel.get)
            if (feature != None && targetParentFeature != None) {

              MoveFeature(feature.get, targetParentFeature.get)
            }
          }

        }

      }

      //=====MAP ASSET TO FEATURES=========
      for (operation <- commitOperations) {
        if (operation.claferOP.startsWith("mapAsset") || operation.claferOP.startsWith("unMapAsset")) {
          val asset = findAssetByName(operation.assetName, root)
          val featureList = operation.assetFeatures.split('|')
          val featureModelAssetName = getFeatureModelName(operation)
          val currentFeatureModelAsset = findAssetByName(featureModelAssetName, root)
          if (!currentFeatureModelAsset.equals(None)) {
            for (featureName <- featureList) {
              val feature = findFeatureByName(featureName, currentFeatureModelAsset.get.featureModel.get)
              if (!asset.equals(None) && !feature.equals(None)) {
                if (operation.claferOP.startsWith("mapAsset"))
                  MapAssetToFeature(asset.get, feature.get) //map feature to asset
                else
                 {
              }} else if (!asset.equals(None) && feature.equals(None)) {
                if (operation.claferOP.startsWith("mapAsset"))
                  MapAssetToFeature(asset.get, Feature(featureName)) //map asset to UNASSIGNED feature. see commit ce0fbed in ClaferMooVisualizer csv. Developer first annotated feature but forgot to add it to FM
              }
            }

          }
        }

      }


      //end
    }

    def getFeatureModelName(operation: ClaferOperation): String = {
      val list = operation.assetFeatures.split('/')
      list(0)
    }

    def getAssetName(path: String): String = {
      val list = path.split('/')
      list(list.length - 1) //e.g., ClaferVisualizer/.vp-project
    }

    def createAsset(operation: ClaferOperation): Asset = {
      val foundAsset: Option[Asset] = createdAssets.find(_.name == operation.assetName)
      var myAsset: Asset = new Asset("none", FileType)
      if (foundAsset.equals(None)) {
        myAsset = new Asset(operation.assetName, getAssetType(operation))
        createdAssets = myAsset :: createdAssets
      } else {
        myAsset = foundAsset.get
      }

      def getAssetType(operation: ClaferOperation): AssetType = {
        operation.assetType match {
          case "FOLDER" => FolderType
          case "FILE" => FileType
          case "FeatureModel" => FileType
          case "FUNCTION" => MethodType
          case "REPOSITORY" => RepositoryType
          case "CLASS" => ClassType
          case "BLOCK" => BlockType

        }
      }

      myAsset
    }

    def createFeature(operation: ClaferOperation): Feature = {
      val foundFeature: Option[Feature] = createdFeatures.find(_.name == operation.assetName)
      var myFeature: Feature = new Feature("none")
      if (foundFeature.equals(None)) {
        myFeature = new Feature(operation.assetName)
        createdFeatures = myFeature :: createdFeatures
      } else {
        myFeature = foundFeature.get
      }
      myFeature


    }

    //can generate a tree of an asset and its children
    def webGraphAssets(root: Asset): Unit = {
      val assets = root :: flat(root.children)
      println("digraph G {")
      assets.foreach(a => printf("%s -> %s %s\n", if (a.parent == None) "ROOT" else getAssetName(a.parent.get.name), getAssetName(a.name), getColourLabel(a)))
      println("}")
    }

    def getColourLabel(asset: Asset): String = {
      asset.assetType match {
        case VPRootType => "[color=\"0.647 0.204 1.000\"]"
        case RepositoryType => "[color=\"0.515 0.762 0.762\"]"
        case FolderType => "[color=\"0.002 0.999 0.999\"]"
        case FileType => "[color=\"0.348 0.839 0.839\"]"
        case MethodType => "[color=\"0.650 0.700 0.700\"]"
        case BlockType => "[color=\"0.499 0.386 1.000\"]"
      }
    }
    //now print repo
    root prettyprint
    //webGraphAssets(root)
  }


}

case class ClaferOperation(
                           val repo:String, val claferOP: String, val vpOperation: String, val assetName: String, val assetType: String, val immediateParent: String, val commitHash: String,
                            val assetFeatures: String, val target: String, val commitLink: String, val commitMessage: String) {


}
