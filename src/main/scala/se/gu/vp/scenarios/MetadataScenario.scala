package se.gu.vp.scenarios

import se.gu.vp.operations.JavaParser.JavaParserMain.readInJavaAssetTree
import net.liftweb.json._
import net.liftweb.json.Serialization.{read, write}
import se.gu.vp.model.{Asset, Feature, FeatureModel, FileType, FolderType, RepositoryType, RootFeature, True, VPRootType}
import se.gu.vp.operations.{AddAsset, AddFeatureModelToAsset, AddFeatureToFeatureModel, MapAssetToFeature}
import se.gu.vp.operations.Utilities.getassetbyname
import se.gu.vp.operations.metadata.{AddAssetMetadata, AssetMetadata, ExternalAssetMetadata, InternalAssetMetadata, Metadata, MetadataConverter}

/**
Copyright [2021] [Christoph Derks]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

abstract class AbstractNode
case class RootNode(name: String, children: List[AbstractNode]) extends AbstractNode
case class Node(name: String, children: List[AbstractNode]) extends AbstractNode

object MetadataScenario {

  def mainMethod():Unit = {
    val asset = readInJavaAssetTree("D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\CalculatorRoot")
    val feature0 = Feature("myOtherFeat")
    val feature1 = Feature("myFeat")
    val feature2 = Feature("myFeat")
    val a0 = getassetbyname("D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\CalculatorRoot\\CalculatorRepo", asset.get)
    val a1 = getassetbyname("D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\CalculatorRoot\\CalculatorRepo\\build.gradle", asset.get)
    val a2 = getassetbyname("D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\CalculatorRoot\\CalculatorRepo\\settings.gradle", asset.get)
    val a3 = getassetbyname("D:\\Dokumente\\_Studium\\masterarbeit\\2021-vpbenchmark\\testEnvironment\\CalculatorRoot\\CalculatorRepo\\src\\main\\java\\org\\easelab\\calculatorexample\\calculator\\BasicCalculator.java", asset.get).get.children(1)
    val rootFeat = new RootFeature("rootFeat")
    val fm = FeatureModel(rootFeat)
    AddFeatureModelToAsset(a0.get, fm, true)
    AddFeatureToFeatureModel(feature0, rootFeat, true, true)
    AddFeatureToFeatureModel(feature1, feature0, true, true)
    AddFeatureToFeatureModel(feature2, rootFeat, true, true)
    MapAssetToFeature(a1.get, feature1, true)
    MapAssetToFeature(a2.get, feature2, true)
    // shallow clone, feature model is not cloned for instance -> thats why its always set to none after cloning!
    val a0Clone = a0.get.doClone

    val myMetadata1 = MetadataConverter.convertAssetToMetadata(a1.get)
    val myMetadata2 = MetadataConverter.convertAssetToMetadata(a3)
    val myMetadata3 = MetadataConverter.convertAssetTreeToMetadata(asset.get)

    implicit val formats = Serialization.formats(FullTypeHints(List(classOf[Metadata])))
    val jval = Extraction.decompose(asset)(formats)
    val jval2 = Extraction.decompose(rootFeat.subfeatures)(formats)
    val jval3 = Extraction.decompose(myMetadata1)(formats)
    val jval4 = Extraction.decompose(myMetadata2)(formats)
    val jval5 = Extraction.decompose(myMetadata3)(formats)
    val ser1 = write(asset)
    val ser5 = write(myMetadata3)
    val test2 = parse(ser5)
    val checkEquivalence = jval5 == test2
    val b = parse(write(myMetadata1))
    val c = parse(write(myMetadata2))
    // 2 evaluated to false, 3 to true, because parse(write( removes the Option[]-elements and they dont get added back again during parse
    val checkEquivalence2 = jval3 == parse(write(myMetadata1))
    val checkEquivalence3 = jval4 == parse(write(myMetadata2))
    // None is getting added back in
    val d = read[InternalAssetMetadata](write(myMetadata1))

    //Treetesting for checking if issue is nested constructors? -> Seems to work fine

    val myMetadata4 = MetadataConverter.convertAssetTreeToMetadata(a3)
    val myMetadata4Written = write(myMetadata4)
    val e = read[AssetMetadata](myMetadata4Written)
    val test = read[ExternalAssetMetadata](ser5)
    val testEq = test == myMetadata3

    val newAsset1 = new Asset("myWonderfulAsset1", RepositoryType)
    val newAsset2 = new Asset("myWonderfulAsset1\\myWonderfullerAsset2", FileType)
    AddAsset(newAsset2, newAsset1, None, true)
    val addOp = AddAsset(newAsset1, a3, None, true)
    val myConvertedMetadata = addOp.metadata
    val toJSON = write(myConvertedMetadata)
    val backFromJSON = read[AddAssetMetadata](toJSON)

    val root1 = Asset("Root1", VPRootType)
    val repo1 = Asset("Repo1", RepositoryType)
    val fold1 = Asset("Fold1", FolderType)
    val file1 = Asset("File1", FileType)
    AddAsset(repo1, root1, None, true)
    AddAsset(fold1, repo1, None, true)
    AddAsset(file1, fold1, None, true)

    val repo2 = Asset("Repo2", RepositoryType)
    val fold2 = Asset("Fold2", FolderType)
    val file2 = Asset("File2", FileType)
    AddAsset(fold2, repo2, None, true)
    AddAsset(file2, fold2, None, true)

    val fold3 = Asset("Fold3", FolderType)
    val file3 = Asset("File3", FileType)
    AddAsset(file3, fold3, None, true)

    val rf1 = new RootFeature("rf1")
    val f11 = Feature("f11")
    val f12 = Feature("f12")
    val f13 = Feature("f13")
    val fm1 = FeatureModel(rf1)
    val addFeat2FMOp1 = AddFeatureToFeatureModel(f11, rf1, true, true)
    val addFeat2FMOp2 = AddFeatureToFeatureModel(f12, rf1, true, true)
    val addFeat2FMOp3 = AddFeatureToFeatureModel(f13, f11, true, true)
    AddFeatureModelToAsset(repo1, fm1, true)

    val rf2 = new RootFeature("rf2")
    val f21 = Feature("f21")
    val f22 = Feature("f22")
    val f23 = Feature("f23")
    val fm2 = FeatureModel(rf2)
    AddFeatureToFeatureModel(f21, rf2, true, true)
    AddFeatureToFeatureModel(f22, rf2, true, true)
    AddFeatureToFeatureModel(f23, f21, true, true)
    AddFeatureModelToAsset(repo2, fm2, true)

    val rf3 = new RootFeature("rf3")
    val f31 = Feature("f31")
    val f32 = Feature("f32")
    val f33 = Feature("f33")
    val fm3 = FeatureModel(rf3)
    AddFeatureToFeatureModel(f31, rf3, true, true)
    AddFeatureToFeatureModel(f32, rf3, true, true)
    AddFeatureToFeatureModel(f33, f31, true, true)
    AddFeatureModelToAsset(fold3, fm3, true)

    val rf4 = new RootFeature("rf4")
    val f41 = Feature("f41")
    val f42 = Feature("f42")
    val f43 = Feature("f43")
    val fm4 = FeatureModel(rf4)
    AddFeatureToFeatureModel(f41, rf4, true, true)
    AddFeatureToFeatureModel(f42, rf4, true, true)
    AddFeatureToFeatureModel(f43, f41, true, true)

    val rf5 = new RootFeature("rf5")
    val f51 = Feature("f51")
    val f52 = Feature("f52")
    val f53 = Feature("f53")
    AddFeatureToFeatureModel(f51, rf5, true, true)
    AddFeatureToFeatureModel(f52, rf5, true, true)
    AddFeatureToFeatureModel(f53, f51, true, true)

    val f61 = Feature("f61")
    val f62 = Feature("f62")
    val f63 = Feature("f63")
    AddFeatureToFeatureModel(f62, f61, true, true)
    AddFeatureToFeatureModel(f63, f62, true, true)

    // is supposed to crash with f32 in both cases
    // the top one should also crash with f22, as f22 is in a different external assettree and it expects file1 to be in an external assettree
    // but you should never have the case where you need to define the main asset tree using convertAssetTreeToMetadata
    file1.presenceCondition = f62 | f52 | f42 | f12 | True()
    file2.presenceCondition = f62 | f52 | f42 | f22 | f12 | True()

    val converted1 = MetadataConverter.convertAssetTreeToMetadata(root1)
    val converted2 = MetadataConverter.convertAssetToMetadata(fold2)

    val featureConversionRF1 = MetadataConverter.convertFeatureToMetadata(rf1)
    val featureConversionF11 = MetadataConverter.convertFeatureToMetadata(f11)
    val featureConversionF12 = MetadataConverter.convertFeatureToMetadata(f12)
    val featureConversionF13 = MetadataConverter.convertFeatureToMetadata(f13)
    val featureConversionRF2 = MetadataConverter.convertFeatureToMetadata(rf2)
    val featureConversionF21 = MetadataConverter.convertFeatureToMetadata(f21)
    val featureConversionF22 = MetadataConverter.convertFeatureToMetadata(f22)
    val featureConversionF23 = MetadataConverter.convertFeatureToMetadata(f23)
    val featureConversionRF3 = MetadataConverter.convertFeatureToMetadata(rf3)
    val featureConversionF31 = MetadataConverter.convertFeatureToMetadata(f31)
    val featureConversionF32 = MetadataConverter.convertFeatureToMetadata(f32)
    val featureConversionF33 = MetadataConverter.convertFeatureToMetadata(f33)
    val featureConversionRF4 = MetadataConverter.convertFeatureToMetadata(rf4)
    val featureConversionF41 = MetadataConverter.convertFeatureToMetadata(f41)
    val featureConversionF42 = MetadataConverter.convertFeatureToMetadata(f42)
    val featureConversionF43 = MetadataConverter.convertFeatureToMetadata(f43)
    val featureConversionRF5 = MetadataConverter.convertFeatureToMetadata(rf5)
    val featureConversionF51 = MetadataConverter.convertFeatureToMetadata(f51)
    val featureConversionF52 = MetadataConverter.convertFeatureToMetadata(f52)
    val featureConversionF53 = MetadataConverter.convertFeatureToMetadata(f53)
    val featureConversionF61 = MetadataConverter.convertFeatureToMetadata(f61)
    val featureConversionF62 = MetadataConverter.convertFeatureToMetadata(f62)
    val featureConversionF63 = MetadataConverter.convertFeatureToMetadata(f63)

    val fmConversion1 = MetadataConverter.convertFeatureModelToMetadata(fm1)
    val fmConversion2 = MetadataConverter.convertFeatureModelToMetadata(fm2)
    val fmConversion4 = MetadataConverter.convertFeatureModelToMetadata(fm4)

    val add3To1 = AddAsset(file3, repo1)
    val add3To1MD = add3To1.metadata.get
    val add2To1 = AddAsset(repo2, root1)
    val add2To1MD = add2To1.metadata.get

    val a = 1
  }
}
