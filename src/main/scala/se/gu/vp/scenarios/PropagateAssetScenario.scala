package se.gu.vp.scenarios

import se.gu.vp.model._
import se.gu.vp.operations._

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

/*
* Author: Wardah Mahmood. Date: 08-02-2019
* Simple scenarios to test every operator
* */
object PropagateAssetScenario{

  def mainMethod():Unit = {
    println(" --------------------- PROPAGATE ASSET --------------------------")
    val root = Asset("root", VPRootType)
    val repo1 = Asset("Repo 1", RepositoryType)
    val folder = Asset("Folder 1.1", FolderType)
    val file = Asset("File 1.1.1", FileType)
    val classvp = Asset("Class 1.1.1.1", ClassType)

    val repo2 = Asset("Repo 2", RepositoryType)

    AddAsset(repo1, root)
    AddAsset(folder, repo1)
    AddAsset(file, folder)
    AddAsset(classvp, file)
    AddAsset(repo2,root)

    println("Before Cloning ----------")
    root.prettyprint

    CloneAsset(folder,repo2)

    println("After Cloning ----------")
    root.prettyprint

    val file3 = Asset("File 1.1'.2",FileType)

    val foldertarget = Utilities.findClones(folder)
    AddAsset(file3,foldertarget(0))

    ChangeAsset(file)
    ChangeAsset(classvp)
    val file2 = Asset("File 1.1.3",FileType)
    val class2 = Asset("Class 1.1.3.1",ClassType)
    AddAsset(file2,folder)
    AddAsset(class2,file2)

    println("-------------- Before Propagation ---------------")
    root.prettyprint

    if(!foldertarget.isEmpty) {
      PropagateToAsset(folder, foldertarget(0))
    }
    println("-------------- After Propagation ---------------")
    root.prettyprint
  }
}