package se.gu.vp.scenarios

import se.gu.vp.model._
import se.gu.vp.operations.Utilities.{getAncestors, isAncestor}
import se.gu.vp.operations._

/**
Copyright [2019] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

/*
* Author: Wardah Mahmood. Date: 08-02-2019
* Simple scenarios to test every operator
* */
object AddAssetScenario{

  def mainMethod():Unit = {


    println(" --------------------- ADD ASSET --------------------------")
    //Test scenario
    val rootfeature1 = new RootFeature("A")
    val fm = new FeatureModel(rootfeature1)
    val rootfeature2 = new Feature("B")
    val rootfeature3 = new Feature("C")
    AddFeatureToFeatureModel(rootfeature1, fm, true, true, true)
    AddFeatureToFeatureModel(rootfeature2, fm, true, true, true)
    AddFeatureToFeatureModel(rootfeature3, fm, true, true, true)

    val root = Asset("root", VPRootType)
    val repo1 = Asset("repo 1", RepositoryType)
    val folder = Asset("folder", FolderType)
    val file = Asset("file", FileType)
    val classvp = Asset("class", ClassType)

    AddFeatureModelToAsset(root, fm)

    repo1.presenceCondition = (rootfeature1 & rootfeature2) | !Feature("W")
    folder.presenceCondition = (rootfeature2 | !Feature("P"))

    AddAsset(repo1, root)
    AddAsset(folder, repo1)
    AddAsset(file, folder)
    AddAsset(classvp, file)
    root.prettyprint
    val mappedassets = rootfeature2.mappedAssets(root)
    println(mappedassets)
    println(isAncestor(file,folder))
    CloneAsset(file,folder)
    CloneAsset(file,folder)
    println(Utilities.findClones(file))
    root.prettyprint
    println(Utilities.getAncestors(classvp))
    val a = getAncestors(file)
    println(a)
  }
}