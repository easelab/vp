package se.gu.vp.model

/**
   Copyright [2018] [Thorsten Berger]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */
object ExpressionConversions {
  implicit def string2Identifier(str : String) = Identifier(str)
}

abstract class Expression{

  def & (other : Expression) : Expression = other match {
    case True() => this
    case _ => And(this, other)
  }
  def | (other : Expression) : Expression = other match {
    case False() => this
    case _ => Or(this, other)
  }
  def unary_!(): Expression = Negation(this)
  def implies (other : Expression) : Expression = Implies(this, other)

  def splitConjunctions : List[Expression] = this match {
      case And(x,True()) => x.splitConjunctions
      case And(True(),y) => y.splitConjunctions
      case And(x,y) => x.splitConjunctions ++ y.splitConjunctions
      case x => List(x)
    }
}

class BinaryExpression(val left : Expression, val right : Expression, op : String) extends Expression{
  override def toString = "(" + left + " " + op + " " + right + ")"
}
class UnaryExpression(val expr : Expression, op : String) extends Expression{
  override def toString = op + expr
}

case class Negation(e: Expression) extends UnaryExpression(e, "!")
case class And(l : Expression, r : Expression) extends BinaryExpression(l, r, "&")
case class Or(l : Expression, r : Expression) extends BinaryExpression(l, r, "|")
case class Implies(l : Expression, r : Expression) extends BinaryExpression(l, r, "=>")

case class Identifier(name : String) extends Expression {

  override def equals( other: Any ): Boolean =
    other match{
      case that:Identifier =>
        (that canEqual this) &&
        name.equalsIgnoreCase( that.name )
      case _ => false
    }

  override def toString = name
  override def hashCode: Int = name.toLowerCase.hashCode
}
case class True() extends Expression {
  override def & (other: Expression) = other
  override def implies (other: Expression) = other
  override def unary_! = False()
}
case class False() extends Expression {
  override def | (other : Expression) = other
  override def unary_! = True()
}

