package se.gu.vp.model

import se.gu.vp.operations.Utilities
import org.kiama.rewriting.Rewriter._
import Utilities._
import se.gu.vp.operations.metadata.{AssetTraceMetadata, FeatureTraceMetadata, MetadataConverter}

/**
Copyright [2018] [Thorsten Berger, Wardah Mahmood, Daniel Strueber]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

//Add reference for feature model in root feature and asset in feature model
/*
* Author: Wardah Mahmood. Date: 08-02-2019
* Data class for asset that has a parameterized constructor having some default values. Asset type
* is an enumeration that can have certain values. Check containable checks the validity of the
* parent-child relationship before cloining or adding an asset to a target asset. This is used
* both in clone asset and add asset.
* */

trait Versioned {
  // version number 0 means the asset is still outside of the AST
  var versionNumber:Long = 0
}

sealed class AssetType
case object VPRootType extends AssetType
case object RepositoryType extends AssetType
case object FolderType extends AssetType
case object FileType extends AssetType
case object ClassType extends AssetType
case object MethodType  extends AssetType
case object FieldType extends AssetType
case object BlockType extends AssetType

case class Asset(var name: String,
                 var assetType: AssetType,
                 var children : List[Asset]=Nil, //immutable list -> Var
                 var presenceCondition: Expression = True(),
                 var featureModel : Option[FeatureModel] = None,
                 var content : Option[List[String]]=None)
  extends Cloneable with Versioned{
  var parent:Option[Asset]=None

  def rename(newname:String)={
    name = newname
  }
  def getRoot( ): Option[Asset] = this match {

    case Asset(_, VPRootType, _, _, _, _) => Some(this)
    case _ => this.parent match {
      case None => None
      case _ => this.parent.get.getRoot()
    }
  }
  def getRepository( ): Option[Asset] = this match {
    case Asset( _, RepositoryType, _, _, _, _) => Some(this)
    case _ => this.parent match {
      case None => None
      case _ => this.parent.get.getRepository()
    }
  }
  import org.kiama.rewriting.Rewriter._

  def isMappedToFeature( f: Feature ) =
    !( collects{
      case x@Feature(_,_,_,_) if x==f => f
    }(presenceCondition) isEmpty )

  def mappedToFeatures =
    collects {
      case f:Feature => f
    }(presenceCondition)

  /**
    *
    * @param fm
    * @return
    */
  def missingFeatures( fm: FeatureModel): List[Feature] = {
    collectl{
      case x@Feature(_,_,_,_) if( !Utilities.featureModelContainsFeature( x, fm ) ) => x
    }(presenceCondition)
  }
  def doClone() = clone().asInstanceOf[Asset]

  def prettyprint{
    println(" *** Asset Tree ***")
      println(name)
      children foreach (_.prettyprint(1))
  }
  def printContent: Unit ={
   content match{
     case None=>;
     case _=>{
       println("Content:")
       content.get.foreach(c=>println("      "+c))
     }
   }
  }
  def getType: String ={
    assetType match{
      case VPRootType => "Root"
      case RepositoryType => "Repository"
      case FolderType => "Folder"
      case FileType => "File"
      case ClassType => "Class"
      case MethodType => "Method"
      case BlockType => "Code Block"
    }
  }

  def prettyprint(nestingLevel: Int){
    if(this.assetType!=BlockType) println( ("  ".toString * nestingLevel) +  name )
    children foreach( _.prettyprint(nestingLevel+1) )
  }
  override def toString = name
}

case class Feature(var name: String,
                   var complete: Boolean = false,
                   var optional: Boolean = false,
                   var dependsOn: Option[Expression] = None) extends Expression with Cloneable with Versioned {
  var subfeatures:List[Feature]=Nil
  var parent:Option[Feature] = None

  def prettyprint{
    println(" *** Feature Model ***")
    println( name )
    subfeatures foreach( _.prettyprint(1) )
  }

  def prettyprint(nestingLevel: Int){
    println( ("  ".toString * nestingLevel) +  name )
    subfeatures foreach( _.prettyprint(nestingLevel+1) )
  }

  def rename(newname:String)={
    name = newname
  }
  // TODO: shouldn't this also be a deep clone?
  def doCloneFeature() = clone().asInstanceOf[Feature]

  override def toString = name

  // TODO: mapped asset gives the childern of the mapped assets as well
  def mappedAssets(ast : Asset):List[Asset] = {
    var assetlist:List[Asset] = transformASTToList(ast)
    var mappedassets:List[Asset] = Nil
    for(asset <- assetlist){
      if(asset.isMappedToFeature(this) && !mappedassets.contains(asset))
        mappedassets = asset :: mappedassets
    }
    return mappedassets
  }
  def getFeatureModelRoot( ): Option[RootFeature] = this match {
    case rf:RootFeature  => Some(this.asInstanceOf[RootFeature])
    case _ => this.parent match {
      case None => None
      case _ => this.parent.get.getFeatureModelRoot()
    }
  }
}

class RootFeature (name: String,
                   complete: Boolean = false,
                   optional: Boolean = false,
                   dependsOn: Option[Expression] = None)
  extends Feature(name,complete,optional,dependsOn){

  var featureModel : Option[FeatureModel] = None
  var UNASSIGNED = Feature("UNASSIGNED")

  this.subfeatures = UNASSIGNED :: Nil
  UNASSIGNED.parent = Some(this)
}

case class FeatureModel(var rootfeature:RootFeature, name:String = "FeatureModel") extends Cloneable {

  var containingAsset : Option[Asset] = None
  rootfeature.featureModel = Some(this)

  def allFeatures = collects{
    case x:Feature => x
  }( rootfeature )

  def prettyprint(){
    rootfeature.prettyprint
  }

  def doCloneFeatureModel() = clone().asInstanceOf[FeatureModel]

}

abstract class PathElement()
case class AssetPathElement(assetname : String, assettype : AssetType) extends PathElement
case class FeaturePathElement (featurename : String) extends PathElement
case class Path(root:Asset, path:Seq[PathElement])

//create function ComputePathElement in Utilities

object ASTWellFormednessConstraints{

  val validContainment: Map[AssetType, List[AssetType]] = Map(
        VPRootType -> List(RepositoryType),
        RepositoryType -> List(FolderType, FileType),
        FolderType -> List(FolderType, FileType),
        FileType -> List(ClassType, FieldType, MethodType,BlockType),
        ClassType -> List(FieldType, MethodType,BlockType),
        FieldType -> Nil,
        MethodType -> List(BlockType)
      )

  def containable(source: Asset, target: Asset ) =
    validContainment( target.assetType ) contains source.assetType
}

case class Trace (source: Asset, target: Asset, relationship: Relationship, versionAt:Long) extends Cloneable {
  def toMetadata : Option[AssetTraceMetadata] = {
    // TODO: Think about when to generate metadata and when not to -> Probably it should be able to be generated always inside the MAT, outside its probably not required.. If a trace cannot be generated otherwise -> problem -> throw exception
    if (source.getRoot.isDefined && target.getRoot.isDefined) {
      val metadata = AssetTraceMetadata(source = MetadataConverter.convertAssetToMetadata(source), target = MetadataConverter.convertAssetToMetadata(target), relationship = relationship, versionAt = versionAt)
      Some(metadata)
    } else {
      None
    }
  }

  def doClone : Trace = {
    clone.asInstanceOf[Trace]
  }
  def prettyprint() = println(this.source + " ; " + this.target + " ; " + this.versionAt)
}

case class FeatureTrace(source: Feature, target:Feature, relationship: Relationship, versionAt:Long) extends Cloneable {
  def toMetadata : Option[FeatureTraceMetadata] = {
    if (isFeatureBeneathRootAsset(source) && isFeatureBeneathRootAsset(target)) {
      val metadata = FeatureTraceMetadata(source = MetadataConverter.convertFeatureToMetadata(source), target = MetadataConverter.convertFeatureToMetadata(target), relationship = relationship)
      Some(metadata)
    } else {
      None
    }
  }

  def doClone : FeatureTrace = {
    clone.asInstanceOf[FeatureTrace]
  }
}

sealed class Relationship {}

case object CloneOf extends Relationship

object TraceDatabase {
  var traces: List[Trace] = Nil
  def prettyprint(){
    TraceDatabase.traces.foreach(c=>c.prettyprint())
  }
}
object FeatureTraceDatabase {
  var traces: List[FeatureTrace] = Nil
}

/////////////////////////////////////////////////////////////////
// some companion objects for easily creating an AST
object VPRoot {
  def apply (name: String, children: List[Asset]) = new Asset(name, VPRootType, children)

  def apply (name: String, child: Asset) = new Asset(name, VPRootType, child :: Nil)
}
object Repository {
  def apply (name: String, children: List[Asset]) = new Asset(name, RepositoryType, children)

  def apply (name: String, child: Asset) = new Asset(name, RepositoryType, child :: Nil)

  def apply (name: String) = new Asset(name, RepositoryType, List.empty)
}
object Folder {
  def apply (name: String) = new Asset(name, FolderType)

  def apply (name: String, children: List[Asset]) = new Asset(name, FolderType, children)

  def apply (name: String, child: Asset) = new Asset(name, FolderType, child :: Nil)
}
object File {
  def apply (name: String) = new Asset(name, FileType)

  def apply (name: String, children: List[Asset]) = new Asset(name, FileType, children)

  def apply (name: String, child: Asset) = new Asset(name, FileType, child :: Nil)
}
case class Dependency (source: Asset, target: Asset)

object Feature{

  def apply(name: String) = new Feature(name)

  def apply(name:String, complete:Boolean) = new Feature(name, complete)

  def apply(name:String, complete:Boolean, optional:Boolean) = new Feature(name, complete, optional)

}
object FeatureModel{
  def apply(rootfeature: RootFeature): FeatureModel = new FeatureModel(rootfeature)
}
