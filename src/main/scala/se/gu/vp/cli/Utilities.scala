package se.gu.vp.cli

import se.gu.vp.model._
import se.gu.vp.operations.{AddFeatureToFeatureModel, CloneAsset, CloneFeature, DiffAssets, MapAssetToFeature, PropagateToAsset2, PropagateToFeature, SerialiseAssetTree}
import se.gu.vp.operations.Utilities._
import se.gu.vp.operations.cli.LoadAT.loadingFirstTime
import java.io.File

import scala.io.Source

/**
Copyright [2022] [Khaled Al Mustafa, Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

object Utilities {

  def getPathToRoot(): String = {
    val file = Source.fromFile("src/main/scala/se/gu/vp/cli/RootPathFile.txt")
    var rootPath = file.getLines().mkString
    if(rootPath.contains("\\\\")) rootPath = rootPath.replace("\\\\","\\")
    rootPath
  }
  def getShortName(asset:Asset):String =
    asset.name.split('\\').last.filter(!_.isDigit).replace("-","")

  def getShortRootPath(): String = {
    val rootPath = getPathToRoot()
    val shortRootPath = rootPath.substring(0, rootPath.lastIndexOf("\\"))
    shortRootPath
  }

  def prepareToComputePath(input: String, astroot: Asset): Seq[PathElement] = {
    var assetName = getPathToRoot() + "\\" + input
    var featureName = ""
    if (input.contains("::")) {
      assetName = (getPathToRoot() + "\\").concat(input.split("::").head)
      featureName = input.substring(input.indexOf("::") + 2)
    }
    var pathElement: Seq[PathElement] = Nil
    val asset = getassetbyname(assetName, astroot)
    if (asset != None) {
      val fm = asset.get.featureModel
      if (fm != None) {
        if (!featureName.isEmpty) {
          val feature = findFeatureByName(featureName, fm.get)
          pathElement = computeAssetPath(asset.get) ++ computeFeaturePath(feature.get)
        } else {
          pathElement = computeAssetPath(asset.get) ++ computeFeaturePath(fm.get.rootfeature)
        }
      } else {
        println("There is no feature model in the given path !")
      }
    }
    pathElement
  }

  def processCliInput(input: Array[String], command: Int): Unit = {
    val rootPath = getPathToRoot()
    val astroot = loadingFirstTime(rootPath)

    if (input.length == 2) {
      if (input(0).contains("::")) {
        //Feature cloning or feature change propagation
        val sourcePath = se.gu.vp.model.Path(astroot, prepareToComputePath(input(0), astroot))
        val targetPath = se.gu.vp.model.Path(astroot, prepareToComputePath(input(1), astroot))
        if (command == 1) {
          CloneFeature(sourcePath, targetPath)
          SerialiseAssetTree(astroot, getShortRootPath())
          println(" ****** Cloned feature " + input(0).split("::").last
            +" from " + input(0).split("::").head +" to " + input(1) + " successfully! ****** ")
        } else {
          PropagateToFeature(sourcePath, targetPath)
          SerialiseAssetTree(astroot, getShortRootPath())
          println(" ****** Propagated feature " + input(0).split("::").last
            +" from " + input(0).split("::").head +" to " + input(1).split("::").head + " successfully! ****** ")
        }
      } else if (!new File(rootPath + "\\" + input(1)).exists()) {

        val srcAsset = getassetbyname(rootPath + "\\" + input(0), astroot).get

        val cloneName = input(1).trim
        if (command == 1) {
          CloneAsset(srcAsset, astroot, None, Some(cloneName))
          SerialiseAssetTree(astroot, getShortRootPath())
          println("Cloned asset to Root!")
        }
      }
      else {

        if (command == 1) {
          val srcAsset = getassetbyname(rootPath + "\\" + input(0), astroot).get
          val targetAsset = getassetbyname((rootPath + "\\" + input(1)), astroot).get
          CloneAsset(srcAsset, targetAsset)
          SerialiseAssetTree(astroot, getShortRootPath())
          println("Cloned asset!")
        } else {
          val srcAsset = getassetbyname(rootPath + "\\" + input(0), astroot).get
          val targetAsset = getassetbyname((rootPath + "\\" + input(1)), astroot).get
          PropagateToAsset2(srcAsset, targetAsset)
          SerialiseAssetTree(astroot, getShortRootPath())
          println("Propagated Asset!")
        }
      }
    } else {
      println("Can't execute ! wrong input!")
    }
  }

  def handleMapCommand(input: Array[String]): Unit = {
    val rootPath = getPathToRoot()
    val astroot = loadingFirstTime(rootPath)
    val asset = getassetbyname(rootPath + "\\" + input(0), astroot)
    if (asset != None && input(1).contains("::")) {
      val fm = getImmediateAncestorFeatureModel(asset)
      if (fm != None) {
        val feature = findFeatureByName(input(1).substring(input(1).indexOf("::") + 2), fm.get)
        if (feature != None) {
          MapAssetToFeature(asset.get, feature.get)
          println("Feature" + input(1) + " mapped to asset" + input(0) + " successfully!")
        } else println("Feature " + input(1).substring(input(1).indexOf("::") + 2) + " not found in the provided repository!")
      }
    } else if (asset == None) println("Asset with name " + input(0).split("::").head + " not found in " + rootPath)
    else println("The input is not a feature. Use :: to specify features")
  }

  def handleGetMappedCommand(input: String): Unit = {
    val rootPath = getPathToRoot()
    val astroot = loadingFirstTime(rootPath)


    var counter:Int = 1
    val asset = getassetbyname(rootPath + "\\" + input.split("::").head, astroot)
    if (asset != None && input.contains("::")) {
      val fm = asset.get.featureModel
      if (fm != None) {
        val feature = findFeatureByName(input.substring(input.indexOf("::") + 2), fm.get)
        if (feature != None) {
          println("\n               ****** Getting mapped assets of feature " + feature.get.name + " in variant " + getShortName(asset.get) + "******\n")
          val mappedAssets = feature.get.mappedAssets(asset.get)
          mappedAssets.foreach(mappedAsset => {
            if (mappedAsset.assetType == BlockType) {
              if (!mappedAsset.parent.get.mappedToFeatures.contains(feature.get)) {
                //println("\n -------------------------------- \n")
                mappedAsset.parent.get.assetType match {
                  case MethodType => println("\n"+ counter +") Block of code in method " + getShortName(mappedAsset.parent.get) + "(), File: " + findImmediateParentFile(mappedAsset).get.name.split('\\').last)
                  case _=>println("\n"+ counter +") Block of code in File: " + findImmediateParentFile(mappedAsset).get.name.split('\\').last)
              }
              if (mappedAsset.content != None) println(mappedAsset.content.get.mkString("\n"))
                counter += 1
              }
            }
            else if(mappedAsset.assetType == MethodType) {
              //println("\n -------------------------------- \n")
              println("\n" + counter +") Method " + getShortName(mappedAsset) + "() in File: " + findImmediateParentFile(mappedAsset).get.name.split('\\').last + " in variant " + getShortName(asset.get.getRepository().get))
//              if(mappedAsset.content!=None) println(mappedAsset.content.get(0))
//              mappedAsset.children.foreach(c=>if(c.content!=None) println(c.content.get.mkString("\n")))
              counter +=1
            }
            else{
              //println("\n -------------------------------- \n")
              println(counter +") Asset name: " + mappedAsset + " Type: " + mappedAsset.getType)
              counter += 1
            }
          }
          )
        }
        else println("The feature " + input.substring(input.indexOf("::") + 2) + " does not exist in " + asset.get.name)

      }
    } else if (asset == None) println("Asset with name " + input.split("::").head + " not found in " + rootPath)
    else println("The input is not a feature. Use :: to specify features")
  }

  def handleAddFeatureToFMCommand(input: Array[String]) {
    val rootPath = getPathToRoot()
    val astroot = loadingFirstTime(rootPath)

    val assetName = rootPath + "\\" + input(1).split("::").head
    val asset = getassetbyname(assetName, astroot)
    if (asset != None) {
      val fm = asset.get.featureModel
      if (fm != None) {

        if (input(1).contains("::")) {
          val featureName = input(1).substring(input(1).indexOf("::") + 2)
          val parentFeature = findFeatureByName(featureName, fm.get)
          if (parentFeature != None) {
            val feature = Feature(input(0).trim)
            AddFeatureToFeatureModel(feature, parentFeature.get)
            println("Feature " + feature + " added to " + parentFeature.get.name + " successfully!")
            SerialiseAssetTree(astroot, getShortRootPath())
          }
        }
        else {
          val feature = Feature(input(0).trim)
          AddFeatureToFeatureModel(feature, fm.get.rootfeature)
          println("Feature " + feature + " added to root feature " + fm.get.rootfeature.name + " successfully!")
          SerialiseAssetTree(astroot, getShortRootPath())
        }

      } else println("No feature model found in " + assetName + "!")
    } else println("Asset with name " + assetName + " not found in " + rootPath)
  }

  def handleGetClonesCommand(input: String): Unit = {
    val rootPath = getPathToRoot()
    val astroot = loadingFirstTime(rootPath)
    val assetPath = rootPath + "\\" + input.split("::").head
    val asset = getassetbyname(assetPath, astroot)

    asset match {
      case None => println("Asset " + assetPath + " not found!")
      case _ => {
        if (!input.contains("::")) {
          //Input is an asset
          val cloneList = findClones(asset.get)
          if (!cloneList.isEmpty) println("\nClones of " + asset.get.name + " are: ")
          cloneList.foreach(clone => {
            if (isCodeAsset(clone)) {
              println("Clone name: " + clone.name + " Type: " + clone.getType + " in File: " + findImmediateParentFile(clone).get.name)
            }
            else {
              println("Clone name: " + clone.name + " Type: " + clone.getType)
            }
          })
        }
        else {
          asset.get.featureModel match {
            case None => println("The provided asset does not contain a feature model")
            case _ => {
              val fm = asset.get.featureModel
              val featureName = input.substring(input.indexOf("::") + 2)
              val feature = findFeatureByName(featureName, fm.get)
              if (feature != None) {
                val featureCloneList = findFeatureClones(feature.get)
                if (!featureCloneList.isEmpty) println("\nClones of " + feature.get.name + " are: ")

                featureCloneList.foreach(clone => {
                  println("Clone name: " + clone.name + " version: " + clone.versionNumber + " cloned in " + clone.getFeatureModelRoot().get.featureModel.get.containingAsset.get.name)
                })
              }
              else println("Feature with feature name " + featureName + "not found in " + assetPath)
            }
          }
        }
      }
    }
  }

  def handleGetLatestTrace(input: Array[String]) {

    val rootPath = getPathToRoot()
    val astroot = loadingFirstTime(rootPath)

    if (!input(0).contains("::") && !input(1).contains("::")) {
      val sourceAssetName = rootPath + "\\" + input(0).trim
      val targetAssetName = rootPath + "\\" + input(1).trim
      val sourceAsset = getassetbyname(sourceAssetName, astroot)
      val targetAsset = getassetbyname(targetAssetName, astroot)

      if (sourceAsset != None && targetAsset != None) {
        val trace = findMostRecentCloneTrace(sourceAsset.get, targetAsset.get.parent.get)
        if (trace != None) {
          println("The last synchronization between " + sourceAssetName + " and " + targetAssetName + " happened on "
            + getDateFromVersion(trace.get.versionAt) + " at " + getTimeFromVersion(trace.get.versionAt))
        }
        else println("The assets" + sourceAsset + " and " + targetAssetName + " are not clones of each other")
      }
      else if (sourceAsset == None) println("Couln't find " + sourceAssetName)
      else println("Couln't find " + targetAssetName)
    }

    else if (input(0).contains("::") && input(1).contains("::")) {

      val sourceAssetName = rootPath + "\\" + input(0).split("::").head
      val targetAssetName = rootPath + "\\" + input(1).split("::").head
      val sourceAsset = getassetbyname(sourceAssetName, astroot)
      val targetAsset = getassetbyname(targetAssetName, astroot)
      
      if (sourceAsset != None && targetAsset != None) {
        if (sourceAsset.get.featureModel == None) println(sourceAssetName + " does not contain a feature model.")
        else if (targetAsset.get.featureModel == None) println(targetAssetName + " does not contain a feature model.")
        else {
          val sourceFeatureName = input(0).substring(input(0).indexOf("::") + 2)
          val targetFeatureName = input(1).substring(input(1).indexOf("::") + 2)
          val sourceFeature = findFeatureByName(sourceFeatureName, sourceAsset.get.featureModel.get)
          val targetFeature = findFeatureByName(targetFeatureName, targetAsset.get.featureModel.get)

          if (sourceFeature == None) println("Feature " + sourceFeatureName + " not found in " + sourceAssetName)
          else if (targetFeature == None) println("Feature " + targetFeatureName + " not found in " + targetAssetName)
          else {
            val trace = findMostRecentFeatureCloneTrace(sourceFeature.get, targetFeature.get)
            if(trace == None) println("The features " + sourceFeatureName + " and " + targetFeatureName + " are not clones of each other")
            else{
              println("The last synchronization between " + sourceFeatureName + " and " + targetFeatureName + " happened on "
                + getDateFromVersion(trace.get.versionAt) + " at " + getTimeFromVersion(trace.get.versionAt))
            }
          }
        }
      }
    }
  }

  def handleDiffCommand(input: Array[String]): Unit = {
    val rootPath = getPathToRoot()
    val astroot = loadingFirstTime(rootPath)

    println("                ****** Getting diff between " + input(0) + " and " + input(1) +" ****** ")

    val asset1 = getassetbyname(rootPath + "\\" + input(0), astroot)
    val asset2 = getassetbyname(rootPath + "\\" + input(1), astroot)
    if (asset1 != None && asset2!= None) DiffAssets(asset1.get,asset2.get)
    else if (asset1 == None) println("Variant " + input(0) + " not found in " + rootPath)
    else if (asset2 == None) println("Variant " + input(1) + " not found in " + rootPath)
  }
}