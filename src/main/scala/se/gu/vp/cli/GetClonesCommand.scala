package se.gu.vp.cli

import java.util.concurrent.Callable

import picocli.CommandLine
import picocli.CommandLine.{Command, Option, Parameters}
import se.gu.vp.cli.Utilities.handleGetClonesCommand

/**
Copyright [2022] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

@Command(name = "getclones", mixinStandardHelpOptions = true, version = Array("vp 1.0"),
  description = Array("Gets all clones of a given asset or feature"))
class GetClonesCommand extends Callable[Int] {

  @CommandLine.Spec
  val spec: CommandLine.Model.CommandSpec = null

  @Option(names = Array( "-option"), arity = "0..*", description = Array("optional parameters"))
  private val option = Array[String]()

  @Parameters(index = "", paramLabel = "input" ,arity = "1", description = Array("The Paths to asset or feature"))
  private var input : String = new String

  def call(): Int = {
    handleGetClonesCommand(input)
    0
  }
}