package se.gu.vp.cli
import picocli.CommandLine
import picocli.CommandLine.{Command, Option, Parameters}
import se.gu.vp.operations.cli.VPInit

import java.io.{File, PrintWriter}
import java.util.concurrent.Callable

/**
Copyright [2022] [Khaled Al Mustafa]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

@Command(name = "init", mixinStandardHelpOptions = true, version = Array("vp 1.0"),
  description = Array("Initiates the structures of the VP."))
class InitCommand extends Callable[Int] {
  @CommandLine.Spec
  val spec : CommandLine.Model.CommandSpec=null

  @Option(names = Array("-option"), arity = "0..*", description = Array("optional parameters"))
  private val option = Array[String]()

  @Parameters(index = "", arity = "1", description = Array("The Root Path"))
  var rootPath : String = ""

  def call(): Int = {
    if (new File(rootPath).exists()){
      val pw = new PrintWriter(new File("src/main/scala/se/gu/vp/cli/RootPathFile.txt"))
      pw.write(rootPath)
      pw.close()
      VPInit.start(new File(rootPath))
    }else{
      println("Please enter a valid path!")
    }
    0
  }
}


