package se.gu.vp.cli

import java.util.concurrent.Callable

import picocli.CommandLine
import picocli.CommandLine.Command

/**
Copyright [2022] [Khaled Al Mustafa, Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

@Command(name="vp",  mixinStandardHelpOptions = true, version = Array("vp 1.0"),
  description = Array("Runs the functions of the VP."), subcommands = Array(classOf[InitCommand],
    classOf[CloneCommand],classOf[PropagateCommand],classOf[MapCommand],
    classOf[AddFeatureToFMCommand], classOf[GetMappedAssets],classOf[GetClonesCommand],
    classOf[GetLatestTraceCommand], classOf[DiffCommand]))
class CliMain extends Callable[Int]{
  @CommandLine.Spec
  val spec: CommandLine.Model.CommandSpec = null

  def call(): Int ={
    throw new CommandLine.ParameterException(spec.commandLine(), "Specify a command")
    0
  }
}

object CliMain {
  def main(args: Array[String]): Unit = {

    //VP Init Command ->
    System.exit(new CommandLine(new CliMain()).execute(args:_*))










  }
}