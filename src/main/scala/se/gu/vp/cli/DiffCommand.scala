package se.gu.vp.cli

import java.util.concurrent.Callable

import picocli.CommandLine
import picocli.CommandLine.{Command, Option, Parameters}
import se.gu.vp.cli.Utilities.handleDiffCommand

/**
Copyright [2022] [Wardah Mahmood]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

@Command(name = "diff", mixinStandardHelpOptions = true, version = Array("vp 1.0"),
  description = Array("Runs and displays the result of the Diff command of the VP"))
class DiffCommand extends Callable[Int] {

  @CommandLine.Spec
  val spec: CommandLine.Model.CommandSpec = null

  @Option(names = Array( "-option"), arity = "0..*", description = Array("optional parameters"))
  private val option = Array[String]()

  @Parameters(index = "", paramLabel = "input" ,arity = "2..3", description = Array("The relative paths to the variants"))
  private var input : Array[String] = new Array[String](2)

  def call(): Int = {
    handleDiffCommand(input)
    0
  }
}