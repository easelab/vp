/*
 * Copyright (c) 2019 Thorsten Berger <thorsten.berger@chalmers.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package se.gu.vp.commons

import org.slf4j.LoggerFactory

/**
Copyright [2018] [Thorsten Berger]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
  */

trait Logging{

  // @TODO logging works nicely, just needs some output formatting, not sure where this can be configured
  // I think this depends on the underlying logging framework that slf4j is using
  // so, figure out whether it is using an underlying framework (e.g., log4j) or whether it
  // falls back to some internal logging; then check how either the external one or the internal one
  // can be configured

  val log = LoggerFactory.getLogger( getClass )

  def error( message: =>String, values:Object* ) =
   if( log isErrorEnabled )
      log error( message, values )
	def error( message: =>String, error: Throwable ) =
    if( log isErrorEnabled )
      log error( message, error )
	def error( message: =>String, error: Throwable, values: Object ) =
		if( log isErrorEnabled )
      log error( message, error:Any, values:Any ) //Added :Any to both error and values

	def warn( message: =>String, values: Object* ) =
    if( log isWarnEnabled )
      log warn( message, values )
	def warn( message: =>String, error: Throwable ) =
    if( log isWarnEnabled )
      log warn( message, error )
	def warn( message: =>String, error: Throwable, values: Object* ) =
		if( log isWarnEnabled )
      log warn( message, error:Any, values:Any ) //Added :Any to both error and values

  def info( message: =>String, values:Object* ) =
    if( log isInfoEnabled )
      log info( message, values )
	def info( message: =>String, error: Throwable ) =
    if( log isInfoEnabled )
      log info( message, error )
	def info( message: =>String, error: Throwable, values: Object* ) =
		if( log isInfoEnabled )
      log info( message, error:Any, values:Any ) //Added :Any to both error and values

  def debug( message: =>String, values: Object* ) =
    if( log isDebugEnabled )
      log debug( message, values )
  def debug( message: =>String, error: Throwable ) =
    if( log isDebugEnabled )
      log debug( message, error )
  def debug( message: =>String, error: Throwable, values: Object* ) =
    if( log isDebugEnabled )
      log debug( message, error:Any, values:Any ) //Added :Any to both error and values

	def trace( message: =>String, values: Object* ) =
    if( log isTraceEnabled )
      log trace( message, values )
	def trace( message: =>String, error: Throwable ) =
    if( log isTraceEnabled )
      log trace( message, error )
	def trace( message: =>String, error: Throwable, values: Object* ) =
		if( log isTraceEnabled )
      log trace( message, error:Any, values:Any ) //Added :Any to both error and values

}