
packagese.gu.vp.operations

importorg.scalatest.FunSuite
importjava.nio.file.{Files,Paths}
importjava.nio.file.attribute.PosixFilePermissions

classSnakeGameTestCaseextendsFunSuite{

    test("SnakeGameTestCase"){
        valfeature=newRootFeature("CalculatorRootJava")
        varfeatureModel=newFeatureModel(feature)

//RelativepathtotheSnakefolder
        valrelativePathToSnake="project/vp_2360-build/Snake"
        valcommitLogFilePath=Paths.get(relativePathToSnake,"CommitLog.txt")

        println(s"LookingforCommitLog.txtat:${commitLogFilePath.toAbsolutePath}")

        if(Files.exists(commitLogFilePath)){
            println("CommitLog.txtexists,checkingpermissions.")

//Checkifthefileisreadable
            if(Files.isReadable(commitLogFilePath)){
                println("CommitLog.txtisreadable,proceedingwiththetest.")
//Proceedwiththetest
                valasset=se.gu.vp.operations.SnakeGame.Simulation.readInJavaAssetTree(commitLogFilePath.toString)
                assetmatch{
                    caseSome(assetTree)=>
                    assetTree.prettyprint
//Definethecorrectrelativepathtotheoutputdirectory
                            valoutputPath="path/to/output/directory"//Replacewithactualrelativepathtooutput
                    SerialiseAssetTree(assetTree,outputPath)
                    caseNone=>
                    fail(s"Assettreecouldnotbereadfrom'$relativePathToSnake'")
                }
            }else{
                println("CommitLog.txtisnotreadable,attemptingtochangepermissions.")
//Trytochangethefilepermissionstobereadablebytheownerandothers
//ThisisPOSIX-specificandwillnotworkonWindows
                try{
                    Files.setPosixFilePermissions(commitLogFilePath,PosixFilePermissions.fromString("rw-r--r--"))
                    println("Permissionschanged:CommitLog.txtisnowreadable.")
//Proceedwiththetest
                }catch{
                    casee:Exception=>
                    fail(s"Failedtochangepermissions:${e.getMessage}")
                }
            }
        }else{
            fail(s"CommitLog.txtfilenotfoundat${commitLogFilePath.toAbsolutePath}")
        }
    }
}
